﻿internalTemp = 0;
externalTemp = 30;
errorInTempFlag = false;

function readTemp() {
    readTextFile("http://localhost/emosquescreen/temp.txt");
    //readTextFile("http://localhost/emosquescreen/emosquescreen/temp.txt");

    if (internalTemp == -99000) {
        errorInTempFlag = true;
    }
    else {
        errorInTempFlag = false;
        internalTemp = internalTemp / 1000;
        internalTemp = internalTemp.toString();
        if (internalTemp.length == 7) {
            internalTemp = internalTemp.substr(0, 5);
        }
        else {
            internalTemp = internalTemp.substr(0, 4);
        }
    }
}


function readTextFile(file) {
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4) {
            if (rawFile.status === 200 || rawFile.status == 0) {
                var allText = rawFile.responseText;
                internalTemp = allText;
            }
        }
    }
    rawFile.send(null);
}

