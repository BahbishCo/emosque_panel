﻿function clockController()
{
    var currentTime = new Date();
    var hour = currentTime.getHours();
    var min = currentTime.getMinutes();
    var sec = currentTime.getSeconds();
    var AmPm;
    if (hour >= 12)
    {
        
        AmPm = "PM";
    }
    else
    {
        AmPm = "AM";
    }

    if (hour > 12)
        hour = hour % 12;
 
    if (hour == 0)
	hour = 12;

	hour=fixTime2(hour);
    min = fixTime2(min);
    sec = fixTime2(sec);
    var now = "<span>" + hour + ":" + min + ":" + sec + "</span>";
    watchEl.innerHTML = now;
}

function fixTime2(time)
{
    if (time < 10)
    {
        time = "0" + time;
    }
    return time;
}

setInterval(clockController,1000);