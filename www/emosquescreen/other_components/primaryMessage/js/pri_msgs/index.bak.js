$(document).ready(function () {
    initDateSelector();
    initSelectors();
});

var cities = {};

var screenBase = $("#screenBase").val();

var insertScreenLoaded = false;
var editScreenLoaded = false;
var viewScreenLoaded = false;

var updatePreviewActive = false;

var activeFrame = '';
var activeEditor = '';

var activeMessageID = -1;

function initDateSelector() {
    $(function () {
        $("#insertMessage_startDatePicker").datepicker({
            minDate: new Date(),
            maxDate: '+2Y',
            dateFormat: "yy-mm-dd",
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });
        $("#insertMessage_endDatePicker").datepicker({
            minDate: new Date(),
            maxDate: '+2Y',
            dateFormat: "yy-mm-dd",
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });

        $("#previewEditMessage_startDatePicker").datepicker({
            maxDate: '+2Y',
            dateFormat: "yy-mm-dd",
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });

        $("#previewEditMessage_endDatePicker").datepicker({
            maxDate: '+2Y',
            dateFormat: "yy-mm-dd",
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });

    });

    $('#insertMessage_startDatePicker').change(function () {
        var errorID = "insertMessageDateErrorLabel";
        var errorLabel = document.getElementById("insertMessageDateErrorLabel");
        var startDateSelector = document.getElementById("insertMessage_startDatePicker");
        var endDateSelector = document.getElementById("insertMessage_endDatePicker");
        var isDate = Date.parseExact(startDateSelector.value, "yyyy-MM-dd");
        if (isDate == null) {
            errorLabel.innerHTML = "ﺢﻴﺤﺻ ﻞﻜﺸﺑ ﺔﻳاﺪﺒﻟا ﺦﻳﺭﺎﺗ ﻝﺎﺧﺩا ﺐﺠﻳ";
            $('#' + errorID).removeClass('display-none');
            if ($('#' + errorID).hasClass('display-block')) {
                $('#' + errorID).removeClass('display-block');
                $('#' + errorID).addClass('display-none');
                setTimeout(function () {
                    $('#' + errorID).addClass('display-block');
                    startDateSelector.value = "";
                }, 500);
            }
            else {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
            }
            return;
        }

        $('#' + errorID).removeClass('display-block');
        $('#' + errorID).addClass('display-none');

        if (endDateSelector.value == "") {
            endDateSelector.value = startDateSelector.value;
        }

        validateDate("insertMessage_startDatePicker", "insertMessage_endDatePicker", "insertMessageDateErrorLabel");

    });


    $('#insertMessage_endDatePicker').change(function () {
        var errorID = "insertMessageDateErrorLabel";
        var errorLabel = document.getElementById("insertMessageDateErrorLabel");
        var endDateSelector = document.getElementById("insertMessage_endDatePicker");
        var startDateSelector = document.getElementById("insertMessage_startDatePicker");
        isDate = Date.parseExact(endDateSelector.value, "yyyy-MM-dd");
        if (isDate == null) {

            errorLabel.innerHTML = "ﺢﻴﺤﺻ ﻞﻜﺸﺑ ﺔﻳﺎﻬﻨﻟا ﺦﻳﺭﺎﺗ ﻝﺎﺧﺩا ﺐﺠﻳ";
            $('#' + errorID).removeClass('display-none');
            if ($('#' + errorID).hasClass('display-block')) {
                $('#' + errorID).removeClass('display-block');
                $('#' + errorID).addClass('display-none');
                setTimeout(function () {
                    $('#' + errorID).addClass('display-block');
                    endDateSelector.value = "";
                }, 500);
            }
            else {
                $('#' + errorID).addClass('display-block');
                endDateSelector.value = "";
            }

            return;
        }

        $('#' + errorID).removeClass('display-block');
        $('#' + errorID).addClass('display-none');

        if (startDateSelector.value != "")
            validateDate("insertMessage_startDatePicker", "insertMessage_endDatePicker", "insertMessageDateErrorLabel");
    });


    $('#previewEditMessage_startDatePicker').change(function () {
        killEditPreviewMessageTab();
        var errorID = "previewMessageDateErrorLabel";
        var errorLabel = document.getElementById("previewMessageDateErrorLabel");
        var startDateSelector = document.getElementById("previewEditMessage_startDatePicker");
        var endDateSelector = document.getElementById("previewEditMessage_endDatePicker");
        var isDate = Date.parseExact(startDateSelector.value, "yyyy-MM-dd");
        if (isDate == null) {
            errorLabel.innerHTML = "ﺢﻴﺤﺻ ﻞﻜﺸﺑ ﺔﻳاﺪﺒﻟا ﺦﻳﺭﺎﺗ ﻝﺎﺧﺩا ﺐﺠﻳ";
            $('#' + errorID).removeClass('display-none');
            if ($('#' + errorID).hasClass('display-block')) {
                $('#' + errorID).removeClass('display-block');
                $('#' + errorID).addClass('display-none');
                setTimeout(function () {
                    $('#' + errorID).addClass('display-block');
                    startDateSelector.value = "";
                }, 500);
            }
            else {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
            }
            return;
        }

        $('#' + errorID).removeClass('display-block');
        $('#' + errorID).addClass('display-none');

        if (endDateSelector.value == "") {
            endDateSelector.value = startDateSelector.value;
        }

        validateDate("previewEditMessage_startDatePicker", "previewEditMessage_endDatePicker", "previewMessageDateErrorLabel");

    });

    $('#previewEditMessage_endDatePicker').change(function () {
        killEditPreviewMessageTab();
        var errorID = "previewMessageDateErrorLabel";
        var errorLabel = document.getElementById("previewMessageDateErrorLabel");
        var startDateSelector = document.getElementById("previewEditMessage_startDatePicker");
        var endDateSelector = document.getElementById("previewEditMessage_endDatePicker");
        var isDate = Date.parseExact(endDateSelector.value, "yyyy-MM-dd");
        if (isDate == null) {
            errorLabel.innerHTML = "ﺢﻴﺤﺻ ﻞﻜﺸﺑ ﺔﻳﺎﻬﻨﻟا ﺦﻳﺭﺎﺗ ﻝﺎﺧﺩا ﺐﺠﻳ";
            $('#' + errorID).removeClass('display-none');
            if ($('#' + errorID).hasClass('display-block')) {
                $('#' + errorID).removeClass('display-block');
                $('#' + errorID).addClass('display-none');
                setTimeout(function () {
                    $('#' + errorID).addClass('display-block');
                    endDateSelector.value = "";
                }, 500);
            }
            else {
                $('#' + errorID).addClass('display-block');
                endDateSelector.value = "";
            }
            return;
        }

        $('#' + errorID).removeClass('display-block');
        $('#' + errorID).addClass('display-none');

        if (endDateSelector.value == "") {
            endDateSelector.value = startDateSelector.value;
        }

        validateDate("previewEditMessage_startDatePicker", "previewEditMessage_endDatePicker", "previewMessageDateErrorLabel");

    });
}


function validateDate(startDateID, endDateID, errorID) {
    var startDateSelector = document.getElementById(startDateID);
    var endDateSelector = document.getElementById(endDateID);
    var errorLabel = document.getElementById(errorID);


    var isDate = Date.parseExact(startDateSelector.value, "yyyy-MM-dd");
    if (isDate == null) {
        errorLabel.innerHTML = "ﺢﻴﺤﺻ ﻞﻜﺸﺑ ﺔﻳاﺪﺒﻟا ﺦﻳﺭﺎﺗ ﻝﺎﺧﺩا ﺐﺠﻳ";
        $('#' + errorID).removeClass('display-none');
        if ($('#' + errorID).hasClass('display-block')) {
            $('#' + errorID).removeClass('display-block');
            $('#' + errorID).addClass('display-none');
            setTimeout(function () {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
            }, 500);
        }
        else {
            $('#' + errorID).addClass('display-block');
        }
        return false;
    }

    isDate = Date.parseExact(endDateSelector.value, "yyyy-MM-dd");
    if (isDate == null) {
        errorLabel.innerHTML = "ﺢﻴﺤﺻ ﻞﻜﺸﺑ ﺔﻳﺎﻬﻨﻟا ﺦﻳﺭﺎﺗ ﻝﺎﺧﺩا ﺐﺠﻳ";
        $('#' + errorID).removeClass('display-none');
        if ($('#' + errorID).hasClass('display-block')) {
            $('#' + errorID).removeClass('display-block');
            $('#' + errorID).addClass('display-none');
            setTimeout(function () {
                $('#' + errorID).addClass('display-block');
                endDateSelector.value = "";
            }, 500);
        }
        else {
            $('#' + errorID).addClass('display-block');
        }
        return false;
    }

    var start = Date.parseExact(startDateSelector.value, "yyyy-MM-dd");
    var end = Date.parseExact(endDateSelector.value, "yyyy-MM-dd");
    console.log("start = " + start);
    console.log("end = " + end);


    if (end.getTime() < start.getTime()) {

        errorLabel.innerHTML = "ﺕﺎﻘﻓاﻮﺘﻣ ﺮﻴﻏ ﺔﻳﺎﻬﻨﻟاﻭ ﺔﻳاﺪﺒﻟا ﺦﻳﺭﺎﺗ";
        $('#' + errorID).removeClass('display-none');
        if ($('#' + errorID).hasClass('display-block')) {
            $('#' + errorID).removeClass('display-block');
            $('#' + errorID).addClass('display-none');
            setTimeout(function () {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
                endDateSelector.value = "";
            }, 500);
        }
        else {
            $('#' + errorID).addClass('display-block');
            startDateSelector.value = "";
            endDateSelector.value = "";
        }
        return false;
    }


    if (startDateSelector.value == "" || endDateSelector.value == "") {

        errorLabel.innerHTML = "ﺔﻳﺎﻬﻨﻟاﻭ ﺔﻳاﺪﺒﻟا ﺦﻳﺭﺎﺗ ﺪﻳﺪﺤﺗ ﺐﺠﻳ";
        $('#' + errorID).removeClass('display-none');
        if ($('#' + errorID).hasClass('display-block')) {
            $('#' + errorID).removeClass('display-block');
            $('#' + errorID).addClass('display-none');
            setTimeout(function () {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
                endDateSelector.value = "";
            }, 500);
        }
        else {
            $('#' + errorID).addClass('display-block');
            startDateSelector.value = "";
            endDateSelector.value = "";
        }
        return false;
    }
    return true;
}


function showMessageTableDiv() {
    $("#countrySelector_preview").parent().removeClass("has-error");
    $("#citySelector_preview").parent().removeClass("has-error");

    var dateValidation = validateDate("previewEditMessage_startDatePicker", "previewEditMessage_endDatePicker", "previewMessageDateErrorLabel");

    if ($("#citySelector_preview").val() != "" && $("#countrySelector_preview").val() != "" && dateValidation == true) {
        var cityID = cities[$("#citySelector_preview").val()];
        var dateFrom = $("#previewEditMessage_startDatePicker").val();
        var dateTo = $("#previewEditMessage_endDatePicker").val();

        $.ajax({
            url: "pri_msgs/listMessages",

            data: {
                cityID: cityID,
                dateFrom: dateFrom,
                dateTo: dateTo
            },

            method: "POST",

            complete: function (response, status, xhr) {
                var data = JSON.parse(response.responseText);
                var message = data.Message;
                var allMessages = data.AllMessages;

                if (status == 'success') {
                    $("#messagesTable").DataTable({
                        data: allMessages,
                        columns: [
                            {"data": "ID"},
                            {"data": "insertedBy"},
                            {"data": "insertionDate"},
                            {"data": "dateTo"},
                            {"data": "dateFrom"},
                            {"data": "cityName"}
                        ],
                        columnDefs: [
                            {
                                className: "text-center",
                                targets: [0],
                                data: null,
                                render: function (data) {
                                    return '<span onclick="setTimeout(showEditMessageDialog(' + data + '),500);">' +
                                        '<a href="#" data-toggle="tooltip" title="ﻞﻳﺪﻌﺗ">' +
                                        '<span class="glyphicon glyphicon-edit"></span>' +
                                        '</a>' +
                                        '</span>' +
                                        '<span onclick="setTimeout(showViewMessageDialog(' + data + '),500);">' +
                                        '<a href="#" data-toggle="tooltip" title="ﺔﻟﺎﺳﺮﻟا ﺽﺮﻋ">' +
                                        '<span class="glyphicon glyphicon-eye-open"></span>' +
                                        '</a>' +
                                        '</span>' +
                                        '<span onclick="setTimeout(removeMessageDialog(' + data + '),500);">' +
                                        '<a href="#" data-toggle="tooltip" title="ﻑﺬﺣ">' +
                                        '<span class="glyphicon glyphicon-remove"></span>' +
                                        '</a>' +
                                        '</span>';
                                }
                            },
                            {
                                className: "text-center",
                                targets: [1, 2, 3, 4, 5],
                                data: null
                            }
                        ],
                        "destroy": true
                    });
                }
                else {
                    // $.each(validation, function(idx, field) {
                    //     if(field.fieldName == 'cityName')
                    //     {
                    //         $("#citySelector_preview").parent().addClass("has-error");
                    //         $("#citySelector_preview").focus();
                    //     }

                    //     if(field.fieldName == 'dateFrom')
                    //     {
                    //         $("#insertMessage_startDatePicker").parent().addClass("has-error");
                    //         $("#insertMessage_startDatePicker").focus();
                    //     }

                    //     if(field.fieldName == 'dateTo')
                    //     {
                    //         $("#insertMessage_endDatePicker").parent().addClass("has-error");
                    //         $("#insertMessage_endDatePicker").focus();
                    //     }
                    // });
                }
            }

        });

        var messagesTable = $('#messagesTableDiv');
        messagesTable.addClass(' display-block');
        messagesTable.removeClass(' display-none');
    }
    else {
        if ($("#citySelector_preview").val() == "") {
            $("#citySelector_preview").parent().addClass("has-error");
            $("#citySelector_preview").focus();
        }

        if ($("#countrySelector_preview").val() == "") {
            $("#countrySelector_preview").parent().addClass("has-error");
            $("#countrySelector_preview").focus();
        }

        return;
    }
}


function killEditPreviewMessageTab() {
    var messagesTable = $('#messagesTableDiv');
    messagesTable.removeClass(' display-block');
    messagesTable.addClass(' display-none');
}


function getParamValueByName(name, windowId) {
    if (windowId == null) {

        var query = window.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }
    else {
        var iframe = document.getElementById(windowId);
        var query = iframe.contentWindow.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }

}

function loadScript(url, callback) {

    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    script.onreadystatechange = callback;
    script.onload = callback;

    head.appendChild(script);
}


function getRequest(url, success, error) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}

function saveNewMessage() {
    var cityID = cities[$("#citySelector").val()];

    var dateFrom = $("#insertMessage_startDatePicker").val();
    var dateTo = $("#insertMessage_endDatePicker").val();

    var messageBody = nicEditors.findEditor("insertMessageEditor").getContent();

    $.ajax({
        url: "pri_msgs/saveNewMessage",

        data: {
            cityID: cityID,
            dateFrom: dateFrom,
            dateTo: dateTo,
            messageBody: messageBody
        },

        method: "POST",

        complete: function (response, status, xhr) {
            var data = JSON.parse(response.responseText);
            var message = data.Message;
            var validation = data.Validation;

            if (status == 'success') {
                alert(message);
            }
            else {
                $.each(validation, function (idx, field) {
                    if (field.fieldName == 'cityID') {
                        $("#citySelector").parent().addClass("has-error");
                        $("#citySelector").focus();
                    }

                    if (field.fieldName == 'dateFrom') {
                        $("#insertMessage_startDatePicker").parent().addClass("has-error");
                        $("#insertMessage_startDatePicker").focus();
                    }

                    if (field.fieldName == 'dateTo') {
                        $("#insertMessage_endDatePicker").parent().addClass("has-error");
                        $("#insertMessage_endDatePicker").focus();
                    }

                    // if(field.fieldName == 'messageBody')
                    // {
                    //     $("#").parent().addClass("has-error");
                    //     $("#").focus();
                    // }
                });
            }
        }

    });
}

function updateMessage(messageID) {
    if (messageID == -1) {
        return false;
    }

    var messageBody = nicEditors.findEditor("editMessageEditor").getContent();

    $.ajax({
        url: "pri_msgs/updateMessage/" + messageID,

        data: {
            messageBody: messageBody
        },

        method: "POST",

        complete: function (response, status, xhr) {
            var data = JSON.parse(response.responseText);
            var message = data.Message;

            alert(message);
        }

    });
}

function showInsertMessageDialog() {
    $("#citySelector").parent().removeClass("has-error");
    $("#countrySelector").parent().removeClass("has-error");

    var dateValidation = validateDate("insertMessage_startDatePicker", "insertMessage_endDatePicker", "insertMessageDateErrorLabel");

    if ($("#countrySelector").val() != "" && $("#citySelector").val() != "" && dateValidation == true) {
        var insertMessageModal = $('#insertMessageModal');
        insertMessageModal.removeClass("display-none");
        insertMessageModal.addClass("display-block");

        var editorDiv = document.getElementById("insertMessageEditorDiv");
        var editor = document.getElementById("insertMessageEditor");
        var previewFrame = document.getElementById("insertMessagePreviewFrame");
        previewFrame.style.visibility = "hidden";

        var editorWidth = editor.clientWidth;
        var editorHeight = editorWidth * 9 / 16;
        var editorDivWidth = editorDiv.clientWidth;
        var editorDivHeight = editorDivWidth * 9 / 16;


        editor.clientWidth = editorWidth;

        previewFrame.style.height = editorDivHeight + "px";
        editor.style.height = editorDivHeight + "px";


        if (!nicEditors.findEditor('insertMessageEditor')) {
            new nicEditor({
                fullPanel: false,
                unselectable: 'off',
                iconsPath: 'images/text-editor/nicEditorIcons.gif',
                buttonList: ['bold', 'italic', 'underline', 'left', 'center', 'right', 'justify', 'indent', 'outdent', 'hr', 'fontFormat'],
                maxHeight: editorHeight,
            }).panelInstance('insertMessageEditor');
        }

        if(insertScreenLoaded == false)
        {
            previewFrame.src = screenBase + "index.html?mode=1&date=-1&country=Jordan&city=Amman";
            insertScreenLoaded = true;
        }
        
        insertMessageModal.addClass("in");
        insertMessageModal.addClass("modal-open");

        updatePreviewActive = true;
        activeFrame = "insertMessagePreviewFrame";
        activeEditor = "insertMessageEditor";
        $(previewFrame).ready(updateMessagePreview);

    }
    else {
        if ($("#citySelector").val() == "") {
            $("#citySelector").parent().addClass("has-error");
            $("#citySelector").focus();
        }

        if ($("#countrySelector").val() == "") {
            $("#countrySelector").parent().addClass("has-error");
            $("#countrySelector").focus();
        }

        return;
    }
}

function hideInsertMessageDialog() {
    updatePreviewActive = false;
    var insertMessageModal = $('#insertMessageModal');
    insertMessageModal.removeClass("in");
    insertMessageModal.removeClass("modal-open");
    insertMessageModal.removeClass("display-block");
    insertMessageModal.addClass("display-none");
}


function showEditMessageDialog(messageID) {
    var editorDiv = document.getElementById("editMessageDiv");
    var editor = document.getElementById("editMessageEditor");
    var previewFrame = document.getElementById("editMessagePreviewFrame");
    previewFrame.style.visibility = "hidden";
    var editMessageModal = $('#editMessageModal');
    editMessageModal.removeClass("display-none");
    editMessageModal.addClass("display-block");

    var editorWidth = editor.clientWidth;
    var editorHeight = editorWidth * 9 / 16;

    var editorDivWidth = editorDiv.clientWidth;
    var editorDivHeight = editorDivWidth * 9 / 16;

    editor.clientWidth = editorWidth;

    previewFrame.style.height = editorDivHeight + "px";
    editor.style.height = editorDivHeight + "px";


    if (!nicEditors.findEditor('editMessageEditor')) {
        new nicEditor({
            fullPanel: false,
            unselectable: 'off',
            iconsPath: 'images/text-editor/nicEditorIcons.gif',
            buttonList: ['bold', 'italic', 'underline', 'left', 'center', 'right', 'justify', 'indent', 'outdent', 'hr', 'fontFormat'],
            maxHeight: editorHeight,
        }).panelInstance('editMessageEditor');
    }

    $.ajax({
        url: "pri_msgs/getMessage/" + messageID,

        method: "GET",

        complete: function (response, status, xhr) {
            var data = JSON.parse(response.responseText);
            var message = data.Message;
            var messageBody = data.MessageBody;

            if (status == 'success') {
                nicEditors.findEditor('editMessageEditor').setContent(messageBody);
            }
            else {
                alert('Error occured while fetching message<br><B>Details: ' + message);
            }
        }

    });

    if(editScreenLoaded == false)
    {
        previewFrame.src = screenBase + "index.html?mode=1&date=-1&country=Jordan&city=Amman";
        editScreenLoaded = true;
    }

    editMessageModal.addClass("in");
    editMessageModal.addClass("modal-open");

    updatePreviewActive = true;
    activeFrame = "editMessagePreviewFrame";
    activeEditor = "editMessageEditor";
    activeMessageID = messageID;
    $(previewFrame).ready(updateMessagePreview);
}

function hideEditMessageDialog() {
    updatePreviewActive = false;
    var editMessageModal = $('#editMessageModal');
    editMessageModal.removeClass("in");
    editMessageModal.removeClass("modal-open");
    editMessageModal.removeClass("display-block");
    editMessageModal.addClass("display-none");
}

function showViewMessageDialog(messageID) {


    var previewFrame = document.getElementById("viewMessagePreviewFrame");
    var previewFrameDiv = document.getElementById("viewMessagePreviewDiv");
    var viewMessageModal = $('#viewMessageModal');
    viewMessageModal.removeClass("display-none");
    viewMessageModal.addClass("display-block");

    var previewFrameDivWidth = previewFrameDiv.clientWidth;
    var previewFrameDivHeight = previewFrameDivWidth * 9 / 16;


    previewFrame.clientWidth = previewFrameDivWidth;

    previewFrame.style.height = previewFrameDivHeight + "px";


    if(viewScreenLoaded == false)
    {
        previewFrame.src = screenBase + "index.html?mode=1&date=-1&country=Jordan&city=Amman";
        viewScreenLoaded = true;
    }
    viewMessageModal.addClass("in");
    viewMessageModal.addClass("modal-open");
    previewFrame.style.visibility = "hidden";
    $(previewFrame).ready(function () {

    });

    $.ajax({
        url: "pri_msgs/getMessage/" + messageID,

        method: "GET",

        complete: function (response, status, xhr) {
            var data = JSON.parse(response.responseText);
            var message = data.Message;
            var messageBody = data.MessageBody;
            if (status == 'success') {
                displayMessage(messageBody);
            }
            else {
                alert('Error occured while fetching message<br><B>Details: ' + message);
            }
        }
    });


}

function hideViewMessageDialog() {
    var insertMessageModal = $('#viewMessageModal');
    insertMessageModal.removeClass("in");
    insertMessageModal.removeClass("modal-open");
    insertMessageModal.removeClass("display-block");
    insertMessageModal.addClass("display-none");
}


function initSelectors() {
    $.ajax({
        url: "cities/listCities",

        method: "GET",

        complete: function (response, status, xhr) {
            var data = JSON.parse(response.responseText);

            if (status == 'success') {
                $.each(data.Cities, function (idx, field) {
                    cities[field.cityName] = field.ID;
                });

                initCountryCitySelector(data);
            }
            else {
                alert('Error occured while fetching cities list<br><B>Message: ' + message);
                //$("#"+field.fieldName).parent().addClass("has-error");
            }
        }

    });
}


function initCountryCitySelector(data) {
    $("#countrySelector,#countrySelector_preview option:first-child").clone().text(data.Country).appendTo("#countrySelector,#countrySelector_preview");

    cities_names = Object.keys(cities);

    if (cities_names.length > 1) {
        cities['All Cities'] = '0';
        $("#citySelector,#citySelector_preview option:first-child").clone().text("All Cities").appendTo("#citySelector,#citySelector_preview");
    }

    $.each(cities_names, function (idx, field) {
        $("#citySelector,#citySelector_preview option:first-child").clone().text(field).appendTo("#citySelector,#citySelector_preview");
    });
}

function updateMessagePreview() {

    var activeFrameWindow = document.getElementById(activeFrame);
    $("#" + activeFrame).ready(function () {
        var iframeContent = activeFrameWindow.contentDocument;
        if(iframeContent == null)
        {
            setTimeout(updateMessagePreview, 1000);
            return;
        }
        if (iframeContent.childNodes.length == 0 && updatePreviewActive == true) {
            setTimeout(updateMessagePreview, 1000);
            return;
        }
        var contentWindow = iframeContent.getElementById("hadeethWindow");

        $(contentWindow).ready(function () {
            if (contentWindow == null) {
                setTimeout(updateMessagePreview, 1000);
                return;
            }
            var contentDoc = contentWindow.contentDocument;
            if(contentDoc.childNodes.length == 0) {
                setTimeout(updateMessagePreview, 1000);
                return;
            }
            var contentDiv = contentDoc.getElementById("previewDiv");
            var editor = nicEditors.findEditor(activeEditor);

            if(contentDiv == null || editor == null) {
                setTimeout(updateMessagePreview, 1000);
                return;
            }

            if(activeFrameWindow.style.visibility == "hidden") {
                activeFrameWindow.style.visibility = "visible";
            }

            contentDiv.innerHTML = editor.getContent();

            if (updatePreviewActive == true) {
                setTimeout(updateMessagePreview, 100);
            }
        });

    });
}

function displayMessage(messageBody) {

    var activeFrameWindow = document.getElementById("viewMessagePreviewFrame");
    $("#viewMessagePreviewFrame").ready(function () {
        var iframeContent = document.getElementById("viewMessagePreviewFrame").contentDocument;
        if(iframeContent == null)
        {
            setTimeout(displayMessage, 1000,messageBody);
            return;
        }
        if (iframeContent.childNodes.length == 0) {
            setTimeout(displayMessage, 1000,messageBody);
            return;
        }
        var contentWindow = iframeContent.getElementById("hadeethWindow");

        $(contentWindow).ready(function () {
            if (contentWindow == null) {
                setTimeout(displayMessage, 1000,messageBody);
                return;
            }
            var contentDoc = contentWindow.contentDocument;
            if (contentDoc.childNodes.length == 0) {
                setTimeout(displayMessage, 1000,messageBody);
                return;
            }
            var contentDiv = contentDoc.getElementById("previewDiv");
            if (contentDiv == null) {
                setTimeout(displayMessage, 1000,messageBody);
                return;
            }
            if(activeFrameWindow.style.visibility == "hidden") {
                activeFrameWindow.style.visibility = "visible";
            }

            contentDiv.innerHTML = messageBody;
        });

    });
}