function syncTime()
{

    var monthName = ["Jan", "Feb", "Mar", "April", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;

    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    var meladiDay = date.getDate();
    var meladiMonth = monthName[month - 1];
    var meladiYear = date.getFullYear();
    
    var dayText = document.getElementById("dayUpdateForm");
    var monthText = document.getElementById("monthUpdateForm");
    var yearText = document.getElementById("yearUpdateForm");

    var hourText = document.getElementById("hourUpdateForm");
    var minText = document.getElementById("minUpdateForm");
    var secText = document.getElementById("secUpdateForm");
    var fullTime = document.getElementById("fullTimeUpdateForm");

    dayText.value = meladiDay;
    monthText.value = meladiMonth;
    yearText.value = meladiYear;

    hourText.value = hour;
    minText.value = min;
    secText.value = sec;

    fullTime.value = dayText.value + " " + monthText.value + " " + yearText.value + " " + hourText.value + ":" + minText.value + ":" + secText.value

    console.log(month);
    console.log("time = " + dayText.value + " " + monthText.value + " " + yearText.value + " " + hourText.value + ":" + minText.value + ":" + secText.value);
    var form = document.getElementById("updateTimeForm");
    form.submit();

}