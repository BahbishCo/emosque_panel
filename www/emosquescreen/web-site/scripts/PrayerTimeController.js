﻿

function adjustPrayers() {

    if (runningMode == Setting.Mode.PI) {

        var lat = screenSetting.prayerSetting.latitude;
        var long = screenSetting.prayerSetting.longitude;
        var elevation = screenSetting.prayerSetting.elevation;
        // var timeZone = screenSetting.prayerSetting.timeZone;
        var d = new Date()
        var n = d.getTimezoneOffset();
        n = -1 * n;
        var timeZone = n / 60;
    }
    else {
        var lat = getParamValueByName("latitiude");
        var long = getParamValueByName("longitude");
        var elevation = getParamValueByName("elevation");
        // var timeZone = getParamValueByName("timeZone");
        var d = new Date()
        var n = d.getTimezoneOffset();
        n = -1 * n;
        var timeZone = n / 60;
    }

    if (runningMode != Setting.Mode.PI) {
        if (getParamValueByName("date") == -1) {
            var date = new Date();
        }
        else {
            var dateString = getParamValueByName("date");
            var year = dateString.split("-")[2];
            var month = parseInt(dateString.split("-")[1]) - 1;
            var day = parseInt(dateString.split("-")[0]) + 1;
            var date = new Date(year, month, day);
        }

    }
    else {
        var date = new Date();
    }


    var times = prayTimes.getTimes(date, [lat, long, elevation], timeZone, 0, '12h');

    var prayerEls = document.getElementsByClassName("prayerEl");

    prayerEls[0].innerHTML = fixTime(times.fajr);

    prayerEls[1].innerHTML = fixTime(times.sunrise);

    prayerEls[2].innerHTML = fixTime(times.dhuhr);

    prayerEls[3].innerHTML = fixTime(times.asr);

    prayerEls[4].innerHTML = fixTime(times.maghrib);

    prayerEls[5].innerHTML = fixTime(times.isha);



    if (screenSetting.language== Setting.Language.English)
    {
        prayerEls[7].innerHTML="FAJR"
    }else
    {
        prayerEls[7].innerHTML="الفجر"
    }


    if (screenSetting.language== Setting.Language.English)
    {
        prayerEls[8].innerHTML="SUN R"
    }else
    {
        prayerEls[8].innerHTML="الشروق"
    }

    if (screenSetting.language== Setting.Language.English)
    {
        prayerEls[9].innerHTML="DUHR"
    }else
    {
        prayerEls[9].innerHTML="الظهر"
    }

    if (screenSetting.language== Setting.Language.English)
    {
        prayerEls[10].innerHTML="ASR"
    }else
    {
        prayerEls[10].innerHTML="العصر"
    }

    if (screenSetting.language== Setting.Language.English)
    {
        prayerEls[11].innerHTML="MGRIB"
    }else
    {
        prayerEls[11].innerHTML="المغرب"
    }

    if (screenSetting.language== Setting.Language.English)
    {
        prayerEls[12].innerHTML="ISHA"
    }else
    {
        prayerEls[12].innerHTML="العشاء"
    }






    var header = document.getElementById("header");

    var elHight = bodyHeight - header.clientHeight - 10 * 6 - 30 + 30 - footerHeight;
    
    elHight = elHight / 6;

    for (var i = 0; i < prayerEls.length; i++) {
        prayerEls[i].style.height = elHight + "px";
    }

}




function ignorePX(input) {
    switch (input.length) {
        case 3:
            return input.substr(0, 1);
        case 4:
            return input.substr(0, 2);
        case 5:
            return input.substr(0, 3);
    }
}

function fixTime(time) {
    var output;
    if (time.length == 6) {
        output = "0" + time[0] + ":0" + time[2];
    }
    else if (time.length == 7) {
        indexOfColon = time.indexOf(":");
        indexOfSpace = time.indexOf(" ");
        output = "" + time.substr(0, indexOfColon);
        output = "0" + output + ":" + time.substr(indexOfColon + 1, indexOfSpace - indexOfColon - 1);
    }
    else {
        output = time[0] + time[1] + ":" + time[3] + time[4];
    }
    return output;
}
