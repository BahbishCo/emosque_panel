# ============== <setting initial permissions> ==========================
sudo chmod 777 /var;
sudo chmod 777 /home/pi/;

sudo chmod 777 -R /var/www;
sudo chmod 777 -R /home/pi/updates;
sudo chmod 777 -R /home/pi/.eMosqueSystem;
sudo chmod 777 -R /home/pi/.eMosqueData;
sudo chmod 777 -R /home/pi/updates;
# ============== </setting initial permissions> =========================

# ============== <preserving important files> ==========================

if [ ! -f /etc/updateAvailable.autofix ];
then
    sudo mkdir /home/pi/preserved-files;
    sudo mv /var/www/emosquescreen/web-site/customFiles /home/pi/preserved-files;
    sudo mv /var/www/emosquescreen/other_components/Authentication/usedPass.txt /home/pi/preserved-files/usedPass.txt;
    sudo mv /var/www/emosquescreen/other_components/Authentication/tempPass.txt /home/pi/preserved-files/tempPass.txt;
    sudo mv /var/www/emosquescreen/version /home/pi/preserved-files;
    sudo mv /home/pi/.eMosqueData/macs /home/pi/preserved-files
    sudo mv /var/www/emosquescreen/userMode.js /home/pi/preserved-files;
    sudo mv /home/pi/.eMosqueSystem/userSelectedMode.txt /home/pi/preserved-files;
fi

# ============== </preserving important files> ==========================

# ============== <applying changes on files outside emosque folders> ==========================
sudo mv /home/pi/updates/temp/scripts/git_polling.sh /etc/git_polling.sh;
# ============== </applying changes on files outside emosque folders> =========================

# ============== <moving update> ==========================

sudo rm -r /var/www;
sudo rm -r /home/pi/.eMosqueSystem;
sudo rm -r /home/pi/.eMosqueData;

sudo chmod 777 -R /home/pi/updates;
sudo mv /home/pi/updates/temp/www /var/www;
sudo mv /home/pi/updates/temp/eMosqueSystem /home/pi/.eMosqueSystem;


mkdir /var/www;
mkdir /home/pi/.eMosqueSystem;
mkdir /home/pi/.eMosqueData;

sudo chmod 777 -R /var/www;
sudo chmod 777 -R /home/pi/.eMosqueSystem;
sudo chmod 777 -R /home/pi/.eMosqueData;
# ============== </moving update> ==========================

# ============== <restoring preserved files> ==========================
if [ ! -f /etc/updateAvailable.autofix ];
then
    sudo cp /home/pi/preserved-files/customFiles /var/www/emosquescreen/web-site -rf;
    sudo cp /home/pi/preserved-files/usedPass.txt /var/www/emosquescreen/other_components/Authentication/usedPass.txt -rf;
    sudo cp /home/pi/preserved-files/tempPass.txt /var/www/emosquescreen/other_components/Authentication/tempPass.txt -rf;
    sudo cp /home/pi/preserved-files/version /var/www/emosquescreen -rf;
    sudo cp /home/pi/preserved-files/macs /home/pi/.eMosqueData;
    sudo cp /home/pi/preserved-files/userMode.js /var/www/emosquescreen;
    sudo cp /home/pi/preserved-files/userSelectedMode.txt /home/pi/.eMosqueSystem;
    sudo rm /home/pi/preserved-files -r;
else
    sudo rm /etc/updateAvailable.autofix;
fi
# ============== <restoring preserved files> ==========================

# ============== <ensure correct permission> ==========================
sudo chmod -R 777 /home/pi/.eMosqueSystem;
sudo chmod -R 777 /home/pi/.eMosqueData;
sudo chmod -R 777 /var/www;
sudo chmod 777 /home/pi;
sudo chmod 777 /var;
# ============== </ensure correct permission> =========================

# ============== <clean up> ==========================
sudo rm -r /home/pi/preserved-files;
sudo rm -r /home/pi/Desktop/*;
sudo rm /var/videos/al_kahf*;
sudo rm /var/posters/al_kahf*;
# ============== </clean up> =========================

# ============== <Auto timezone[Amman]> ==============
if [ ! -f /etc/updateAvailable.autofix ];
then
    sudo /var/www/emosquescreen/web-site/customFiles/settings/set-auto-timezone.sh
fi
# ============== </Auto timezone[Amman]> ==============

