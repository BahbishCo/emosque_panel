var primary_CurrentState = Setting.PrimaryMessageStates.Unknown;
var primary_PrevState = Setting.PrimaryMessageStates.Unknown;

var secondary_CurrentState = Setting.PrimaryMessageStates.Unknown;
var secondary_PrevState = Setting.PrimaryMessageStates.Unknown;

var isPrimaryMessage = false;
var isSecondaryMessageFlag = false;
var isStaticMessage = false;
var changeHadeethFlag = false;
var hadeethID = -1;
var displayHadeeth = true;
var primaryFrame;
var secondaryFrame;

var primaryMessagePrevFileName = "";
var primaryMessageCurrentFileName = "";
var primaryMessageFolder = "";

var fullSecondaryMessage = "";

var displayAppAdFlag = false;

function setMessageFramesHeight() {

    primaryFrame = $('#hadeethWindow');
    secondaryFrame = $('#horizTextFrame');
    isSecondaryMessage();

}


function loadContent() {


    if (secondary_CurrentState != secondary_PrevState || secondary_CurrentState == Setting.SecondaryMessageStates.Unknown) {
        if (secondary_CurrentState == Setting.SecondaryMessageStates.Hidden) {
            hideSecondaryMessage();
            secondary_PrevState = Setting.SecondaryMessageStates.Hidden;
        }
        else if (secondary_CurrentState == Setting.SecondaryMessageStates.Unknown) {
            //loadSecondaryContent();
        }
        else if (secondary_CurrentState == Setting.SecondaryMessageStates.Visible) {
            showSecondaryMessage();
            secondary_PrevState = Setting.SecondaryMessageStates.Visible;
        }
    }


    if (primary_CurrentState != primary_PrevState || primary_CurrentState == Setting.PrimaryMessageStates.Hadeeth
        || primary_CurrentState == Setting.PrimaryMessageStates.ServerMessage || primary_CurrentState == Setting.PrimaryMessageStates.Unknown) {
        primary_PrevState = primary_CurrentState;
        loadPrimaryContent();
    }
    else {
        reloadContent();
    }
}

function loadPrimaryContent() {
    switch (primary_CurrentState) {
        case Setting.PrimaryMessageStates.Unknown: {
            primary_CurrentState = Setting.PrimaryMessageStates.Hadeeth;
            primaryFrame.attr("src", "");
            primaryFrame.css("visibility", "hidden");
            reloadContent();
            break;
        }
        case Setting.PrimaryMessageStates.Hadeeth: {
            showPrimaryContent_Hadeeth_Message();
            break;
        }
        case Setting.PrimaryMessageStates.ServerMessage: {
            showPrimaryContent_Hadeeth_Message();
            break;
        }
        case Setting.PrimaryMessageStates.ImamMessage: {
            break;
        }
        case Setting.PrimaryMessageStates.Azkar: {
            if (screenSetting.language == Setting.Language.English) {
                primaryFrame.attr("src", Setting.MainContentFrameSrc.AzkarEn);

            } else {
                primaryFrame.attr("src", Setting.MainContentFrameSrc.Azkar);

            }
            primaryFrame.css("visibility", "visible");
            reloadContent();
            break;

        }
        case Setting.PrimaryMessageStates.Hidden: {
            if (screenSetting.displayAdAfterPrayer) {
                primaryFrame.attr("src", "");
                setTimeout(displayAd, 90 * 1000);
            }
            else {
                primaryFrame.attr("src", "");
                primaryFrame.css("visibility", "hidden");
            }

            reloadContent();
            break;
        }
        default: {
            reloadContent();
            break;
        }
    }
    primary_PrevState = primary_CurrentState;
}


function displayAd() {
    if(screenSetting.language == Setting.Language.English)
    {
        primaryFrame.attr("src", Setting.MainContentFrameSrc.SilentAppAdEn);
        primaryFrame.css("visibility", "visible");
    }else
    {
        primaryFrame.attr("src", Setting.MainContentFrameSrc.SilentAppAd);
        primaryFrame.css("visibility", "visible");
    }

}

function showPrimaryContent_Hadeeth_Message() {
    $.get(Setting.HttpRequest.GetPrimaryMessages, function (response, result) {
        if (result == "success") {
            console.log("result = " + response);
            response = JSON.parse(response);
            if (response.type != "0") {
                primaryMessageCurrentFileName = response.fileName;
                isPrimaryMessage = true;
                if (response.type == "2") {
                    primaryMessageFolder = Setting.MainContentFrameSrc.ImamMessage;
                }
                else {
                    primaryMessageFolder = Setting.MainContentFrameSrc.ServerMessage;
                }
            }
            else {
                primaryMessageCurrentFileName = "";
                isPrimaryMessage = false;
            }
        }
        else {
            isPrimaryMessage = false;
            primaryMessageCurrentFileName = "";
            primaryMessagePrevFileName = "";
        }

        if (isPrimaryMessage) {
            if (primaryMessageCurrentFileName != primaryMessagePrevFileName) {
                primaryMessagePrevFileName = primaryMessageCurrentFileName;
                primary_CurrentState = Setting.PrimaryMessageStates.ServerMessage;
                primaryFrame.attr("src", primaryMessageFolder + primaryMessageCurrentFileName);
                primaryFrame.css("visibility", "hidden");
                setTimeout(addScrollingScript, 3000);
                setTimeout(addCssFile, 3000);
                // setTimeout(function () {
                //     primaryFrame.css("visibility", "visible");
                // }, 4000)
            }

        }
        else {
            if (changeHadeethFlag == true || hadeethID == -1) {
                hadeethID = getNewHadeethID();
                displayHadeeth = false;
                primaryFrame.attr("src", Setting.MainContentFrameSrc.Hadeeth + "?hadeethID=" + hadeethID + "&lang=" + screenSetting.language);
                primaryFrame.css("visibility", "visible");

            }
            else if (displayHadeeth == true) {
                displayHadeeth = false;
                primaryFrame.attr("src", Setting.MainContentFrameSrc.Hadeeth + "?hadeethID=" + hadeethID + "&lang=" + screenSetting.language);
                primaryFrame.css("visibility", "visible");
            }
        }
        if (screenSetting.showBahbishMessage) {
            displayAppAdFlag = true;
        }
        reloadContent();
    });
}


function getNewHadeethID() {
    if (screenSetting.language == Setting.Language.English) {
        var hadeeths = hadeethGroupEn;

    } else {
        var hadeeths = hadeethGroup;

    }
    var randomNum = 1;
    while (true) {
        randomNum = parseInt(Math.random() * (hadeeths.length - 1));
        var newID = randomNum;
        dayContent = hadeeths[newID].content;
        if (dayContent.split(" ").length <= 100 && newID != hadeethID) {
            break;
        }
    }
    return newID;
}

function isSecondaryMessage() {
    var date = new Date();
    var fileName = getFileNameFromDate(date);

    isStaticMessage = false;

    getRequest(Setting.HttpRequest.GetSecondaryMessage + fileName,
        function (response) {
            isSecondaryMessageFlag = true;
            fullSecondaryMessage = response;
            cont();
        }, function () {
            isSecondaryMessageFlag = false;
            fullSecondaryMessage = "";
            cont();
        });

    function cont() {
        if (isSecondaryMessageFlag) {
            var secondaryFrameHeight = (bodyHeight - headerHeight - 10 * 6 - footerHeight) / 6;
            var secondaryFrameWidth = 8 / 12 * bodyWidth - 10;
            var primaryFrameHeight = (bodyHeight - headerHeight - 10 * 7 - footerHeight) / 6 * 5 + 7 * 10;
            //No Comment
            primaryFrame.css("height", (primaryFrameHeight + "px"));
            secondaryFrame.css("height", secondaryFrameHeight + "px");
            secondaryFrame.css("width", secondaryFrameWidth + "px");
            secondaryFrame.css("display", "block");
            loadSecondaryContent()
        }
        else {
            var primaryFrameHeight = bodyHeight - headerHeight - 10;
            primaryFrame.css("height", (primaryFrameHeight + "px"));
            secondaryFrame.css("display", "none");
            secondary_PrevState = Setting.SecondaryMessageStates.Hidden;

        }
    }

    //$.get(Setting.HttpRequest.GetSecondaryMessage + fileName, function (response, result) {
    //    if (result == "success") {
    //
    //    }
    //    else {
    //
    //    }
    //});
}


function loadSecondaryContent() {

    if (isSecondaryMessageFlag) {
        fullSecondaryMessage = fullSecondaryMessage;
    }

    if (isStaticMessage) {
        for (var i = 0; i < staticMessages.length; i++) {
            if (staticMessages[i].enabled) {
                if (isSecondaryMessageFlag) {
                    fullSecondaryMessage = fullSecondaryMessage + " *** " + staticMessages[i].content;
                }
                else {
                    if (i == 0) {
                        fullSecondaryMessage = fullSecondaryMessage + staticMessages[i].content;

                    }
                    else {
                        fullSecondaryMessage = fullSecondaryMessage + " *** " + staticMessages[i].content;
                    }
                }
            }
        }
    }

    document.getElementById("horizTextFrame").contentWindow.document.getElementById('horizMarquee').innerHTML = fullSecondaryMessage;
    secondaryFrame.css("visibility", "visible");
    secondaryFrame.css("display", "block");

    secondary_CurrentState = Setting.SecondaryMessageStates.Visible;
}

function hideSecondaryMessage() {
    secondaryFrame.css("visibility", "hidden");
    secondaryFrame.css("display", "none");
    secondary_CurrentState = Setting.SecondaryMessageStates.Hidden;
}

function showSecondaryMessage() {
    if(isSecondaryMessageFlag)
    {
        secondaryFrame.css("visibility", "visible");
        secondaryFrame.css("display", "block");
        secondary_CurrentState = Setting.SecondaryMessageStates.Visible;
        if(screenSetting.language == Setting.Language.English)
        {
            document.getElementById("horizTextFrame").contentWindow.setDirection("left");
        }else
        {
            document.getElementById("horizTextFrame").contentWindow.setDirection("right");
        }
    }
}

function isBahbishMessage() {
    isStaticMessage = screenSetting.showBahbishMessage;
    return screenSetting.showBahbishMessage;
}

function reloadContent() {
    var xFunc = setInterval(function x(){
			
    if (secondary_CurrentState != secondary_PrevState || secondary_CurrentState == Setting.SecondaryMessageStates.Unknown) {
        if (secondary_CurrentState == Setting.SecondaryMessageStates.Hidden) {
            hideSecondaryMessage();
            secondary_PrevState = Setting.SecondaryMessageStates.Hidden;
        }
        else if (secondary_CurrentState == Setting.SecondaryMessageStates.Unknown) {
            //loadSecondaryContent();
        }
        else if (secondary_CurrentState == Setting.SecondaryMessageStates.Visible) {
            showSecondaryMessage();
            secondary_PrevState = Setting.SecondaryMessageStates.Visible;
        }
    }


    if (primary_CurrentState != primary_PrevState || primary_CurrentState == Setting.PrimaryMessageStates.Hadeeth
        || primary_CurrentState == Setting.PrimaryMessageStates.ServerMessage || primary_CurrentState == Setting.PrimaryMessageStates.Unknown) {
        primary_PrevState = primary_CurrentState;
        loadPrimaryContent();
    }
    else {
        reloadContent();
    }
		clearInterval(xFunc);
		}, 500);
}


function getFileNameFromDate(date) {

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    if (day <= 9) {
        day = "0" + day;
    }
    if (month <= 9) {
        month = "0" + month;
    }
    var output = day + "-" + month + "-" + year + ".html";
    return output;
}


function showBigIqama() {
	primary_CurrentState = Setting.PrimaryMessageStates.Hidden;
    if (!isJumaaPrayerTime) {
        if (screenSetting.language == Setting.Language.English) {
            document.getElementById("bigIqamaText").innerHTML = "IQAMA";

        } else {
            document.getElementById("bigIqamaText").innerHTML = "اقامة";

        }
        document.getElementById("bigIqamaText").style.display = "block";
        document.getElementById("bigIqamaCounter").style.display = "block";
        document.getElementById("bigIqamaText").style.visibility = "visible";
        document.getElementById("bigIqamaCounter").style.visibility = "visible";
    }
}

function hideBigIqama() {


    document.getElementById("bigIqamaText").style.display = "block";
    document.getElementById("bigIqamaText").style.visibility = "hidden";


    document.getElementById("bigIqamaCounter").style.display = "block";
    document.getElementById("bigIqamaCounter").style.visibility = "hidden";


}


function addScrollingScript() {
    var contentFrame = document.getElementById("hadeethWindow");

    var script = contentFrame.contentWindow.document.createElement("script");
    script.type = "text/javascript";
    if (runningMode == Setting.Mode.PI) {
        script.src = "../../../../scripts/jquery-1.9.1.js";
    }
    else {
        script.src = "../../../../../eMosqueScreen/scripts/jquery-1.9.1.js";
    }

    contentFrame.contentWindow.document.body.appendChild(script);

    script = contentFrame.contentWindow.document.createElement("script");
    script.type = "text/javascript";
    if (runningMode == Setting.Mode.PI) {
        script.src = "../../../../scripts/scrollPage.js";
    }
    else {
        script.src = "../../../../../eMosqueScreen/scripts/scrollPage.js";
    }

    contentFrame.contentWindow.document.body.appendChild(script);


}

function addCssFile() {
    var contentFrame = document.getElementById("hadeethWindow");
    var link = contentFrame.contentWindow.document.createElement("link");

    if (runningMode == Setting.Mode.PI) {
        link.href = "../../../../css/primaryMessageStyle.css";
        link.rel = "stylesheet";
    }
    else {
        link.href = "../../../../../eMosqueScreen/css/primaryMessageStyle.css";
        link.rel = "stylesheet";
    }
    contentFrame.contentWindow.document.head.appendChild(link);

    link = contentFrame.contentWindow.document.createElement("link");

    if (runningMode == Setting.Mode.PI) {
        link.href = "../../../../customFiles/CSS/primaryMsgColor.css";
        link.rel = "stylesheet";
    }
    contentFrame.contentWindow.document.head.appendChild(link);


}


function displayAppAd() {
    if (displayAppAdFlag) {

        if(screenSetting.language == Setting.Language.English)
        {

            console.log("show Ad");
            primaryFrame.attr("src", Setting.MainContentFrameSrc.SilentAppAdEn);
            setTimeout(switchFromAdToHadeeth, Setting.AppAdDisplayPeroid);
        }else
        {

            console.log("show Ad");
            primaryFrame.attr("src", Setting.MainContentFrameSrc.SilentAppAd);
            setTimeout(switchFromAdToHadeeth, Setting.AppAdDisplayPeroid);
        }


    }
    else {
        switchFromAdToHadeeth();
    }

}

function switchFromAdToHadeeth() {

    setTimeout(function () {
        displayHadeeth = true;
        primaryMessagePrevFileName = "";
        primary_CurrentState = Setting.PrimaryMessageStates.Unknown;
    }, 2000);
}

function setPrimaryFrameVisibility(isVisible)
{

    if(isVisible)
    {
        primaryFrame.css("visibility", "visible");
    }else
    {
        primaryFrame.css("visibility", "hidden");
    }
}


