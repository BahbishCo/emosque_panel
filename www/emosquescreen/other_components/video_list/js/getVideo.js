/**
 * Created by Dua' on 7/11/2016.
 */

function getVideo(vName) {
    playVideoPhp(vName);
}

function playVideoPhp(videoName) {
    getRequest(
        './php/getVideo.php?videoName='+videoName,
        loadSuccessHandler,  // handle successful request
        loadFailureHandler    // handle error
    );
}

function loadSuccessHandler(responseText) {
   // alert(responseText);
}

function loadFailureHandler(responseText) {
    alert(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
}

function stopVideo() {
    stopVideoPhp();
}

function stopVideoPhp() {
    getRequest(
        './php/stopVideo.php',
        stopSuccessHandler,  // handle successful request
        stopFailureHandler    // handle error
    );
}

function stopSuccessHandler(responseText) {
    //alert(responseText);
}

function stopFailureHandler(responseText) {
    alert(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
}

function uploadVideo() {
    uploadVideoPhp("../../../../../videos/");
}

function uploadVideoPhp(videoPath) {
    getRequest(
        './php/uploadVideo.php?videoPath='+videoPath,
        uploadSuccessHandler,  // handle successful request
        uploadFailureHandler    // handle error
    );
}

function uploadSuccessHandler(responseText) {
    var res= responseText.split(";");
	var select = document.getElementById("select_video"); 
    for(var i = 0; i < res.length-1; i++) {
		 var row = select.insertRow(0);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	
	cell1.innerHTML= "<table><tr><th><img src=./posters/"+res[i]+".jpg onclick=getVideo('"+res[i]+"') style='width:150px;height:100px;' ></th> </tr>"
	+" <tr><th><b><font size='3.5pt' color='#778899' align='center' onclick=getVideo('"+res[i]+"')>"+res[i]+"</font></b></th> </tr></table>";
	
	cell2.innerHTML=  "<table><tr><th><button id='test-button' name='test' onclick=getVideo('"+res[i]+"') style='width:180px;height: 50px;margin-left:10px;margin-top:10px;'>play video</button></th></tr>"
	+"<tr><th><button id='test-button' name='test' onclick='stopVideo()' style='width:180px;height: 50px;margin-left:10px;margin-top:10px;'>stop video</button></th></tr> </table>";
  
	}
	
   }

function uploadFailureHandler(responseText) {
    alert(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
}

function getRequest(url, success, error) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}
