<?php

$content = $_POST["content"];
$dates = $_POST["date"];
$response = "failed";
$pathToWrite = "/var/www/emosquescreen/web-site/customFiles/contents/primaryMessages/ImamMessages/";
//$pathToWrite = "C:\\xampp\\htdocs\\eMosqueSystem\\www\\emosquescreen\\web-site\\customFiles\\contents\\primaryMessages\\ImamMessages\\";

$hadeethHeader = "حديث شريف";
$hadeethFooter = "صدق رسول الله";

$timeConstant = "00:00_23:59";
if ($content == null || $dates == null) {
    $response = "0";
} else {
    $fileName = $dates . "_" . $dates . "." . $timeConstant . ".html";


    $stringToWrite = "<h1 style='text-align:center;'>" . $hadeethHeader . "</h1>" . "<p style='text-align:center;'>" . $content . "</p>" .
        "<h1 style='text-align:left;'>" . $hadeethFooter . "</h1>";

    $fileToWrite = fopen($pathToWrite . $fileName, "w");
    fwrite($fileToWrite, $stringToWrite);
    fclose($fileToWrite);
    $response = "1";
}
$today = date("Y-m-d");
if ($today == $dates) {
    exec("sudo pkill midori");
    exec("sudo pkill iceweasel");
}
echo json_encode(array('response' => $response));

?>