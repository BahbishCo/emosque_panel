import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by hazem on 12/11/16.
 */
public class HelpingFunctions {
	public static void writeFile(String path, String data) {
		File file;
		FileWriter fw;
		BufferedWriter bw;
		file = new File(path);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			fw = new FileWriter(file.getAbsoluteFile());
			bw = new BufferedWriter(fw);
			bw.write("" + data);
			bw.close();
		} catch (IOException e) {
			System.err.println("IOException: " + path);
		}
	}

	public static String execCmd(String cmd) {
		return execCmdOptionalOutput(cmd, true);
	}

	public static String execCmdOptionalOutput(String cmd, boolean waitFor) {
		System.err.println("--executing command: " + cmd);

		StringBuilder sb = new StringBuilder();
		try {
			Process p = Runtime.getRuntime().exec(cmd);

			if (!waitFor) {
				System.err.println("Returning without waiting for the output of the command");
				return null;
			}
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (IOException | InterruptedException e) {
		}
		String output = sb.toString();
		System.err.print(output);
		return output;
	}

	public static String readFile(String path) {
		String content = null;
		BufferedReader br;
		StringBuilder sb = new StringBuilder();
		try {
			br = new BufferedReader(new FileReader(path));
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			content = sb.toString();
			br.close();
		} catch (FileNotFoundException e) {
			System.err.println("FileNotFoundException: " + path);
		} catch (IOException e) {
			System.err.println("IOException: " + path);
		}
		return content;
	}

	public static boolean downloadFile(String urlToDownload, String target) {
		boolean downloaded = false;
		try {
			File targetParent = new File(target).getParentFile();
			if (!targetParent.exists()) {
				targetParent.mkdirs();
			}
			BufferedInputStream in;
			in = new BufferedInputStream(new URL(urlToDownload).openStream());
			FileOutputStream fout;
			fout = new FileOutputStream(target);
			final byte data[] = new byte[1024];
			int count;
			while ((count = in.read(data, 0, 1024)) != -1) {
				fout.write(data, 0, count);
				downloaded = true;
			}
			in.close();
			fout.close();
		} catch (IOException e) {
		}
		return downloaded;
	}

	public static String downloadString(String URLString) {

		URL url;
		InputStream is = null;
		BufferedReader br;
		String line;
		boolean error = false;
		System.err.println("Downloading String: " + URLString);
		StringBuilder response = new StringBuilder();
		try {
			url = new URL(URLString);
			is = url.openStream(); // throws an IOException
			br = new BufferedReader(new InputStreamReader(is));

			while ((line = br.readLine()) != null) {
				response.append(line);
			}
		} catch (MalformedURLException mue) {
			System.err.println("Error downloading String: " + URLString + "\nMalformedURLException");
			error = true;
		} catch (IOException ioe) {
			System.err.println("Error downloading String: " + URLString + "\nIOException");
			error = true;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException ioe) {
				error = true;
			}
		}

		if (error || response.toString().length() == 0) {
			return null;
		} else {
			return response.toString();
		}
	}

	public static String executePost(String targetURL, String urlParameters) {
		HttpURLConnection connection = null;
		System.out.println("Posting " + urlParameters + " to " + targetURL);
		try {
			// Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.close();

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			StringBuilder response = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	public static boolean fileExists(String path) {
		return new File(path).exists();
	}

	public static void makeSureDirExists(String path) {
		if (!fileExists(path))
			new File(path).mkdir();
	}

	public static boolean twoStringsAreLogicallyEqual(String str1, String str2) {
		if (str1 == null && str2 == null)
			return true;
		if (str1 == null && str2 != null || str1 != null && str2 == null)
			return false;
		return str1.trim().equals(str2.trim());
	}

	public static boolean stringISEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}

	static class LiveStreamingHandle {

		static void startLiveStreaming() {
			System.out.println("Starting live streaming..");
			new Thread() {
				@Override
				public void run() {
					writeFile(Main.ISLIVESTREAMING_FLAG_PATH, "");
					execCmdOptionalOutput(Main.LIVE_STREAMING_START_SCRIPT, false);
					while (true) {
					}
				}
			}.start();
		}

		static void stopLiveStreaming() {
			System.out.println("Stopping live streaming..");
			HelpingFunctions.execCmd(Main.LIVE_STREAMING_STOP_SCRIPT);
		}

		static boolean isCurrentlyLiveStreaming() {
			System.out.println("Checking the status of LiveStreaming..");
			return fileExists("/home/pi/isStreaming");
		}
	}

	public static boolean isTempSensorConnected() {
		boolean isConnected = false;
		String cmd = Constants.Commands.ListUSB;
		Runtime rt = Runtime.getRuntime();
		Process proc;
		try {
			proc = rt.exec(cmd);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String output = "";
			String line;
			while ((line = stdInput.readLine()) != null) {
				output = output + line + "\n";
			}
			System.out.println(output);
			isConnected = findTempSensorFromString(output);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return isConnected;
	}

	public static boolean findTempSensorFromString(String str) {
		String[] strArray = str.split(" ");
		for (int i = 0; i < strArray.length; i++) {
			if (strArray[i].equals(Constants.tempSensorIDs)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isValidIP(byte lastDigit) throws IOException {
		boolean isRechable = false;
		InetAddress inet;
		inet = InetAddress.getByAddress(new byte[] { (byte) 192, (byte) 168, 0, lastDigit });
		isRechable = inet.isReachable(5000);
		//System.out.println("Sending Ping Request to " + inet + (isRechable ? ": Sucessed" : ": Failed"));
		return isRechable;
	}

}
