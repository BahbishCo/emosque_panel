var submitButton;
var timeZoneSelector;
var hijriDateCorrectionSelector;

$(document).ready(function () {
    var submitButton = document.getElementById("submitButton");
    var timeZoneSelector = document.getElementById("timeZoneSelector");
    var hijriDateCorrectionSelector = document.getElementById("hijriCorrection");
    $('select').on('change', function () {
        submitButton.disabled = false;
        submitButton.style.background = '#00FF00';
        submitButton.style.color = '#000000'
    });

    initDays(miladiDaySelector);
    initMonths(miladiMonthSelector);
    initYears(miladiYearSelector);
    initHijriCorrection(hijriDateCorrectionSelector);
    validateSettingFile();
    initHoursAndMinSelectors();
    adjustDate();
    attachListeners();

    loadTimeZoneSelectors();

    loadPanelSetting();


});

function reduceOneHour() {
    if (timeZoneSelector.selectedIndex > 0) {
        timeZoneSelector.selectedIndex = timeZoneSelector.selectedIndex - 1;
        adjustPrayers();
        submitButton.disabled = false;
        submitButton.style.background = '#afbec5';
        submitButton.style.color = '#000000'
    }
}

function addOneHour() {
    if (timeZoneSelector.selectedIndex < 48) {
        timeZoneSelector.selectedIndex = timeZoneSelector.selectedIndex + 1;
        adjustPrayers();
        submitButton.disabled = false;
        submitButton.style.background = '#afbec5';
        submitButton.style.color = '#000000'
    }
}

function loadPanelSetting() {
    //prayers time settings
    var countrySelector = document.getElementById("countrySelector");
    var citySelector = document.getElementById("citySelector");
    var highLatSelector = document.getElementById("highLatSelector");
    var asrJuristicSelector = document.getElementById("asrJuristicSelector");
    var calMethodeSelector = document.getElementById("calMethodeSelector");
    var midnightMethod = document.getElementById("midnightMethod");
    var langSelector = document.getElementById("langSelector");

    var options = langSelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.language) {
            options[i].selected = true;
            break;
        }
    }


    var options = countrySelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.country) {
            options[i].selected = true;
            break;
        }
    }

    removeOptions(citySelector)
    loadCities(countrySelector.value);

    var options = citySelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.city) {
            options[i].selected = true;
            break;
        }
    }


    var options = calMethodeSelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.calculationMethod) {
            options[i].selected = true;
            break;
        }
    }


    var options = asrJuristicSelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.asrJuristic) {
            options[i].selected = true;
            break;
        }
    }


    var options = highLatSelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.adjusthighLat) {
            options[i].selected = true;
            break;
        }
    }


    var options = midnightMethod.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.midNightMethod) {
            options[i].selected = true;
            break;
        }
    }

    var d = new Date()
    var n = d.getTimezoneOffset();
    n = -1 * n;
    var timeZone = n / 60;
    timeZoneSelector.options[timeZone + parseInt(timeZoneSelector.options.length / 2)].selected = true;


    var QREnable = document.getElementById("showQRCodeCheckbox");
    var showIntenalTempCheckbox = document.getElementById("showIntenalTempCheckbox");
    var azanEnable = document.getElementById("playAzan");
//    var displayAnalogClock = document.getElementById("displayAnalogClock");
    var displayBahbishMessage = document.getElementById("displayBahbishMessage");
    var adAtPrayerEnable = document.getElementById("displayAdAtPrayerTime");


    if (screenSetting.showBahbishMessage == true) {
        displayBahbishMessage.checked = true;
    }
    else {
        displayBahbishMessage.checked = false;
    }


   /* if (screenSetting.showAnalogClock == true) {
        displayAnalogClock.checked = true;
    }
    else {
        displayAnalogClock.checked = false;
    }*/


    if (screenSetting.appQREnable == true) {
        QREnable.checked = true;
    }

    if (screenSetting.internalTempEnable == true) {
        showIntenalTempCheckbox.checked = true;
    }


    if (screenSetting.playAzan == true) {
        azanEnable.checked = true;
    }
    else {
        azanEnable.checked = false;
    }


    if (screenSetting.displayAdAfterPrayer == true) {
        adAtPrayerEnable.checked = true;
    }
    else {
        adAtPrayerEnable.checked = false;
    }

    // Panel Mode
    var primaryEthEnable = document.getElementById("primaryEth");
    var primaryWifiEnable = document.getElementById("primaryWifi");
    var primaryHotspotEnable = document.getElementById("primaryHotspot");
    var secondaryEthEnable = document.getElementById("secondaryEth");
    var secondaryWifiEnable = document.getElementById("secondaryWifi");

    primaryEthEnable.checked = false;
    primaryWifiEnable.checked = false;
    primaryHotspotEnable.checked = false;
    secondaryEthEnable.checked = false;
    secondaryWifiEnable.checked = false;


    if (typeof userMode === 'undefined') {
        // USER MODE IS NOT SELECTED
    } else {
        switch (userMode) {
            case 1:
                primaryEthEnable.checked = true;
                break;
            case 2:
                primaryWifiEnable.checked = true;
                break;
            case 3:
                primaryHotspotEnable.checked = true;
                break;
            case 4:
                secondaryEthEnable.checked = true;
                break;
            case 5:
                secondaryWifiEnable = true;
                break;
        }
    }


    //Azkar setting
    var fajrAzkarDisplayPeriod = document.getElementById("fajrAzkarDisplayPeriod");
    var duhurAzkarDisplayPeriod = document.getElementById("duhurAzkarDisplayPeriod");
    var asrAzkarDisplayPeriod = document.getElementById("asrAzkarDisplayPeriod");
    var maghribAzkarDisplayPeriod = document.getElementById("maghribAzkarDisplayPeriod");
    var ishaaAzkarDisplayPeriod = document.getElementById("ishaaAzkarDisplayPeriod");
    var jumaaAzkarDisplayPeriod = document.getElementById("jumaaAzkarDisplayPeriod");

    fajrAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.fajr].selected = true;
    duhurAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.duhur].selected = true;
    asrAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.asr].selected = true;
    maghribAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.maghrib].selected = true;
    ishaaAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.ishaa].selected = true;
    jumaaAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.jumaa].selected = true;


    //Iqama setting
    var fajrIqamaTime = document.getElementById("fajrIqamaTime");
    var duhurIqamaTime = document.getElementById("duhurIqamaTime");
    var asrIqamaTime = document.getElementById("asrIqamaTime");
    var maghribIqamaTime = document.getElementById("maghribIqamaTime");
    var ishaaIqamaTime = document.getElementById("ishaaIqamaTime");
    var jumaaIqamaTime = document.getElementById("jumaaIqamaTime");

    fajrIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.fajr].selected = true;
    duhurIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.duhur].selected = true;
    asrIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.asr].selected = true;
    maghribIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.maghrib].selected = true;
    ishaaIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.ishaa].selected = true;
    jumaaIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.jumaa].selected = true;


    var beforFajrAzkarPeriod = document.getElementById("beforFajrAzkarPeriod");
    var beforduhurAzkarPeriod = document.getElementById("beforduhurAzkarPeriod");
    var beforAsrAzkarPeriod = document.getElementById("beforAsrAzkarPeriod");
    var beforMaghribAzkarPeriod = document.getElementById("beforMaghribAzkarPeriod");
    var beforIshaaAzkarPeriod = document.getElementById("beforIshaaAzkarPeriod");
    var beforJumaaAzkarPeriod = document.getElementById("beforJumaaAzkarPeriod");

    beforFajrAzkarPeriod.options[parseInt(screenSetting.timeBetweenAzanAndAzkar.fajr) - parseInt(screenSetting.timeBetweenAzanAndIqama.fajr)].selected = true;
    beforduhurAzkarPeriod.options[parseInt(screenSetting.timeBetweenAzanAndAzkar.duhur) - parseInt(screenSetting.timeBetweenAzanAndIqama.duhur)].selected = true;
    beforAsrAzkarPeriod.options[parseInt(screenSetting.timeBetweenAzanAndAzkar.asr) - parseInt(screenSetting.timeBetweenAzanAndIqama.asr)].selected = true;
    beforMaghribAzkarPeriod.options[parseInt(screenSetting.timeBetweenAzanAndAzkar.maghrib) - parseInt(screenSetting.timeBetweenAzanAndIqama.maghrib)].selected = true;
    beforIshaaAzkarPeriod.options[parseInt(screenSetting.timeBetweenAzanAndAzkar.ishaa) - parseInt(screenSetting.timeBetweenAzanAndIqama.ishaa)].selected = true;
    beforJumaaAzkarPeriod.options[parseInt(screenSetting.timeBetweenAzanAndAzkar.jumaa) - parseInt(screenSetting.timeBetweenAzanAndIqama.jumaa)].selected = true;


    //Prayers correction setting
    var fajrTimeCorrection = document.getElementById("fajrTimeCorrection");
    var duhurTimeCorrection = document.getElementById("duhurTimeCorrection");
    var asrTimeCorrection = document.getElementById("asrTimeCorrection");
    var maghribTimeCorrection = document.getElementById("maghribTimeCorrection");
    var ishaaTimeCorrection = document.getElementById("ishaaTimeCorrection");

    fajrTimeCorrection.options[screenSetting.prayerSetting.offset.fajr + parseInt(fajrTimeCorrection.options.length / 2)].selected = true;
    duhurTimeCorrection.options[screenSetting.prayerSetting.offset.dhuhr + parseInt(duhurTimeCorrection.options.length / 2)].selected = true;
    asrTimeCorrection.options[screenSetting.prayerSetting.offset.asr + parseInt(asrTimeCorrection.options.length / 2)].selected = true;
    maghribTimeCorrection.options[screenSetting.prayerSetting.offset.maghrib + parseInt(maghribTimeCorrection.options.length / 2)].selected = true;
    ishaaTimeCorrection.options[screenSetting.prayerSetting.offset.isha + parseInt(ishaaTimeCorrection.options.length / 2)].selected = true;

    timeZoneSelector.options[timeZone + parseInt(timeZoneSelector.options.length / 2)].selected = true;
    gmtLabel.innerHTML = "GMT " + timeZoneSelector.options[timeZoneSelector.selectedIndex].value;

    console.log("screenSetting.hijriCorrection = " + screenSetting.hijriCorrection);
    hijriDateCorrectionSelector.options[screenSetting.hijriCorrection + 3].selected = true;
    console.log("screenSetting.hijriCorrection = " + hijriDateCorrectionSelector.options[hijriDateCorrectionSelector.selectedIndex].value);
    onMiladiDateChange();

    adjustPrayers();
    //onMiladiDateChange();

}

function validateSettingFile() {

    var d = new Date()
    var n = d.getTimezoneOffset();
    n = -1 * n;
    var timeZone = n / 60;

    if (typeof screenSetting === "undefined") {
        console.log("screen setting object is not defined");
        screenSetting =
        {
            prayerSetting: {
                country: "Jordan",
                city: "Amman",
                calculationMethod: "Karachi",
                asrJuristic: "Standard",
                adjusthighLat: "AngleBased",
                midNightMethod: "Standard",
                fajrAngle: 18,
                ishaAngle: 18,
                latitude: 32.02027777777778,
                longitude: 35.82944444444445,
                elevation: 1100,
                timeZone: timeZone,
                offset: {fajr: 0, dhuhr: 0, asr: 0, maghrib: 0, isha: 0,},
            },
            hijriCorrection: 0,
            timeBetweenAzanAndIqama: {fajr: 30, duhur: 18, asr: 18, maghrib: 8, ishaa: 13, jumaa: 0,},
            timeBetweenAzanAndAzkar: {fajr: 38, duhur: 26, asr: 26, maghrib: 18, ishaa: 25, jumaa: 50,},
            azkarDisplayPeriod: {fajr: 10, duhur: 10, asr: 10, maghrib: 10, ishaa: 10, jumaa: 10,},
            appQREnable: false,
            internalTempEnable: true,
            playAzan: false,
            showAnalogClock: false,
            showBahbishMessage: true,
            displayAdAfterPrayer: true,
        }
    }
    else {
        if (checkVarValidity(null, "prayerSetting", {
                country: "Jordan",
                city: "Amman",
                calculationMethod: "Karachi",
                asrJuristic: "Standard",
                adjusthighLat: "AngleBased",
                midNightMethod: "Standard",
                fajrAngle: 18,
                ishaAngle: 18,
                latitude: 32.02027777777778,
                longitude: 35.82944444444445,
                elevation: 1100,
                timeZone: timeZone,
                offset: {fajr: 0, dhuhr: 0, asr: 0, maghrib: 0, isha: 0,},
            })) {
            checkVarValidity("prayerSetting", "country", "Jordan");
            checkVarValidity("prayerSetting", "city", "Amman");
            checkVarValidity("prayerSetting", "calculationMethod", "Karachi");
            checkVarValidity("prayerSetting", "asrJuristic", "Standard");
            checkVarValidity("prayerSetting", "adjusthighLat", "AngleBased");
            checkVarValidity("prayerSetting", "midNightMethod", "Standard");
            checkVarValidity("prayerSetting", "fajrAngle", 18);
            checkVarValidity("prayerSetting", "ishaAngle", 18);
            checkVarValidity("prayerSetting", "latitude", 32.02027777777778);
            checkVarValidity("prayerSetting", "longitude", 35.82944444444445);
            checkVarValidity("prayerSetting", "elevation", 1100);
            checkVarValidity("prayerSetting", "timeZone", timeZone);
            checkVarValidity("prayerSetting", "offset", {fajr: 0, dhuhr: 0, asr: 0, maghrib: 0, isha: 0,});
        }

        checkVarValidity(null, "hijriCorrection", 0);
        checkVarValidity(null, "timeBetweenAzanAndIqama", {
            fajr: 30,
            duhur: 18,
            asr: 18,
            maghrib: 8,
            ishaa: 13,
            jumaa: 0,
        });
        checkVarValidity(null, "timeBetweenAzanAndAzkar", {
            fajr: 38,
            duhur: 26,
            asr: 26,
            maghrib: 18,
            ishaa: 25,
            jumaa: 50,
        });
        checkVarValidity(null, "azkarDisplayPeriod", {
            fajr: 10,
            duhur: 10,
            asr: 10,
            maghrib: 10,
            ishaa: 10,
            jumaa: 10,
        });
        checkVarValidity(null, "appQREnable", false);
        checkVarValidity(null, "internalTempEnable", true);
        checkVarValidity(null, "playAzan", false);
        checkVarValidity(null, "showAnalogClock", false);
        checkVarValidity(null, "showBahbishMessage", true);
        checkVarValidity(null, "displayAdAfterPrayer", true);
    }
}
function checkVarValidity(varParent, varName, varDefaultValue) {

    var isValid = true;
    if (varParent !== null) {
        if (typeof screenSetting[varParent][varName] === "undefined") {
            screenSetting[varParent][varName] = varDefaultValue;
            isValid = false;
        }
    }
    else {
        if (typeof screenSetting[varName] === "undefined") {
            screenSetting[varName] = varDefaultValue;
            isValid = false;
        }
    }
    return isValid;
}

function modeSelectedCallback(chx) {
    var primaryEthEnable = document.getElementById("primaryEth");
    var primaryWifiEnable = document.getElementById("primaryWifi");
    var primaryHotspotEnable = document.getElementById("primaryHotspot");
    var secondaryEthEnable = document.getElementById("secondaryEth");
    var secondaryWifiEnable = document.getElementById("secondaryWifi");

    primaryEthEnable.checked = false;
    primaryWifiEnable.checked = false;
    primaryHotspotEnable.checked = false;
    secondaryEthEnable.checked = false;
    secondaryWifiEnable.checked = false;


    chx.checked = true;

}
