<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 7/4/2017
 * Time: 4:13 PM
 */


$op = $_GET["op"];


$baseFolder = "/usr/share/zoneinfo";

if ($op == "read") {
//$baseFolder = "C:\\xampp\\htdocs\\emosque_panel_dev\\www\\emosquescreen\\other_components\\setting\\zoneinfo";

    $allowedDirectory = ["Africa", "America", "Antarctica", "Australia", "Arctic", "Asia", "Atlantic",
        "Europe", "Indian", "Pacific", "US"];

    $result = array();

    $directories = glob($baseFolder . '/*', GLOB_ONLYDIR);
    $dir;
    $files;
    $file;
    $obj;
    for ($i = 0; $i < sizeof($directories); $i++) {
        $dir = $directories[$i];
        $dirName = basename($dir);
        if (in_array($dirName, $allowedDirectory)) {
            $obj = array();
            $obj["folderName"] = $dirName;
            $obj["files"] = array();
            $files = glob($baseFolder . '/' . $dirName . '/*');
            for ($j = 0; $j < sizeof($files); $j++) {
                $file = $files[$j];
                if (is_dir($file)) {
                    $subFolderName = basename($file);
                    $subFiles = glob($file . '/*');
                    for ($k = 0; $k < sizeof($subFiles); $k++) {
                        if (!is_dir($subFiles[$k])) {
                            array_push($obj["files"], $subFolderName . "/" . basename($subFiles[$k]));
                        }
                    }
                } else {
                    array_push($obj["files"], basename($file));
                }
            }
            array_push($result, $obj);
        }
    }

    echo json_encode($result);

} else if ($op == "write") {
    $selectedTZ = $_GET["TZ"];
//    echo $selectedTZ;
    $srcPath = $baseFolder . "/" . $selectedTZ;
    $destFile = "/etc/localtime";
    $tzFile = "/etc/timezone";
//    echo "sudo cp " . $srcPath . " " . $destFile;
    shell_exec("sudo cp " . $srcPath . " " . $destFile);
//    echo "sudo echo " ."\"" . $selectedTZ . "\"" . " > " . $tzFile;
    shell_exec("sudo echo " ."\"" . $selectedTZ . "\"" . " > " . $tzFile);

    //create an empty file to indicate that the user had selected the time zone of the panel.
    shell_exec("sudo touch /var/www/emosquescreen/web-site/customFiles/settings/time-zone-is-selected.txt");
    
    shell_exec("sudo pkill midori");
    shell_exec("sudo pkill iceweasel");
    echo "تمت العملية بنجاح";

} else if($op == "readTZ") {
//    date +%Z
    echo shell_exec("date +%z");
}


?>

