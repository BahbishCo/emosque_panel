<?php

function getParentAbsolutePath()
{
    $list = explode('/', __FILE__);
    $parent = '';
    for ($i = 1; $i < count($list) - 1; $i++)
    {
        $parent .= '/' . $list[$i];
    }
    return $parent;
}

function hasAccess($mac_address, $authorized_macs_file)
{
    if (strcmp(trim(shell_exec("cat $authorized_macs_file | grep $mac_address"), " \t\n"), $mac_address) == 0)
    {
        return 1;
    } else
    {
        return 0;   
    }
}


if (!isset($_GET["mac_address"]))
{
    die('0');
}

$data_dir = '/home/pi/.eMosqueData';

$authorized_macs_file = $data_dir . '/macs';

shell_exec(getParentAbsolutePath() . '/init.sh');

$mac_address = $_GET["mac_address"];

if (hasAccess($mac_address, $authorized_macs_file))
{
    die('1');
} else
{
    $lsusb = shell_exec("lsusb | grep 'Microdia'");
    if (strpos($lsusb, 'Microdia'))
    {
        die('0');
    } else
    {
        shell_exec(getParentAbsolutePath() . '/store_mac.sh ' . $mac_address);
        die('1');
    }
}