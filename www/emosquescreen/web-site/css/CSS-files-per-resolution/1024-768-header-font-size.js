/*
    This CSS file is used to set the font size of all elements for different resolutions

*/


/**************************************/
/*           (1024 * 768)             */
/**************************************/

dateElementFontSize = {
	meladiDate: "60px",
	hijriDate: "33px",
	miladiArabicWord: "45px",
	miladiArabicWordMargin: "15px",
        headerMiladiArabicWord: "66px",
        headerHijriArabicWord: "47px",
        tempFontSize: "50px",
}



