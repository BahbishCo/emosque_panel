#!/bin/sh
sudo wmctrl -k on
if [ ! -f /home/pi/.eMosqueSystem/.isdev ]; then

cd /etc/panel;
git reflog expire --all --expire=now
git gc --prune=now --aggressive


cd /home/pi;
sudo git clone /etc/panel;
sudo rm /home/pi/updates/ -r;
sudo mkdir /home/pi/updates/;
sudo mv /home/pi/panel/ /home/pi/updates/temp;

sudo rm /home/pi/updates/temp/.git -r;
sudo php /home/pi/.eMosqueSystem/processes/updatezip/zip.php;
fi
cd /etc/panel;sudo echo $(git rev-list --count master) > /var/www/emosquescreen/other_components/version/system.txt;
sudo chmod -R 777 /home/pi/updates;
sudo ln -s /home/pi/updates /var/www;
sudo chmod 777 /var/www -R;

