<?php
	$passRequest = 1;
	if($_GET["requestType"] != $passRequest)
	{
		echo "bad request";
		return;
		
	}	

	$newPassFile = $_SERVER['DOCUMENT_ROOT'] . "/emosquescreen/other_components/Authentication/tempPass.txt";
	$tempSensorStatusFile = $_SERVER['DOCUMENT_ROOT'] . "/emosquescreen/temp.txt";

	$file = fopen($tempSensorStatusFile, 'r') or die("can't read temp status");
	$sensorStatus = false;
	if ($file) 
	{
		//echo "reading step sensor status";
		$line = fgets($file);
		//echo $line;
		if ($line == -99000)
		{
			$sensorStatus = true;
		}
		
	}
	$password = "";
	if ($sensorStatus)
	{
		$password = generateRandomString();
		if(saveNewPassword($password, $newPassFile))
		{
			echo $password;
		}
	}
	else
	{
		echo "UnAuthorized";
	}

    fclose($file); 


	function generateRandomString($length = 10) 
	{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
	}


	function saveNewPassword($text, $path)
	{
		$file = fopen($path, "w") or die("can't save to temp file");
		if($file)
		{
			if(fwrite($file, $text))
			{
				fclose($file);
				return true;
			}
			return false;
		}
		return false;
	}
?>
