import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class ScreenMonitor {
	public static void main(String[] args) {
		try {
			Runtime.getRuntime().exec("rm /etc/isStartUpScriptRunning");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		new CompareImages().run();
	}

}

class CompareImages extends Thread {
	public static String readFile(String path) {
		if (!(new File(path).exists()))
			return null;
		BufferedReader br = null;
		StringBuilder fileContent = new StringBuilder();
		try {
			br = new BufferedReader(new FileReader(path));

			String currentLine;
			while ((currentLine = br.readLine()) != null) {
				fileContent.append(currentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return fileContent.toString();
	}

	public void run() {
		while (true) {
			monitorScreen();
			try {
				sleep(30 * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	void monitorScreen() {
		Robot robot;
		try {
			robot = new Robot();
			BufferedImage screenShot1 = robot
					.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			sleep(10 * 1000);
			BufferedImage screenShot2 = robot
					.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			if (compareImages(screenShot1, screenShot2)) {
				Runtime r = Runtime.getRuntime();
				Process p;
				if (readFile("/var/www/emosquescreen/screenMode.js").trim().equals("isServer = true;")) {
					p = r.exec("pkill midori");
					p = r.exec("pkill iceweasel");
				} else
					p = r.exec("reboot");
				System.out.println("Same Images, System will reboot");
			} else {
				System.out.println("Different Images");
			}
		} catch (AWTException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	boolean compareImages(BufferedImage img1, BufferedImage img2) {
		if (img1.getWidth() == img2.getWidth() && img1.getHeight() == img2.getHeight()) {
			for (int x = 0; x < img1.getWidth(); x++) {
				for (int y = 0; y < img1.getHeight(); y++) {
					if (img1.getRGB(x, y) != img2.getRGB(x, y))
						return false;
				}
			}
		} else {
			return false;
		}
		return true;
	}

}
