/*
 * Created by Salah on 3/13/2016.
 */

function createMsgsTable() {
    var todayDate = new Date().addDays(-365);
    var toDate = todayDate.addDays(730);

    var timeDiff = toDate.getTime() - todayDate.getTime();
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    if (diffDays >= 0) {
        var datesArray = new Array();
        for (var i = 0; i <= diffDays; i++) {
            datesArray[i] = formatDate(todayDate.addDays(i));
        }
        callphp(datesArray);
    }
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

function formatDate(date) {
    var day = date.getDate();
    var month = (date.getMonth() + 1);
    var year = date.getFullYear();

    if (parseInt(day) < 10) {
        day = '0' + day;
    }

    if (parseInt(month) < 10) {
        month = '0' + month;
    }

    return day + '-' + month + '-' + year;
}

function deformatDate(date) {
    var parts = date.split('-');
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function callphp(dates) {
    getRequest(
        'php/getMsgs.php?dates=' + dates, // URL for the PHP file
        loadSuccessHandler,  // handle successful request
        loadFailureHandler    // handle error
    );
}

function loadSuccessHandler(responseText) {
    if (responseText === "") {
        //window.cpjs.sendToAndroid("لا يوجد أي رسائل.");
        alert("لا يوجد أي رسائل.");
    } else {
        var files = responseText.split(':');
        var msgsDates = files[0].split(';');
        var msgsContents = files[1].split(';');

        var msgsTable = document.getElementById("msgsTable");
        var yesterdayRow = null;

        for (j = 0; j < msgsDates.length - 1; j++) {
            var row = msgsTable.insertRow(j);
            var dateCell = row.insertCell(0);
            var contentCell = row.insertCell(1);
            var operationCell = row.insertCell(2);

            dateCell.innerHTML = '<span>' + msgsDates[j] + '</span>';
            contentCell.innerHTML = '<span>' + msgsContents[j] + '</span>';
            operationCell.innerHTML = '<div>' +
                '<input class="deleteCell" type="button" onclick="deleteMsg()" value="حذف"/>' +
                '<input class="editCell" type="button" onclick="editMsg()" value="تعديل"/>' +
                '</div>';

            dateCell.className = 'dateCell';
            contentCell.className = 'contentCell';

            if (msgsDates[j] == formatDate((new Date().addDays(-1))))
                yesterdayRow = row;
        }

        var firstRow = msgsTable.insertRow(0);
        var dateCellTitle = firstRow.insertCell(0);
        var contentCellTitle = firstRow.insertCell(1);

        dateCellTitle.innerHTML = '<span>' + "تاريخ الظهور" + '</span>';
        contentCellTitle.innerHTML = '<span>' + "نص الرسالة" + '</span>';

        dateCellTitle.className = 'dateCellTitle';
        contentCellTitle.className = 'contentCellTitle';

        if (yesterdayRow != null) {
            yesterdayRow.scrollIntoView();
            $(window).scrollTop($(window).scrollTop() - 5);
        }
    }
}

function deleteMsg() {
    $('#msgsTable').find('tr').unbind().click(function () {
        var msgDate = $(this).find('td').siblings().eq(0).text();
        var today = false;
        var todayDate = new Date();
        if (msgDate == formatDate(todayDate)) {
            today = true;
        }
        var all = false;
        var result = confirm("سوف يتم حذف الرسالة بتاريخ" + " " + msgDate + " هل تريد المتابعة؟");
        if (result == true) {
            phpDeleteMsg(msgDate, all, today);
        }
        else {
            //this is to remove the on click listener
            $('#msgsTable').find('tr').unbind().click(function () {
            });
        }
    });
}

function deleteAllMsg() {
    var msgDate = false;
    var all = true;
    var today = true;

    var result = confirm("سوف يتم حذف جميع الرسائل ، هل تريد المتابعة؟");
    if (result == true) {
        phpDeleteMsg(msgDate, all, today);
    }
    else {
        return;
    }
}

function editMsg() {
    $('#msgsTable').find('tr').click(function () {
        var msgDate = $(this).find('td').siblings().eq(0).text();
        var msgText = $(this).find('td').siblings().eq(1).text();
        goToWriteMsgsPage(msgDate, msgText)
    });
}

function loadFailureHandler(responseText) {
    //window.cpjs.sendToAndroid(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
    alert(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
}


function getRequest(url, success, error) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}

function goToWriteMsgsPage(dateCell, contentCell) {
    if (typeof(Storage) !== "undefined") {
        localStorage.setItem("dateCell", dateCell);
        localStorage.setItem("contentCell", contentCell);
    } else {
        //window.cpjs.sendToAndroid(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
        alert(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
    }
    location.replace("./secondaryMsg.html?pass=bahbishAdmin&page=2");
}

function goBack() {
    location.replace("./secondaryMsg.html?pass=bahbishAdmin&page=2");
}

function phpDeleteMsg(date, all, today) {
    getRequest(
        'php/deleteMsgs.php?date=' + date + '&all=' + all + '&today=' + today, // URL for the PHP file
        deleteSuccessHandler,  // handle successful request
        deleteFailureHandler    // handle error
    );
}

function deleteFailureHandler(responseText) {
    //window.cpjs.sendToAndroid(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
    alert(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
}

function deleteSuccessHandler(responseText) {
    location.reload();
}