var flag = 1;
var swichingState = 0;
var stopSwitching = false;
var currentlyHighlighted = -1;
var internalTempOld = 0;
var stuckCount = 0;


function startIqamaCounterF(){
var startIqamaCounter = setInterval(function() {
	var iqamaTime = document.getElementById("iqamaTimeToBlink");
    var iqamaBigCounter = document.getElementById("bigIqamaCounter");
    var bigIqamaText = document.getElementById("bigIqamaText");
	var iqma = true;
	var now = ((new Date().getHours() * 60 + new Date().getMinutes()) * 60 + new Date().getSeconds());
	var distance = currentPrayerIqamaTime - now;

	if(currentPrayerIqamaTime != null && distance >= 0){

        var min = ~~(distance / 60);
        var sec = distance % 60;
        if (min.toString().length == 1) {
            min = 0 + min.toString();
        }
        if (sec.toString().length == 1) {
            sec = 0 + sec.toString();
        }
	
	if(Number(distance) <= 61 ) {
			primary_CurrentState = Setting.PrimaryMessageStates.Hidden;
			iqma = false;
}
	if(Number(distance) < 60 ){
        showBigIqama();
        iqma = false;
		
	}
	if(Number(distance) > 0 ){
        iqamaBigCounter.innerHTML = sec;
        iqamaTime.innerHTML = min + ":" + sec;
        iqamaTime.style.visibility = "visible";
        iqma = false;
	}
	if (Number(distance) == 0) {
		iqma = false;
		iqamaBigCounter.innerHTML = sec;
        iqamaTime.innerHTML = min + ":" + sec;
        iqamaTime.style.visibility = "visible";

		clearInterval(startIqamaCounter);
        blinking(30);
	}else if(iqma){
		
			iqamaBigCounter.style.visibility = "hidden";
			iqamaTime.style.visibility = "hidden";
			bigIqamaText.style.visibility = "hidden";
		}
	
}
}, 1000);

}
state = 0;
function blinking(counter) {
    var iqamaTime = document.getElementById("iqamaTimeToBlink");
    var bigIqamaTime = document.getElementById("bigIqamaCounter");
        primary_CurrentState = Setting.PrimaryMessageStates.Hidden;

    counter = counter - 1;
    if (counter == 0) {
        removeIqama();
        hideBigIqama();
        secondary_CurrentState = Setting.SecondaryMessageStates.Hidden
        return;
    }
    else {
        blink();
    }

    function blink() {
        if (state == 0) {
            state = 1;
            iqamaTime.style.visibility = "hidden";
            bigIqamaTime.style.visibility = "hidden";
        }
        else {
            state = 0;
            iqamaTime.style.visibility = "visible";
            bigIqamaTime.style.visibility = "visible";
            
        }
        setTimeout(blinking, 1000, counter);
    }
}

function highLight(id) {


    if (currentlyHighlighted == id) {
        return;
    }
    currentlyHighlighted = id;
    var textIds = ["fajr", "thohor", "asr", "magrib", "ishaa", "shoroq"];
    var timeIds = ["fajrTime", "thohorTime", "asrTime", "magribTime", "ishaaTime", "shoroqTime"];
    var timeOutIds = ["fajrTimeOut", "thohorTimeOut", "asrTimeOut", "magribTimeOut", "ishaaTimeOut"];
    for (var i = 0; i < 6; i++) {
        if (i == id) {
            document.getElementById(textIds[i]).style.opacity = 1;
            document.getElementById(timeIds[i]).style.opacity = 1;
        }
        else {
            document.getElementById(textIds[i]).style.opacity = .6;
            document.getElementById(timeIds[i]).style.opacity = .6;
        }
    }
    if (stopSwitching == false) {
        switchingHandler = setInterval(timeOut_PrayerTime_Switching, 2000, id);
    }
}


function timeOut_PrayerTime_Switching(id) {


    var textIds = ["fajr", "thohor", "asr", "magrib", "ishaa", "shoroq"];
    var timeIds = ["fajrTime", "thohorTime", "asrTime", "magribTime", "ishaaTime", "shoroqTime"];
    var timeOutIds = ["fajrTimeOut", "thohorTimeOut", "asrTimeOut", "magribTimeOut", "ishaaTimeOut"];

    var textEl = document.getElementById(textIds[id]);
    var prayerTimeEl = document.getElementById(timeIds[id]);
    var prayerTimeoutEl = document.getElementById(timeOutIds[id]);


    if (stopSwitching) {

        textEl.style.visibility = "visible";
        prayerTimeEl.style.visibility = "visible";
        prayerTimeoutEl.style.visibility = "hidden";
        swichingState = 0;
        clearInterval(switchingHandler);
        return;
    }
    if (swichingState == 1) {
        swichingState = 0;
        var timeOutText = getTimeOutText(id);
        if (timeOutText == "") {
            return;
        }
        else {
            textEl.style.visibility = "hidden";
            prayerTimeEl.style.visibility = "hidden";
            prayerTimeoutEl.innerHTML = timeOutText
            prayerTimeoutEl.style.visibility = "visible";
        }

    }
    else {
        swichingState = 1;
        textEl.style.visibility = "visible";
        prayerTimeEl.style.visibility = "visible";
        prayerTimeoutEl.style.visibility = "hidden";
    }
}


function getTimeOutText(id) {

    var textEn ="In "
    var text = " بعد"

    var currentTimeInMinutes = (new Date().getHours() * 60 + new Date().getMinutes());
    var fajrAthanTimeInMinutes = (parseInt((prayerTime.fajr.substr(0, 2))) * 60 + parseInt((prayerTime.fajr.substr(3, 2))));
    var dhuhrAthanTimeInMinutes = (parseInt((prayerTime.dhuhr.substr(0, 2))) * 60 + parseInt((prayerTime.dhuhr.substr(3, 2))));
    var asrAthanTimeInMinutes = (parseInt((prayerTime.asr.substr(0, 2))) * 60 + parseInt((prayerTime.asr.substr(3, 2))));
    var maghribAthanTimeInMinutes = (parseInt((prayerTime.maghrib.substr(0, 2))) * 60 + parseInt((prayerTime.maghrib.substr(3, 2))));
    var ishaaAthanTimeInMinutes = (parseInt((prayerTime.isha.substr(0, 2))) * 60 + parseInt((prayerTime.isha.substr(3, 2))));

    var times = [fajrAthanTimeInMinutes, dhuhrAthanTimeInMinutes, asrAthanTimeInMinutes, maghribAthanTimeInMinutes, ishaaAthanTimeInMinutes]

    var diffInMin = times[id] - currentTimeInMinutes;
    if (diffInMin < 0) {
        diffInMin = 24 * 60 + diffInMin;
    }
    var hoursNumber = Math.floor(diffInMin / 60);
    var minNumber = Math.floor(diffInMin % 60);

    var ishour = false;

    var hoursNumberArabic = "";
    var hoursNumberString = hoursNumber.toString();

    switch (hoursNumber) {
        case 0:
        {
            ishour = false;
            text = text + ""
            textEn = textEn+ "";
            break;
        }
        case 1:
        {
            ishour = true;
            text = text + " ساعة"
            textEn = textEn + "1 Hr "
            break;
        }
        case 2:
        {
            ishour = true;
            text = text + " ساعتين"
            textEn = textEn + "2 Hr "
            break;
        }
        default:
        {
            ishour = true;
            for (var i = 0; i < hoursNumberString.length; i++) {
                hoursNumberArabic = hoursNumberArabic + replaceNumbers(hoursNumberString[i])
            }
            text = text + " " + hoursNumberArabic + " ساعات"
            textEn = textEn + hoursNumberString + " Hr "
            break;
        }
    }


    var minNumberArabic = "";
    if (ishour && minNumber > 0) {
        text = text + " و";
        textEn = textEn + " ";
    }
    var minNumberString = minNumber.toString();
    switch (minNumber) {
        case 0:
        {
            text = text + ""
            textEn = textEn + ""
            break;
        }
        default:
        {
            for (var i = 0; i < minNumberString.length; i++) {
                minNumberArabic = minNumberArabic + replaceNumbers(minNumberString[i])
            }
            if (ishour) {
                text = text + " " + minNumberArabic + " د";
                textEn = textEn + " " + minNumberString +" Min";

            }
            else {
                switch (minNumber) {
                    case 1:
                    {
                        text = text + " دقيقة";
                        textEn = textEn + "1 Minute";

                        break;

                    }
                    case 2:
                    {
                        text = text + " دقيقتين";
                        text = text + " 2 Minutes";
                        break;
                    }
                    default:
                    {
                        if (minNumber <= 10) {
                            text = text + " " + minNumberArabic + " دقائق";
                            textEn = textEn + " " + minNumberString +" Minutes";

                        }
                        else {
                            text = text + " " + minNumberArabic + " دقيقة";
                            textEn = textEn + " " + minNumberString +" Minutes";

                        }
                    }
                }
            }
            break;
        }
    }

    if (hoursNumber == 0 && minNumber == 0) {
        text = "";
    }
    if(screenSetting.language== Setting.Language.English)
    {
        return textEn;
    }else
    {
        return text;
    }
}

function removeHighlight(id) {

    var textIds = ["fajr", "thohor", "asr", "magrib", "ishaa", "shoroq"];
    var timeIds = ["fajrTime", "thohorTime", "asrTime", "magribTime", "ishaaTime", "shoroqTime"];
    var timeOutIds = ["fajrTimeOut", "thohorTimeOut", "asrTimeOut", "magribTimeOut", "ishaaTimeOut"];
    document.getElementById(timeOutIds[id]).style.visibility = "hidden";
    document.getElementById(textIds[id]).style.visiblity = "visible";
    document.getElementById(timeIds[id]).style.visiblity = "visible";
    for (var i = 0; i < 6; i++) {
        if (i == id)
            continue;
        else {
            document.getElementById(textIds[i]).style.opacity = 1;
            document.getElementById(timeIds[i]).style.opacity = 1;
        }
    }
    flag = 1;

    switch (id) {
        case 4:
        {
            setTimeout(highLight, 5 * 1000, 0);
            break;
        }
        default:
        {
            setTimeout(highLight, 5 * 1000, id + 1);
            break;

        }
    }


}


var headerState = 0;
function switchPageHeader() {
    var dateMeladi = document.getElementById("dateMeladi");
    //dateMeladi.style.visibility = "hidden";

    //var dateMiladiInnerHtmlString = '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><p id="" class="headerEl" style="width: inherit;text-align: right; display: table-cell; vertical-align: middle; line-height: 0;text-align:right; padding-right: ' + dateElementFontSize.miladiArabicWordMargin + '; font-size:' + dateElementFontSize.miladiArabicWord + ';height: ' + headerHeight + 'px;">ميلادي</p></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><p id="dateMeladi" class="headerEl" style="width: inherit;text-align: left; display: table-cell; vertical-align: middle; line-height: 1.2;height: ' + headerHeight + 'px;font-size:' + dateElementFontSize.headerMiladiArabicWord +'">'+ fullMeladiDate + '</p></div>';
    if (screenSetting.language == Setting.Language.English)
    {
        var dateMiladiInnerHtmlString = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><p id="dateMeladi" class="headerEl" style="width: inherit;text-align: center; display: block; vertical-align: middle; line-height: 1.2;height: ' + headerHeight + 'px;font-size:' + dateElementFontSize.meladiDate + '">' + fullMeladiDate + '</p></div>';
        var dateHijriInnerHtmlString = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:right;"><p id="dateMeladi" class="headerEl" style=";width=100%;float:right;height: ' + headerHeight + 'px;font-size:' + dateElementFontSize.headerhejriArabicWord + '">' + fullHijriDate + '</p></div>';

    }else
    {

        var dateHijriInnerHtmlString = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:right;"><p id="dateMeladi" class="headerEl" style=";width=100%;float:right;height: ' + headerHeight + 'px;font-size:' + dateElementFontSize.headerhejriArabicWord + '">' + fullHijriDate + '</p></div>';
        var dateMiladiInnerHtmlString = '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><p id="" class="headerEl" style="width: inherit;text-align: right; display: table-cell; vertical-align: middle; line-height: 0;text-align:right; padding-right: ' + '1px' + '; font-size:' + dateElementFontSize.miladiArabicWord + ';height: ' + headerHeight + 'px;">ميلادي</p></div><div class="col-lg-8 col-md-8 col-sm-8 col-xs-8"><p id="dateMeladi" class="headerEl" style="width: inherit;text-align: left; display: table-cell; vertical-align: middle; line-height: 1.2;height: ' + headerHeight + 'px;font-size:' + dateElementFontSize.meladiDate + '">' + fullMeladiDate + '</p></div>';

    }
    if (headerState == 0) {


        //dateMeladi.innerHTML = '<span style = font-size:' + dateElementFontSize.hijriDate+'>' + fullHijriDate + '</span>';
        document.getElementById("dateRow").innerHTML = dateHijriInnerHtmlString;

        //dateMeladi.innerHTML = "<span>" + fullHijriDate + "</span>";
        headerState = 1;
    }

    else if (headerState == 1) {

        //dateMeladi.innerHTML = '<span style = font-size:' + dateElementFontSize.meladiDate+'> م' + '</span>';

        //dateMeladi.innerHTML = '<span style = font-size:' + dateElementFontSize.meladiDate+'>' + fullMeladiDate + '</span>';

        document.getElementById("dateRow").innerHTML = dateMiladiInnerHtmlString;

        //dateMeladi.innerHTML = "<span>" + fullMeladiDate + "</span>";
        headerState = 0;
    }

    else if (headerState == 2) {

        if (internalTemp == -99000) {
            if (stuckCount == 4) {
                internalTemp = 0;
                internalTempOld = 0;
                stuckCount = 0;
            }
            else {
                internalTemp = internalTempOld;
                stuckCount++;
            }
        }
        else {
            internalTempOld = internalTemp;
        }

        document.getElementById("dateRow").innerHTML = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;"><p id="internalTemp" class="headerEl" style=";text-align=center;width=100%;height: ' + headerHeight + 'px;float:left;text-align:center;font-size:' + dateElementFontSize.tempFontSize + '">' + internalTemp + "&deg;C      درجة الحرارة" + '</p></div>';
        //dateMeladi.innerHTML = "<span id='externalTemp'>" + internalTemp + '</span>&deg;<span class="degreeSymbol"></span>' ;

        headerState = 0;
    }
    setTimeout(displayHeader, 300);
}


function refreshTemp() {
    var internalTempEl = document.getElementById("internalTemp");

    if (screenSetting.internalTempEnable == true && errorInTempFlag == false) {

        if (internalTemp == -99000) {
            if (stuckCount == 4) {
                internalTemp = 0;
                internalTempOld = 0;
                stuckCount = 0;
            }
            else {
                internalTemp = internalTempOld;
                stuckCount++;
            }
        }
        else {
            internalTempOld = internalTemp;
        }
        var internalTempText = internalTemp + ' C';
        internalTempEl.innerHTML = internalTempText;
    }
    else {
        internalTempEl.innerHTML = "";
    }


}

meladiDateEl = document.getElementById("dateMeladi");
function displayHeader() {
    meladiDateEl.style.visibility = "visible";
}

setInterval(refreshTemp, 5000);

setInterval(switchPageHeader, 5000);


