#!/bin/bash
sudo rm -r /home/pi/roll_back;
sudo mkdir -p /home/pi/roll_back;
sudo chmod 777 /home/pi/roll_back;
cd /home/pi/roll_back;

sudo git clone /etc/panel;
cd panel;

sudo git reset --hard $1;

sudo git push origin -u --all --force;
sudo reboot;