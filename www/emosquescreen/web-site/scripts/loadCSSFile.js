
function loadCSSFile()
{
    var screenWidth = screen.width;
    var screenHeight = screen.height;
    cssFile = document.createElement("link");
    if(screenSetting.language== Setting.Language.English)
    {
        cssFile.href = "css/CSS-files-per-resolution/en/"+screenWidth+"-"+screenHeight+".css";

    }else
    {
        cssFile.href = "css/CSS-files-per-resolution/"+screenWidth+"-"+screenHeight+".css";
    }
    cssFile.rel = "stylesheet";
    document.body.appendChild(cssFile);

}

function loadDefaultCSSFile()
{
    cssFile = document.createElement("link");
    cssFile.href = "css/"+"RelativeTextSize"+".css";
    cssFile.rel = "stylesheet";
    document.body.appendChild(cssFile); 
}

