<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 7/3/2017
 * Time: 1:33 PM
 */


$javaFile = file_get_contents("/home/pi/.eMosqueSystem/userSelectedMode.txt");
$settingsFile = file_get_contents("/var/www/emosquescreen/web-site/customFiles/settings/setting.js");


if($javaFile === false || $settingsFile === false)
{
    echo "";
    return;
}

$temp = explode("\n", $javaFile);
$temp = explode("=",$temp[0]);
$userMode = $temp[1];

$response = array();
$response["userMode"] = trim($userMode);
$response["screenSetting"] = $settingsFile;


echo json_encode($response);