
function createHadeethTableEn() {
    var todayDate = new Date().addDays(-365);
    var toDate = todayDate.addDays(730);

    var timeDiff = toDate.getTime() - todayDate.getTime();
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    if (diffDays >= 0) {
        var datesArray = new Array();
        for (var i = 0; i <= diffDays; i++) {
            datesArray[i] = formatDate(todayDate.addDays(i));
        }
        getHadeethFromServer(datesArray);
    }
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

function formatDate(date) {
    var day = date.getDate();
    var month = (date.getMonth() + 1);
    var year = date.getFullYear();

    if (parseInt(day) < 10) {
        day = '0' + day;
    }

    if (parseInt(month) < 10) {
        month = '0' + month;
    }

    return year + '-' + month + '-' + day;
}

function deformatDate(date) {
    var parts = date.split('-');
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function getHadeethFromServer(dates) {
    getRequest(
        'php/getHadeethList.php?dates=' + dates, // URL for the PHP file
        displayHadeethTable,  // handle successful request
        loadFailureHandler    // handle error
    );
}

function displayHadeethTable(responseText) {
    if (responseText === "") {
        //window.cpjs.sendToAndroid("لا يوجد أي رسائل.");
        alert("No Messages Found");
    } else {
        responseText = JSON.parse(responseText);
        //var files = responseText.dates;
        //var files = responseText.split(':');
        var msgsDates = responseText.dates;
        var msgsContents = responseText.text;

        console.log("dates = " + msgsDates);
        console.log("msgsContents = " + msgsContents);


        var msgsTable = document.getElementById("msgsTable");
        var yesterdayRow = null;

        for (j = 0; j < msgsDates.length; j++) {
            var row = msgsTable.insertRow(j);
            var dateCell = row.insertCell(0);
            var contentCell = row.insertCell(1);
            var operationCell = row.insertCell(2);

            dateCell.innerHTML = '<span>' + msgsDates[j] + '</span>';
            contentCell.innerHTML = '<span>' + msgsContents[j] + '</span>';
            operationCell.innerHTML = '<div>' +
                '<input class="deleteCell" type="button" onclick="deleteHadeeth()" value="delete"/>' +
                '<input class="editCell" type="button" onclick="editHadeeth()" value="edit"/>' +
                '</div>';

            dateCell.className = 'dateCell';
            contentCell.className = 'contentCell';

            if (msgsDates[j] == formatDate((new Date().addDays(-1))))
                yesterdayRow = row;
        }

        var firstRow = msgsTable.insertRow(0);
        var dateCellTitle = firstRow.insertCell(0);
        var contentCellTitle = firstRow.insertCell(1);

        dateCellTitle.innerHTML = '<span>' + "Displaying Date" + '</span>';
        contentCellTitle.innerHTML = '<span>' + "Message" + '</span>';

        dateCellTitle.className = 'dateCellTitle';
        contentCellTitle.className = 'contentCellTitle';

        if (yesterdayRow != null) {
            yesterdayRow.scrollIntoView();
            $(window).scrollTop($(window).scrollTop() - 5);
        }
    }
}

function deleteHadeeth() {
    $('#msgsTable').find('tr').unbind().click(function () {
        var msgDate = $(this).find('td').siblings().eq(0).text();
        var today = false;
        var todayDate = new Date();
        if (msgDate == formatDate(todayDate)) {
            today = true;
        }
        var all = false;
        var result = confirm("The message will be deleted by" + " " + msgDate + "Do you want to continue?");
        if (result == true) {
            console.log("accepted");
            phpDeleteHadeeth(msgDate, all, today);
        }
        else {
            //this is to remove the on click listener
            $('#msgsTable').find('tr').unbind().click(function () {
            });
        }
    });
}

function deleteAllHadeeth() {
    var msgDate = false;
    var all = true;
    var today = true;

    var result = confirm("All messages will be deleted, do you want to continue?");
    if (result == true) {
        phpDeleteMsg(msgDate, all, today);
    }
    else {
        return;
    }
}

function editHadeeth() {
    $('#msgsTable').find('tr').click(function () {
        var msgDate = $(this).find('td').siblings().eq(0).text();
        var msgText = $(this).find('td').siblings().eq(1).text();
        goToSelectHadeethPage(msgDate, msgText)
    });
}

function loadFailureHandler(responseText) {
    alert("Something wrong happened, please try again.");
}


function getRequest(url, success, error) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}

function goToSelectHadeethPage(dateCell, contentCell) {
    if (typeof(Storage) !== "undefined") {
        localStorage.setItem("dateCell", dateCell);
        localStorage.setItem("contentCell", contentCell);
    } else {
        //window.cpjs.sendToAndroid(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
        alert("Something wrong happened, please try again.");
    }
    location.replace("secondaryMsg-en.html?pass=bahbishAdmin&page=1");
}

function goBackFromHadeethTable() {
    location.replace("secondaryMsg-en.html?pass=bahbishAdmin&page=1");
}

function phpDeleteHadeeth(date, all, today) {
    console.log('php/deleteHadeeth.php?date=' + date + '&all=' + all + '&today=' + today);
    getRequest(
        'php/deleteHadeeth.php?date=' + date + '&all=' + all + '&today=' + today, // URL for the PHP file
        deleteSuccessHandler,  // handle successful request
        deleteFailureHandler    // handle error
    );
}

function deleteFailureHandler(responseText) {
    alert("Something wrong happened, please try again.");
}

function deleteSuccessHandler(responseText) {
    location.reload();
}