import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

public class SubmitClientInfo extends Thread
{

    int piMode;

    public SubmitClientInfo(int piMode)
    {
	this.piMode = piMode;
    }

    public void run()
    {
	int rand = (int) (Math.random() * 60);
	try
	{
	    sleep(rand * 1000);
	} catch (InterruptedException e)
	{
	    e.printStackTrace();
	}
	String BSSID = getEthBSSID2();
	String systemVersion = getSystemVersion();
	String contentVersion = getContentVersion();

	String url;
	if (piMode == Constants.isClient)
	{
	    url = Constants.Locations.clientPanelInfoHandler;
	} else
	{
	    url = Constants.Locations.serverPanelInfoHandler;
	}

	HttpGetRequset submitPanelInfo = new HttpGetRequset(url);
	submitPanelInfo.addParam("BSSID", BSSID);
	submitPanelInfo.addParam("systemVersion", systemVersion);
	submitPanelInfo.addParam("contentVersion", contentVersion);
	
	if (piMode == Constants.Modes.server)
	{
	    submitPanelInfo.addParam("panelMode", "server");

	} else
	{
	    submitPanelInfo.addParam("panelMode", "client");
	}
	int responseCode = submitPanelInfo.excuteRequest().responseCode;
	if (responseCode == 200)
	{

	} else
	{

	}
    }

    public String getEthBSSID2()
    {
	String BSSID = Utils.readFile(Constants.Locations.ethernetMacAddres);
	return BSSID.substring(0, BSSID.length() - 1);
    }

    public String getEthBSSID()
    {
	String BSSID = "";
	InetAddress ip;
	try
	{
	    ip = InetAddress.getLocalHost();
	    // System.out.println("Current IP address : " +
	    // ip.getHostAddress());
	    NetworkInterface network = NetworkInterface.getByInetAddress(ip);
	    byte[] mac = network.getHardwareAddress();
	    System.out.print("Current MAC address : ");
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < mac.length; i++)
	    {
		sb.append(String.format("%02X%s", mac[i],
			(i < mac.length - 1) ? "-" : ""));
	    }
	    // System.out.println(sb.toString());
	    BSSID = sb.toString();

	} catch (UnknownHostException e)
	{
	    e.printStackTrace();
	} catch (SocketException e)
	{
	    e.printStackTrace();
	}
	return BSSID;
    }

    public String getSystemVersion()
    {
	String systemVersion = Utils
		.readFile(Constants.Locations.systemVersionNumber);
	if (systemVersion != null)
	{
	    return systemVersion.substring(0, systemVersion.length() - 1);
	} else
	{
	    return "-1";
	}
    }

    public String getContentVersion()
    {
	String contentVersion = Utils
		.readFile(Constants.Locations.contentVersionNumber);
if(contentVersion == null)
{
	return "-1";
}
	return contentVersion.substring(0, contentVersion.length() - 1);
    }

}
