window.onload = scrollPage();
function scrollPage()
{
    setTimeout(pageScroll,3000);
}

function pageScroll() {
    
    if((window.innerHeight + window.scrollY) >= document.body.offsetHeight)
    {       
       setTimeout(scrollToTop,3000);       
       return;        
    }
    window.scrollBy(0,1);
    setTimeout(pageScroll,100);        
}

function scrollToTop()
{
    window.scrollTo(0,0);
    setTimeout(pageScroll,3000);
}