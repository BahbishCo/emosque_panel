﻿function replaceNumbers(num) {
    num = num.toString();
    num = num.replace("0", "\u0660");
    num = num.replace("1", "\u0661");
    num = num.replace("2", "\u0662");
    num = num.replace("3", "\u0663");
    num = num.replace("4", "\u0664");
    num = num.replace("5", "\u0665");
    num = num.replace("6", "\u0666");
    num = num.replace("7", "\u0667");
    num = num.replace("8", "\u0668");
    num = num.replace("9", "\u0669");
    return num;
}


function reverseString(input) {
    input = input.toString();
    var output = "";
    var i = 0;
    var length = input.length;
    for (i = length - 1; i >= 0; i--) {
        output = output + input[i];
    }
    return output;

}


function adjustDate() {


    var daysNames = new Array("الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت");

    var iMonthNames = new Array("محرم", "صفر", "ربيع الأول", "ربيع الثاني",
        "جمادة الأولى", "جمادة الأخرة", "رجب", "شعبان",
        "رمضان", "شوال", "ذو القعدة", "ذو الحجة");

    var daysNamesEnglish = new Array("Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat");

    var iMonthNamesEnglish = new Array("Muḥarram", "	Ṣafar", "Rabī‘ alawwal", "Rabī‘ ath-thānī",
        "Jumādá al-ūlá", "Jumādá al-ākhirah", "	Rajab", "Sha‘bān",
        "Ramaḍān", "	Shawwāl", "Dhū al-Qa‘dah", "Dhū al-Ḥijjah");


    if (runningMode != Setting.Mode.PI) {
        if (getParamValueByName("date") == -1) {
            var date = new Date();
        }
        else {
            var dateString = getParamValueByName("date");
            var year = dateString.split("-")[2];
            var month = parseInt(dateString.split("-")[1]) - 1;
            var day = parseInt(dateString.split("-")[0]);
            var date = new Date(year, month, day);
        }
    }
    else {
        var date = new Date();
    }


    var dateMeladi = document.getElementById("dateMeladi");
    var month = parseInt(date.getMonth()) + 1;

    meladiDay = date.getDate().toString();
    meladiMonth = month.toString();
    meladiYear = date.getFullYear().toString();
    var hijriDate = writeIslamicDate(screenSetting.hijriCorrection, date);
    var dayNo = hijriDate[5];
    var year = hijriDate[7];
    var month = hijriDate[6];
    hijriDate = document.getElementById("dateHijri");

    if (screenSetting.language == Setting.Language.English) {

        fullMeladiDate = meladiDay.concat(" - ", meladiMonth, " - ", meladiYear);
        var dateMiladiInnerHtmlString = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><p id="dateMeladi" class="headerEl" style="width: inherit;text-align: center; display: block; vertical-align: middle; line-height: 1.2;height: ' + headerHeight + 'px;font-size:' + dateElementFontSize.meladiDate + '">' + fullMeladiDate + '</p></div>';

        day = daysNamesEnglish[parseInt(date.getDay())];
        hijriDay = day.toString();
        hijriMonth = iMonthNamesEnglish[month];
        dayNo = dayNo.toString();
        year = year.toString();
        fullHijriDate = hijriDay + "," + (dayNo) + " " + hijriMonth + " " + (year[0]) + (year[1]) + (year[2]) + (year[3]);
        fullHijriDate = fullHijriDate + " Hijri";

    } else {
        day = daysNames[parseInt(date.getDay())];
        // var dayNo = hijriDate[5];
        // var year = hijriDate[7];
        // var month = hijriDate[6];

        hijriDay = day.toString();
        hijriMonth = iMonthNames[month];
        dayNo = dayNo.toString();
        year = year.toString();

        meladiYear = replaceNumbers(meladiYear[0]) + replaceNumbers(meladiYear[1]) + replaceNumbers(meladiYear[2]) + replaceNumbers(meladiYear[3]);
        if (meladiMonth.length == 1) {
            meladiMonth = replaceNumbers(meladiMonth[0]);
        }
        else {
            meladiMonth = replaceNumbers(meladiMonth[0]) + replaceNumbers(meladiMonth[1]);
        }

        if (meladiDay.length == 1) {
            meladiDay = replaceNumbers(meladiDay[0]);
        }
        else {
            meladiDay = replaceNumbers(meladiDay[0]) + replaceNumbers(meladiDay[1]);
        }
        fullMeladiDate = meladiDay.concat(" - ", meladiMonth, " - ", meladiYear);

        if (dayNo.length == 1) {
            fullHijriDate = hijriDay + "، " + replaceNumbers(dayNo) + " " + hijriMonth + " " + replaceNumbers(year[0]) + replaceNumbers(year[1]) + replaceNumbers(year[2]) + replaceNumbers(year[3]);
            //hijriDayNo = replaceNumbers(dayNo);
        }
        if (dayNo.length == 2) {
            fullHijriDate = hijriDay + "، " + replaceNumbers(dayNo[0]) + replaceNumbers(dayNo[1]) + " " + hijriMonth + " " + replaceNumbers(year[0]) + replaceNumbers(year[1]) + replaceNumbers(year[2]) + replaceNumbers(year[3]);
            //hijriDayNo = replaceNumbers(dayNo[0]) + replaceNumbers(dayNo[1]);
        }
        fullHijriDate = fullHijriDate + " هجري"


        var dateMiladiInnerHtmlString = '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><p id="" class="headerEl" style="width: inherit;text-align: right; display: table-cell; vertical-align: middle; line-height: 0;text-align:right; padding-right: ' + dateElementFontSize.miladiArabicWordMargin + '; font-size:' + dateElementFontSize.miladiArabicWord + ';height: ' + headerHeight + 'px;">ميلادي</p></div><div class="col-lg-8 col-md-8 col-sm-8 col-xs-8"><p id="dateMeladi" class="headerEl" style="width: inherit;text-align: left; display: table-cell; vertical-align: middle; line-height: 1.2;height: ' + headerHeight + 'px;font-size:' + dateElementFontSize.meladiDate + '">' + fullMeladiDate + '</p></div>';

    }


//var dateMiladiInnerHtmlString = '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><p id="" class="headerEl" style="text-align:right; margin-right: '+dateElementFontSize.miladiArabicWordMargin+'; font-size:'+dateElementFontSize.miladiArabicWord + '">ميلادي</p></div><div class="col-lg-7 col-md-7 col-sm-7 col-xs-7"><p id="dateMeladi" class="headerEl" style="text-align:left;font-size:'+dateElementFontSize.meladiDate+'">'+ fullMeladiDate+'</p></div>';


//dateMeladi.innerHTML = '<span style = font-size:' + dateElementFontSize.meladiDate+'>' + fullMeladiDate + '</span>';
    document.getElementById("dateRow").innerHTML = dateMiladiInnerHtmlString;

}










