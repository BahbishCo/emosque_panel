﻿
var fajrCorrectedTime = document.getElementById("fajrCorrectedTime");
var duhurCorrectedTime = document.getElementById("duhurCorrectedTime");
var asrCorrectedTime = document.getElementById("asrCorrectedTime");
var maghribCorrectedTime = document.getElementById("maghribCorrectedTime");
var ishaaCorrectedTime = document.getElementById("ishaaCorrectedTime");

var fajrCurrentTime = document.getElementById("fajrCurrentTime");
var duhurCurrentTime = document.getElementById("duhurCurrentTime");
var asrCurrentTime = document.getElementById("asrCurrentTime");
var maghribCurrentTime = document.getElementById("maghribCurrentTime");
var ishaaCurrentTime = document.getElementById("ishaaCurrentTime");

var fajrCurrentTime2 = document.getElementById("fajrCurrentTime2");
var duhurCurrentTime2 = document.getElementById("duhurCurrentTime2");
var asrCurrentTime2 = document.getElementById("asrCurrentTime2");
var maghribCurrentTime2 = document.getElementById("maghribCurrentTime2");
var ishaaCurrentTime2 = document.getElementById("ishaaCurrentTime2");


var fajrTimeCorrection = document.getElementById("fajrTimeCorrection");
var duhurTimeCorrection = document.getElementById("duhurTimeCorrection");
var asrTimeCorrection = document.getElementById("asrTimeCorrection");
var maghribTimeCorrection = document.getElementById("maghribTimeCorrection");
var ishaaTimeCorrection = document.getElementById("ishaaTimeCorrection");


var timeZoneSelector = document.getElementById("timeZoneSelector");
var defaultPrayerSettingCheckBox = document.getElementById("loadDefaultPrayerSettingCheckBox");
var highLatSelector = document.getElementById("highLatSelector");
var asrJuristicSelector = document.getElementById("asrJuristicSelector");
var calMethodeSelector = document.getElementById("calMethodeSelector");
var midnightMethod = document.getElementById("midnightMethod");
var ishaaAngleSelector = document.getElementById("ishaaAngleSelector");
var fajrAngleSelector = document.getElementById("fajrAngleSelector");

adjustPrayers();

initPrayerCorrectionSelectors(-15, 15);
adjustPrayers();
attachListeners();


function adjustPrayers()
{

    var countrySelector = document.getElementById("countrySelector");
    var selectedCountry = countrySelector.value;

    var citySelector = document.getElementById("citySelector");
    var selectedCity = citySelector.value;

    var miladiDaySelector = document.getElementById("miladiDay");
    var miladiMonthSelector = document.getElementById("miladiMonth");
    var miladiYearSelector = document.getElementById("miladiYear");

    if (defaultPrayerSettingCheckBox.checked == true)
    {
        var latitude = countries[selectedCountry][selectedCity].latitude;
        var longitude = countries[selectedCountry][selectedCity].longitude;
        var elevation = countries[selectedCountry][selectedCity].elevation;
        var timeZone = timeZoneSelector.value;
        var calculationMethod = countries[selectedCountry][selectedCity].prayerCalcMethode;
        var asrJuristic = countries[selectedCountry][selectedCity].asrJuristic;
        var adjustHighLats = countries[selectedCountry][selectedCity].adjustHighLats
        var ishaaAngle = countries[selectedCountry][selectedCity].ishaaAngle
        var fajrAngle = countries[selectedCountry][selectedCity].fajrAngle
        var prayersOffset = countries[selectedCountry][selectedCity].offset
    }
    else
    {
        var latitude = countries[selectedCountry][selectedCity].latitude;
        var longitude = countries[selectedCountry][selectedCity].longitude;
        var elevation = countries[selectedCountry][selectedCity].elevation;
        var timeZone = timeZoneSelector.value;
        var calculationMethod = calMethodeSelector.value;
        var asrJuristic = asrJuristicSelector.value;
        var adjustHighLats = highLatSelector.value;
        var ishaaAngle = countries[selectedCountry][selectedCity].ishaaAngle
        var fajrAngle = countries[selectedCountry][selectedCity].fajrAngle
        var prayersOffset = countries[selectedCountry][selectedCity].offset

    }
    var date = new Date();
    prayTimes.setMethod(calculationMethod);
    prayTimes.adjust({ asr: asrJuristic, highLats: adjustHighLats, fajr: fajrAngle, isha: ishaaAngle });
    //prayTimes.tune(prayersOffset);

    var times = prayTimes.getTimes(date, [latitude, longitude, elevation], timeZone, 0, '12h');


    fajrCurrentTime.innerHTML = fixTime(times.fajr);
    duhurCurrentTime.innerHTML = fixTime(times.dhuhr);
    asrCurrentTime.innerHTML = fixTime(times.asr);
    maghribCurrentTime.innerHTML = fixTime(times.maghrib);
    ishaaCurrentTime.innerHTML = fixTime(times.isha);


    fajrCurrentTime2.innerHTML = fixTime(times.fajr);
    duhurCurrentTime2.innerHTML = fixTime(times.dhuhr);
    asrCurrentTime2.innerHTML = fixTime(times.asr);
    maghribCurrentTime2.innerHTML = fixTime(times.maghrib);
    ishaaCurrentTime2.innerHTML = fixTime(times.isha);


    var fajrCorrectionValue = fajrTimeCorrection.value;
    var duhurCorrectionValue = duhurTimeCorrection.value;
    var asrCorrectionValue = asrTimeCorrection.value;
    var maghribCorrectionValue = maghribTimeCorrection.value;
    var ishaaCorrectionValue = ishaaTimeCorrection.value;

    fajrCorrectedTime.innerHTML = computeCorrectedTime(fixTime(times.fajr), fajrCorrectionValue);
    duhurCorrectedTime.innerHTML = computeCorrectedTime(fixTime(times.dhuhr), duhurCorrectionValue);
    asrCorrectedTime.innerHTML = computeCorrectedTime(fixTime(times.asr), asrCorrectionValue);
    maghribCorrectedTime.innerHTML = computeCorrectedTime(fixTime(times.maghrib), maghribCorrectionValue);
    ishaaCorrectedTime.innerHTML = computeCorrectedTime(fixTime(times.isha), ishaaCorrectionValue);


}


function ignorePX(input)
{
    switch (input.length)
    {
        case 3:
            return input.substr(0, 1);
        case 4:
            return input.substr(0, 2);
        case 5:
            return input.substr(0, 3);
    }
}

function fixTime(time)
{
    var output;
    if (time.length == 6)
    {
        output = "0" + time[0] + ":0" + time[2];
    }
    else if (time.length == 7)
    {
        indexOfColon = time.indexOf(":");
        indexOfSpace = time.indexOf(" ");
        output = "" + time.substr(0, indexOfColon);
        output = "0" + output + ":" + time.substr(indexOfColon + 1, indexOfSpace - indexOfColon - 1);
    }
    else
    {
        output = time[0] + time[1] + ":" + time[3] + time[4];
    }
    return output;
}


function initPrayerCorrectionSelectors(minTime, maxTime)
{
    for (var i = minTime; i <= maxTime; i++)
    {
        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        if (i == 0)
        {
            option.selected = "true"
        }
        fajrTimeCorrection.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        if (i == 0)
        {
            option.selected = "true"
        }
        duhurTimeCorrection.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        if (i == 0)
        {
            option.selected = "true"
        }
        asrTimeCorrection.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        if (i == 0)
        {
            option.selected = "true"
        }
        maghribTimeCorrection.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        if (i == 0)
        {
            option.selected = "true"
        }
        ishaaTimeCorrection.add(option);
    }
}

function computeCorrectedTime(time, correctionValue)
{
    var hour = parseInt(time.substr(0, 2));
    var min = parseInt(time.substr(3, 2));
    correctionValue = parseInt(correctionValue)
    if (min + correctionValue >= 60)
    {
        min = (min + correctionValue) % 60;
        if (hour + 1 > 12)
        {
            hour = (hour + 1) % 13;
        }
        else
        {
            hour = hour + 1;
        }
    }
    else if (min + correctionValue < 0)
    {
        min = 60 - Math.abs(min + correctionValue);
        if (hour - 1 < 0)
        {
            hour = 12
        }
        else
        {
            hour = hour - 1;
        }
    }
    else
    {
        min = min + correctionValue;
    }

    if (hour < 10)
    {
        hour = "0" + hour;
    }
    if (min < 10)
    {
        min = "0" + min;
    }

    var output = hour + ":" + min;

    return output;

}


function attachListeners()
{
    if (fajrTimeCorrection.addEventListener)
    {
        fajrTimeCorrection.addEventListener('change', adjustPrayers, false);
        duhurTimeCorrection.addEventListener('change', adjustPrayers, false);
        asrTimeCorrection.addEventListener('change', adjustPrayers, false);
        maghribTimeCorrection.addEventListener('change', adjustPrayers, false);
        ishaaTimeCorrection.addEventListener('change', adjustPrayers, false);


        timeZoneSelector.addEventListener('change', adjustPrayers, false);
        highLatSelector.addEventListener('change', adjustPrayers, false);
        asrJuristicSelector.addEventListener('change', adjustPrayers, false);
        calMethodeSelector.addEventListener('change', adjustPrayers, false);
        midnightMethod.addEventListener('change', adjustPrayers, false);
        ishaaAngleSelector.addEventListener('change', adjustPrayers, false);
        fajrAngleSelector.addEventListener('change', adjustPrayers, false);
        

    }
    else
    {
        //IE 
        fajrTimeCorrection.attachEvent('onchange', adjustPrayers, false);
        duhurTimeCorrection.attachEvent('onchange', adjustPrayers, false);
        asrTimeCorrection.attachEvent('onchange', adjustPrayers, false);
        maghribTimeCorrection.addEventListener('change', adjustPrayers, false);
        ishaaTimeCorrection.addEventListener('change', adjustPrayers, false);

    }
}
