/*
    This CSS file is used to set the font size of all elements for different resolutions

*/


/**************************************/
/*           (1024 * 768)             */
/**************************************/

dateElementFontSize = {
	meladiDate: "80px",
	hijriDate: "48px",
	miladiArabicWord: "50px",
	miladiArabicWordMargin: "30px",
  headerMiladiArabicWord: "66px",
        headerHijriArabicWord: "47px",
        tempFontSize: "50px"
}



