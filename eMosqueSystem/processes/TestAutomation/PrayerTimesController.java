
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Ahmad on 8/10/2015.
 */
public class PrayerTimesController {

    double latitude;
    double longitude;
    int timeZone;
    int[] offset;
    PrayerTimes prayerTimes;
    int timeFormat;
    int method;
    int asrJuristic;
    int adjustHighLats;
    double elevation;
    float fajrAngle,ishaaAngle,ishaaMinutes;
    int midNightMethod;
    public PrayerTimesController(double latitude, double longitude, double elevation, int timeZone, int[] offset,
                                 int timeFormat, int method, int asrJuristic, int adjustHighLats,
                                 float fajrAngle, float ishaaAngle, float ishaaMinutes, int midNightMethod
                                 ) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timeZone = timeZone;
        this.offset = offset;
        this.timeFormat = timeFormat;
        this.method = method;
        this.asrJuristic = asrJuristic;
        this.adjustHighLats = adjustHighLats;
        this.elevation = elevation;
        this.fajrAngle = fajrAngle;
        this.ishaaAngle = ishaaAngle;
        this.ishaaMinutes = ishaaMinutes;
        this.midNightMethod = midNightMethod;
        prayerTimes = new PrayerTimes();
    }

    public ArrayList<String> getPrayerTimes() {
        ArrayList<String> times;
        prayerTimes.setTimeFormat(timeFormat);
        prayerTimes.setCalcMethod(method);
        prayerTimes.setAsrJuristic(asrJuristic);
        prayerTimes.setAdjustHighLats(adjustHighLats);
        prayerTimes.setElevation(elevation);
        //prayerTimes.setFajrAngle(18.5);
        //prayerTimes.setIshaAngle(18);
        //prayerTimes.tune(offset);
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        times = prayerTimes.getPrayerTimes(cal, latitude, longitude, timeZone);
        return times;

    }

    public void displayPrayerTimes(ArrayList<String> prayerTimes) {
//        Log.d("appLog", "fajr : " + prayerTimes.get(0));
//        Log.d("appLog", "sunrise : " + prayerTimes.get(1));
//        Log.d("appLog", "duhur : " + prayerTimes.get(2));
//        Log.d("appLog", "asr : " + prayerTimes.get(3));
//        Log.d("appLog", "sunset : " + prayerTimes.get(4));
//        Log.d("appLog", "magrib : " + prayerTimes.get(5));
//        Log.d("appLog", "ishaa : " + prayerTimes.get(6));
//        Log.d("appLog", "===================================================================");

    }

    public PrayerTimesInMin getPrayerTimesInMin(ArrayList<String> prayerTimes) {
        PrayerTimesInMin times = new PrayerTimesInMin();
        times.FAJR = getTimeInMin(prayerTimes.get(0));
        times.SHOROQ = getTimeInMin(prayerTimes.get(1));
        times.DUHUR = getTimeInMin(prayerTimes.get(2));
        times.ASR = getTimeInMin(prayerTimes.get(3));
        times.MAGHRIB = getTimeInMin(prayerTimes.get(5));
        times.ISHAA = getTimeInMin(prayerTimes.get(6));
        return times;
    }

    public int getTimeInMin(String time) {
        int hour = Integer.parseInt(time.substring(0, 2));
        int min = Integer.parseInt(time.substring(3, 5));
        return hour * 60 + min;
    }

    public class PrayerTimesInMin {
        public int FAJR;
        public int SHOROQ;
        public int DUHUR;
        public int ASR;
        public int MAGHRIB;
        public int ISHAA;
    }

}
