<html>
<body>
<table>
    <?php
    foreach ($_POST as $key => $value) {
        echo "<tr>";
        echo "<td>";
        echo $key;
        echo "</td>";
        echo "<td>";
        echo $value;
        echo "</td>";
        echo "</tr>";
    }
    ?>
</table>


<?php
if ($_POST["prayerCalMethod"] == null) {
    echo("<script> " . "alert(\"Some Information Is Missing!!, Please fill the form again and re-submit it.\")" . "</script>");
    return;
}
//echo ($_SERVER['DOCUMENT_ROOT'] . "/emosquescreen/web-site/JSON/setting.js");
$file = fopen($_SERVER['DOCUMENT_ROOT'] . "/emosquescreen/web-site/customFiles/settings/setting.js", 'w') or die("can't open file");
//$file = fopen($_SERVER['DOCUMENT_ROOT'] . "/emosquescreen/www/www/emosquescreen/web-site/JSON/setting.js", 'w') or die("can't open file");
//$file = fopen("/var/www/emosquescreen/emosqueLocal/JSON/location.js", 'w') or die("can't open file");


$stringData = "var screenSetting = \n" .
    "{\n" .
    "prayerSetting: \n" .
    "{\n" .
    "country: " . "\"" . $_POST["countryName"] . "\"" . ",\n" .
    "city: " . "\"" . $_POST["cityName"] . "\"" . ",\n" .
    "calculationMethod: " . "\"" . $_POST["prayerCalMethod"] . "\"" . ",\n" .
    "asrJuristic: " . "\"" . $_POST["asrJuristicParam"] . "\"" . ",\n" .
    "adjusthighLat: " . "\"" . $_POST["highLatParam"] . "\"" . ",\n" .
    "midNightMethod: " . "\"" . $_POST["midNightMethod"] . "\"" . ",\n" .
    "fajrAngle: " . $_POST["fajrAngle"] . ",\n" .
    "ishaAngle: " . $_POST["ishaAngle"] . ",\n" .
    "latitude: " . $_POST["latitude"] . ",\n" .
    "longitude: " . $_POST["longitude"] . ",\n" .
    "elevation: " . $_POST["elevation"] . ",\n" .
    "timeZone: " . $_POST["timeZone"] . ",\n" .
    "offset: {" . "fajr: " . $_POST["fajrTimeCorrection"] . ", " .
    "dhuhr: " . $_POST["duhurTimeCorrection"] . ", " .
    "asr: " . $_POST["asrTimeCorrection"] . ", " .
    "maghrib: " . $_POST["maghribTimeCorrection"] . ", " .
    "isha: " . $_POST["ishaaTimeCorrection"] . ", " .
    "},\n" .
    "},\n" .

    "hijriCorrection: " . $_POST["hijriCorrection"] . ",\n" .

    "timeBetweenAzanAndIqama: {" . "fajr: " . $_POST["fajrIqamaTime"] . ", " .
    "duhur: " . $_POST["duhurIqamaTime"] . ", " .
    "asr: " . $_POST["asrIqamaTime"] . ", " .
    "maghrib: " . $_POST["maghribIqamaTime"] . ", " .
    "ishaa: " . $_POST["ishaaIqamaTime"] . ", " .
    "jumaa: " . $_POST["jumaaIqamaTime"] . ", " .
    "},\n" .

    "timeBetweenAzanAndAzkar: {" . "fajr: " . $_POST["beforFajrAzkarPeriod"] . ", " .
    "duhur: " . $_POST["beforduhurAzkarPeriod"] . ", " .
    "asr: " . $_POST["beforAsrAzkarPeriod"] . ", " .
    "maghrib: " . $_POST["beforMaghribAzkarPeriod"] . ", " .
    "ishaa: " . $_POST["beforIshaaAzkarPeriod"] . ", " .
    "jumaa: " . $_POST["beforJumaaAzkarPeriod"] . ", " .
    "},\n" .

    "azkarDisplayPeriod: {" . "fajr: " . $_POST["fajrAzkarDisplayPeriod"] . ", " .
    "duhur: " . $_POST["duhurAzkarDisplayPeriod"] . ", " .
    "asr: " . $_POST["asrAzkarDisplayPeriod"] . ", " .
    "maghrib: " . $_POST["maghribAzkarDisplayPeriod"] . ", " .
    "ishaa: " . $_POST["ishaaAzkarDisplayPeriod"] . ", " .
    "jumaa: " . $_POST["jumaaAzkarDisplayPeriod"] . ", " .
    "},\n" .

    "appQREnable: " . $_POST["showQRCodeCheckbox"] . ",\n" .
    "internalTempEnable: " . $_POST["showIntenalTempCheckbox"] . ",\n" .
    "playAzan: " . $_POST["playAzan"] . ",\n" .
//    "showAnalogClock: " . $_POST["displayAnalogClock"] . ",\n" .
    "showBahbishMessage: " . $_POST["displayBahbishMessage"] . ",\n" .
    "displayAdAfterPrayer: " . $_POST["displayAdAtPrayerTime"] . ",\n" .
    "language: \"" . $_POST["langSelector"] . "\",\n" .
    "}";


fwrite($file, $stringData);
fclose($file);


$changeDateCmd = "sudo date -s \"" .
    //$_POST["miladiDayName"] . ", " .
    $_POST["miladiDay"] . " " .
    $_POST["miladiMonth"] . " " .
    $_POST["miladiYear"] . " " .
    $_POST["hour"] . ":" . $_POST["min"] . ":00" . "\"";
//$_POST["timeZone"] . " " .
//"+0" . " " .


//echo "<br>" . $changeDateCmd . "</br>";

$syncTimeWithClockCmd = "sudo hwclock -w";
$rebootCmd = "sudo reboot";
$killMidori = "sudo pkill midori";
$killFirefox = "sudo pkill iceweasel";

if ($_POST["miladiDateCorrection"] == "true") {
    exec($changeDateCmd, $x, $y);
    if ($y == 0) {
        echo("new date is :" . $x[0]);
        exec($syncTimeWithClockCmd, $x, $w);
        if ($w == 0) {
            echo("<br> clock is updated </br>");
        }
    }
}
updateSettingVersion();
exec($killMidori, $x, $y);
exec($killFirefox, $x, $y);


function updateSettingVersion()
{
    $settingVersionFilePath = $_SERVER['DOCUMENT_ROOT'] . "/emosquescreen/version/settings.txt";
    $settingFile = fopen($settingVersionFilePath, r);
    if ($settingFile) {
        $line = fgets($settingFile);
        $settingVersionNumber = intval($line);
        $settingVersionNumber = $settingVersionNumber + 1;
        if ($settingVersionNumber != 0) {
            fclose($settingFile);
            $settingFile = fopen($settingVersionFilePath, 'w');
            fwrite($settingFile, $settingVersionNumber);
        }
        fclose($settingFile);
    } else {
        echo 'Unable To Open Setting Version File';
    }
}

function curPageURL()
{
    $pageURL = 'http';
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

?>

</body>
</html>
