import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TempReader extends Thread {
    static String readingPATH = "/home/pi/.eMosqueSystem/processes/temp-reader/";
    static String readingFile = "reading";
    static String writingPATH = "/var/www/emosquescreen/";
    static String writingFile = "temp.txt";
    static PrintWriter writer;
    static FileReader fr;
    static BufferedReader textReader;
    static String text;
    static Pattern r;
    static Matcher m;
    private static int counter;

    public TempReader() {
    }

    public static void main(String[] var0) {
        
    	new ResetUsbPort().start();
    	new File(readingPATH);
        r = Pattern.compile("F[\\s]([0-9|\\.|-]{0,6})C");
	String var2;
        String var3;
        String[] var4;
        while (true) {
            try {
            	
            	
            	var2 = ReadTempSensorUsingShell();
            	
                //fr = new FileReader(readingPATH + readingFile);
                //textReader = new BufferedReader(fr);

                //for (text = ""; (var2 = textReader.readLine()) != null; text = text + var2) {
                //    ;
                //}

                //fr.close();
                //textReader.close();
		text = var2;                
		m = r.matcher(text);
                if (m.find()) {
                    var3 = m.group(1);
                    var4 = var3.split("\\.");
                    var3 = var4[0] + var4[1] + "0";
                    counter = 0;
                } else {
                    var3 = "-99000";
                    System.out.println("did not read the temperature!");
                }

//                System.out.println("=======");
 //               System.out.println(var3);
  //              System.out.println("=======");

                if (var3!=null && !(var3.equals("-99000") && ++counter < 3)) {
                    writer = new PrintWriter(writingPATH + writingFile, "UTF-8");
                    writer.println(var3);
                    writer.close();
                }else{
                System.out.println("skipped this reading."+ counter);
}
            } catch (FileNotFoundException var6) {
                System.out.println("file is not found! make sure reading and writing files are correclty addressed");
            } catch (IOException var7) {
            }

            try {
                sleep(10000);
            } catch (InterruptedException var5) {
            }
        }
    }

	private static String ReadTempSensorUsingShell() {
		Process process = CMDHelper.execCmdWithoutBlockExec(Commands.readTempCmd);
		String procOut =  CMDHelper.readProcessOutput(process);
		System.out.println("***************Reading Temp****************");
		System.out.println(procOut);
		System.out.println("********************************************");
		return procOut;	
	}
}



class ResetUsbPort extends Thread{
	
	public void run()
	{
		Process process;
		String procOut;
		long sleepFor = 10 * 60 * 1000;
		String sensorId="";
		String temp[];
		while(true)
		{
			try
			{
				System.out.println("***************Reset Port****************");
				System.out.println(Commands.runResetPortProgram);
				process = CMDHelper.execCmdWithoutBlockExec(Commands.runResetPortProgram);				
				procOut =  CMDHelper.readProcessOutput(process);				
				System.out.println(procOut);
				System.out.println("********************************************");
				sleep(sleepFor);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}


class Commands{
	public static String listTempSensor = "lsusb";
	public static String runResetPortProgram = "sudo ./usb-reset.sh";
	public static String readTempCmd = "sudo /home/pi/.eMosqueSystem/processes/temp-reader/temperv14";

}


