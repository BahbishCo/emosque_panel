var height;
var width;
var hadeethBox;
var title;
var dateSelector;
var saveButton;
var arrowRight;
var arrowLeft;
var id;
var fileName = "";

var saveHadeethPHP = "php/saveHadeeth.php";

var hadeethArray = [];
var wordsCount = [];
var highLight = false;
var isSearching = false;
var hadeethTab;
var imamMsgTab;
var hadeethTabRef;
var imamMsgTabRef;


var ids = [];

$(document).ready(function () {

    hadeethTab = $('#hadeethSelectionTab');
    imamMsgTab = $('#imamMessages');
    hadeethTabRef = $('#hadeethSelectionRef');
    imamMsgTabRef = $('#imamMessagesRef');


    var pageToOpen = getParamValueByName("page");
    if (pageToOpen == null) {
        pageToOpen = 1;
    }

    if (pageToOpen == 1) {
        imamMsgTab.removeClass("in");
        imamMsgTab.removeClass("active");
        imamMsgTabRef.removeClass("active");
        hadeethTab.addClass("in");
        hadeethTab.addClass("active");
        hadeethTabRef.addClass("active");
    }
    else {
        hadeethTab.removeClass("in");
        hadeethTab.removeClass("active");
        hadeethTabRef.removeClass("active");
        imamMsgTab.addClass("in");
        imamMsgTab.addClass("active");
        imamMsgTabRef.addClass("active");
    }
    initHadeethPage();
    initSecondaryMsgPage();
    loadContent(pageToOpen);

});


function loadContent(pageToOpen) {

    var dateCell = localStorage.getItem("dateCell");
    var contentCell = localStorage.getItem("contentCell");
    console.log("dateCell = " + dateCell);
    console.log("contentCell = " + contentCell);

    if (pageToOpen == 1) {
        if ((typeof dateCell !== 'undefined' && dateCell !== null) && (typeof contentCell !== 'undefined' && contentCell !== null)) {
            {
                document.getElementById('daySelection').value = dateCell;
                document.getElementById('hadeethDisplayWindow').innerHTML = contentCell;
                localStorage.clear();
            }
        }
        else {
            nextHadeeth();
        }
    }
    else {
        if ((typeof dateCell !== 'undefined' && dateCell !== null) && (typeof contentCell !== 'undefined' && contentCell !== null)) {
            {
                //document.getElementById('inputText').value = localStorage.getItem("contentCell");
                //document.getElementById('startDatePicker').value = dateCell;
                //document.getElementById('endDatePicker').value = dateCell;
                //localStorage.clear();
            }
        }
    }


}

function initHadeethPage() {
    height = window.innerHeight;
    width = window.innerWidth;
    hadeethBox = document.getElementById("hadeethDisplayWindow");
    title = document.getElementById("title");
    dateSelector = document.getElementById("daySelection");
    saveButton = document.getElementById("saveButton");
    arrowRight = document.getElementById("rightArrow");
    arrowLeft = document.getElementById("leftArrow");
    initDateEl();
    setElSizes();
    if (document.title == "Messages") {
        id = parseInt(Math.random() * (hadeethGroupEn.length - 2));
        hadeethArray = hadeethGroupEn;

        $("#searchBox").keyup(function () {
            while (isSearching) {

            }
            searchForHadeeth();
        });

        $("#daySelection").change(function () {
            fileName = dateSelector.value;
        });
    }
    else {


        id = parseInt(Math.random() * (hadeethGroup.length - 2));
        hadeethArray = hadeethGroup;

        $("#searchBox").keyup(function () {
            while (isSearching) {

            }
            searchForHadeeth();
        });

        $("#daySelection").change(function () {
            fileName = dateSelector.value;
        });

    }


}


function initSecondaryMsgPage() {

    var todayDate = new Date();
    document.getElementById('startDatePicker').value = formatDate(todayDate);
    document.getElementById('endDatePicker').value = formatDate(todayDate);

    var dateCell = localStorage.getItem("dateCell");
    if (typeof dateCell !== 'undefined' && dateCell !== null) {
        document.getElementById('inputText').value = localStorage.getItem("contentCell");

        var timeDiff = deformatDate(localStorage.getItem("dateCell")) - todayDate;
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays >= 0) {
            document.getElementById('startDatePicker').value = dateCell;
            document.getElementById('endDatePicker').value = dateCell;
            localStorage.clear();
        }
    } else {
        if (document.getElementById('endDatePicker').value == formatDate(todayDate)) {
            phpLoadTodayMsg(document.getElementById('endDatePicker').value);
        }
    }

    $(function () {
        $("#startDatePicker").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date(),
            maxDate: '+1Y',
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });
        $("#endDatePicker").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date(),
            maxDate: '+1Y',
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });
    });

    $('#inputText').on('input', function () {
        document.getElementById('marqueeText').innerHTML = document.getElementById('inputText').value;
    });

    hasFocus = false;


}

function initDateEl() {
    $("#daySelection").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(),
        maxDate: '+2Y',
        onSelect: function (d, i) {
            if (d !== i.lastVal) {
                $(this).change();
            }
        }
    });


}

function setElSizes() {
    var hadeethBoxheight = height * .70 - title.clientHeight - dateSelector.clientHeight - saveButton.clientHeight - saveButton.clientHeight;
    hadeethBoxheight = hadeethBoxheight - arrowLeft.clientHeight;
    hadeethBox.style.height = hadeethBoxheight + "px";
    //
    //arrowLeft.style.top = hadeethBoxheight * 0.8 / 2 + "px";
    //arrowLeft.style.left = 0 + "px";
    //
    //arrowRight.style.top = hadeethBoxheight * 0.8 / 2 + "px";
    //arrowRight.style.left = 0 + "px";


}


function searchForHadeeth() {
    isSearching = true;


    if(document.title == "Messages")
    {
        var hadeethGroup = hadeethGroupEn;
    }else
    {
        var hadeethGroup = hadeethGroup;
    }

    var searchForText = document.getElementById("searchBox").value;
    searchForText = searchForText.trim().replace(/[^(\u0600-\u06FF|\u0020-\u007F|\u003A|\u060c|\u061f)|\s]*/g, '');
    if (searchForText == "") {
        highLight = false;
        id = parseInt(Math.random() * (hadeethGroup.length - 2));
        nextHadeeth();
        isSearching = false;
        return;
    }


    if (searchForText.length < 3) {
        highLight = false;
        id = parseInt(Math.random() * (hadeethGroup.length - 2));
        nextHadeeth();
        isSearching = false;
        return;
    }
    hadeethArray = hadeethGroup;

    var wholeString = [];
    wordsCount = [];
    ids = [];
    for (var i = 0; i < hadeethGroup.length; i++) {
        var regex = /[^(\u0600-\u06FF|\u0020-\u007F|\u003A|\u060c|\u061f)|\s]*/g;
        var hadeeth = hadeethGroup[i].content.trim().replace(regex, '');

        var isFound = hadeeth.search(searchForText);
        if (isFound != -1) {
            // console.log("**************************************************");
            var numberOfWords = hadeeth.trim().substr(0, isFound - 1).split(/\s+/g).length;
            wholeString.push(hadeethGroup[i]);
            wordsCount.push(numberOfWords);
            ids.push(i);
            // console.log("before = " + hadeethGroup[i].content);
            // console.log("after  = " + hadeeth);
            // console.log("index  = " + isFound);
            // console.log("numberOfWords  = " + numberOfWords);
            // console.log("word is  = " + hadeethGroup[i].content.trim().split(/\s+/g)[numberOfWords]);
            // console.log("index is  = " + i);
            // console.log("**************************************************");
            /* var t = hadeethGroup[22].content.trim().split(" ");
             for (var j = 0; j<t.length;j++)
             {
             console.log("word " + j + " = " + t[j]);
             }
             console.log("**************************************************");*/

        }
    }

    var words = searchForText.trim().split(/\s+/g);
    if (words.length > 1) {
        for (var j = 0; j < hadeethGroup.length; j++) {
            var regex = /[^(\u0600-\u06FF|\u0020-\u007F|\u003A|\u060c|\u061f)|\s]*/g;
            var hadeeth = hadeethGroup[j].content.trim().replace(regex, '');
            for (var i = 0; i < words.length; i++) {
                var isFound = hadeeth.search(words[i].trim());
                if (isFound != -1) {
                    console.log("searching for = " + words[i].trim());
                    console.log("before = " + hadeethGroup[j].content);
                    console.log("after  = " + hadeeth);
                    console.log("index  = " + isFound);
                    console.log("numberOfWords  = " + numberOfWords);
                    console.log("word is  = " + hadeethGroup[j].content.trim().split(/\s+/g)[numberOfWords]);
                    console.log("index is  = " + j);
                    console.log("**************************************************");

                    var numberOfWords = hadeeth.trim().substr(0, isFound - 1).split(/\s+/g).length;
                    wholeString.push(hadeethGroup[j]);
                    wordsCount.push(numberOfWords);
                    ids.push(j);
                }
            }
        }
    }

    if (wholeString.length > 0) {
        highLight = true;
        hadeethArray = wholeString;
        id = -1;
        nextHadeeth();
    }
    else {
        if(document.title == "Messages")
        {
            hadeethBox.innerHTML = '<span style=color:red>' + "No Results Found" + "</span>";
        }else
        {
            hadeethBox.innerHTML = '<span style=color:red>' + "لم يتم العثور على نتائج" + "</span>";
        }
    }
    isSearching = false;
}

function nextHadeeth() {

    $(hadeethBox).fadeOut(250, function () {
        id++;
        if (id >= hadeethArray.length || hadeethArray[id] == undefined) {
            id = 0;
        }

        if(document.title == "Messages")
        {
            hadeethArray = hadeethGroupEn;
        }else
        {
            hadeethArray = hadeethGroup;
        }

        var hadeethText = hadeethArray[id].content.trim();
        if (highLight) {
            var wordsArray = hadeethText.split(/\s+/g);
            var highLightedHadeeth = "";
            var firstWordIndex = wordsCount[id] - 1;
            var lastWordIndex = wordsCount[id] + 1;
            if (firstWordIndex < 0) {
                firstWordIndex = 0;
            }
            if (lastWordIndex > wordsArray.length - 1) {
                lastWordIndex = wordsArray.length - 1;
            }
            for (var i = 0; i < firstWordIndex; i++) {
                highLightedHadeeth = highLightedHadeeth + wordsArray[i] + " ";
            }
            highLightedHadeeth = highLightedHadeeth + " " + " <span class = 'highLight' style='background-color: yellow;'>";
            for (var i = firstWordIndex; i <= lastWordIndex; i++) {
                highLightedHadeeth = highLightedHadeeth + " " + wordsArray[i];
            }
            highLightedHadeeth = highLightedHadeeth + " </span> ";
            for (var i = lastWordIndex + 1; i < wordsArray.length; i++) {
                highLightedHadeeth = highLightedHadeeth + wordsArray[i] + " ";
            }
            hadeethText = highLightedHadeeth;
        }
        hadeethBox.innerHTML = hadeethText;

        $(hadeethBox).fadeIn(250);
    });

}


function prevHadeeth() {

    $(hadeethBox).fadeOut(250, function () {
        id--;
        if (id == -1) {
            id = hadeethArray.length - 1;
        }
        var hadeethText = hadeethArray[id].content;
        if (highLight) {
            var wordsArray = hadeethText.split(/\s+/g);
            var highLightedHadeeth = "";
            var firstWordIndex = wordsCount[id] - 1;
            var lastWordIndex = wordsCount[id] + 1;
            if (firstWordIndex < 0) {
                firstWordIndex = 0;
            }
            if (lastWordIndex > wordsArray.length - 1) {
                lastWordIndex = wordsArray.length - 1;
            }
            for (var i = 0; i < firstWordIndex; i++) {
                highLightedHadeeth = highLightedHadeeth + wordsArray[i] + " ";
            }
            highLightedHadeeth = highLightedHadeeth + " " + " <span class = 'highLight' style='background-color: yellow;'>";
            for (var i = firstWordIndex; i <= lastWordIndex; i++) {
                highLightedHadeeth = highLightedHadeeth + " " + wordsArray[i];
            }
            highLightedHadeeth = highLightedHadeeth + " </span> ";
            for (var i = lastWordIndex + 1; i < wordsArray.length; i++) {
                highLightedHadeeth = highLightedHadeeth + wordsArray[i] + " ";
            }
            hadeethText = highLightedHadeeth;
        }
        hadeethBox.innerHTML = hadeethText;
        $(hadeethBox).fadeIn(250);
    });
}

function closeAlert() {
    $("#alertModal").css("display", "none");
    $("#alertModal").removeClass("in");
    $("#alertModal").removeClass("modal-open");
}


function saveHadeeth() {
    fileName = dateSelector.value;

    if (fileName == "") {
        $("#alertModal").css("display", "block");
        $("#alertModal").addClass("in");
        $("#alertModal").addClass("modal-open");
        return;
    }

    console.log(fileName);
    var idToSave = id;
    if (highLight) {
        idToSave = ids[id];
    }
    var content = null;
    if (document.title == "Messages") {
        content = hadeethGroupEn[idToSave].content;
    } else {
        content = hadeethGroup[idToSave].content;
    }

    if(document.title == "Messages")
    {
        saveHadeethPHP = saveHadeethPHP + "?lang=en"
    }
    $.post(saveHadeethPHP,
        {
            content: content,
            date: fileName
        },
        function (data, status) {
            console.log(data);
            data = JSON.parse(data);
            if (data.response == "1") {
                $("#successModal").css("display", "block");
                $("#successModal").addClass("in");
                $("#successModal").addClass("modal-open");
            }
            else {
                console.log("failed");
            }
        });
}

function viewHadeethList() {
    if (document.title == "Messages") {

        location.replace("hadeethTable-En.html");
    }

    else {

        location.replace("hadeethTable.html");

    }

}

function closeSuccessAlert() {
    $("#successModal").css("display", "none");
    $("#successModal").removeClass("in");
    $("#successModal").removeClass("modal-open");
}

function stopSearch() {
    highLight = false;
    if(document.title == "Messages")
    {
        hadeethArray = hadeethGroupEn;
    }else
    {
        hadeethArray = hadeethGroup;
    }
    document.getElementById("searchBox").value = "";
    nextHadeeth();
}


function getParamValueByName(name, windowId) {
    if (windowId == null) {

        var query = window.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }
    else {
        var iframe = document.getElementById(windowId);
        var query = iframe.contentWindow.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }

}