/**
 * Created by Ahmad on 7/4/2017.
 */


var foldersData = {};
function loadTimeZoneSelectors() {


    $.get('emosquescreen/other_components/setting/time-zone.php?op=read', function (data) {

        var folders = JSON.parse(data);
        var dir;
        var folderName;
        var files;
        var objects = {};
        var temp2 = [];
        for (var i = 0; i < folders.length; i++) {
            dir = folders[i];
            folderName = dir.folderName;
            files = dir.files;
            var temp = [];
            for (var j = 0; j < files.length; j++) {
                temp.push(files[j]);
            }
            objects[folderName] = temp.slice();
            temp2.push(folderName);
        }
        objects.folderNames = temp2.slice();
        console.log(objects);
        foldersData = objects;
        initTZSelector1();
    });

    $.get('emosquescreen/other_components/setting/time-zone.php?op=readTZ', function (data) {

        if (data.length == 5) {
            var timeZone = data;
            var gmtLabel = document.getElementById("gmtLabel");
            gmtLabel.innerHTML = "GMT " + timeZone.substring(0, 3) + ":" + timeZone.substring(3, 5);
        }

    });


}


function initTZSelector1() {
    var TZSelector1 = document.getElementById("timeZoneSelector1");

    var folderNames = foldersData.folderNames;
    for (var i = 0; i < folderNames.length; i++) {
        var option = document.createElement("option");
        option.text = folderNames[i];
        option.value = folderNames[i];
        TZSelector1.add(option);
    }
}


function initTZSelector2(TZSelector1) {
    var TZSelector2 = document.getElementById("timeZoneSelector2");
    TZSelector2.innerHTML = "";
    var folderName = TZSelector1.options[TZSelector1.selectedIndex].value;
    var files = foldersData[folderName];
    for (var i = 0; i < files.length; i++) {
        var option = document.createElement("option");
        option.text = files[i];
        option.value = folderName + "/" + files[i];
        TZSelector2.add(option);
    }

    TZSelector2.style.visibility = "visible";
}


function submitTimeZone() {
    var TZSelector2 = document.getElementById("timeZoneSelector2");
    var TZ = TZSelector2.options[TZSelector2.selectedIndex].value;
    console.log('emosquescreen/other_components/setting/time-zone.php?op=write&TZ=' + TZ);
    $.get('emosquescreen/other_components/setting/time-zone.php?op=write&TZ=' + TZ, function (data) {
        window.alert(data);
    });
}
