var submitButton = document.getElementById("submitButton");
var timeZoneSelector = document.getElementById("timeZoneSelector");
var gmtLabel = document.getElementById("gmtLabel");
var fajrCurrentTime = document.getElementById("fajrCurrentTime");
var duhurCurrentTime = document.getElementById("duhurCurrentTime");
var asrCurrentTime = document.getElementById("asrCurrentTime");
var maghribCurrentTime = document.getElementById("maghribCurrentTime");
var ishaaCurrentTime = document.getElementById("ishaaCurrentTime");

$(document).ready(function () {
    $('select').on('change', function () {
        submitButton.disabled = false;
        submitButton.style.background = '#00FF00';
        submitButton.style.color = '#000000'
    });
});

function enableSubmitButton() {
    submitButton.disabled = false;
    submitButton.style.background = '#afbec5';
    submitButton.style.color = '#000000'
}

function reduceOneHour() {
    if (timeZoneSelector.selectedIndex > 0) {
        timeZoneSelector.selectedIndex = timeZoneSelector.selectedIndex - 1;
        gmtLabel.innerHTML = "GMT " + timeZoneSelector.options[timeZoneSelector.selectedIndex].value;
        adjustPrayers();
        submitButton.disabled = false;
        submitButton.style.background = '#afbec5';
        submitButton.style.color = '#000000'

        fajrCurrentTime.style.visibility = 'hidden';
        duhurCurrentTime.style.visibility = 'hidden';
        asrCurrentTime.style.visibility = 'hidden';
        maghribCurrentTime.style.visibility = 'hidden';
        ishaaCurrentTime.style.visibility = 'hidden';

        setTimeout(function () {
            fajrCurrentTime.style.visibility = 'visible';
            duhurCurrentTime.style.visibility = 'visible';
            asrCurrentTime.style.visibility = 'visible';
            maghribCurrentTime.style.visibility = 'visible';
            ishaaCurrentTime.style.visibility = 'visible';
        }, 170);
    }
}

function addOneHour() {
    if (timeZoneSelector.selectedIndex < 48) {
        timeZoneSelector.selectedIndex = timeZoneSelector.selectedIndex + 1;
        gmtLabel.innerHTML = "GMT " + timeZoneSelector.options[timeZoneSelector.selectedIndex].value;
        adjustPrayers();
        submitButton.disabled = false;
        submitButton.style.background = '#afbec5';
        submitButton.style.color = '#000000'

        fajrCurrentTime.style.visibility = 'hidden';
        duhurCurrentTime.style.visibility = 'hidden';
        asrCurrentTime.style.visibility = 'hidden';
        maghribCurrentTime.style.visibility = 'hidden';
        ishaaCurrentTime.style.visibility = 'hidden';

        setTimeout(function () {
            fajrCurrentTime.style.visibility = 'visible';
            duhurCurrentTime.style.visibility = 'visible';
            asrCurrentTime.style.visibility = 'visible';
            maghribCurrentTime.style.visibility = 'visible';
            ishaaCurrentTime.style.visibility = 'visible';
        }, 170);
    }
}

//loadQuickPanelSetting();
function loadQuickPanelSetting() {
    //prayers time settings
    var countrySelector = document.getElementById("countrySelector");
    var citySelector = document.getElementById("citySelector");
    var highLatSelector = document.getElementById("highLatSelector");
    var asrJuristicSelector = document.getElementById("asrJuristicSelector");
    var calMethodeSelector = document.getElementById("calMethodeSelector");
    var midnightMethod = document.getElementById("midnightMethod");


    var options = countrySelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.country) {
            options[i].selected = true;
            break;
        }
    }

    removeOptions(citySelector)
    loadCities(countrySelector.value);

    var options = citySelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.city) {
            options[i].selected = true;
            break;
        }
    }


    var options = calMethodeSelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.calculationMethod) {
            options[i].selected = true;
            break;
        }
    }


    var options = asrJuristicSelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.asrJuristic) {
            options[i].selected = true;
            break;
        }
    }


    var options = highLatSelector.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.adjusthighLat) {
            options[i].selected = true;
            break;
        }
    }


    var options = midnightMethod.options;
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === screenSetting.prayerSetting.midNightMethod) {
            options[i].selected = true;
            break;
        }
    }
    var d = new Date()
    var n = d.getTimezoneOffset();
    n = -1 * n;
    var timeZone = n / 60;

    timeZoneSelector.options[timeZone + parseInt(timeZoneSelector.options.length / 2)].selected = true;
    // gmtLabel.innerHTML = "GMT " + timeZoneSelector.options[timeZoneSelector.selectedIndex].value;


    var hijriDateCorrectionSelector = document.getElementById("hijriCorrection");
    hijriDateCorrectionSelector.options[screenSetting.hijriCorrection + parseInt(hijriDateCorrectionSelector.options.length / 2)].selected = true;


    var QREnable = document.getElementById("showQRCodeCheckbox");
    var showIntenalTempCheckbox = document.getElementById("showIntenalTempCheckbox");
    var azanEnable = document.getElementById("playAzan");
//    var displayAnalogClock = document.getElementById("displayAnalogClock");
    var displayBahbishMessage = document.getElementById("displayBahbishMessage");


    if (screenSetting.showBahbishMessage == true) {
        displayBahbishMessage.checked = true;
    }
    else {
        displayBahbishMessage.checked = false;
    }


   /* if (screenSetting.showAnalogClock == true) {
        displayAnalogClock.checked = true;
    }
    else {
        displayAnalogClock.checked = false;
    }*/


    if (screenSetting.appQREnable == true) {
        QREnable.checked = true;
    }

    if (screenSetting.internalTempEnable == true) {
        showIntenalTempCheckbox.checked = true;
    }

    if (screenSetting.playAzan == true) {
        azanEnable.checked = true;
    }
    else {
        azanEnable.checked = false;
    }


    //Azkar setting
    var fajrAzkarDisplayPeriod = document.getElementById("fajrAzkarDisplayPeriod");
    var duhurAzkarDisplayPeriod = document.getElementById("duhurAzkarDisplayPeriod");
    var asrAzkarDisplayPeriod = document.getElementById("asrAzkarDisplayPeriod");
    var maghribAzkarDisplayPeriod = document.getElementById("maghribAzkarDisplayPeriod");
    var ishaaAzkarDisplayPeriod = document.getElementById("ishaaAzkarDisplayPeriod");
    var jumaaAzkarDisplayPeriod = document.getElementById("jumaaAzkarDisplayPeriod");

    fajrAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.fajr].selected = true;
    duhurAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.duhur].selected = true;
    asrAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.asr].selected = true;
    maghribAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.maghrib].selected = true;
    ishaaAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.ishaa].selected = true;
    jumaaAzkarDisplayPeriod.options[screenSetting.azkarDisplayPeriod.jumaa].selected = true;


    var beforFajrAzkarPeriod = document.getElementById("beforFajrAzkarPeriod");
    var beforduhurAzkarPeriod = document.getElementById("beforduhurAzkarPeriod");
    var beforAsrAzkarPeriod = document.getElementById("beforAsrAzkarPeriod");
    var beforMaghribAzkarPeriod = document.getElementById("beforMaghribAzkarPeriod");
    var beforIshaaAzkarPeriod = document.getElementById("beforIshaaAzkarPeriod");
    var beforJumaaAzkarPeriod = document.getElementById("beforJumaaAzkarPeriod");

    beforFajrAzkarPeriod.options[screenSetting.timeBetweenAzanAndAzkar.fajr].selected = true;
    beforduhurAzkarPeriod.options[screenSetting.timeBetweenAzanAndAzkar.duhur].selected = true;
    beforAsrAzkarPeriod.options[screenSetting.timeBetweenAzanAndAzkar.asr].selected = true;
    beforMaghribAzkarPeriod.options[screenSetting.timeBetweenAzanAndAzkar.maghrib].selected = true;
    beforIshaaAzkarPeriod.options[screenSetting.timeBetweenAzanAndAzkar.ishaa].selected = true;
    beforJumaaAzkarPeriod.options[screenSetting.timeBetweenAzanAndAzkar.jumaa].selected = true;


    //Iqama setting
    var fajrIqamaTime = document.getElementById("fajrIqamaTime");
    var duhurIqamaTime = document.getElementById("duhurIqamaTime");
    var asrIqamaTime = document.getElementById("asrIqamaTime");
    var maghribIqamaTime = document.getElementById("maghribIqamaTime");
    var ishaaIqamaTime = document.getElementById("ishaaIqamaTime");
    var jumaaIqamaTime = document.getElementById("jumaaIqamaTime");

    fajrIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.fajr].selected = true;
    duhurIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.duhur].selected = true;
    asrIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.asr].selected = true;
    maghribIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.maghrib].selected = true;
    ishaaIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.ishaa].selected = true;
    jumaaIqamaTime.options[screenSetting.timeBetweenAzanAndIqama.jumaa].selected = true;


    //Prayers correction setting
    var fajrTimeCorrection = document.getElementById("fajrTimeCorrection");
    var duhurTimeCorrection = document.getElementById("duhurTimeCorrection");
    var asrTimeCorrection = document.getElementById("asrTimeCorrection");
    var maghribTimeCorrection = document.getElementById("maghribTimeCorrection");
    var ishaaTimeCorrection = document.getElementById("ishaaTimeCorrection");

    fajrTimeCorrection.options[screenSetting.prayerSetting.offset.fajrOffset + parseInt(fajrTimeCorrection.options.length / 2)].selected = true;
    duhurTimeCorrection.options[screenSetting.prayerSetting.offset.duhurOffset + parseInt(duhurTimeCorrection.options.length / 2)].selected = true;
    asrTimeCorrection.options[screenSetting.prayerSetting.offset.asrOffset + parseInt(asrTimeCorrection.options.length / 2)].selected = true;
    maghribTimeCorrection.options[screenSetting.prayerSetting.offset.maghribOffset + parseInt(maghribTimeCorrection.options.length / 2)].selected = true;
    ishaaTimeCorrection.options[screenSetting.prayerSetting.offset.ishaaOffset + parseInt(ishaaTimeCorrection.options.length / 2)].selected = true;


    adjustPrayers();
    //onMiladiDateChange();

}


