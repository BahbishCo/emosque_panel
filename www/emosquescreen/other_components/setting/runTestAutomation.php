<?php
if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'startTestAutomation':
            startTestAutomationFromMobile();
            break;
    }
}
else{
startTestAutomationFromPI();
}

function startTestAutomationFromMobile() {
    exec("sudo java -cp /home/pi/.eMosqueSystem/processes/TestAutomation TestAutomation");
    exit;
}

function startTestAutomationFromPi() {
    shell_exec('sudo gnome-terminal -e "bash -c \"/home/pi/.eMosqueSystem/processes/TestAutomation/runTestAutomation.sh; exec bash\""');
    exit;
}