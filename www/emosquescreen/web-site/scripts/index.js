// reload the page when ever the window size is changed
footerHeight = 10;
window.onresize = function (event) {
    clearTimeout(reloadPage);
    setTimeout(reloadPage, 2000);
}

function reloadPage() {

    window.location.href = window.location.href;
}


$(document).ready(function () {
    if (getParamValueByName("mode") == Setting.Mode.PI) {
        var screenWidth = screen.width;
        var screenHeight = screen.height;

        setTimeout(function () {
            loadScript("css/CSS-files-per-resolution/" + screenWidth + "-" + screenHeight + "-header-font-size" + ".js", init);

            document.getElementById("header").style.visibility = "visible";
            document.getElementsByTagName("body")[0].style.visibility = "visible";
            document.getElementById("pageContainer").style.visibility = "visible";
        }, 2000);


    }
    else if (getParamValueByName("mode") == Setting.Mode.PreviewFrameSize) {
        var screenWidth = screen.width;
        var screenHeight = screen.height;

        loadDefaultCSSFile();
        loadScript("css/" + "default-header-font-size" + ".js", init);

        document.getElementById("header").style.visibility = "visible";
        document.getElementsByTagName("body")[0].style.visibility = "visible";
        document.getElementById("pageContainer").style.visibility = "visible";
    }
    else if (getParamValueByName("mode") == Setting.Mode.PreviewFullScreen) {
        var screenWidth = screen.width;
        var screenHeight = screen.height;
        loadCSSFile();
        loadScript("css/CSS-files-per-resolution/" + screenWidth + "-" + screenHeight + "-header-font-size" + ".js", init);
        document.getElementById("header").style.visibility = "visible";
        document.getElementsByTagName("body")[0].style.visibility = "visible";
        document.getElementById("pageContainer").style.visibility = "visible";
    }
});


function writeIamAlive() {
    getRequest(
        '../../browserMonitorPHP/IamAliveWriter.php', // URL for the PHP file
        null,  // handle successful request
        null    // handle error
    );
}

function getParamValueByName(name, windowId) {
    if (windowId == null) {

        var query = window.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }
    else {
        var iframe = document.getElementById(windowId);
        var query = iframe.contentWindow.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }

}

function removeIqama(id) {
    hideBigIqama();

    document.getElementsByClassName("ishaaIqamaBox")[0].style.display = "none";
    document.getElementsByClassName("ishaaIqamaBox")[1].style.display = "none";

    var header = document.getElementById("header");

    var elHight = bodyHeight - header.clientHeight - 10 * 6 - 30 + 30 - footerHeight;
    elHight = elHight / 6;


    var prayerEls = document.getElementsByClassName("prayerEl");

    for (var i = 0; i < prayerEls.length; i++) {
        prayerEls[i].style.height = elHight + "px";
    }


    cssFile = document.createElement("link");
    cssFile.href = "css/CSS-files-per-resolution/withOutIqama.css";
    cssFile.rel = "stylesheet";
    document.body.appendChild(cssFile);

}


function showIqama(time) {
    var header = document.getElementById("header");
    elHight = bodyHeight - header.clientHeight - 10 * 7 - 30 + 30 - footerHeight;
    elHight = elHight / 7;


    var prayerEls = document.getElementsByClassName("prayerEl");

    for (var i = 0; i < prayerEls.length; i++) {
        prayerEls[i].style.height = elHight + "px";
    }

    var iqama = document.getElementById("ishaaIqama");
    var iqamaTime = document.getElementById("ishaaIqamaTime");


    if(screenSetting.language== Setting.Language.English)
    {
        iqama.innerHTML ="IQAMA";
    }else
    {
        iqama.innerHTML ="اقامة";
    }


    document.getElementsByClassName("ishaaIqamaBox")[0].style.display = "table-cell";
    document.getElementsByClassName("ishaaIqamaBox")[1].style.display = "table-cell";

    iqamaTime.style.height = elHight + "px";
    iqamaTime.innerHTML = "<span id='iqamaTimeToBlink'></span>";

    document.getElementById("bigIqamaCounter").innerHTML = "<span id='bigIqamaTimeToBlink'>" + time + "</span>";

    iqama.style.height = elHight + "px";

    cssFile = document.createElement("link");
    cssFile.href = "css/CSS-files-per-resolution/withIqama.css";
    cssFile.rel = "stylesheet";
    document.body.appendChild(cssFile);
    startIqamaCounterF();
    stopSwitching = true;
}

function hideSecondaryMessage() {
    var secondaryMessage = document.getElementById("horizTextFrame");
    var silentLogo = document.getElementById("silentLogo")
    //silentLogo.style.visibility = "hidden";
    secondaryMessage.style.visibility = "hidden";
}

function displaySecondaryMessage() {

    var secondaryMessage = document.getElementById("horizTextFrame");
    var silentLogo = document.getElementById("silentLogo");
    secondaryMessage.style.visibility = "visible";
    if (showLogo) {
        silentLogo.style.visibility = "visible";
    }
}

function displayAfterPrayerWindow() {
    displaySecondaryMessage();

    changeHadeeth = true;

    var contentFrame = document.getElementById("hadeethWindow");
    contentFrame.style.visibility = "visible";
    contentFrame.style.display = "none";
    $("#hadeethWindow").fadeIn(1000);
    if (runningMode == Setting.Mode.PI) {
        contentFrame.src = "html-pages/afterPrayerAzkar/afterPrayerAzkarFrame.html";
    }
}

function removeAfterPrayerWindow() {
    isSecondaryMessageToday();
}


function playAzan(azanMode) {
    if (azanMode == 0) {
        getRequest(
            '../other_components/azan_player/startAzan.php?azanMode=0', // URL for the PHP file
            null,  // handle successful request
            null    // handle error
        );
    }
    else {
        getRequest(
            '../other_components/azan_player/startAzan.php?azanMode=1', // URL for the PHP file
            null,  // handle successful request
            null    // handle error
        );
    }
}

function isJumaa() {
    var date = new Date();
    if (date.getDay() == 5) {
        return true;
    }
}

function loadScript(url, callback) {
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

    // Fire the loading
    head.appendChild(script);
}


function getRequest(url, success, error) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}





