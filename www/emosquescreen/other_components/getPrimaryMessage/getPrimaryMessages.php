<?php


class Locations
{
    var $serverMessagesPath = "../../web-site/customFiles/contents/primaryMessages/ServerMessages";
    var $imamMessagesPath = "../../web-site/customFiles/contents/primaryMessages/ImamMessages";


    //var $serverMessagesPath = "C:\\xampp\\htdocs\\eMosqueSystem\\www\\emosquescreen\\web-site\\customFiles\\contents\\primaryMessages\\ServerMessages";
    //var $imamMessagesPath = "C:\\xampp\\htdocs\\eMosqueSystem\\www\\emosquescreen\\web-site\\customFiles\\contents\\primaryMessages\\ImamMessages";


}


class MessageInfo
{

    var $startDate;
    var $endDate;
    var $startTime;
    var $endTime;

    function __construct()
    {

    }

    function setMessageInfo($startDate, $endDate, $startTime, $endTime)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
//        $this->startTime = str_replace(";", ":", $startTime);
//        $this->endTime = str_replace(";", ":", $endTime);
    }
}


$fileName = "";
/*splitting char*/
$dateTimeSplitChar = '.';
$startEndDatesSplitChar = '_';
$startEndTimeSplitChar = '_';
$withinDateSplitChar = "-";
$withinTimeSplitChar = ":";
//$withinTimeSplitChar = "-";

//echo print_r($directoyContent);
$messagesArray = array();

$tempMessageInfo = new MessageInfo();
$displayMessage = false;
$responseJSON = array();


function isWithinMessageTime($message)
{
    $format = 'Y-m-d H:i';
    //echo $message->startDate . "<br>";
    //echo $message->startTime . "<br>";
    $startTimeString = $message->startDate . " " . $message->startTime;
    $endTimeString = $message->endDate . " " . $message->endTime;
    $startDate = DateTime::createFromFormat($format, $startTimeString);
    $endDate = DateTime::createFromFormat($format, $endTimeString);
    $currentDate = new DateTime();


    if (($currentDate->getTimestamp() >= $startDate->getTimestamp()) && ($currentDate->getTimestamp() <= $endDate->getTimestamp())) {
        return true;
    } else {
        return false;
    }
}


function messageToString($message)
{
    $test = new MessageInfo();

    $str = "{" . "\n" .
        "startDate:" . "\"" . $message->startDate . "\"" . "," . "\n" .
        "endDate:" . "\"" . $message->endDate . "\"" . "," . "\n" .
        "startTime:" . "\"" . $message->startTime . "\"" . "," . "\n" .
        "endTime:" . "\"" . $message->endTime . "\"" . "," . "\n" .
        "},";

    return $str;
}


function writeFile($messages)
{
    $str = "primaryMessages = [" . "\n";

    for ($i = 0; $i < sizeof($messages); $i++) {
        $str = $str . (messageToString($messages[$i])) . "\n";
    }

    $str = $str . "];";

    $file = fopen((new Locatiions())->writeLocation, 'w') or die("can't open file");
    fwrite($file, $str);
    fclose($file);
}

function getMessageInfoFromString($str)
{
    $temp = explode(".", $str);
    $dateString = $temp[0];
    $timeString = $temp[1];

    $temp = explode("_", $dateString);
    $startDate = $temp[0];
    $endDate = $temp[1];

    $temp = explode("_", $timeString);
    $startTime = $temp[0];
    $endTime = $temp[1];

    $MessageInfo = new MessageInfo();
    $MessageInfo->setMessageInfo($startDate, $endDate, $startTime, $endTime);

    return $MessageInfo;
}

function checkForServerMessages()
{
    $directoyContent = @scandir((new Locations())->serverMessagesPath);

    for ($i = 0; $i < sizeof($directoyContent); $i++) {
        if ($directoyContent[$i] == "." || $directoyContent[$i] == "..") {
            continue;
        }

        $extension = strtolower(substr($directoyContent[$i], strrpos($directoyContent[$i], '.') + 1));
        if ($extension == "html") {
            $tempMessageInfo = getMessageInfoFromString(substr($directoyContent[$i], 0, strrpos($directoyContent[$i], '.')));
            $displayMessage = isWithinMessageTime($tempMessageInfo);
            if ($displayMessage) {
                $GLOBALS['fileName'] = $directoyContent[$i];
                return true;
            }
            //array_push($messagesArray, $tempMessageInfo);
        }
    }
    return false;
}


function checkForImamMessage()
{
    $directoyContent = @scandir((new Locations())->imamMessagesPath);
    for ($i = 0; $i < sizeof($directoyContent); $i++) {
        if ($directoyContent[$i] == "." || $directoyContent[$i] == "..") {
            continue;
        }
        $extension = strtolower(substr($directoyContent[$i], strrpos($directoyContent[$i], '.') + 1));
        if ($extension == "html") {
            $tempMessageInfo = getMessageInfoFromString(substr($directoyContent[$i], 0, strrpos($directoyContent[$i], '.')));
            $displayMessage = isWithinMessageTime($tempMessageInfo);
            if ($displayMessage) {
                $GLOBALS['fileName'] = $directoyContent[$i];
                return true;
            }
            //array_push($messagesArray, $tempMessageInfo);
        }
    }
    return false;
}


if (checkForServerMessages()) {
    $responseJSON = array('type' => 1, 'fileName' => $fileName);
} else if (checkForImamMessage()) {
    $responseJSON = array('type' => 2, 'fileName' => $fileName);
} else {
    $responseJSON = array('type' => 0, 'fileName' => "");

}


echo(json_encode($responseJSON));
?>