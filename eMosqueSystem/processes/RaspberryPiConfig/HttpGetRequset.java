import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpGetRequset
{
    String url;
    String param = "";
    GetResponse reqResponse;
    
    public HttpGetRequset(String urlString)
    {
	this.url = urlString;
    }

    public void addParam(String name, String value)
    {
	if (param.equals(""))
	{
	    param = name + "=" + value;
	} else
	{
	    param = param + "&" + name + "=" + value;
	}
    }

    public GetResponse excuteRequest()
    {

	String fullUrl = url + "?" + param;
	URL url;
	try
	{
	    url = new URL(fullUrl);
	    System.out.println(fullUrl);

	    HttpURLConnection con = (HttpURLConnection) url.openConnection();

	    con.setRequestMethod("GET");

	    int responseCode = con.getResponseCode();

	    BufferedReader in = new BufferedReader(new InputStreamReader(
		    con.getInputStream()));
	    String inputLine;
	    StringBuffer response = new StringBuffer();

	    while ((inputLine = in.readLine()) != null)
	    {
		response.append(inputLine);
	    }
	    in.close();

	    reqResponse = new GetResponse();
	    reqResponse.responseCode = responseCode;
	    reqResponse.responseText = response.toString();
	    
	} catch (MalformedURLException e)
	{
	    e.printStackTrace();
	} catch (IOException e)
	{
	    e.printStackTrace();
	}

	return reqResponse;
	
    }
    
    public class GetResponse
    {
	int responseCode;
	String responseText;
    }
}
