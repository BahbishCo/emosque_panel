#!/bin/bash
wmctrl -k on
gnome-terminal -e "bash -c \"cd /home/pi/.eMosqueSystem/processes/temp-reader/;./read-script.sh; exec bash\""
wmctrl -k on
sleep 6
gnome-terminal -e "bash -c \"cd /home/pi/.eMosqueSystem/processes/temp-reader/;java TempReader; exec bash\""
wmctrl -k on
gnome-terminal -e "bash -c \"cd /home/pi/.eMosqueSystem/processes/PiPriMessagesDownloader/;sudo java -classpath :json.jar Main; exec bash\""
wmctrl -k on
gnome-terminal -e "bash -c \"cd /home/pi/.eMosqueSystem/processes/browserMonitor/;sudo java Main; exec bash\""
wmctrl -k on
gnome-terminal -e "bash -c \"cd /home/pi/.eMosqueSystem/processes/browserScreenMonitor/;sudo java ScreenMonitor; exec bash\""
wmctrl -k on
gnome-terminal -e "bash -c \"cd /home/pi/.eMosqueSystem/processes/beaconBrodcasting/;./beaconBrodcasting.sh; exec bash\""
wmctrl -k on
gnome-terminal -e "bash -c \"cd /home/pi/.eMosqueSystem/processes/startVinoServer/;./startVino.sh; exec bash\""