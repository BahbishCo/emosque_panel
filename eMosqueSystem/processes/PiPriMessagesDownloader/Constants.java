public class Constants {
    public static boolean release = false;
    public static final byte isServer = 0, isClient = 1;
    public static final String tempSensorIDs = "0c45:7401";
    public static final String wifiAdapterIDs_white = "0a5c:bd1e";
    public static final String wifiAdapterIDs_black = "0bda:8176";


    public class PI_Version {
        public static final String keyName = "Revision";
        public static final String PiZeroKeyValue = "900092";
        public static final String PiTwoKeyValue_1 = "a01041";
        public static final String PiTwoKeyValue_2 = "a21041";
        public static final String PiThreeKeyValue_1 = "a02082";
        public static final String PiThreeKeyValue_2 = "a22082";
        public static final int PiZeroVersion = 0;
        public static final int PiTwoVersion = 2;
        public static final int PiThreeVersion = 3;


    }


    public class Commands {

        public static final String enableBrodcastingScript = "sudo /home/pi/.eMosqueSystem/processes/hotSpot/enableBrodcasting.sh";
        public static final String disableBrodcastingScript = "sudo /home/pi/.eMosqueSystem/processes/hotSpot/disableBrodcasting.sh";

        public static final String restartDHCPD = "sudo service dhcpcd restart";
        public static final String restartSOFTAP = "sudo service hostapd restart";
        public static final String restartDNS = "sudo service dnsmasq restart";

        public static final String stopDHCPD = "sudo service dhcpcd stop";
        public static final String stopSOFTAP = "sudo service hostapd stop";
        //public static final String stopDNS = "sudo service dnsmasq stop";

        public static final String startBrodcasting = "sudo /usr/sbin/hostapd /etc/hostapd/hostapd.conf";

        public static final String IFDOWN_ETH0 = "sudo ifdown eth0";
        public static final String IFDOWN_WLAN0 = "sudo ifdown wlan0";
        public static final String IFUP_ETH0 = "sudo ifup eth0";
        public static final String IFUP_WLAN0 = "sudo ifup wlan0";
        public static final String startServerMode = "sudo /home/pi/.eMosqueSystem/server.sh";
        public static final String startClientMode = "sudo /home/pi/.eMosqueSystem/client.sh";
        public static final String reboot = "sudo reboot";
        public static final String ListUSB = "lsusb";
        public static final String minimizeWindows = "sudo wmctrl -k on";
        public static final String killMidori = "pkill midori";
        public static final String makeScripExcutable = "sudo chmod +x " + Constants.Locations.clientExtractedZipFilePath + "/scripts/do.sh";
        public static final String excuteDoScript = "sudo " + Constants.Locations.clientExtractedZipFilePath + "/scripts/do.sh";
    }

    public class Locations {
        public static final String LOC_WLAN0_PATH = "/sys/class/net/wlan0";
        public static final String LOC_ETH0_STAT = "/sys/class/net/eth0/carrier";
        public static final String LOC_WLAN0_STAT = LOC_WLAN0_PATH + "/carrier";
        public static final String LOC_STAT_FILE = "/home/pi/.eMosqueSystem/ipstatus";
        public static final String LOC_INTERFACES = "/etc/network/interfaces";
        public static final String ethernetMacAddres = "/sys/class/net/eth0/address";
        public static final String nextPiConfigMode = "/home/pi/.eMosqueSystem/piMode.txt";
        public static final String systemVersionNumber = "/var/www/emosquescreen/version/system.txt";
        public static final String contentVersionNumber = "/var/www/emosquescreen/version/content.txt";
        public static final String clientPanelInfoHandler = "http://192.168.0.200/emosquescreen/other_components/client_panels/php/savePanelData.php";
        public static final String serverPanelInfoHandler = "http://localhost/emosquescreen/other_components/client_panels/php/savePanelData.php";
        public static final String piModeFile = "/var/www/emosquescreen/screenMode.js";

        // Sync Panels Files Paths

        public static final String versionSettingsPath = "/var/www/emosquescreen/version/settings.txt";
        public static final String versionWebsitePath = "/var/www/emosquescreen/version/system.txt";
        public static final String versionContentPath = "/var/www/emosquescreen/version/content.txt";
        public static final String serverZipFilePath = "http://192.168.0.200/updates/emosquescreen.zip";

        public static final String clientZipFilePath = "/home/pi/updates/emosquescreen.zip";
        //public static final String clientZipFilePath = "C:\\\\Users\\Ahmad\\Desktop\\shit\\emosque.zip";

        public static final String clientExtractedZipFilePath = "/home/pi/updates/temp";
//	public static final String clientExtractedZipFilePath = "C:\\\\Users\\Ahmad\\Desktop\\shit\\temp";

        public static final String serverSettingVersionFiles = "http://192.168.0.200/emosquescreen/version/settings.txt";
        public static final String serverContentVersionFiles = "http://192.168.0.200/emosquescreen/version/content.txt";
        public static final String serverSystemVersionFiles = "http://192.168.0.200/emosquescreen/version/system.txt";
        public static final String clientSettingFile = "/var/www/emosquescreen/web-site/JSON/setting.js";
        public static final String serverSettingFile = "http://192.168.0.200/emosquescreen/web-site/JSON/setting.js";

        public static final String clientContentFile = "/var/www/emosquescreen/web-site/JSON/specEvent.js";
        public static final String serverContentFile = "http://192.168.0.200/emosquescreen/web-site/JSON/specEvent.js";

        public static final String restoreVersionURL = "http://localhost/emosquescreen/other_components/upload/roll_back.php";

        public static final String piVersionPath = "/proc/cpuinfo";
		public static final String SettingFile = "/var/www/emosquescreen/web-site/customFiles/settings/setting.js";
//		public static final String SettingFile = "C:\\xampp\\htdocs\\emosque_panel_dev\\www\\emosquescreen\\web-site\\customFiles\\settings/setting.js";

    }

    public class Interfaces {
        public static final String client1 = "auto lo\niface lo inet loopback\n\nallow-hotplug wlan0\niface wlan0 inet static\naddress 192.168.0.";
        public static final String client2 = "\nnetmask 255.255.255.0\ngateway 192.168.0.1\nwpa-conf /etc/wpa_supplicant/wpa_supplicant.conf\n";
        public static final String serverEthernet = "auto lo\niface lo inet loopback\n\nauto eth0\nallow-hotplug eth0\niface eth0 inet static\naddress 192.168.0.200\nnetmask 255.255.255.0\n";
        public static final String dynamicInterfaces = serverEthernet
                + "\n\nallow-hotplug wlan0\niface wlan0 inet dhcp\n";
        //public static final String serverWifi = "auto lo\niface lo inet loopback\n\nauto wlan0\nallow-hotplug wlan0\niface wlan0 inet static\naddress 192.168.0.200\nnetmask 255.255.255.0\nwpa-conf /etc/wpa_supplicant/wpa_supplicant.conf";
        public static final String serverWifi = "auto lo\niface lo inet loopback\nauto wlan0\nallow-hotplug wlan0\niface wlan0 inet static\naddress 192.168.0.200\nnetmask 255.255.255.0\n";
        public static final String dynamicWlan = "auto lo\n\niface lo inet loopback\n\nallow-hotplug wlan0\niface wlan0 inet dhcp\nwpa-conf /etc/wpa_supplicant/wpa_supplicant.conf";
        public static final String serverWifiWithRounter = serverWifi + "network 192.168.0.0\nbroadcast 192.168.0.255\ngateway 192.168.0.1\nwpa-conf /etc/wpa_supplicant/wpa_supplicant.conf";
    }

    public class Modes {
        public static final int server = 0;
        public static final int client = 1;
        public static final int unknown = 2;
    }

}
