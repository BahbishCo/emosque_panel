import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.Date;

class Utils {

	public static Runtime rt = Runtime.getRuntime();

	public static void writeFile(String path, String data) {
		File file;
		FileWriter fw = null;
		BufferedWriter bw;
		file = new File(path);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			fw = new FileWriter(file.getAbsoluteFile());
			bw = new BufferedWriter(fw);
			bw.write("" + data);
			bw.close();
		} catch (IOException e) {
			Log.err("failed to write to " + path);
		}
	}

	public static String readFile(String path) {
		String content = null;
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		try {
			br = new BufferedReader(new FileReader(path));
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			content = sb.toString();
			br.close();
		} catch (FileNotFoundException e) {
			Log.err("Failed to find the file: " + path);
		} catch (IOException e) {
			Log.err("failed to read content of file: " + path);
		}
		return content;
	}

}

public class IPHandler {
	public static int piMode;

	public static void main(String[] a) {

		try {
			Utils.rt.exec(Constants.Commands.disableBrodcastingScript).waitFor();
			Utils.rt.exec(Constants.Commands.minimizeWindows).waitFor();
			IPHandlerOperations.unknownMode();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class IPHandlerOperations extends Thread {

	public static boolean isEhtConnected = false;

	public static boolean wlan0_present = new File(Constants.Locations.LOC_WLAN0_PATH).exists();

	public static void updateStatusFile(byte status) {
		Log.out("updating the status file to: " + String.valueOf(status));
		Utils.writeFile(Constants.Locations.LOC_STAT_FILE, String.valueOf(status));
	}

	public static int getPiVersion() {
		int version = -1;

		String piVersionFileAsString = Utils.readFile(Constants.Locations.piVersionPath);

		String[] piVersionFileAsLineArray = piVersionFileAsString.split("\\n");
		String paramName, paramValue;
		for (int i = 0; i < piVersionFileAsLineArray.length; i++) {
			if (piVersionFileAsLineArray[i].length() == 0) {
				continue;
			}
			paramName = piVersionFileAsLineArray[i].split(":")[0].split(" ")[0];
			paramName = paramName.substring(0, paramName.length() - 1);
			paramValue = piVersionFileAsLineArray[i].split(":")[1].split(" ")[1];
			// paramValue = paramValue.substring(0, paramValue.length() - 1);
			if (paramName.equals(Constants.PI_Version.keyName)) {
				Log.out(paramValue);
				if (paramValue.equals(Constants.PI_Version.PiZeroKeyValue)) {
					version = Constants.PI_Version.PiZeroVersion;

				} else if (paramValue.equals(Constants.PI_Version.PiTwoKeyValue_1)
						|| paramValue.equals(Constants.PI_Version.PiTwoKeyValue_2)) {
					version = Constants.PI_Version.PiTwoVersion;

				} else if (paramValue.equals(Constants.PI_Version.PiThreeKeyValue_1)
						|| paramValue.equals(Constants.PI_Version.PiThreeKeyValue_2)) {
					version = Constants.PI_Version.PiThreeVersion;
				}
			}
		}
		return version;
	}

	public static boolean isTempSensorConnected() {
		boolean isConnected = false;
		String cmd = Constants.Commands.ListUSB;
		Runtime rt = Runtime.getRuntime();
		Process proc;
		try {
			proc = rt.exec(cmd);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String output = "";
			String line;
			while ((line = stdInput.readLine()) != null) {
				output = output + line + "\n";
			}
			isConnected = findTempSensorFromString(output);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return isConnected;
	}

	public static boolean isWifiAdapterConnected() {
		boolean isConnected = false;
		String cmd = Constants.Commands.ListUSB;
		Runtime rt = Runtime.getRuntime();
		Process proc;
		try {
			proc = rt.exec(cmd);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String output = "";
			String line;
			while ((line = stdInput.readLine()) != null) {
				output = output + line + "\n";
			}
			isConnected = findWifiAdapterFromString(output);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return isConnected;
	}

	public static boolean findWifiAdapterFromString(String str) {
		String[] strArray = str.split(" ");
		for (int i = 0; i < strArray.length; i++) {
			if (strArray[i].equals(Constants.wifiAdapterIDs_black)
					|| strArray[i].equals(Constants.wifiAdapterIDs_white)) {
				return true;
			}
		}
		return false;
	}

	public static boolean findTempSensorFromString(String str) {
		String[] strArray = str.split(" ");
		for (int i = 0; i < strArray.length; i++) {
			if (strArray[i].equals(Constants.tempSensorIDs)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isEthernetCableAttached() {
		String eth0 = Utils.readFile(Constants.Locations.LOC_ETH0_STAT);
		return eth0 == null ? false : Integer.parseInt(eth0.trim()) == 1;
	}

	public static int readPiMode() {
		String eth0 = Utils.readFile(Constants.Locations.nextPiConfigMode);
		return Integer.parseInt(eth0.trim());
	}

	public static void writePiMode(int mode) {
		Utils.writeFile(Constants.Locations.nextPiConfigMode, mode + "");
	}

	public static void updateInterfacesFile(boolean isServer) {
		String contentOfInterfaces;
		int rand = (int) (100 + (Math.random() * ((199 - 100) + 1)));
		if (isServer)
			contentOfInterfaces = Constants.Interfaces.staticEthernet;
		else
			contentOfInterfaces = Constants.Interfaces.client1 + rand + Constants.Interfaces.client2;
		Utils.writeFile(Constants.Locations.LOC_INTERFACES, contentOfInterfaces);
	}

	public static void disableEth() {
		try {
			Log.out("#Disabling Ethernet");
			executeCmd(Constants.Commands.IFDOWN_ETH0);
			executeCmd(Constants.Commands.restartSystemctl);
			executeCmd(Constants.Commands.reloadNetworksInterfaces);
			// Utils.rt.exec(Constants.Commands.IFDOWN_ETH0).waitFor();
		} catch (Exception e) {
			Log.err("failed shuting down interfaces");
		}
	}

	public static void disableWlan() {
		try {
			Log.out("#Disabling Ethernet");
			executeCmd(Constants.Commands.IFDOWN_WLAN0);
			executeCmd(Constants.Commands.restartSystemctl);
			executeCmd(Constants.Commands.reloadNetworksInterfaces);
			// Utils.rt.exec(Constants.Commands.IFDOWN_WLAN0).waitFor();
		} catch (Exception e) {
			Log.err("failed shuting down interfaces");
		}
	}

	public static void enableEth() {
		try {
			Log.out("#Enabling Ethernet");
			executeCmd(Constants.Commands.IFDOWN_ETH0);
			executeCmd(Constants.Commands.IFUP_ETH0);

		} catch (Exception e) {
			Log.err("failed shuting down interfaces");
		}
	}

	public static void enableWlan() {
		try {
			Log.out("#Enabling Wlan0");
			executeCmd(Constants.Commands.IFDOWN_WLAN0);
			sleep(2000);
			executeCmd(Constants.Commands.IFUP_WLAN0);

		} catch (Exception e) {
			Log.err("failed shuting down interfaces");
		}
	}

	public static void shutDownInterfaces() {
		try {
			Log.out("#Disabling Interfaces");
			// Utils.rt.exec(Constants.Commands.IFDOWN_ETH0).waitFor();
			// Utils.rt.exec(Constants.Commands.IFDOWN_WLAN0).waitFor();
			executeCmd(Constants.Commands.IFDOWN_ETH0);
			executeCmd(Constants.Commands.IFDOWN_WLAN0);
		} catch (Exception e) {
			Log.err("failed shuting down interfaces");
		}
	}

	public static void activateInterfaces(boolean isServer) {
		try {
			Log.out("#Enabling Interfaces");
			executeCmd(Constants.Commands.IFDOWN_ETH0);
			executeCmd(Constants.Commands.IFDOWN_WLAN0);

			if (true) {
				executeCmd(Constants.Commands.IFUP_ETH0);
				// Utils.rt.exec(Constants.Commands.IFDOWN_ETH0).waitFor();
				// Utils.rt.exec(Constants.Commands.IFUP_ETH0).waitFor();
			}
			if (true) {
				executeCmd(Constants.Commands.IFUP_WLAN0);
				// Utils.rt.exec(Constants.Commands.IFDOWN_WLAN0).waitFor();
				// Utils.rt.exec(Constants.Commands.IFUP_WLAN0).waitFor();
			}

			// Utils.rt.exec(Constants.Commands.reloadNetworksInterfaces).waitFor();

		} catch (Exception e) {
			Log.err("failed shuting down interfaces");
		}
	}

	public static Process executeCmd(String cmd) {
		try {
			Log.out("********************************************");
			Process proc = Utils.rt.exec(cmd);
			proc.waitFor();
			printProcessOutput(proc, cmd);
			Log.out("============================================");
			return proc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean isValidIP(byte lastDigit) {
		boolean isRechable = false;
		for (int i = 0; i < 5; i++) {
			try {

				sleep(1000);
				InetAddress inet;
				inet = InetAddress.getByAddress(new byte[] { (byte) 192, (byte) 168, 0, lastDigit });
				isRechable = inet.isReachable(5000);
				Log.out("Sending Ping Request to " + inet + (isRechable ? ": Sucessed" : ": Failed"));
				if (isRechable) {
					break;
				}

			} catch (Exception e) {
				isRechable = false;
				Log.out("Exception, Sending Ping Request to " + lastDigit + (isRechable ? ": Sucessed" : ": Failed"));
			}
		}
		return isRechable;
	}

	public static void serverMode(boolean withWifiBrodcasting) {
		writePiMode(Constants.Modes.unknown);
		new Thread() {
			public void run() {
				try {
					// IPHandlerOperations.shutDownInterfaces();
					executeCmd(Constants.Commands.enableBrodcastingScript);
					Utils.rt.exec(Constants.Commands.startServerMode);
					IPHandler.piMode = Constants.Modes.server;

					if (withWifiBrodcasting) {
						sleep(15 * 1000);
						// IPHandlerOperations.activateInterfaces(false);
//						executeCmd(Constants.Commands.enableBrodcastingScript);

					} else {

						// Utils.rt.exec(Constants.Commands.disableBrodcastingScript);
						sleep(15 * 1000);
						// IPHandlerOperations.activateInterfaces(false);
					}

					new SubmitClientInfo(IPHandler.piMode).start();

				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}

		}.start();

	}

	public static void clientMode() {
		writePiMode(Constants.Modes.unknown);
		new Thread() {
			public void run() {
				try {

					// Utils.rt.exec(Constants.Commands.disableBrodcastingScript);

					sleep(3 * 1000);
					// IPHandlerOperations.shutDownInterfaces();
					// IPHandlerOperations.activateInterfaces(false);

					sleep(10 * 1000);
					Utils.rt.exec(Constants.Commands.startClientMode);
					IPHandler.piMode = Constants.Modes.client;
					new SubmitClientInfo(IPHandler.piMode).start();
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	public static void printProcessOutput(Process proc, String cmd) {
		try {
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

			// read the output from the command

			System.out.println("Executing Command : " + cmd);
			System.out.println("Here is the standard output of the command:\n");
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String getProcessOutput(Process proc) {
		String out = "";

		try {
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

			// read the output from the command
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				out = out + s;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}

	public static void unknownMode() {

		int piVersion = IPHandlerOperations.getPiVersion();
		if (piVersion == -1 || piVersion == Constants.PI_Version.PiZeroVersion) {
			Log.out("Pi Version Is Not Supported");
			return;
		}
		boolean isEthernetCableAttached = IPHandlerOperations.isEthernetCableAttached();
		isEhtConnected = isEthernetCableAttached;
		boolean isTempSensorConnected = IPHandlerOperations.isTempSensorConnected();
		boolean isWifiAdapterConnected = IPHandlerOperations.isWifiAdapterConnected();
		int userDefinedMode = getUserMode();

		Log.out("# Your Pi Is: "
				+ (piVersion == Constants.PI_Version.PiTwoVersion ? "Raspberry Pi 2" : "Raspberry Pi 3"));
		Log.out("# is Ethernet Conneted: " + (isEthernetCableAttached ? "YES." : "NO."));
		Log.out("# is Temp Sensor Conneted: " + (isTempSensorConnected ? "YES." : "NO."));
		Log.out("# is Wifi Adapter Conneted: " + (isWifiAdapterConnected ? "YES." : "NO."));
		System.out.print("# user selected mode is: ");
		switch (userDefinedMode) {
		case Constants.Modes.primaryEthMode:
			System.out.println(Constants.Modes.primaryEthModeStr);
			primaryEthMode();
			try {
				sleep(5000);
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}
			Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.primaryEth);
			break;
		case Constants.Modes.primaryWifiMode:
			System.out.println(Constants.Modes.primaryWifiModeStr);
			primaryWifiMode();
			try {
				sleep(5000);
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}
			Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.primaryWifi);
			break;
		case Constants.Modes.primaryHotspotMode:
			System.out.println(Constants.Modes.primaryHotspotModeStr);
			primaryHotspotMode();
			try {
				sleep(5000);
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}
			Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.primaryHotspot);
			break;
		case Constants.Modes.secondaryEthMode:
			System.out.println(Constants.Modes.secondaryEthModeStr);
			try {
				sleep(15000);
			} catch (Exception e) {
			}
			secondaryEthMode();
			try {
				sleep(5000);
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}
			Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.secondaryEth);
			break;
		case Constants.Modes.secondaryWifiMode:
			System.out.println(Constants.Modes.secondaryWifiModeStr);
			try {
				sleep(20000);
			} catch (Exception e) {
			}
			secondaryWifiMode();
			try {
				sleep(5000);
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}
			Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.secondaryWifi);
			break;
		default:
			break;
		}
		if (true) {
			return;
		}
		/*********************************************************************/
		/* Raspberry Pi 2 */
		/*********************************************************************/

		if (piVersion == Constants.PI_Version.PiTwoVersion) {
			if (isEthernetCableAttached) {
				Log.out("# Pi Is Server with Ethernet Connection.");

				Log.out("*******************************************************");
				Log.out("network str = " + Constants.Interfaces.staticEthernet);
				Log.out("*******************************************************");

				IPHandlerOperations.shutDownInterfaces();
				Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.server + "");
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.staticEthernet);

				IPHandlerOperations.activateInterfaces(false);
				Utils.writeFile(Constants.Locations.piModeFile, "isServer = true;");
				serverMode(false);
			} else if (isTempSensorConnected && isWifiAdapterConnected) {
				Log.out("# Pi Is Server with Wifi Connection.");
				IPHandlerOperations.shutDownInterfaces();
				Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.server + "");
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.serverWifi);

				Log.out("*******************************************************");
				Log.out("network str = " + Constants.Interfaces.serverWifi);
				Log.out("*******************************************************");

				IPHandlerOperations.activateInterfaces(false);
				Utils.writeFile(Constants.Locations.piModeFile, "isServer = true;");
				serverMode(true);
			} else {
				IPHandlerOperations.shutDownInterfaces();
				Utils.writeFile(Constants.Locations.piModeFile, "isServer = false;");
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.dynamicWlan);

				Log.out("*******************************************************");
				Log.out("network str = " + Constants.Interfaces.dynamicWlan);
				Log.out("*******************************************************");

				Log.out("Pinging Server: Dynamic Wlan0");
				IPHandlerOperations.shutDownInterfaces();
				IPHandlerOperations.activateInterfaces(false);
				try {
					sleep(3000);
					boolean isServerReachable = isValidIP((byte) (200));
					if (isServerReachable) {
						int rand = (int) (10 + (Math.random() * (50)));
						IPHandlerOperations.shutDownInterfaces();
						Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.client + "");
						Utils.writeFile(Constants.Locations.LOC_INTERFACES,
								Constants.Interfaces.client1 + rand + Constants.Interfaces.client2);

						Log.out("*******************************************************");
						Log.out("network str = " + Constants.Interfaces.client1 + rand + Constants.Interfaces.client2);
						Log.out("*******************************************************");

						Log.out("Writing Static Wlan0 Interface");
						IPHandlerOperations.shutDownInterfaces();
						IPHandlerOperations.activateInterfaces(false);
						SyncPanelsOperations.syncPanels();
						clientMode();
						Log.out("# Pi Is Client -> Connected To Server.");
						Log.out("Wlan0 Ip Adress is: 192.168.0." + rand);
						new MidnightRebootThread().start();
					} else {

						Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.server + "");
						int rand = (int) (10 + (Math.random() * (50)));
						IPHandlerOperations.shutDownInterfaces();
						Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.client + "");
						Utils.writeFile(Constants.Locations.LOC_INTERFACES,
								Constants.Interfaces.client1 + rand + Constants.Interfaces.client2);

						Log.out("*******************************************************");
						Log.out("network str = " + Constants.Interfaces.client1 + rand + Constants.Interfaces.client2);
						Log.out("*******************************************************");

						Log.out("Writing Static Wlan0 Interface");
						IPHandlerOperations.shutDownInterfaces();
						serverMode(false);
						new Thread() {
							public void run() {
								try {
									sleep(15 * 1000);
									IPHandlerOperations.activateInterfaces(false);
									new ServerPolling().start();
									Log.out("# Start Pi as Server.");
									Log.out("# Pi Is Client -> Wainting To Server.");
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}.start();
					}

				} catch (Exception e) {
					Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.server + "");
					IPHandlerOperations.shutDownInterfaces();

					int rand = (int) (10 + (Math.random() * (50)));
					Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.client + "");
					Utils.writeFile(Constants.Locations.LOC_INTERFACES,
							Constants.Interfaces.client1 + rand + Constants.Interfaces.client2);

					Log.out("*******************************************************");
					Log.out("network str = " + Constants.Interfaces.client1 + rand + Constants.Interfaces.client2);
					Log.out("*******************************************************");

					Log.out("Writing Static Wlan0 Interface");

					IPHandlerOperations.shutDownInterfaces();
					serverMode(false);
					IPHandlerOperations.activateInterfaces(false);
					Log.out("# Start Pi as Server.");
					Log.out("# Pi Is Client -> Wainting To Server.");
					new ServerPolling().start();
					e.printStackTrace();
				}
			}
		}

		/*********************************************************************/
		/* Raspberry Pi 3 */
		/*********************************************************************/

		else if (piVersion == Constants.PI_Version.PiThreeVersion) {

			if (isEthernetCableAttached && isTempSensorConnected) {

				Log.out("# Pi Is Server with Ethernet Connection.");
				Utils.writeFile(Constants.Locations.piModeFile, "isServer = true;");
				Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.server + "");

				Log.err("*******************************************************");
				Log.err("network str = " + Constants.Interfaces.staticEthernet);
				Log.err("*******************************************************");

				IPHandlerOperations.shutDownInterfaces();
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.staticEthernet);
				executeCmd(Constants.Commands.restartSystemctl);
				executeCmd(Constants.Commands.reloadNetworksInterfaces);
				IPHandlerOperations.enableEth();
				serverMode(false);

			} else if (isTempSensorConnected) {
				try {
					Log.out("# Pi Is Server with Wifi Connection.");
					Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.server + "");
					IPHandlerOperations.shutDownInterfaces();
					Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.wlanMultiNetwork);
					executeCmd(Constants.Commands.restartSystemctl);
					executeCmd(Constants.Commands.reloadNetworksInterfaces);
					IPHandlerOperations.enableWlan();
					sleep(5000);
					Log.out("Pinging router: Dynamic Wlan0");
					boolean isRounterReachable = isValidIP((byte) (1));
					Log.out("# Router is " + (isRounterReachable ? "" : "Not ") + "Reachable");

					if (isRounterReachable) {
						Utils.writeFile(Constants.Locations.piModeFile, "isServer = true;");
						serverMode(false);
					} else if (!isRounterReachable && isEmosqueNetworkInRange()) {
						executeCmd(Constants.Commands.reboot);
					} else {
						IPHandlerOperations.shutDownInterfaces();
						Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.serverWifi);
						executeCmd(Constants.Commands.restartSystemctl);
						executeCmd(Constants.Commands.reloadNetworksInterfaces);
						IPHandlerOperations.enableWlan();
						Log.out("*******************************************************");
						Log.out("network str = " + Constants.Interfaces.serverWifi);
						Log.out("*******************************************************");
						Utils.writeFile(Constants.Locations.piModeFile, "isServer = true;");
						serverMode(true);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				Utils.writeFile(Constants.Locations.piModeFile, "isServer = false;");
				IPHandlerOperations.shutDownInterfaces();
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.dynamicWlan);
				executeCmd(Constants.Commands.restartSystemctl);
				executeCmd(Constants.Commands.reloadNetworksInterfaces);
				IPHandlerOperations.enableWlan();
				try {
					sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Log.out("*******************************************************");
				Log.out("network str = " + Constants.Interfaces.dynamicWlan);
				Log.out("*******************************************************");

				Log.out("Pinging Server: Dynamic Wlan0");
				try {
					boolean isServerReachable = isValidIP((byte) (200));
					if (isServerReachable) {
						SyncPanelsOperations.syncPanels();
						clientMode();
						Log.out("# Pi Is Client -> Connected To Server.");
						Log.out("Wlan0 Ip Adress is: 192.168.0.");
						new MidnightRebootThread().start();
					} else {
						serverMode(false);
						new Thread() {
							public void run() {
								try {
									sleep(15 * 1000);
									// IPHandlerOperations.activateInterfaces(false);
									new ServerPolling().start();
									Log.out("# Start Pi as Server.");
									Log.out("# Pi Is Client -> Wainting To Server.");
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}.start();
					}

				} catch (Exception e) {

					serverMode(false);
					Log.out("# Start Pi as Server.");
					Log.out("# Pi Is Client -> Wainting To Server.");
					new ServerPolling().start();
					e.printStackTrace();
				}
			}
		}
	}

	private static void secondaryWifiMode() {
		String currentConfig = Utils.readFile(Constants.Locations.LOC_INTERFACES);
		String requestedConfig = Constants.Interfaces.secondaryWifi;
		if (getPiVersion() == Constants.PI_Version.PiThreeVersion) {
			boolean isAlreadyConfigured = isSameConfig(currentConfig, requestedConfig);
			if (!isAlreadyConfigured) {
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.secondaryWifi);
				executeCmd(Constants.Commands.reboot);
			}
			// IPHandlerOperations.enableWlan();
			boolean isServerReachable = isValidIP((byte) (200));
			if (isServerReachable) {
				Utils.writeFile(Constants.Locations.piModeFile, "isServer = false;");
				Log.out("# Pi Is Client(Wlan) -> Connected To Server.");
//				SyncPanelsOperations.syncPanels();
				clientMode();
				new MidnightRebootThread().start();
			} else {
				primaryHotspotMode();
			}
		} else if (getPiVersion() == Constants.PI_Version.PiTwoVersion) {

		}
	}

	private static void secondaryEthMode() {
		String currentConfig = Utils.readFile(Constants.Locations.LOC_INTERFACES);
		String requestedConfig = Constants.Interfaces.secondaryEth;
		if (getPiVersion() == Constants.PI_Version.PiThreeVersion) {
			boolean isAlreadyConfigured = isSameConfig(currentConfig, requestedConfig);
			if (!isAlreadyConfigured) {
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.secondaryEth);
				executeCmd(Constants.Commands.reboot);
			}
			if (isEhtConnected) {
				boolean isServerReachable = isValidIP((byte) (200));
				if (isServerReachable) {
					Utils.writeFile(Constants.Locations.piModeFile, "isServer = false;");
					Log.out("# Pi Is Client(Eth) -> Connected To Server.");
//					SyncPanelsOperations.syncPanels();
					clientMode();
					new MidnightRebootThread().start();
				} else {
					primaryHotspotMode();
				}
			} else {
				primaryHotspotMode();
			}
		} else if (getPiVersion() == Constants.PI_Version.PiTwoVersion) {

		}
	}

	private static void primaryHotspotMode() {
		String currentConfig = Utils.readFile(Constants.Locations.LOC_INTERFACES);
		String requestedConfig = Constants.Interfaces.primaryHotspot;
		if (getPiVersion() == Constants.PI_Version.PiThreeVersion) {
			boolean isAlreadyConfigured = isSameConfig(currentConfig, requestedConfig);
			if (!isAlreadyConfigured) {
				IPHandlerOperations.shutDownInterfaces();
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.primaryHotspot);
				executeCmd(Constants.Commands.restartSystemctl);
				executeCmd(Constants.Commands.reloadNetworksInterfaces);
				IPHandlerOperations.enableWlan();
			}
			Utils.writeFile(Constants.Locations.piModeFile, "isServer = true;");
			Log.out("# Pi Is Server with Hotspot.");
			serverMode(true);

		} else if (getPiVersion() == Constants.PI_Version.PiTwoVersion) {

		}
	}

	private static void primaryWifiMode() {
		String currentConfig = Utils.readFile(Constants.Locations.LOC_INTERFACES);
		String requestedConfig = Constants.Interfaces.primaryWifi;
		if (getPiVersion() == Constants.PI_Version.PiThreeVersion) {
			boolean isAlreadyConfigured = isSameConfig(currentConfig, requestedConfig);
			if (!isAlreadyConfigured) {
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.primaryWifi);
				executeCmd(Constants.Commands.reboot);
			}
			// IPHandlerOperations.enableWlan();
			boolean isRounterReachable = isValidIP((byte) (1));
			if (isRounterReachable) {
				Log.out("# Pi Is Server with Wifi Connection.");
				Utils.writeFile(Constants.Locations.piModeFile, "isServer = true;");
				Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.server + "");
				serverMode(false);
			} else if (isEmosqueNetworkInRange()) {
				executeCmd(Constants.Commands.reboot);
			} else {
				primaryHotspotMode();
			}
		} else if (getPiVersion() == Constants.PI_Version.PiTwoVersion) {

		}
	}

	private static void primaryEthMode() {

		String currentConfig = Utils.readFile(Constants.Locations.LOC_INTERFACES);
		String requestedConfig = Constants.Interfaces.primaryEth;
		if (getPiVersion() == Constants.PI_Version.PiThreeVersion) {
			boolean isAlreadyConfigured = isSameConfig(currentConfig, requestedConfig);
			if (!isAlreadyConfigured) {
				Utils.writeFile(Constants.Locations.LOC_INTERFACES, Constants.Interfaces.primaryEth);
				executeCmd(Constants.Commands.reboot);
			}
			if (isEhtConnected) {
				Log.out("# Pi Is Server with Ethernet Connection.");
				Utils.writeFile(Constants.Locations.piModeFile, "isServer = true;");
				Utils.writeFile(Constants.Locations.nextPiConfigMode, Constants.Modes.server + "");
				serverMode(false);
			} else {
				primaryHotspotMode();
			}
		} else if (getPiVersion() == Constants.PI_Version.PiTwoVersion) {

		}

	}

	private static boolean isSameConfig(String currentConfig, String requestedConfig) {
		String curlines[] = currentConfig.split(System.lineSeparator());
		String reqLines[] = requestedConfig.split("\n");
		// int count = curlines.length > reqLines.length ? curlines.length :
		// reqLines.length;
		// for (int i = 0; i < count; i++) {
		// if (i < curlines.length) {
		// System.out.print(curlines[i] + " ; ");
		// }
		// if (i < reqLines.length) {
		// System.out.print(reqLines[i]);
		// }
		// System.out.println();
		//
		// }

		if (curlines.length == reqLines.length) {
			for (int i = 0; i < curlines.length; i++) {
				if (!curlines[i].equals(reqLines[i])) {
					return false;
				}
			}
			return true;
		}

		return false;
	}

	private static int getUserMode() {

		File file = new File(Constants.Locations.userSelectedMode);
		if (!file.exists()) {
			Utils.writeFile(Constants.Locations.userSelectedMode, "");
		}

		String str = Utils.readFile(Constants.Locations.userSelectedMode);
		String lines[] = str.split(System.lineSeparator());
		String l;
		String temp[];
		int userMode = Constants.Modes.primaryHotspotMode;
		boolean isConfigured = false;
		for (int i = 0; i < lines.length; i++) {
			l = lines[i];
			if (l.contains("#") || !l.contains("=")) {
				continue;
			}
			temp = l.split("=");
			if (temp.length != 2) {
				continue;
			}
			if (temp[0].equals("mode")) {
				try {
					userMode = Integer.parseInt(temp[1].trim());
					isConfigured = true;
				} catch (Exception e) {

				}
			}
		}

		if (!isConfigured) {
			if (isEhtConnected) {
				userMode = Constants.Modes.primaryEthMode;
				writeUserMode(Constants.Modes.primaryEthMode);
			} else {
				userMode = Constants.Modes.primaryHotspotMode;
				writeUserMode(Constants.Modes.primaryHotspotMode);
			}
			executeCmd("sudo chmod 777 " + Constants.Locations.userSelectedMode);
		}
		return userMode;
	}

	private static void writeUserMode(int mode) {
		String str = "";
		switch (mode) {
		case Constants.Modes.primaryEthMode:
			str = str + "mode=" + Constants.Modes.primaryEthMode + System.lineSeparator();
			str = str + "description=" + Constants.Modes.primaryEthModeStr + System.lineSeparator();
			break;
		case Constants.Modes.primaryWifiMode:
			str = str + "mode=" + Constants.Modes.primaryWifiMode + System.lineSeparator();
			str = str + "description=" + Constants.Modes.primaryWifiModeStr + System.lineSeparator();
			break;
		case Constants.Modes.primaryHotspotMode:
			str = str + "mode=" + Constants.Modes.primaryHotspotMode + System.lineSeparator();
			str = str + "description=" + Constants.Modes.primaryHotspotModeStr + System.lineSeparator();
			break;
		case Constants.Modes.secondaryEthMode:
			str = str + "mode=" + Constants.Modes.secondaryEthMode + System.lineSeparator();
			str = str + "description=" + Constants.Modes.secondaryEthModeStr + System.lineSeparator();
			break;
		case Constants.Modes.secondaryWifiMode:
			str = str + "mode=" + Constants.Modes.secondaryWifiMode + System.lineSeparator();
			str = str + "description=" + Constants.Modes.secondaryWifiModeStr + System.lineSeparator();
			break;
		default:
			break;
		}

		if (!str.equals("")) {
			Utils.writeFile(Constants.Locations.userSelectedMode, str);
		}

	}

	private static boolean isEmosqueNetworkInRange() {
		Process process = executeCmd(Constants.Commands.scanForNetworks);
		String out = getProcessOutput(process);
		if (out.contains("eMosque")) {
			return true;
		}
		return false;
	}

	private static int readPrevConfig() {

		return 0;
	}
}

class ServerPolling extends Thread {
	public void run() {
		while (true) {

			try {
				boolean isServerReachable = IPHandlerOperations.isValidIP((byte) (200));
				if (isServerReachable) {

					Log.out("192.168.0.200 is reachable");
					Utils.rt.exec(Constants.Commands.reboot);
				}

				sleep(10 * 1000);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

class MidnightRebootThread extends Thread {
	public void run() {

		while (true) {
			Date now = new Date();
			@SuppressWarnings("deprecation")
			int nowInMinutes = now.getHours() * 60 + now.getMinutes();

			if (nowInMinutes == 0) {
				try {
					Utils.rt.exec(Constants.Commands.reboot);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					sleep(30 * 1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}
}

class Log {

	public static void out(String msg) {
		if (!Constants.release)
			System.out.println(msg);
	}

	public static void err(String msg) {
		if (!Constants.release)
			System.err.println(msg);
	}

}
