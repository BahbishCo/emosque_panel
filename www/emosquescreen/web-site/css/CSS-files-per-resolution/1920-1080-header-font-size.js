/*
    This CSS file is used to set the font size of all elements for different resolutions

*/


/**************************************/
/*           (1024 * 768)             */
/**************************************/

dateElementFontSize = {
	meladiDate: "100px",
	hijriDate: "64px",
	miladiArabicWord: "65px",
	miladiArabicWordMargin: "40px",
  headerMiladiArabicWord: "80px",
        headerHijriArabicWord: "70px",
        tempFontSize: "70px"
}



