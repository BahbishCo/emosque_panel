var fajrIqamaTime = document.getElementById("fajrIqamaTime");
var duhurIqamaTime = document.getElementById("duhurIqamaTime");
var asrIqamaTime = document.getElementById("asrIqamaTime");
var maghribIqamaTime = document.getElementById("maghribIqamaTime");
var ishaaIqamaTime = document.getElementById("ishaaIqamaTime");
var jumaaIqamaTime = document.getElementById("jumaaIqamaTime");


var fajrAzkarDisplayPeriod = document.getElementById("fajrAzkarDisplayPeriod");
var duhurAzkarDisplayPeriod = document.getElementById("duhurAzkarDisplayPeriod");
var asrAzkarDisplayPeriod = document.getElementById("asrAzkarDisplayPeriod");
var maghribAzkarDisplayPeriod = document.getElementById("maghribAzkarDisplayPeriod");
var ishaaAzkarDisplayPeriod = document.getElementById("ishaaAzkarDisplayPeriod");
var jumaaAzkarDisplayPeriod = document.getElementById("jumaaAzkarDisplayPeriod");


var beforFajrAzkarPeriod = document.getElementById("beforFajrAzkarPeriod");
var beforduhurAzkarPeriod = document.getElementById("beforduhurAzkarPeriod");
var beforAsrAzkarPeriod = document.getElementById("beforAsrAzkarPeriod");
var beforMaghribAzkarPeriod = document.getElementById("beforMaghribAzkarPeriod");
var beforIshaaAzkarPeriod = document.getElementById("beforIshaaAzkarPeriod");
var beforJumaaAzkarPeriod = document.getElementById("beforJumaaAzkarPeriod");


initIqamaSelector(120);
initAzkarDisplayPeriod(0,120);
initBeforeAzkarPeriod(0,120);

function initIqamaSelector(maxTime)
{
    for(var i = 0; i <= maxTime; i++)
    {
        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        fajrIqamaTime.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        duhurIqamaTime.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        asrIqamaTime.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        maghribIqamaTime.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        ishaaIqamaTime.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        jumaaIqamaTime.add(option);
    }
}



function initAzkarDisplayPeriod(minTime,maxTime)
{
    for (var i = minTime; i <= maxTime; i++)
    {
        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        fajrAzkarDisplayPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        duhurAzkarDisplayPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        asrAzkarDisplayPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        maghribAzkarDisplayPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        ishaaAzkarDisplayPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        jumaaAzkarDisplayPeriod.add(option);
    }
}

function initBeforeAzkarPeriod(minTime, maxTime)
{
    for (var i = minTime; i <= maxTime; i++)
    {
        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        beforFajrAzkarPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        beforduhurAzkarPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        beforAsrAzkarPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        beforMaghribAzkarPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        beforIshaaAzkarPeriod.add(option);

        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        beforJumaaAzkarPeriod.add(option);
    }
}
