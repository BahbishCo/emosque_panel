var screenSetting =
{
prayerSetting:
{
country: "Jordan",
city: "Amman",
calculationMethod: "Karachi",
asrJuristic: "Standard",
adjusthighLat: "AngleBased",
midNightMethod: "Standard",
fajrAngle: 18,
ishaAngle: 18,
latitude: 31.971979,
longitude: 35.832527,
elevation: 1100,
timeZone: +2.0,
offset: {fajrOffset: 0, duhurOffset: 0, asrOffset: 0, maghribOffset: 0, ishaaOffset: 0, },
},
hijriCorrection: 0,
timeBetweenAzanAndIqama: {fajr: 30, duhur: 18, asr: 18, maghrib: 10, ishaa: 23, jumaa: 50, },
timeBetweenAzanAndAzkar: {fajr: 36, duhur: 26, asr: 26, maghrib: 17, ishaa: 32, jumaa: 50, },
azkarDisplayPeriod: {fajr: 10, duhur: 10, asr: 10, maghrib: 10, ishaa: 10, jumaa: 10, },
appQREnable: false,
internalTempEnable: true,
playAzan: false,
}
