<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 8/2/2016
 * Time: 4:32 AM
 */
$killMidori = "sudo pkill midori";
$killFirefox = "sudo pkill iceweasel";
$cssFolderPath = dirname(dirname(dirname(getcwd()))) . "/web-site/customFiles/CSS/";
$backgroundColorPath = $cssFolderPath . "background.css";
$mainTextColorPath = $cssFolderPath . "main-text-color.css";
$hadeethColorPath = $cssFolderPath . "hadeethColor.css";
$azkarColorPath = $cssFolderPath . "azkarColor.css";
$secondaryMsgColorPath = $cssFolderPath . "secondaryMsgColor.css";
$primaryMsgColorPath = $cssFolderPath . "primaryMsgColor.css";
$appAdColorPath = $cssFolderPath . "app-ad-color.css";
$currentColorJSFile = $cssFolderPath . "currentColors.js";

$colorType = 0;
$imgType = 1;
$stringToWrite = "";
$backgroundType = $_POST["backgroundType"];
if ($backgroundType == $colorType) {
    $stringToWrite = $stringToWrite . "body { " . "\n" .
        "background-color:" . $_POST["backgroundColor"] . ";" . "\n"
        . "}";
    writeFile($stringToWrite, $backgroundColorPath);
    $stringToWrite = "";

    $stringToWrite = $stringToWrite . "  body {
	color:" . $_POST["mainTextColor"] . ";" . "\n"
        . "}" . "\n" .
        ".headerEl{" . "\n" .
        "color:" . $_POST["headerElColor"] . ";" . "\n" .
        "}" . "\n" .
        ".timeOutEl{" . "\n" .
        "color:" . $_POST["timeoutElColor"] . ";" . "\n" .
        "}" .
        ".prayerElTimes{" . "\n" .
        "color:" . $_POST["prayerTimesColor"] . ";" . "\n" .
        "}" . "\n" .
        ".prayerElNames{" . "\n" .
        "color:" . $_POST["prayerNamesColor"] . ";" . "\n" .
        "}" . "\n" .
        "#internalTemp{" . "\n" .
        "color:" . $_POST["internalTempColor"] . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $mainTextColorPath);
    $stringToWrite = "";

    $stringToWrite = "body{" . "\n" .
        "color:" . $_POST["hadeethColor"] . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $hadeethColorPath);

    $stringToWrite = "body{" . "\n" .
        "color:" . $_POST["azkarColor"] . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $azkarColorPath);

    $stringToWrite = "body{" . "\n" .
        "color:" . $_POST["primaryMsgColor"] . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $primaryMsgColorPath);

    $stringToWrite = "body{" . "\n" .
        "color:" . $_POST["secondaryMsgColor"] . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $secondaryMsgColorPath);

    $stringToWrite = "body{" . "\n" .
        "color:" . $_POST["silentAppAdColor"] . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $appAdColorPath);

    $stringToWrite = "var backgroundColor = \"" . $_POST["backgroundColor"] . "\";
var mainTextColor = \"" . $_POST["mainTextColor"] . "\";
var headerElColor = \"" . $_POST["headerElColor"] . "\";
var timeoutColor = \"" . $_POST["timeoutElColor"] . "\";
var prayerTimesColor = \"" . $_POST["prayerTimesColor"] . "\";
var prayerNameColor = \"" . $_POST["prayerNamesColor"] . "\";
var internalTempColor = \"" . $_POST["internalTempColor"] . "\";
var hadeethColor = \"" . $_POST["hadeethColor"] . "\";
var azkarColor = \"" . $_POST["azkarColor"] . "\";
var secondaryColor = \"" . $_POST["primaryMsgColor"] . "\";
var primaryColor = \"" . $_POST["secondaryMsgColor"] . "\";
var silentAppAdColor = \"" . $_POST["silentAppAdColor"] . "\";";

    writeFile($stringToWrite, $currentColorJSFile);


} else {

    $themeID = $_POST["themeID"];
    if ($themeID == 1) {
        $imageURL = 'url("../background/theme1.jpg")';
        $opacity = 0.5;
    } else if ($themeID == 2) {
        $imageURL = 'url("../background/theme2.jpg")';
        $opacity = 0.5;
    } else {
        return;
    }
    $stringToWrite = $stringToWrite . "body { " . "\n" .
        "background-image:" . $imageURL . ";" . "\n" .
        "background-size: cover;" . "\n"
        . "}" .
        "#blackBackground{" . "\n" .
        "background-color: black;" . "\n" .
        "opacity:" . $opacity . ";" . "\n" .
        "}" . "\n";



    writeFile($stringToWrite, $backgroundColorPath);
    $stringToWrite = "";

    $stringToWrite = $stringToWrite . "  body {
	color:" . $_POST["mainTextColor"] . ";" . "\n"
        . "}" . "\n" .
        ".headerEl{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}" . "\n" .
        ".timeOutEl{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}" .
        ".prayerElTimes{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}" . "\n" .
        ".prayerElNames{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}" . "\n" .
        "#internalTemp{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $mainTextColorPath);
    $stringToWrite = "";

    $stringToWrite = "body{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $hadeethColorPath);

    $stringToWrite = "body{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $azkarColorPath);

    $stringToWrite = "body{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $primaryMsgColorPath);

    $stringToWrite = "body{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $secondaryMsgColorPath);

    $stringToWrite = "body{" . "\n" .
        "color:" . "#FFFFFF" . ";" . "\n" .
        "}";
    writeFile($stringToWrite, $appAdColorPath);

    $stringToWrite = "";

    writeFile($stringToWrite, $currentColorJSFile);


}


exec($killMidori, $x, $y);
exec($killFirefox, $x, $y);

function writeFile($text, $path)
{
    $file = fopen($path, "w") or die("can't open file");
    fwrite($file, $text);
    fclose($file);
}