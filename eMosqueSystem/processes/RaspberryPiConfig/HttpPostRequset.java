import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpPostRequset
{
    URL url;
    HttpURLConnection conn;
    String param = "";
    PostResponse response;

    public HttpPostRequset(String urlString)
    {

	try
	{
	    this.url = new URL(urlString);
	} catch (MalformedURLException e)
	{
	    e.printStackTrace();
	}

    }

    public void addParam(String name, String value)
    {
	if (param.equals(""))
	{
	    param = name + "=" + value;
	} else
	{
	    param = param + "&" + name + "=" + value;
	}
    }

    public PostResponse excuteRequest()
    {

	try
	{
	    conn = (HttpURLConnection)url.openConnection();
	    conn.setDoOutput(true);
	    OutputStreamWriter writer = new OutputStreamWriter(
		    conn.getOutputStream());
	    writer.write(param);
	    writer.flush();
	    

	    String responseText = "";
	    String line;
	    BufferedReader reader = new BufferedReader(new InputStreamReader(
		    conn.getInputStream()));
	    while ((line = reader.readLine()) != null)
	    {
		responseText = responseText + line;
	    }
	    response = new PostResponse();	   
	    response.responseCode = conn.getResponseCode();
	    response.responseText = responseText;
	    writer.close();
	    reader.close();
	} catch (IOException e)
	{
	    e.printStackTrace();
	    return null;
	}
	return response;
    }
    public class PostResponse
    {
	int responseCode;
	String responseText;
    }
}
