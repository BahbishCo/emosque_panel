
var countrySelector = document.getElementById("countrySelector");
var citySelector = document.getElementById("citySelector");
var timeZoneSelector = document.getElementById("timeZoneSelector");
var defaultPrayerSettingCheckBox = document.getElementById("loadDefaultPrayerSettingCheckBox");
var highLatSelector = document.getElementById("highLatSelector");
var asrJuristicSelector = document.getElementById("asrJuristicSelector");
var calMethodeSelector = document.getElementById("calMethodeSelector");
var midnightMethod = document.getElementById("midnightMethod");
var ishaaAngleSelector = document.getElementById("ishaaAngleSelector");
var fajrAngleSelector = document.getElementById("fajrAngleSelector");

initPrayerTimeSettingOptions();
loadCountries();
loadCities(countrySelector.value);
loadTimeZone();
loadCityParam(countrySelector.value, citySelector.value);
attachListeners();

function loadCountries()
{
    var countriesCount = countries.countriesName.length;
    for (var i = 0; i < countriesCount; i++)
    {
        var option = document.createElement("option");
        option.text = countries.countriesName[i];
        option.value = countries.countriesName[i];
        countrySelector.add(option);
    }
}



function loadCities(countryName)
{
    cityCount = countries[countryName].citiesNames.length

    for (var i = 0; i < cityCount; i++)
    {
        var option = document.createElement("option");
        option.text = countries[countryName].citiesNames[i];
        option.value = countries[countryName].citiesNames[i];
        citySelector.add(option);
    }
}


function onCitySelectChange()
{
    loadCityParam(countrySelector.value, citySelector.value);
    adjustPrayers();

}

function onCountrySelectChange()
{
    removeOptions(citySelector)
    loadCities(countrySelector.value);
    loadCityParam(countrySelector.value, citySelector.value);
    adjustPrayers();

}


function loadCityParam(countryName,cityName)
{
    timeZoneSelector.options[countries[countryName][cityName].timeZone + 24].selected = true;
    var options = calMethodeSelector.options;
    for (var i = 0; i < options.length; i++)
    {
        if (options[i].value === countries[countryName][cityName].prayerCalcMethode)
        {
            options[i].selected= true;
            break;
        }
    }


    var options = asrJuristicSelector.options;
    for (var i = 0; i < options.length; i++)
    {
        if (options[i].value === countries[countryName][cityName].asrJuristic)
        {
            options[i].selected = true;
            break;
        }
    }


    var options = highLatSelector.options;
    for (var i = 0; i < options.length; i++)
    {
        if (options[i].value === countries[countryName][cityName].adjustHighLats)
        {
            options[i].selected = true;
            break;
        }
    }


    var options = midnightMethod.options;
    for (var i = 0; i < options.length; i++)
    {
        if (options[i].value === countries[countryName][cityName].midnightMethod)
        {
            options[i].selected = true;
            break;
        }
    }

    //ishaaAngleSelector.options[countries[countryName][cityName].ishaaAngle].selected = true;
    //fajrAngleSelector.options[countries[countryName][cityName].fajrAngle].selected = true;
}


function initPrayerTimeSettingOptions()
{
    for (var i = 0; i < Constants.PrayerTimeCalculation.CalculationMethod.Names.length; i++)
    {
        var option = document.createElement("option");
        option.text = Constants.PrayerTimeCalculation.CalculationMethod.Names[i];
        option.value = Constants.PrayerTimeCalculation.CalculationMethod.Names[i];
        calMethodeSelector.add(option);
    }

    for (var i = 0; i < Constants.PrayerTimeCalculation.AsrJuristic.Names.length; i++)
    {
        var option = document.createElement("option");
        option.text = Constants.PrayerTimeCalculation.AsrJuristic.Names[i];
        option.value = Constants.PrayerTimeCalculation.AsrJuristic.Names[i];
        asrJuristicSelector.add(option);
    }

    for (var i = 0; i < Constants.PrayerTimeCalculation.MidnightMethods.Names.length; i++)
    {
        var option = document.createElement("option");
        option.text = Constants.PrayerTimeCalculation.MidnightMethods.Names[i];
        option.value = Constants.PrayerTimeCalculation.MidnightMethods.Names[i];
        midnightMethod.add(option);
    }

    for (var i = 0; i < Constants.PrayerTimeCalculation.HighLatMethods.Names.length; i++)
    {
        var option = document.createElement("option");
        option.text = Constants.PrayerTimeCalculation.HighLatMethods.Names[i];
        option.value = Constants.PrayerTimeCalculation.HighLatMethods.Names[i];
        highLatSelector.add(option);
    }

    for (var i = 0; i < 90; i++)
    {
        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        fajrAngleSelector.add(option);
    }

    for (var i = 0; i < 90; i++)
    {
        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        ishaaAngleSelector.add(option);
    }

   
}

function loadTimeZone()
{
    for (var i = -24; i <= 24; i++)
    {
        var option = document.createElement("option");
        option.text = "GMT ";
        if (i > 0)
        {
		option.text += " +";
	    	option.value = "+" + i;
	}
        else
	{
		option.text += " -";
		option.value = i;
	}
            
        option.text += Math.abs(i);
        timeZoneSelector.add(option);
    }
}

function prayerSettingCheckBoxListener()
{
    if (defaultPrayerSettingCheckBox.checked)
    {
        loadCityParam(countrySelector.value, citySelector.value);
        highLatSelector.disabled = true;
        asrJuristicSelector.disabled = true;
        calMethodeSelector.disabled = true;
        ishaaAngleSelector.disabled = true;
        fajrAngleSelector.disabled = true;
        midnightMethod.disabled = true;

    }
    else
    {
        highLatSelector.disabled = false;
        asrJuristicSelector.disabled = false;
        calMethodeSelector.disabled = false;
        ishaaAngleSelector.disabled = false;
        fajrAngleSelector.disabled = false;
        midnightMethod.disabled = false;
    }
}

function attachListeners()
{
    
    if (citySelector.addEventListener)
    {
        citySelector.addEventListener('change', onCitySelectChange, false);
        countrySelector.addEventListener('change', onCountrySelectChange, false);
        defaultPrayerSettingCheckBox.addEventListener('change', prayerSettingCheckBoxListener);
    }
    else
    {
        //IE 
        countrySelector.attachEvent('onchange', onCitySelectChange, false);
        countrySelector.attachEvent('onchange', onCountrySelectChange, false);
        defaultPrayerSettingCheckBox.attachEvent('change', prayerSettingCheckBoxListener);

    }
}


function removeOptions(selectbox)
{
    var i;
    for (i = selectbox.options.length - 1; i >= 0; i--)
    {
        selectbox.remove(i);
    }
}