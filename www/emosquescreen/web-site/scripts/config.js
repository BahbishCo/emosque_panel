var Setting =
    {
        Mode: {
            PI: "0",
            PreviewFrameSize: "1",
            PreviewFullScreen: "2",
        },
        PrimaryMessageStates: {
            Unknown: 0,
            Hadeeth: 1,
            ServerMessage: 2,
            ImamMessage: 3,
            Azkar: 4,
            Hidden: 5,
            Preview: 6
        },
        SecondaryMessageStates: {
            Unknown: 0,
            Visible: 1,
            Hidden: 2,
            Preview: 3
        },
        MainContentFrameSrc: {
            Hadeeth: "html-pages/hadeethFrame/hadeethFrame.html",
            Azkar: "html-pages/afterPrayerAzkar/afterPrayerAzkarFrame.html",
            AzkarEn: "html-pages/afterPrayerAzkar/afterPrayerAzkarFrame-En.html",
            ServerMessage: "customFiles/contents/primaryMessages/ServerMessages/",
            ImamMessage: "customFiles/contents/primaryMessages/ImamMessages/",
            Preview: "html-pages/previewFrame/previewFrame.html",
            SilentAppAd:"html-pages/silentAppAd/silentAppAd.html",
            SilentAppAdEn:"html-pages/silentAppAd/silentAppAd-en.html"
        },
        SecondaryContentFrameSrc: {
            ImamMessage: "html-pages/secondaryMessage/secondaryMessage.html",
            ServerMessage: "html-pages/secondaryMessage/secondaryMessage.html",
            Preview: "html-pages/secondaryMessage/secondaryMessage.html"
        },

        HttpRequest: {
            GetPrimaryMessages: "../other_components/getPrimaryMessage/getPrimaryMessages.php",
            GetSecondaryMessage: "customFiles/contents/secondaryMessages/ImamMessages/",
            GetBahbishMessages: ""
        },
        Language:{
            English: "en",
            Arabic: "ar"
        },
        PrayerBlinkingCounter:30,
        AppAdDisplayPeroid:5 * 1000
    }
    ;
