var submitButton = document.getElementById("submitButton");
submitButton.addEventListener('click', submitForm, false);


function submitForm() {

    submitButton.disabled = true;
    submitButton.style.background = '';
    submitButton.style.color = 'grey';

    // checking Prayer Setting

    var countrySelector = document.getElementById("countrySelector");
    var citySelector = document.getElementById("citySelector");
    var timeZoneSelector = document.getElementById("timeZoneSelector");
    var defaultPrayerSettingCheckBox = document.getElementById("loadDefaultPrayerSettingCheckBox");
    var highLatSelector = document.getElementById("highLatSelector");
    var asrJuristicSelector = document.getElementById("asrJuristicSelector");
    var calMethodeSelector = document.getElementById("calMethodeSelector");
    var midnightMethod = document.getElementById("midnightMethod");
    var ishaaAngleSelector = document.getElementById("ishaaAngleSelector");
    var fajrAngleSelector = document.getElementById("fajrAngleSelector");
    var latitudeText = document.getElementById("latitude");
    var longitudeText = document.getElementById("longitude");
    var elevationText = document.getElementById("elevation");

    var miladiMonthSelector = document.getElementById("miladiMonth");


    calMethodeSelector.disabled = false
    asrJuristicSelector.disabled = false
    highLatSelector.disabled = false
    midnightMethod.disabled = false
    ishaaAngleSelector.disabled = false
    fajrAngleSelector.disabled = false
    timeZoneSelector.disabled = false


    latitudeText.value = countries[countrySelector.value][citySelector.value].latitude;
    longitudeText.value = countries[countrySelector.value][citySelector.value].longitude;
    elevationText.value = countries[countrySelector.value][citySelector.value].elevation;

    if (countries[countrySelector.value][citySelector.value].ishaaAngle.length > 3) {
        ishaaAngleSelector.options[ishaaAngleSelector.selectedIndex].value = "\"" + countries[countrySelector.value][citySelector.value].ishaaAngle + "\"";

    }

    else {
        ishaaAngleSelector.options[ishaaAngleSelector.selectedIndex].value = countries[countrySelector.value][citySelector.value].ishaaAngle;

    }

    fajrAngleSelector.options[fajrAngleSelector.selectedIndex].value = countries[countrySelector.value][citySelector.value].fajrAngle;

    if (defaultPrayerSettingCheckBox.checked == true) {
        console.log("true")
        defaultPrayerSettingCheckBox.value = "true";

        //timeZoneSelector.options[countries[countrySelector.value][citySelector.value].timeZone + 24].selected = true;
        calMethodeSelector.options[calMethodeSelector.selectedIndex].value = countries[countrySelector.value][citySelector.value].prayerCalcMethode;
        asrJuristicSelector.options[asrJuristicSelector.selectedIndex].value = countries[countrySelector.value][citySelector.value].asrJuristic;
        highLatSelector.options[highLatSelector.selectedIndex].value = countries[countrySelector.value][citySelector.value].adjustHighLats;
        midnightMethod.options[midnightMethod.selectedIndex].value = countries[countrySelector.value][citySelector.value].midnightMethod;
    }
    else {
        defaultPrayerSettingCheckBox.checked = true;
        defaultPrayerSettingCheckBox.value = "false";
    }

    // end of checking Prayer Setting

    /***************************************************************/
    /***************************************************************/
    /***************************************************************/
    /***************************************************************/
    /***************************************************************/


    // checking Date Setting

    var editMiladiDateCheckBox = document.getElementById("editMiladiDateCheckBox");
    var miladiDayName = document.getElementById("miladiDayName");
    var miladiDaySelector = document.getElementById("miladiDay");
    var miladiMonthSelector = document.getElementById("miladiMonth");
    var miladiYearSelector = document.getElementById("miladiYear");
    var hourSelector = document.getElementById("hour");
    var minSelector = document.getElementById("min");


    var hijriDateCorrectionSelector = document.getElementById("hijriCorrection");

    if (editMiladiDateCheckBox.checked == true) {
        editMiladiDateCheckBox.value = "true";

    }
    else {
        editMiladiDateCheckBox.checked = true;
        editMiladiDateCheckBox.value = "false";
        miladiDayName.disabled = false;
        miladiDaySelector.disabled = false;
        miladiMonthSelector.disabled = false;
        miladiYearSelector.disabled = false;
        hourSelector.disabled = false;
        minSelector.disabled = false;

    }

    console.log("month = " + Constants.MonthNameEnglish[miladiMonthSelector.value - 1]);
    //miladiMonthSelector.value = Constants.MonthNameEnglish[miladiMonthSelector.value - 1];
    miladiMonthSelector.options[miladiMonthSelector.value - 1].value = Constants.MonthNameEnglish[miladiMonthSelector.value - 1];

    console.log("selected month = " + miladiMonthSelector.value);

    //end of checking Date Setting

    /***************************************************************/
    /***************************************************************/
    /***************************************************************/
    /***************************************************************/
    /***************************************************************/


    // checking general setting

    var QREnable = document.getElementById("showQRCodeCheckbox");
    var showIntenalTempCheckbox = document.getElementById("showIntenalTempCheckbox");
    var azanEnable = document.getElementById("playAzan");
//    var displayAnalogClock = document.getElementById("displayAnalogClock");

    var displayBahbishMessage = document.getElementById("displayBahbishMessage");
    var adAtPrayerEnable = document.getElementById("displayAdAtPrayerTime");


    if (displayBahbishMessage.checked == true) {
        displayBahbishMessage.value = "true";
    }
    else {
        displayBahbishMessage.checked = true;
        displayBahbishMessage.value = "false";
    }


/*    if (displayAnalogClock.checked == true) {
        displayAnalogClock.value = "true";
    }
    else {
        displayAnalogClock.checked = true;
        displayAnalogClock.value = "false";
    }

*/
    azanEnable.disabled = false;

    if (QREnable.checked == true) {
        QREnable.value = "true";
    }
    else {
        QREnable.checked = true;
        QREnable.value = "false";
    }


    if (showIntenalTempCheckbox.checked == true) {
        showIntenalTempCheckbox.value = "true";
    }
    else {
        showIntenalTempCheckbox.checked = true;
        showIntenalTempCheckbox.value = "false";
    }


    if (azanEnable.checked == true) {
        azanEnable.value = "true";
    }
    else {
        azanEnable.checked = true;
        azanEnable.value = "false";
    }

    if (adAtPrayerEnable.checked == true)
    {
        adAtPrayerEnable.value = "true";
    }
    else
    {
        adAtPrayerEnable.checked = true;
        adAtPrayerEnable.value = "false";
    }

    /**************************************************************************************************/


    var fajrPrayerTime = document.getElementById("beforFajrAzkarPeriod");
    var duhurPrayerTime = document.getElementById("beforduhurAzkarPeriod");
    var asrPrayerTime = document.getElementById("beforAsrAzkarPeriod");
    var maghribPrayerTime = document.getElementById("beforMaghribAzkarPeriod");
    var ishaaPrayerTime = document.getElementById("beforIshaaAzkarPeriod");
    var jumaaPrayerTime = document.getElementById("beforJumaaAzkarPeriod");

    var fajrIqamaTime = document.getElementById("fajrIqamaTime");
    var duhurIqamaTime = document.getElementById("duhurIqamaTime");
    var asrIqamaTime = document.getElementById("asrIqamaTime");
    var maghribIqamaTime = document.getElementById("maghribIqamaTime");
    var ishaaIqamaTime = document.getElementById("ishaaIqamaTime");
    var jumaaIqamaTime = document.getElementById("jumaaIqamaTime");

    fajrPrayerTime.options[fajrPrayerTime.selectedIndex].value =  parseInt(fajrPrayerTime.options[fajrPrayerTime.selectedIndex].value)
        +   parseInt(fajrIqamaTime.options[fajrIqamaTime.selectedIndex].value)

    duhurPrayerTime.options[duhurPrayerTime.selectedIndex].value =  parseInt(duhurPrayerTime.options[duhurPrayerTime.selectedIndex].value)
        +   parseInt(duhurIqamaTime.options[duhurIqamaTime.selectedIndex].value)

    asrPrayerTime.options[asrPrayerTime.selectedIndex].value =  parseInt(asrPrayerTime.options[asrPrayerTime.selectedIndex].value)
        +   parseInt(asrIqamaTime.options[asrIqamaTime.selectedIndex].value)

    maghribPrayerTime.options[maghribPrayerTime.selectedIndex].value =  parseInt(maghribPrayerTime.options[maghribPrayerTime.selectedIndex].value)
        +   parseInt(maghribIqamaTime.options[maghribIqamaTime.selectedIndex].value)

    ishaaPrayerTime.options[ishaaPrayerTime.selectedIndex].value =  parseInt(ishaaPrayerTime.options[ishaaPrayerTime.selectedIndex].value)
        +   parseInt(ishaaIqamaTime.options[ishaaIqamaTime.selectedIndex].value)

    jumaaPrayerTime.options[jumaaPrayerTime.selectedIndex].value =  parseInt(jumaaPrayerTime.options[jumaaPrayerTime.selectedIndex].value)
        +   parseInt(jumaaIqamaTime.options[jumaaIqamaTime.selectedIndex].value)


    /****************************************************************************************************/


    //end of checking general Setting


    document.forms['form'].submit();
    window.location.reload(true);
    loadQuickPanelSetting();

}
