var index = 0;
var images;
var srcList = [];
var minSize = 0;
var scaleMode = 0;
var pages = [];
var width, height;
// window.onload = setTimeout(scrollPage,1000);
// window.onload = setTimeout(start, 1000);
// $("body").css("display", "none");
document.body.style.display = "none";
// setTimeout(switchPages,5000);
window.onload = setTimeout(start, 1000);
// document.addEventListener('DOMContentLoaded', scrollPage, false);
var page = createPage();
var pageFooter = "<h1 style='font-size: 50px;visibility: hidden;'>hidden element</h1><h1 style='font-size: 50px;visibility: hidden;'>hidden element</h1>"
setTimeout(addCssFile, 5000);

function start() {
    width = window.innerWidth - 10;
    height = window.innerHeight - 10;
    minSize = width;
    scaleMode = 0;
    if (minSize > height) {
        minSize = height;
        scaleMode = 1;
    }
    // setTimeout(splitToPages, 1000, );
    splitToPages(document.body);
    if (page.content.length != 0 && page.type == "text") {
        pages.push(page);
    }
    parent.setPrimaryFrameVisibility(true);
    switchPages();
}


function createPage() {
    var page = {};
    page["type"] = "text";
    page["content"] = [];
    return page;
}


function test() {

    width = window.innerWidth - 10;
    height = window.innerHeight - 10;
    minSize = width;
    scaleMode = 0;
    if (minSize > height) {
        minSize = height;
        scaleMode = 1;
    }

    var bodyContent = document.body.childNodes;

    var firstEl = document.body.firstChild.childNodes;

    if (firstEl.length != 0) {
        bodyContent = firstEl;
    }

    var page = createPage();
    for (var i = 0; i < bodyContent.length; i++) {
        if (bodyContent[i].nodeName == "SCRIPT" || bodyContent[i].nodeName == "CSS") {
            continue;
        }
        if (bodyContent[i].nodeName == "IMG") {
            if (page.content.length != 0 && !isEmptyTags(page.content)) {
                pages.push(page);
            }
            page = createPage();
            page.type = "image";
            if (scaleMode == 0) {
                bodyContent[i].width = minSize;
                bodyContent[i].removeAttribute("height");
            } else {
                bodyContent[i].height = minSize;
                bodyContent[i].removeAttribute("width");
            }
            page.content.push(bodyContent[i]);
            pages.push(page);
            page = createPage();
        } else {
            page.type = "text";
            page.content.push(bodyContent[i]);
        }
        console.log("node " + i + " = " + bodyContent[i].textContent + ", node type " + i + " = " + bodyContent[i].nodeName);
    }
    if (page.content.length != 0 && page.type == "text") {
        pages.push(page);
    }

    for (var i = 0; i < pages.length; i++) {
        console.log(i + " , " + pages[i].type + " , " + JSON.stringify(pages[i].content.length));
    }
    document.body.innerHTML = "";
    switchPages();
}

function splitToPages(parentEl) {
    var bodyContent = parentEl.childNodes;
    for (var i = 0; i < bodyContent.length; i++) {
        var el = bodyContent[i];
        if (el.nodeName == "SCRIPT" || el.nodeName == "CSS") {
            continue;
        }
        var hasChildren = el.childNodes.length;
        if (hasChildren != 0) {
            splitToPages(el);
        }

        if (el.nodeName == "IMG") {
            if (page.content.length != 0 && !isEmptyTags(page.content)) {
                pages.push(page);
            }
            page = createPage();
            page.type = "image";
            if (scaleMode == 0) {
                el.width = minSize;
                el.removeAttribute("height");
                el.style.height = "";
            } else {
                el.height = minSize;
                el.removeAttribute("width");
                el.style.width = "";
            }
            page.content.push(el);
            pages.push(page);
            page = createPage();
        } else if (el.nodeName == "BR" || (el.nodeName == "#text" && el.textContent.trim().length != 0)) {
            page.type = "text";
            page.content.push(el);
        }
    }


}


function isEmptyTags(nodes) {
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].nodeName != "BR" && nodes[i].textContent.trim().length != 0) {
            return false;
        }
    }
    return true;
}


function divideContentIntoPages() {
    var bodyEl = document.body;
    images = document.getElementsByTagName('img');
    if (images.length != 0) {
        elToBreak = images[0];
        elToBeginFrom = bodyEl.firstChild;
        var i = 0;
        while (i < images.length) {
            var elements = $(elToBeginFrom).nextUntil(elToBreak);
            if (elements.length == -1 || true) {
                pages.push(elements);
            }
            elToBeginFrom = elToBreak;
            i++;
            if (i < images.length) {
                elToBreak = images[i];
            }
        }
        pages.push($(elToBeginFrom).nextAll());
    } else {
        pages.push($(bodyEl.firstChild).nextAll());
    }

    for (var i = 0; i < pages.length; i++) {
        console.log("page " + i + " = " + JSON.stringify(pages[i]));
    }
    setTimeout(switchPages, 1000);
}
var index2 = 0;

function switchPages() {
    document.body.innerHTML = "";
    window.scrollTo(0, 0);
    // document.body.style.visibility = "hidden";

    if (pages.length == 1 && pages[0].type == "image") {
        for (var i = 0; i < pages[0].content.length; i++) {
            document.body.appendChild(pages[0].content[i]);
        }
        document.body.style.display = "block";
        return;
    }


    for (var i = 0; i < pages[index2].content.length; i++) {
        document.body.appendChild(pages[index2].content[i]);
    }
    $("body").fadeIn("slow", function () {
        if (pages[index2].type == "text") {
            document.body.innerHTML += pageFooter;
            pageScroll();
        } else {
            setTimeout(focusImg, 5000);
        }
        index2++;
        if (index2 == pages.length) {
            index2 = 0;
        }
    });
    // document.body.style.visibility = "visible";


}
function pageScroll() {
    scrollToHeight(document.body.offsetHeight);
}

function scrollToHeight(height) {
    // console.log("window.scrollY = " + window.scrollY);
    // console.log("height = " + height);
    var t = 50;
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
//        setTimeout(scrollToTop, 3000);
        //parent.displayAppAd();
        console.log("end of page")
        if (parent.displayAppAdFlag == true) {
            console.log("display ad");
            setTimeout(parent.displayAppAd, 3000);
            return;
        } else {
            console.log("scroll to top");
            setTimeout(scrollToTop, 3000);
            return;
        }
    }
    window.scrollBy(0, 2);
    setTimeout(scrollToHeight, t, height);
}

function focusImg() {
    $("body").fadeOut("slow", function () {
        switchPages();
    });
    // setTimeout(switchPages, 5000);
}


function clearOtherImages() {
    for (var i = 0; i < images.length; i++) {
        if (i == index) {
            if (scaleMode == 0) {
                images[i].width = minSize;
            } else {
                images[i].height = minSize;
            }
            continue;
        }
        images[i].src = "";
        if (scaleMode == 0) {
            images[i].width = 0;
        } else {
            images[i].height = 0;
        }
        //images[i].style.display = "none";
    }
}

function getElOffset(i) {
    var element = images[i];
    var bodyRect = document.body.getBoundingClientRect();
    var elemRect = element.getBoundingClientRect();
    var offset = elemRect.top - bodyRect.top;
    console.log("offset = " + offset);
    return offset;
}

function getBottomOfEl(i) {
    var element = images[i];
    var bodyRect = document.body.getBoundingClientRect();
    var elemRect = element.getBoundingClientRect();
    var offset = elemRect.bottom - bodyRect.top;
    console.log("bottom offset = " + offset);
    return offset;
}

function scrollToTop() {
    $("body").fadeOut("slow", function () {
        switchPages();
    });

    // setTimeout(switchPages, 500);
}

function addCssFile() {
    link = document.createElement("link");
    if (true) {
        link.href = "../../../../../eMosqueScreen/css/primaryMessageStyle.css";
        link.rel = "stylesheet";
    }
    else {
        link.href = "css/primaryMessageStyle.css";
        link.rel = "stylesheet";
    }
    document.head.appendChild(link);
    link = document.createElement("link");

    if (true) {
        link.href = "../../../../customFiles/CSS/primaryMsgColor.css";
        link.rel = "stylesheet";
    }
    document.head.appendChild(link);
}
