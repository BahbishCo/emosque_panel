

$(document).keypress(function(e) {
    if(e.which == 13) {
        document.activeElement.blur();
    }
});

function setCursorPos() {
    if (hasFocus == false) {
        hasFocus = true;
        var inputText = $('#inputText');
        var strLength = inputText.val().length * 2;
        inputText.focus();
        inputText[0].setSelectionRange(strLength, strLength);
    }
}

function toggleFocus(){
    hasFocus = false;
}

function saveScrollingText() {
    if (document.getElementById('inputText').value == "") {
        alert("Please insert text first");
        //window.cpjs.sendToAndroid("الرجاء إدخال النص أولاً");
    }
    else {
        var today = false;
        var todayDate = new Date();
        var fromDate = document.getElementById('startDatePicker').value;
        var toDate = document.getElementById('endDatePicker').value;

        var date1 = deformatDate(fromDate);
        var date2 = deformatDate(toDate);
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

        if (diffDays >= 0) {
            var datesArray = new Array();
            for (var i = 0; i <= diffDays; i++) {
                if (formatDate(date1.addDays(i)) == formatDate(todayDate)) {
                    today = true;
                }
                datesArray[i] = formatDate(date1.addDays(i));
            }

        callWritePhp(document.getElementById('inputText').value, datesArray, today);
	window.JSInterface.storeToAzure(document.getElementById('inputText').value);


        }
        else {
            //window.cpjs.sendToAndroid("الرجاء التأكد من تاريخ البداية والنهاية!");
            alert("Please make sure the starting and ending dates are correct!");
        }
    }
}

function showMsgs() {
    location.replace("./myMsgs-En.html");
}

function formatDate(date) {
    var day = date.getDate();
    var month = (date.getMonth() + 1);
    var year = date.getFullYear();

    if (parseInt(day) < 10) {
        day = '0' + day;
    }

    if (parseInt(month) < 10) {
        month = '0' + month;
    }

    return day + '-' + month + '-' + year;
}

function deformatDate(date) {
    var parts = date.split("-");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

function callWritePhp(message, dates, today) {
    getRequest(
        'php/writeMsgs.php?message=' + message + '&dates=' + dates + '&today=' + today, // URL for the PHP file
        writeSuccessHandler,  // handle successful request
        writeFailureHandler    // handle error
    );
}

function writeSuccessHandler(responseText) {
    //window.cpjs.sendToAndroid("تمت العملية بنجاح.");
    alert("Done");
}


function writeFailureHandler() {
    //window.cpjs.sendToAndroid(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
    alert("Something wrong happened please try again.");
}

function phpLoadTodayMsg(date) {
    getRequest(
        'php/loadTodayMsg.php?date=' + date, // URL for the PHP file
        loadTodayMsgSuccessHandler,  // handle successful request
        loadTodayMsgFailureHandler    // handle error
    );
}

function loadTodayMsgSuccessHandler(responseText) {
    //window.cpjs.sendToAndroid("تمت العملية بنجاح.");
    //alert("تمت العملية بنجاح.");
    document.getElementById('inputText').value = responseText;
	document.getElementById('marqueeText').innerHTML = responseText;
}


function loadTodayMsgFailureHandler() {
    //window.cpjs.sendToAndroid(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
    //alert(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
}

function getRequest(url, success, error) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}
