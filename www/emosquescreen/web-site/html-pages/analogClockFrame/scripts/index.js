setTimeout(setCanvasDimension,3000);


function setCanvasDimension() {

    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var rad = windowWidth;
    if (windowHeight < windowWidth) {
        rad = windowHeight;
    }

    canvas = document.getElementById("clockCanvas");
    canvas.width = rad;
    canvas.height = rad;

	

    var date = new Date();
    var second = date.getSeconds();
    var delay = 60 - second;
    setTimeout(setCanvasDimension,500);
    setTimeout(function () {
        setInterval(drawClock, 1000 * 60);
    }, delay * 1000);
    drawCircle();
    drawClock();


}

function drawClock() {
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.translate(radius, radius);
    drawFace(ctx, radius);
    drawNumbers(ctx, radius);
    drawTime(ctx, radius);
}

function drawCircle() {
    canvas = document.getElementById("clockCanvas");
    ctx = canvas.getContext("2d");
    radius = canvas.height / 2;
    ctx.translate(radius, radius);
    radius = radius * 0.95;

    function drawClock() {
        ctx.arc(0, 0, radius, 0, 2 * Math.PI);
        ctx.fillStyle = "#000000";
        ctx.fill();
    }

}


function drawFace(ctx, radius) {
    var grad;

    	ctx.beginPath();
        ctx.arc(0, 0, radius, 0, 2 * Math.PI);
    ctx.fillStyle ='rgba(255,255,255,.2)'
    ctx.fill();

    grad = ctx.createRadialGradient(0, 0, radius * 0.95, 0, 0, radius * 1.05);
    grad.addColorStop(0, '#333');
    grad.addColorStop(0.5, 'white');
    grad.addColorStop(1, '#333');
    ctx.strokeStyle = grad;
    ctx.lineWidth = radius * 0.1;
    //ctx.stroke();

    ctx.beginPath();
    ctx.arc(0, 0, radius * 0.1, 0, 2 * Math.PI);
    ctx.fillStyle = '#000000';
    ctx.fill();

}


function drawNumbers(ctx, radius) {
    var ang;
    var num;
    ctx.font = radius * 0.15 + "px arial";
    ctx.textBaseline = "middle";
    ctx.textAlign = "center";
    ctx.fillStyle = 'black';

    for (num = 1; num < 13; num++) {
        if (num % 3 != 0) {
            ang = num * Math.PI / 6;
            ctx.rotate(ang);
            ctx.translate(0, -radius * 0.9);
            ctx.rotate(-ang);
            drawHand(ctx, ang, radius * .1, radius * .02, "number");
            //ctx.fillText(num.toString(), 0, 0);
            ctx.rotate(ang);
            ctx.translate(0, radius * 0.9);
            ctx.rotate(-ang);
        }
        else {
            ang = num * Math.PI / 6;
            ctx.rotate(ang);
            ctx.translate(0, -radius * 0.9);
            ctx.rotate(-ang);
            ctx.fillText(num.toString(), 0, 0);
            ctx.rotate(ang);
            ctx.translate(0, radius * 0.9);
            ctx.rotate(-ang);
        }
    }
}


function drawHand(ctx, pos, length, width, mode) {

    ctx.fillStyle = 'white';
    ctx.strokeStyle= 'black';
    ctx.beginPath();
    ctx.lineWidth = width;
    if (mode == "hand")
    {
        ctx.lineCap = "round";
    }

    if(mode == "number")
    {
        ctx.fillStyle = 'black';
    }
    ctx.moveTo(0, 0);
    ctx.rotate(pos);
    ctx.lineTo(0, -length);
    ctx.stroke();
    ctx.rotate(-pos);
    canvas.style.visibility = "visible";

}

function drawTime(ctx, radius) {
    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    //hour
    hour = hour % 12;
    hour = (hour * Math.PI / 6) + (minute * Math.PI / (6 * 60)) + (second * Math.PI / (360 * 60));
    drawHand(ctx, hour, radius * 0.55, radius * 0.07,"hand");
    //minute
    minute = (minute * Math.PI / 30) + (second * Math.PI / (30 * 60));
    drawHand(ctx, minute, radius * 0.85, radius * 0.07, "hand");
    // second
    //second = (second * Math.PI / 30);
    //drawHand(ctx, second, radius * 0.95, radius * 0.02);
}

