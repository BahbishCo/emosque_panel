/**
 * Created by Dua' on 7/11/2016.
 */

function getVideo(vName) {
    playVideoPhp(vName);
}

function playVideoPhp(videoName) {
    getRequest(
        './php/getVideo.php?videoName='+videoName,
        loadSuccessHandler,  // handle successful request
        loadFailureHandler    // handle error
    );
}

function loadSuccessHandler(responseText) {
   // alert(responseText);
}

function loadFailureHandler(responseText) {
    alert(".حصل مشكلة في تشغيل الفيديو");
}


	var timeoutel=0;
function playTimeVideo(vName,iteration) {
	var vid = document.getElementById("videoPlayer"+iteration);
	var Timing =vid.currentTime;
	vid.pause();
    playVideoATPhp(vName,Timing);
}

function playVideoATPhp(videoName,Timing) {
    getRequest(
        './php/playVideoAt.php?videoName='+videoName+"&Timing="+encodeURIComponent(Timing),
        loadATSuccessHandler,  // handle successful request
        loadATFailureHandler    // handle error
    );
}


function loadATSuccessHandler(responseText) {
    //alert(responseText);
}

function loadATFailureHandler(responseText) {
    alert(".حصل مشكلة في تشغيل الفيديو");
}

function stopVideo() {
    stopVideoPhp();
}

function stopVideoPhp() {
    getRequest(
        './php/stopVideo.php',
        stopSuccessHandler,  // handle successful request
        stopFailureHandler    // handle error
    );
}

function stopSuccessHandler(responseText) {
    //alert(responseText);
}

function stopFailureHandler(responseText) {
    alert(".حدث خطأ ما ، الرجاء المحاولة مرة أخرى");
}

function uploadVideo() {
    uploadVideoPhp("../../../../../videos/");
}

function uploadVideoPhp(videoPath) {
    getRequest(
        './php/uploadVideo.php?videoPath='+videoPath,
        uploadSuccessHandler,  // handle successful request
        uploadFailureHandler    // handle error
    );
}

function uploadSuccessHandler(responseText) {
   var res= responseText.split(";");
	var select = document.getElementById("select_video"); 
    for(var i = 0; i < res.length-1; i++) {
		 var row = select.insertRow(0);
		     var cell1 = row.insertCell(0);

     cell1.innerHTML="<table>"
	+"<tr>"
	+"<td><img src=./posters/"+res[i]+".jpg onclick=getVideo('"+res[i]+"') style='width:150px;height:100px;margin-top:10px;' ></td>"
	+"<td><button id='test-button' name='test' onclick=getVideo('"+res[i]+"') style='width:100%;height: 50px;margin-left:10px;margin-top:10px;'>تشغيل الفيديو</button></td>"
    +"</tr>"
	+" <tr>"
	+"<td><b><font size='3.5pt' color='#778899' align='center'  onclick=getVideo('"+res[i]+"')>"+res[i]+"</font></b></td>"
	+"<td><button id='test-button' name='test' onclick='stopVideo()' style='width:100%;height: 50px;margin-left:10px;margin-top:10px;'>إيقاف الفيديو</button></td>"
    +"</tr>"
	+"</table>"
	+"<hr>"
	+"<video id='videoPlayer"+i+"' width=100% height='0' margin-top=20px controls='true' muted onseeking=donothing() onseeked=doSomething('"+res[i]+"','"+i+"')><source src=./videos/"+res[i]+"></video><style>video::-webkit-media-controls-fullscreen-button {display: none;} video::-webkit-media-controls-mute-button { display: none;} video::-webkit-media-controls-toggle-closed-captions-button { display: none;} video::-webkit-media-controls-volume-slider { display: none;}</style><hr>";
    }
   }
	
function pauseVideo(iteration)
{
	var vid = document.getElementById("videoPlayer"+iteration);
    vid.pause();
}

function donothing(vName,iteration){}

function doSomething(vName,iteration)
{if(timeoutel!=0){clearTimeout(timeoutel);}
	timeoutel=setTimeout((playTimeVideo(vName,iteration),100))}

function uploadFailureHandler(responseText) {
    // alert(".لا يوجد أي فيديو ليتم تحميله");
}



function getCurTime() {
    alert(vid.currentTime);
}

function getRequest(url, success, error) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}
