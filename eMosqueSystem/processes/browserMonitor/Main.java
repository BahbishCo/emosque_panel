/*			Browser Monitor Process
 * 	
 * This process is used to control the browser behavior as the following:
 * 	1- open the browser process 
 * 	2- check every "checkEvery" second the browser process and apply one of the following:
 * 		A- if process is alive then sleep for "checkEvery" seconds.
 * 		B- if process is dead then kill current process and open new one. 
 * 		C- if current time is midnight then refresh the browser(destroy process and open new one).
 */

import java.util.Date;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.URL;
import java.util.Scanner;
import java.awt.Robot;

public class Main
{
	public static void main(String[] args)
	{
		
		BrowserMonitor BF = new BrowserMonitor();
		BF.openBrowserProcess();
	}
}



class BrowserMonitor extends Thread
{
	Process browserProcess;
	int randomNum = (int)(Math.random() * 1000000); // used to ensure loading original copy not cashed copy
	String configFilePath = "/home/pi/.config/midori/config";
	String configFilePath_root = "/root/.config/midori/config";

	String startFirefox = "sudo iceweasel -url http://localhost/emosquescreen/web-site/index.html?mode=0&rand=";
	String maxmizeFirefox = "sudo xdotool search --sync --onlyvisible --class \"Firefox\" windowactivate key F11";
	
	String debianCommand = "sudo midori";
	String rebootCmd = "sudo reboot";
	int checkEvery = 10;							//period in seconds between current check and next check

	
	// this function is used to open the browser process.
	public void openBrowserProcess()
	{
		try
		{
			Robot robot = new Robot();
			robot.mouseMove(3000,3000);
			
		}
		catch(Exception e)
		{
			System.out.println("unable to move mouse");
		}
		
		randomNum = (int)(Math.random() * 1000000);
				
		try
		{
			System.out.println("Openning New Process");
			configurationWriter();
			configurationWriter_root();
			if(firefoxIsInstalled())
			{
				System.out.println("Firefox browser");
				Runtime.getRuntime().exec("./maximize.sh");
				browserProcess = Runtime.getRuntime().exec(startFirefox+(int)(Math.random() * 1000000));
			}else
			{
				System.out.println("Midori browser");
				browserProcess = Runtime.getRuntime().exec(debianCommand);
			}
			monitorProcess();
			
		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Could not open process");
		}
	}

	private boolean firefoxIsInstalled() {

		String checkFirefoxCMD = "dpkg -l iceweasel"; 
		
		try {
			Process process = Runtime.getRuntime().exec(checkFirefoxCMD);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String output = "";
			String line;
			while ((line = stdInput.readLine()) != null) {
				output = line;
			}
			
			
			if(!output.contains("none"))
			{
				return true;
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return false;
	}

	// this function is used to monitor the status of the opened browser process. 
	public void monitorProcess()
	{
		while (true)
		{
			Date now = new Date();
			@SuppressWarnings("deprecation")
			int nowInMinutes = now.getHours() * 60 + now.getMinutes();			
			if(nowInMinutes == 0)
			{
				System.out
				.println("_________________________________________________________");
				System.out.println("Rebooting");
				browserProcess.destroy();

				try
				{
					Runtime.getRuntime().exec(rebootCmd);
					sleep(60*1000);

				} catch (Exception e)
				{
					System.out.println("Error in sleep");
				}
				System.out.println("openning Browser");
				openBrowserProcess();
				System.out
				.println("_________________________________________________________");
			}
				
			if (browserProcess.isAlive())
			{
				System.out.println("Process is alive");
				try
				{
					Thread.sleep(checkEvery * 1000);
				} catch (Exception e)
				{
					System.out.println("Error in sleep");
				}
			} else
			{
				System.out.println("Process isn't alive");
				System.out.println("Killing Process");
				browserProcess.destroy();
				System.out
						.println("_________________________________________________________");
				openBrowserProcess();
				break;
			}
		}
	}




/*
[settings]
default-encoding=ISO-8859-1
default-font-family=Droid Arabic Naskh
javascript-can-open-windows-automatically=true
enable-site-specific-quirks=true
last-window-width=1918
last-window-height=1023
last-window-state=MIDORI_WINDOW_FULLSCREEN
show-navigationbar=false
show-statusbar=false
location-entry-search=https://duckduckgo.com/?q=%s
load-on-startup=MIDORI_STARTUP_HOMEPAGE
homepage=http://localhost/emosquescreen/emosqueOnline
enable-html5-database=true
maximum-cache-size=0
user-agent=Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-gb) AppleWebKit/535+ (KHTML, like Gecko) Version/5.0 Safari/535.22+ Midori/0.4

*/



	
	
	public void configurationWriter_root()
	{
		try
		{

			File configFile = new File(configFilePath_root);			
			PrintWriter writer = new PrintWriter(configFilePath_root);
			writer.print("");
			writer.close();
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(configFilePath_root, true)));

			//writing browser configuration file
			out.println("[settings]");
			out.println("[settings]");
			out.println("default-encoding=ISO-8859-1");
			out.println("default-font-family=Droid Arabic Naskh");
			out.println("javascript-can-open-windows-automatically=true");
			out.println("enable-site-specific-quirks=true");
			out.println("last-window-width=1918");
			out.println("last-window-height=1023");
			out.println("last-window-state=MIDORI_WINDOW_FULLSCREEN");
			out.println("show-navigationbar=false");
			out.println("show-statusbar=false");
			out.println("location-entry-search=https://duckduckgo.com/?q=%s");
			out.println("load-on-startup=MIDORI_STARTUP_HOMEPAGE");
			out.println("homepage=http://localhost/emosquescreen/web-site/index.html?mode=0");
			out.println("enable-html5-database=true");
			out.println("maximum-cache-size=0");
			out.println("user-agent=Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-gb) AppleWebKit/535+ (KHTML, like Gecko) Version/5.0 Safari/535.22+ Midori/0.4");
			out.close();
		} catch (IOException e)
		{

			System.out.println("could not write to LOG file");
		}
	}
	
	public void configurationWriter()
	{
		try
		{

			File configFile = new File(configFilePath);			
			PrintWriter writer = new PrintWriter(configFilePath);
			writer.print("");
			writer.close();
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(configFilePath, true)));

			//writing browser configuration file
			out.println("[settings]");
			out.println("[settings]");
			out.println("default-encoding=ISO-8859-1");
			out.println("default-font-family=Droid Arabic Naskh");
			out.println("javascript-can-open-windows-automatically=true");
			out.println("enable-site-specific-quirks=true");
			out.println("last-window-width=1918");
			out.println("last-window-height=1023");
			out.println("last-window-state=MIDORI_WINDOW_FULLSCREEN");
			out.println("show-navigationbar=false");
			out.println("show-statusbar=false");
			out.println("location-entry-search=https://duckduckgo.com/?q=%s");
			out.println("load-on-startup=MIDORI_STARTUP_HOMEPAGE");
			out.println("homepage=http://localhost/emosquescreen/web-site/index.html?mode=0");
			out.println("enable-html5-database=true");
			out.println("maximum-cache-size=0");
			out.println("user-agent=Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-gb) AppleWebKit/535+ (KHTML, like Gecko) Version/5.0 Safari/535.22+ Midori/0.4");
			out.close();
		} catch (IOException e)
		{

			System.out.println("could not write to LOG file");
		}
	}



}
