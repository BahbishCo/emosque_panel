<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 7/3/2017
 * Time: 1:33 PM
 */

if ($_POST["userMode"] == null || $_POST["screenSetting"] == null ) {
    echo("<script> " . "alert(\"Some Information Is Missing!!, Please fill the form again and re-submit it.\")" . "</script>");
    return;
}
exec("sudo chmod 777 " . "/home/pi/.eMosqueSystem/userSelectedMode.txt", $x, $y);
exec("sudo chmod 777 " . "/var/www/emosquescreen/userMode.js", $x, $y);
exec("sudo chmod 777 " . $_SERVER['DOCUMENT_ROOT'] . "/emosquescreen/web-site/customFiles/settings/setting.js", $x, $y);

$userMode = $_POST["userMode"];
$panelSettings = $_POST["screenSetting"];
$settingFile= fopen($_SERVER['DOCUMENT_ROOT'] . "/emosquescreen/web-site/customFiles/settings/setting.js", 'w') or die("can't open settings file");
$javaFile = fopen("/home/pi/.eMosqueSystem/userSelectedMode.txt", 'w') or die("can't open user mode JAVA file");
$jsFile = fopen("/var/www/emosquescreen/userMode.js", 'w') or die("can't open user mode JS file");

$strJava = "mode=" . $userMode;
$strJS = "userMode = " . $userMode;
fwrite($jsFile, $strJS);
fclose($jsFile);
fwrite($javaFile, $strJava);
fclose($javaFile);
fwrite($settingFile, $panelSettings);
fclose($settingFile);
echo $panelSettings;
exec("sudo chmod 777 " . "/home/pi/.eMosqueSystem/userSelectedMode.txt", $x, $y);
exec("sudo chmod 777 " . "/var/www/emosquescreen/userMode.js", $x, $y);
exec("sudo chmod 777 " . $_SERVER['DOCUMENT_ROOT'] . "/emosquescreen/web-site/customFiles/settings/setting.js", $x, $y);

echo("Operation Completed");
$killMidori = "sudo pkill midori";
$killFirefox = "sudo pkill iceweasel";
exec($killMidori, $x, $y);
exec($killFirefox, $x, $y);
echo "ok";