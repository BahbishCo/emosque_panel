import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;

public class RaspberryPiMain {

    private static String clientContentFile;
    private static String clientSettingsFile;
    private static String clientWebsiteFile;
    private static String serverContentFile;
    private static String serverSettingsFile;
    private static String serverWebsiteFile;
    private static String myIpAdress = "";
    private static String month;
    private static URL clockUrl;
    private static URL temperatureURL;
    private static URL contentURL;
    private static URL settingsURL;
    private static URL websiteURL;
    private static Runtime rt = Runtime.getRuntime();
    private static String serverCommand = "/home/pi/.eMosqueSystem/server.sh";
    private static String clientCommand = "/home/pi/.eMosqueSystem/client.sh";
    private static String killMidori = "pkill midori";
    private static String versionContentPath = "/var/www/emosquescreen/other_components/version/content.txt";
    private static String versionSettingsPath = "/var/www/emosquescreen/other_components/version/settings.txt";
    private static String versionWebsitePath = "/var/www/emosquescreen/other_components/version/system.txt";
    private static String serverZipFilePath = "http://192.168.0.200/updates/emosquescreen.zip";
    private static String clientZipFilePath = "/home/pi/updates/emosquescreen.zip";
    public static final String STAT_FILE = "/home/pi/.eMosqueSystem/ipstatus";
    static String clientExtractedZipFilePath = "/home/pi/updates/temp";


    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub

        // ---- Get my device IP addresses ---- //

        // getDeviceIpAddresses();
        String everything = null;
        BufferedReader br = new BufferedReader(new FileReader("/sys/class/net/eth0/operstate"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            everything = sb.toString();
        } finally {
            br.close();
        }

        if (everything!=null && everything.trim().equals("up"))
        {
            return;
        }


        final int deviceType = 1;//TODO getDeviceType();

        // ---- check if i'm a server or a client ---- //

        if (deviceType == 0) {
            System.out.println("I'm a Server");
            try {
                rt.exec(serverCommand);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            System.out.println("I'm a Client");

            if (deviceType == 1) {
                // ---- Get the server clock and change the client clock
                // accordingly
                // ---- //
                changeClientClock();

                try {
                    rt.exec(clientCommand);
                    System.out.println("executed client command");

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    System.out.println("could not execute client command");

                }
                new Thread() {
                    public void run() {
                        while (true) {
                            try {
                                if (deviceType == 1) {
                                    // ---- Wait for 5 seconds ---- //
                                    sleep(5000);

                                    // ---- Get the server temperature and write
                                    // it
                                    // in
                                    // client temp file ---- //
                                    changeTemp();

                                    // ---- Read the version files' contents
                                    // from my
                                    // device ---- //
                                    readClientVersionFiles();

                                    // ---- Read the version files' contents
                                    // from
                                    // the
                                    // server ---- //
                                    readServerVersionFiles();

                                    // ---- Check if any changes are made ----
                                    // //

                                    if (!clientContentFile.equals(serverContentFile)) {
                                        // ---- change client SpecEvent File's
                                        // Content
                                        // ---- //
                                        System.out.println(
                                                "Content File Has Changed, getting the new file from the server ..");
                                        changeClientSpecEventFile();
                                        killMidori();
                                    }
                                    if (!clientSettingsFile.equals(serverSettingsFile)) {
                                        // ---- Change client settings file's
                                        // content
                                        // ---- //
                                        System.out.println(
                                                "Settings File Has Changed, getting the new file from the server ..");
                                        changeClientSettingsFile();
                                        killMidori();
                                    }
                                    if (!clientWebsiteFile.equals(serverWebsiteFile)) {
                                        System.out.println(
                                                "Website File Has Changed, getting the new file from the server ..");
                                        downloadZipFileFromServer();
                                        try {
                                            File file = new File(clientExtractedZipFilePath);
                                            file.mkdirs();

                                            // ---- execute do.sh bash file ----
                                            // //
                                            unZipFile(clientZipFilePath, clientExtractedZipFilePath);
                                            rt.exec("sudo chmod +x /home/pi/.eMosqueSystem/processes/RaspberryPiConfig/execUpdate.sh");
                                            rt.exec("sudo /home/pi/.eMosqueSystem/processes/RaspberryPiConfig/execUpdate.sh");
                                            // ---- Change client Website file's
                                            // content
                                            // ---- //
                                            // changeClientWebsiteFile();
                                        } catch (IOException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                System.out.println("error connecting with the server");
                            }
                        }
                    }
                }.start();

            } else {
                try {
                    rt.exec(clientCommand);
                    System.out.println("executed client command");

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    System.out.println("could not execute client command");

                }
            }

        }
    }

    public static boolean isWifiConnected() {
        boolean isConnected = false;
        try {
            FileInputStream fis = new FileInputStream(new File(STAT_FILE));
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = br.readLine();
            int connectionStatus = Integer.parseInt(line);
            if (connectionStatus == 1) {
                isConnected = true;
            } else {
                isConnected = false;
            }
            br.close();
            System.out.println("Is Wifi Connected: " + isConnected);

        } catch (Exception e) {
            System.out.println("Unable to Read Connection Status File");
        }
        return isConnected;
    }

    public static boolean isValidIP(byte lastDigit) throws IOException {
        InetAddress inet;
        inet = InetAddress.getByAddress(new byte[]{(byte) 192, (byte) 168, 0, lastDigit});
        System.out.println("Sending Ping Request to " + inet);
        return !inet.isReachable(5000);
    }

    public static int getDeviceType() {
        try {
            String ipStatusFile = STAT_FILE;
            FileInputStream fstream = new FileInputStream(ipStatusFile);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine = br.readLine();
            if (strLine == null) {
                return 0;
            } else {
                return Integer.parseInt(strLine);
            }
        } catch (Exception e) {
            return 0;
        }

    }

    public static void killMidori() {

        try {
            rt.exec(killMidori);
            rt.exec(Constants.Commands.killFirefox);            
            System.out.println("killed midori");
        } catch (IOException e) {
            System.out.println("unable to kill midori");
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getDeviceIpAddresses() {
        try {
            Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
            while (e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                if (!n.getName().equals("lo")) {
                    Enumeration<InetAddress> ee = n.getInetAddresses();
                    while (ee.hasMoreElements()) {
                        InetAddress i = (InetAddress) ee.nextElement();
                        myIpAdress = i.getHostAddress();
                        if (myIpAdress.equals("192.168.0.200"))
                            break;
                    }
                    if (myIpAdress.equals("192.168.0.200"))
                        break;
                }
            }

            System.out.println(myIpAdress);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void changeClientClock() {
        try {
            clockUrl = new URL("http://192.168.0.200/emosquescreen/other_components/SharedSettings/clock.php?");
            URLConnection clockConnection = clockUrl.openConnection();
            clockConnection.connect();

            BufferedReader clockBufReader = new BufferedReader(new InputStreamReader(clockConnection.getInputStream()));
            String serverClock = clockBufReader.readLine();
            System.out.println("Server Clock : " + serverClock);

            String[] DateAndTime = serverClock.split(" ", -1);
            String[] Date = DateAndTime[0].split("/", -1);
            String[] Time = DateAndTime[1].split(":", -1);
            clockBufReader.close();

            if (Date[1].equals("01")) {
                month = "Jan";
            } else if (Date[1].equals("02")) {
                month = "Feb";
            } else if (Date[1].equals("03")) {
                month = "Mar";
            } else if (Date[1].equals("04")) {
                month = "Apr";
            } else if (Date[1].equals("05")) {
                month = "May";
            } else if (Date[1].equals("06")) {
                month = "Jun";
            } else if (Date[1].equals("07")) {
                month = "Jul";
            } else if (Date[1].equals("08")) {
                month = "Aug";
            } else if (Date[1].equals("09")) {
                month = "Sep";
            } else if (Date[1].equals("10")) {
                month = "Oct";
            } else if (Date[1].equals("11")) {
                month = "Nov";
            } else if (Date[1].equals("12")) {
                month = "Dec";
            }

            rt.exec(new String[]{"sudo", "date", "-s",
                    month + "  " + Date[0] + " " + Time[0] + ":" + Time[1] + ":" + Time[2] + " EET " + Date[2]});

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void changeTemp() {
        try {
            temperatureURL = new URL("http://192.168.0.200/emosquescreen/temp.txt");
            URLConnection temperatureConnection = temperatureURL.openConnection();
            temperatureConnection.connect();

            BufferedReader temperatureBufReader = new BufferedReader(
                    new InputStreamReader(temperatureConnection.getInputStream()));
            String serverTemperature = temperatureBufReader.readLine();
            System.out.println("Server Temperature : " + serverTemperature);

            BufferedWriter clientTempBufWriter = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream("/var/www/emosquescreen/temp.txt")));
            clientTempBufWriter.write(serverTemperature);
            clientTempBufWriter.flush();
            temperatureBufReader.close();
            clientTempBufWriter.close();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void readClientVersionFiles() {
        BufferedReader clientContentBufReader;
        try {
            clientContentBufReader = new BufferedReader(new InputStreamReader(new FileInputStream(versionContentPath)));
            BufferedReader clientSettingsBufReader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(versionSettingsPath)));
            BufferedReader clientWebsiteBufReader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(versionWebsitePath)));

            clientContentFile = clientContentBufReader.readLine();
            clientSettingsFile = clientSettingsBufReader.readLine();
            clientWebsiteFile = clientWebsiteBufReader.readLine();

            if (clientContentFile == null)
                clientContentFile = "0";

            if (clientSettingsFile == null)
                clientSettingsFile = "0";

            if (clientWebsiteFile == null)
                clientWebsiteFile = "0";

            System.out.print("Client Content : " + clientContentFile + ", ");
            System.out.print("Client Settings : " + clientSettingsFile + ", ");
            System.out.print("Client Website : " + clientWebsiteFile);
            System.out.println();

            clientContentBufReader.close();
            clientSettingsBufReader.close();
            clientWebsiteBufReader.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void readServerVersionFiles() {
        try {
            contentURL = new URL("http://192.168.0.200/emosquescreen/other_components/version/content.txt");
            URLConnection contentConnection = contentURL.openConnection();
            contentConnection.connect();

            BufferedReader contentBufReader = new BufferedReader(
                    new InputStreamReader(contentConnection.getInputStream()));
            serverContentFile = contentBufReader.readLine();
            System.out.print("Server Content : " + serverContentFile + ", ");

            contentBufReader.close();

            settingsURL = new URL("http://192.168.0.200/emosquescreen/other_components/version/settings.txt");
            URLConnection settingsConnection = settingsURL.openConnection();
            settingsConnection.connect();

            BufferedReader settingsBufReader = new BufferedReader(
                    new InputStreamReader(settingsConnection.getInputStream()));
            serverSettingsFile = settingsBufReader.readLine();
            System.out.print("Server Settings : " + serverSettingsFile + ", ");


            settingsBufReader.close();

            websiteURL = new URL("http://192.168.0.200/emosquescreen/other_components/version/system.txt");
            URLConnection websiteConnection = websiteURL.openConnection();
            websiteConnection.connect();

            BufferedReader websiteBufReader = new BufferedReader(
                    new InputStreamReader(websiteConnection.getInputStream()));
            serverWebsiteFile = websiteBufReader.readLine();
            System.out.print("Server Website : " + serverWebsiteFile);
            System.out.println();
            websiteBufReader.close();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void changeClientSpecEventFile() {
        try {
            String serverSpecEventContent = downloadFileContent(
                    "http://192.168.0.200/emosquescreen/web-site/JSON/specEvent.js");
            BufferedWriter specEventBufWriter;
            specEventBufWriter = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream("/var/www/emosquescreen/web-site/JSON/specEvent.js")));
            specEventBufWriter.write(serverSpecEventContent);

            BufferedWriter clientContentBufWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("/var/www/emosquescreen/other_components/version/content.txt")));
            if (serverContentFile != null)
                clientContentBufWriter.write(serverContentFile);

            specEventBufWriter.flush();
            specEventBufWriter.close();
            clientContentBufWriter.flush();
            clientContentBufWriter.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void changeClientSettingsFile() {
        try {
            String serverSettings = downloadFileContent("http://192.168.0.200/emosquescreen/web-site/JSON/setting.js");
            BufferedWriter SettingsBufWriter;
            SettingsBufWriter = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream("/var/www/emosquescreen/web-site/JSON/setting.js")));
            SettingsBufWriter.write(serverSettings);

            BufferedWriter clientSettingsBufWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("/var/www/emosquescreen/other_components/version/settings.txt")));
            if (serverSettingsFile != null)
                clientSettingsBufWriter.write(serverSettingsFile);

            SettingsBufWriter.flush();
            SettingsBufWriter.close();
            clientSettingsBufWriter.flush();
            clientSettingsBufWriter.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String downloadFileContent(String URLString) {
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;
        boolean error = false;

        StringBuilder response = new StringBuilder();
        try {
            url = new URL(URLString);
            is = url.openStream(); // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                response.append(line + "\n");
            }
        } catch (MalformedURLException mue) {
            error = true;
            mue.printStackTrace();
        } catch (IOException ioe) {
            error = true;
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null)
                    is.close();
            } catch (IOException ioe) {
                error = true;
            }
        }

        if (error || response.toString().length() == 0)
            return null;
        else
            return response.toString();
    }

    static public void downloadZipFileFromServer() {
        try {
            File file = new File(clientZipFilePath);
            file.getParentFile().mkdirs();
            BufferedInputStream in = null;
            FileOutputStream fout = null;
            try {
                in = new BufferedInputStream(new URL(serverZipFilePath).openStream());
                fout = new FileOutputStream(clientZipFilePath);

                final byte data[] = new byte[1024];
                int count;
                while ((count = in.read(data, 0, 1024)) != -1) {
                    fout.write(data, 0, count);
                }
            } finally {
                if (in != null) {
                    in.close();
                }
                if (fout != null) {
                    fout.close();
                }
            }

//            URL url = new URL(serverZipFilePath);
//            URLConnection conn = url.openConnection();
//            InputStream in = conn.getInputStream();

//            FileOutputStream out = new FileOutputStream(clientZipFilePath);
//            byte[] b = new byte[1024];
//            int count;
//            while ((count = in.read(b)) >= 0) {
//                out.write(b, 0, count);
//            }
//            out.flush();
//            out.close();
//            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static public void unZipFile(String inputZip, String destinationDirectory) throws IOException {


        ZipUtils.extract(new File(inputZip), new File(destinationDirectory));

//        String source = "folder/source.zip";
//        String destination = "folder/source/";
//
//        try {
//            ZipFile zipFile = new ZipFile(source);
//            zipFile.extra(destination);
//        } catch (ZipException e) {
//            e.printStackTrace();
//        }

//        int BUFFER = 2048;
//        List zipFiles = new ArrayList();
//        File sourceZipFile = new File(inputZip);
//        File unzipDestinationDirectory = new File(destinationDirectory);
//        unzipDestinationDirectory.getParentFile().mkdirs();
//
//        ZipFile zipFile;
//        // Open Zip file for reading
//        zipFile = new ZipFile(sourceZipFile, ZipFile.OPEN_READ);
//
//        // Create an enumeration of the entries in the zip file
//        Enumeration zipFileEntries = zipFile.entries();
//
//        // Process each entry
//        while (zipFileEntries.hasMoreElements()) {
//            // grab a zip file entry
//            ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
//
//            String currentEntry = entry.getName();
//
//            File destFile = new File(unzipDestinationDirectory, currentEntry);
//            destFile = new File(unzipDestinationDirectory, destFile.getName());
//
//            if (currentEntry.endsWith(".zip")) {
//                zipFiles.add(destFile.getAbsolutePath());
//            }
//
//            // grab file's parent directory structure
//            File destinationParent = destFile.getParentFile();
//
//            // create the parent directory structure if needed
//            destinationParent.mkdirs();
//
//            try {
//                // extract file if not a directory
//                if (!entry.isDirectory()) {
//                    BufferedInputStream is = new BufferedInputStream(zipFile.getInputStream(entry));
//                    int currentByte;
//                    // establish buffer for writing file
//                    byte data[] = new byte[BUFFER];
//
//                    // write the current file to disk
//                    FileOutputStream fos = new FileOutputStream(destFile);
//                    BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
//
//                    // read and write until last byte is encountered
//                    while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
//                        dest.write(data, 0, currentByte);
//                    }
//                    dest.flush();
//                    dest.close();
//                    is.close();
//                }
//            } catch (IOException ioe) {
//                ioe.printStackTrace();
//            }
//        }
//        zipFile.close();
//
//        for (Iterator iter = zipFiles.iterator(); iter.hasNext(); ) {
//            String zipName = (String) iter.next();
//            unZipFile(zipName,
//                    destinationDirectory + File.separatorChar + zipName.substring(0, zipName.lastIndexOf(".zip")));
//        }

    }
}