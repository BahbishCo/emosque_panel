function pageScroll() {

    //setTimeout(parent.switchFromAdToHadeeth(),9000);
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        //setTimeout(scrollToTop, 3000);
        //setTimeout(parent.switchFromAdToHadeeth(),9000);
        return;
    }
    window.scrollBy(0, 1);
    //setTimeout(pageScroll, 100);
}

function scrollToTop() {
    window.scrollTo(0, 0);
    setTimeout(pageScroll, 3000);
}


function getParamValueByName(name, windowId) {
    if (windowId == null) {

        var query = window.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }
    else {
        var iframe = document.getElementById(windowId);
        var query = iframe.contentWindow.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }

}