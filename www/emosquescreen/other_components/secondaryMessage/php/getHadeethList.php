<?php


//$imamMessagesPath = "C:\\xampp\\htdocs\\eMosqueSystem\\www\\emosquescreen\\web-site\\customFiles\\contents\\primaryMessages\\ImamMessages";
$imamMessagesPath = "/var/www/emosquescreen/web-site/customFiles/contents/primaryMessages/ImamMessages/";

$directoyContent = @scandir($imamMessagesPath);

$haeethArrayDate = [];
$hadeethArrayText = [];

$today = date("Y-m-d");

for ($i = 0; $i < sizeof($directoyContent); $i++) {
    if ($directoyContent[$i] == "." || $directoyContent[$i] == "..") {
        continue;
    }

    $extension = strtolower(substr($directoyContent[$i], strrpos($directoyContent[$i], '.') + 1));
    if ($extension == "html") {
        $hadeethDate = getHadeethDate(substr($directoyContent[$i], 0, strrpos($directoyContent[$i], '.')));
        $hadeethText = getHadeethText($directoyContent[$i]);
        if ($today == $hadeethDate) {
            array_unshift($haeethArrayDate, $hadeethDate);
            array_unshift($hadeethArrayText, $hadeethText);
        } else {
            array_push($haeethArrayDate, $hadeethDate);
            array_push($hadeethArrayText, $hadeethText);
        }

    }
}
//print_r($hadeethArrayText);
echo(json_encode(array("dates" => $haeethArrayDate, "text" => $hadeethArrayText)));



function getHadeethDate($str)
{
    $temp = explode(".", $str);
    $dateString = $temp[0];
    $temp = explode("_", $dateString);
    $startDate = $temp[0];
    return $startDate;
}


function getHadeethText($str)
{
    $text = file_get_contents($GLOBALS["imamMessagesPath"] . $str, FILE_USE_INCLUDE_PATH);

    $text = explode("<p", $text);

    $text = explode(">", $text[1]);

    $text = explode("</p>", $text[1]);

    $text = explode("<", $text[0]);
    //$text = explode("\r\n", $text[0]);
    return $text[0];
}


?>