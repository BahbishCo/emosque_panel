<?php

// Turn off all error reporting
error_reporting(0);

//Check if we are getting the image
if (isset($_FILES['image'])) {
    //Get the image array of details
    $img = $_FILES['image'];
    //The new path of the uploaded image, rand is just used for the sake of it
    //$path = "C:\\xampp\\htdocs\\emosque_panel_dev\\www\\emosquescreen\\other_components\\primaryMessage\\php/uploaded/" . rand().$img["name"];

    $path = dirname(dirname(dirname((getcwd())))) . "/web-site/customFiles/contents/primaryMessages/ImamMessages/imgs/";
    mkdir($path, 0777, true);
    $fileName = rand() . $img["name"];
    $path = $path . $fileName;
    move_uploaded_file($img['tmp_name'], $path);

    //echo $path;
    //Move the file to our new path
    //Get image info, reuiqred to biuld the JSON object
    $data = getimagesize($path);
    //The direct link to the uploaded image, this might varyu depending on your script location
//    $link = "http://$_SERVER[HTTP_HOST]" . "/emosque_panel_dev/www/emosquescreen/web-site/customFiles/contents/primaryMessages/ImamMessages/imgs/" . $fileName;
    $link = "http://$_SERVER[HTTP_HOST]" . "/emosquescreen/web-site/customFiles/contents/primaryMessages/ImamMessages/imgs/" . $fileName;
    //echo $link;
    //Here we are constructing the JSON Object
//    $res = array("upload" => array(
//        "links" => array("original" => $link),
//        "image" => array("width" => $data[0],
//            "height" => $data[1]
//        )
//    ));
    $res = array("data" => array("link" => $link, "width" => $data[0], "height" => $data[1]));
    //echo out the response :)
    echo json_encode($res);
}
?>