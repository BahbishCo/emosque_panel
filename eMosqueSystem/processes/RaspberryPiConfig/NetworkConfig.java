
public class NetworkConfig {

	int type = Constants.NetworkConfig.wifi;
	String wifiName;
	String password;
	boolean isValid = false;
	String netWorkID;
	String ipAddress;
	String netMask;
	String gateWay;

	public NetworkConfig(String fileContent) {
		String[] lines = fileContent.split(System.lineSeparator());
		String temp[];
		for (String line : lines) {
			temp = line.split("=");
			temp[0] = temp[0].trim();
			temp[1] = temp[1].trim();
			if (temp[0].equals("type")) {
				try {
					this.type = Integer.parseInt(temp[1]);
				} catch (Exception e) {
					type = Constants.NetworkConfig.wifi;
				}
			}
			if(temp[0].equals("wifi-name(ssid)"))
			{
				wifiName = temp[1];
			}
			if(temp[0].equals("password"))
			{
				password= temp[1];
			}
			if(temp[0].equals("valid"))
			{
				isValid= temp[1].toLowerCase().equals("true")?true:false;
			}
		}
	}

}
