var screenSetting = 
{
prayerSetting: 
{
country: "Jordan",
city: "Amman",
calculationMethod: "Karachi",
asrJuristic: "Standard",
adjusthighLat: "AngleBased",
midNightMethod: "Standard",
fajrAngle: 18,
ishaAngle: 18,
latitude: 32.02027777777778,
longitude: 35.82944444444445,
elevation: 1100,
timeZone: +3,
offset: {fajr: 0, dhuhr: 0, asr: 0, maghrib: 0, isha: 0, },
},
hijriCorrection: -1,
timeBetweenAzanAndIqama: {fajr: 30, duhur: 18, asr: 15, maghrib: 8, ishaa: 13, jumaa: 0, },
timeBetweenAzanAndAzkar: {fajr: 38, duhur: 26, asr: 23, maghrib: 18, ishaa: 25, jumaa: 50, },
azkarDisplayPeriod: {fajr: 5, duhur: 5, asr: 5, maghrib: 5, ishaa: 5, jumaa: 10, },
appQREnable: false,
internalTempEnable: true,
playAzan: false,
showAnalogClock: false,
showBahbishMessage: false,
displayAdAfterPrayer: true,
}