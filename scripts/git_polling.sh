#!/bin/bash
cd /etc/panel;
oldCommit=$(git log --name-status HEAD^..HEAD | grep commit);
newCommit=$(git log --name-status HEAD^..HEAD | grep commit);

# The loop stage.
while [ "$oldCommit" == "$newCommit" ]
do
DIR="/etc/panel";
if [ "$(ls -A $DIR)" ]; then
    commits=$(git rev-list --count master);
    re='^[0-9]+$'
    if ! [[ $commits =~ $re ]] ; then
        echo "Not a number; cannot determine the commit ID now, will skip to the next iteration.";
    else
       echo "is a number, can ";
       echo no change;
       newCommit=$(git log --name-status HEAD^..HEAD | grep commit);
    fi
else
    echo "$DIR is Empty"
fi
sleep 5;
done
sudo touch /etc/updateAvailable;
sudo chmod 777 /etc/updateAvailable;