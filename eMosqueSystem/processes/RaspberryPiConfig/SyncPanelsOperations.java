import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class SyncPanelsOperations
{

    private static String clientContentFile;
    private static String clientSettingsFile;
    private static String clientWebsiteFile;
    private static String serverContentFile;
    private static String serverSettingsFile;
    private static String serverWebsiteFile;
    private static URL contentURL;
    private static URL settingsURL;
    private static URL websiteURL;

    public static void syncPanels()
    {
	new Thread()
	{
	    public void run()
	    {
		while (true)
		{
		    try
		    {
			readClientVersionFiles();
			readServerVersionFiles();
			updateContentFile();
			updateSettingFile();
			updateSystem();
			sleep(15 * 1000);
		    } catch (Exception e)
		    {
			e.printStackTrace();
		    }
		}
	    }
	}.start();
    }

    public static void readClientVersionFiles()
    {
	try
	{
	    BufferedReader clientContentBufReader = new BufferedReader(
		    new InputStreamReader(new FileInputStream(
			    Constants.Locations.versionContentPath)));
	    BufferedReader clientSettingsBufReader = new BufferedReader(
		    new InputStreamReader(new FileInputStream(
			    Constants.Locations.versionSettingsPath)));
	    BufferedReader clientWebsiteBufReader = new BufferedReader(
		    new InputStreamReader(new FileInputStream(
			    Constants.Locations.versionWebsitePath)));

	    clientContentFile = clientContentBufReader.readLine();
	    clientSettingsFile = clientSettingsBufReader.readLine();
	    clientWebsiteFile = clientWebsiteBufReader.readLine();

	    if (clientContentFile == null)
		clientContentFile = "0";

	    if (clientSettingsFile == null)
		clientSettingsFile = "0";

	    if (clientWebsiteFile == null)
		clientWebsiteFile = "0";

	    System.out.print("Client Content : " + clientContentFile + ", ");
	    System.out.print("Client Settings : " + clientSettingsFile + ", ");
	    System.out.print("Client Website : " + clientWebsiteFile);
	    System.out.println();

	    clientContentBufReader.close();
	    clientSettingsBufReader.close();
	    clientWebsiteBufReader.close();
	} catch (FileNotFoundException e)
	{
	    e.printStackTrace();
	} catch (IOException e)
	{
	    e.printStackTrace();
	}
    }

    public static void readServerVersionFiles()
    {
	try
	{
	    contentURL = new URL(Constants.Locations.serverContentVersionFiles);
	    URLConnection contentConnection = contentURL.openConnection();
	    contentConnection.connect();

	    BufferedReader contentBufReader = new BufferedReader(
		    new InputStreamReader(contentConnection.getInputStream()));
	    serverContentFile = contentBufReader.readLine();
	    System.out.print("Server Content : " + serverContentFile + ", ");

	    contentBufReader.close();

	    settingsURL = new URL(Constants.Locations.serverSettingVersionFiles);
	    URLConnection settingsConnection = settingsURL.openConnection();
	    settingsConnection.connect();

	    BufferedReader settingsBufReader = new BufferedReader(
		    new InputStreamReader(settingsConnection.getInputStream()));
	    serverSettingsFile = settingsBufReader.readLine();
	    System.out.print("Server Settings : " + serverSettingsFile + ", ");

	    settingsBufReader.close();

	    websiteURL = new URL(Constants.Locations.serverSystemVersionFiles);
	    URLConnection websiteConnection = websiteURL.openConnection();
	    websiteConnection.connect();

	    BufferedReader websiteBufReader = new BufferedReader(
		    new InputStreamReader(websiteConnection.getInputStream()));
	    serverWebsiteFile = websiteBufReader.readLine();
	    System.out.print("Server Website : " + serverWebsiteFile);
	    System.out.println();
	    websiteBufReader.close();
	} catch (MalformedURLException e)
	{
	    e.printStackTrace();
	} catch (IOException e)
	{
	    e.printStackTrace();
	}
    }

    public static void updateContentFile()
    {
	if (!clientContentFile.equals(serverContentFile))
	{
	    // ---- change client SpecEvent File's
	    // Content
	    // ---- //
	    System.out
		    .println("Content File Has Changed, getting the new file from the server ..");
	    changeClientSpecEventFile();
	}
    }

    public static void updateSettingFile()
    {
	if (!clientSettingsFile.equals(serverSettingsFile))
	{
	    // ---- Change client settings file's
	    // content
	    // ---- //
	    System.out
		    .println("Settings File Has Changed, getting the new file from the server ..");
	    changeClientSettingsFile();
	}
    }

    public static void updateSystem()
    {

	int clientSystemVersionNumber = Integer.parseInt(clientWebsiteFile);
	int serverSystemVersionNumber = Integer.parseInt(serverWebsiteFile);

	if (clientSystemVersionNumber < serverSystemVersionNumber)
	{
	    System.out
		    .println("Website File Has Changed, getting the new file from the server ..");
	    downloadZipFileFromServer();
	    try
	    {
		File file = new File(
			Constants.Locations.clientExtractedZipFilePath);
		file.mkdirs();

		// ---- execute do.sh bash file ----
		// //
		unZipFile(Constants.Locations.clientZipFilePath,
			Constants.Locations.clientExtractedZipFilePath);
		Utils.rt.exec(Constants.Commands.makeScripExcutable);
		Utils.rt.exec(Constants.Commands.excuteDoScript);
		// ---- Change client Website file's
		// content
		// ---- //
		// changeClientWebsiteFile();
	    } catch (IOException e)
	    {
		e.printStackTrace();
	    }
	} else if (clientSystemVersionNumber > serverSystemVersionNumber)
	{
	    HttpGetRequset restorePrevVersionRequest = new HttpGetRequset(
		    Constants.Locations.restoreVersionURL);
	    restorePrevVersionRequest.excuteRequest();
	}
    }

    public static void changeClientSpecEventFile()
    {
	try
	{
	    String serverSpecEventContent = downloadFileContent(Constants.Locations.serverContentFile);
	    BufferedWriter specEventBufWriter;
	    specEventBufWriter = new BufferedWriter(
		    new OutputStreamWriter(new FileOutputStream(
			    Constants.Locations.clientContentFile)));
	    specEventBufWriter.write(serverSpecEventContent);

	    BufferedWriter clientContentBufWriter = new BufferedWriter(
		    new OutputStreamWriter(new FileOutputStream(
			    Constants.Locations.versionContentPath)));
	    if (serverContentFile != null)
		clientContentBufWriter.write(serverContentFile);

	    specEventBufWriter.flush();
	    specEventBufWriter.close();
	    clientContentBufWriter.flush();
	    clientContentBufWriter.close();
	} catch (FileNotFoundException e)
	{
	    e.printStackTrace();
	} catch (IOException e)
	{
	    e.printStackTrace();
	}
    }

    public static String downloadFileContent(String URLString)
    {
	URL url;
	InputStream is = null;
	BufferedReader br;
	String line;
	boolean error = false;

	StringBuilder response = new StringBuilder();
	try
	{
	    url = new URL(URLString);
	    is = url.openStream(); // throws an IOException
	    br = new BufferedReader(new InputStreamReader(is));

	    while ((line = br.readLine()) != null)
	    {
		response.append(line + "\n");
	    }
	} catch (MalformedURLException mue)
	{
	    error = true;
	    mue.printStackTrace();
	} catch (IOException ioe)
	{
	    error = true;
	    ioe.printStackTrace();
	} finally
	{
	    try
	    {
		if (is != null)
		    is.close();
	    } catch (IOException ioe)
	    {
		error = true;
	    }
	}

	if (error || response.toString().length() == 0)
	    return null;
	else
	    return response.toString();
    }

    public static void killMidori()
    {

	try
	{
	    Utils.rt.exec(Constants.Commands.killMidori);
	    Utils.rt.exec(Constants.Commands.killFirefox);
	    System.out.println("killed midori");
	} catch (IOException e)
	{
	    System.out.println("unable to kill midori");
	    e.printStackTrace();
	}
    }

    public static void changeClientSettingsFile()
    {
	try
	{
	    String serverSettings = downloadFileContent(Constants.Locations.serverSettingFile);
	    BufferedWriter SettingsBufWriter;
	    SettingsBufWriter = new BufferedWriter(
		    new OutputStreamWriter(new FileOutputStream(
			    Constants.Locations.clientSettingFile)));
	    SettingsBufWriter.write(serverSettings);

	    BufferedWriter clientSettingsBufWriter = new BufferedWriter(
		    new OutputStreamWriter(new FileOutputStream(
			    Constants.Locations.versionSettingsPath)));
	    if (serverSettingsFile != null)
		clientSettingsBufWriter.write(serverSettingsFile);

	    SettingsBufWriter.flush();
	    SettingsBufWriter.close();
	    clientSettingsBufWriter.flush();
	    clientSettingsBufWriter.close();
	} catch (FileNotFoundException e)
	{
	    e.printStackTrace();
	} catch (IOException e)
	{
	    e.printStackTrace();
	}
    }

    static public void downloadZipFileFromServer()
    {
	try
	{
	    File file = new File(Constants.Locations.clientZipFilePath);
	    file.getParentFile().mkdirs();
	    BufferedInputStream in = null;
	    FileOutputStream fout = null;
	    try
	    {
		in = new BufferedInputStream(new URL(
			Constants.Locations.serverZipFilePath).openStream());
		fout = new FileOutputStream(
			Constants.Locations.clientZipFilePath);

		final byte data[] = new byte[1024];
		int count;
		while ((count = in.read(data, 0, 1024)) != -1)
		{
		    fout.write(data, 0, count);
		}
	    } finally
	    {
		if (in != null)
		{
		    in.close();
		}
		if (fout != null)
		{
		    fout.close();
		}
	    }

	    // URL url = new URL(serverZipFilePath);
	    // URLConnection conn = url.openConnection();
	    // InputStream in = conn.getInputStream();

	    // FileOutputStream out = new FileOutputStream(clientZipFilePath);
	    // byte[] b = new byte[1024];
	    // int count;
	    // while ((count = in.read(b)) >= 0) {
	    // out.write(b, 0, count);
	    // }
	    // out.flush();
	    // out.close();
	    // in.close();

	} catch (IOException e)
	{
	    e.printStackTrace();
	}
    }

    static public void unZipFile(String inputZip, String destinationDirectory)
    {
	ZipUtils.extract(new File(inputZip), new File(destinationDirectory));
    }

}
