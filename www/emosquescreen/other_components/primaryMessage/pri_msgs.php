<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 12/9/2016
 * Time: 4:02 PM
 */?>


<div id="myWrapper" class="content-wrapper">

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <h2 class="text-center">ﻣﺴﺠﺪﻱ - ﻧﻈﺎﻡ اﻟﺮﺳﺎﺋﻞ</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-md-12">
            <div class="tabbable">
                <ul class="nav nav-tabs pull-right">
                    <li>
                        <a href="#editMessageTab" data-toggle="tab">ﻋﺮﺽ ﻭﺗﻌﺪﻳﻞ اﻟﺮﺳﺎﺋﻞ</a>
                    </li>
                    <li class="active">
                        <a onclick="killEditPreviewMessageTab();" href="#addMessageTab" data-toggle="tab">اﺿﺎﻓﺔ
                            ﺭﺳﺎﻟﺔ</a>
                    </li>
                </ul>
                <div class="clearfix"></div>
                <div class="tab-content">
                    <div class="tab-pane  active fade in" id="addMessageTab">
                        <div class="row">
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title text-right">
                                            <span>اﺿﺎﻓﺔ ﺭﺳﺎﻟﺔ
                                            </span>
                                        </h4>
                                    </div>
                                    <div id="messageSettingForm" class="panel-collapse collapse in">
                                        <div class="panel-body">

                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right">
                                                    <label for="countrySelector"
                                                           class="pull-right inline-element text-right"> اﺧﺘﺮ
                                                        اﻟﺪﻭﻟﺔ </label>
                                                    <!-- updateCitySelector(this); -->
                                                    <select onchange=""
                                                            class="form-control pull-right text-align-right"
                                                            id="countrySelector">
                                                        <option class="pull-right text-right"></option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right">
                                                    <label for="citySelector"
                                                           class="pull-right inline-element text-right"> اﺧﺘﺮ
                                                        اﻟﻤﺪﻳﻨﺔ </label>
                                                    <select onchange=""
                                                            class="form-control pull-right"
                                                            id="citySelector">
                                                        <option class="pull-right text-right"></option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right">

                                                    <label for="insertMessage_startDatePicker"
                                                           class="pull-right inline-element">
                                                        ﺗﺎﺭﻳﺦ
                                                        ﺑﺪاﻳﺔ
                                                        اﻟﺮﺳﺎﻟﺔ </label>
                                                    <input class="form-control pull-right text-right" type="text"
                                                           id="insertMessage_startDatePicker"
                                                    >
                                                </div>


                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right">

                                                    <label for="insertMessage_endDatePicker"
                                                           class="pull-right inline-element"> ﺗﺎﺭﻳﺦ
                                                        ﻧﻬﺎﻳﺔ
                                                        اﻟﺮﺳﺎﻟﺔ </label>
                                                    <input class="form-control pull-right text-right" type="text"
                                                           id="insertMessage_endDatePicker"
                                                    >
                                                </div>

                                            </div>


                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right text-right">

                                                    ارسال الرسالة لجميع الجهات
                                                    <input class="pull-right text-right"
                                                           type="checkbox"
                                                           checked="true"
                                                           id="msgDest_all"
                                                    >
                                                </div>


                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right text-right">

                                                    ارسال الرسالة للوحات المساجد
                                                    <input class="pull-right text-right"
                                                           type="checkbox"
                                                           id="msgDest_mosquePanels"
                                                    >
                                                </div>


                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right text-right"
                                                     style="display: none">


                                                    ارسال الرسالة لائمة المساجد
                                                    <input class=""
                                                           type="checkbox"
                                                           id="msgDest_adminApp"
                                                    >

                                                </div>

                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right text-right">

                                                    ارسال الرسالة لمستخدمي تطبيق الصامت
                                                    <input class="pull-right text-right"
                                                           type="checkbox"
                                                           id="msgDest_silentApp"
                                                    >
                                                </div>


                                            </div>


                                            <div class="row">
                                                <div class="col-md-12 col-lg-12 col-sm-12 ">
                                                    <label class="pull-right alert alert-danger display-none"
                                                           id="insertMessageDateErrorLabel">
                                                        اﻟﺨﻄﺎ
                                                    </label>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="panel-footer">

                                            <button onclick="showInsertMessageDialog()"
                                                    class="btn btn-default">اﺩﺧﺎﻝ
                                                ﻣﺤﺘﻮﻯ اﻟﺮﺳﺎﻟﺔ
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="editMessageTab">
                        <div class="row">
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title text-right">
                                            <a>ﻋﺮﺽ ﻭﺗﻌﺪﻳﻞ اﻟﺮﺳﺎﺋﻞ
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="messagesTableSetting" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right">
                                                    <label for="countrySelector"
                                                           class="pull-right inline-element text-right"> اﺧﺘﺮ
                                                        اﻟﺪﻭﻟﺔ </label>
                                                    <select
                                                        onchange="updateCitySelector(this);hideMessageEditingDiv();"
                                                        class="form-control pull-right text-align-right"
                                                        id="countrySelector_preview">
                                                        <option class="pull-right text-right"></option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right">
                                                    <label for="citySelector"
                                                           class="pull-right inline-element text-right"> اﺧﺘﺮ
                                                        اﻟﻤﺪﻳﻨﺔ </label>
                                                    <select onchange="hideMessageEditingDiv();"
                                                            class="form-control pull-right"
                                                            id="citySelector_preview">
                                                        <option class="pull-right text-right"></option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right">

                                                    <label for="previewEditMessage_startDatePicker"
                                                           class="pull-right inline-element">ﺗﺎﺭﻳﺦ
                                                        اﻟﺒﺪاﻳﺔ </label>
                                                    <input class="form-control pull-right text-right" type="text"
                                                           id="previewEditMessage_startDatePicker"
                                                    >
                                                </div>


                                                <div class="col-lg-3 col-md-6 col-sm-12 pull-right">

                                                    <label for="previewEditMessage_endDatePicker"
                                                           class="pull-right inline-element">ﺗﺎﺭﻳﺦ
                                                        اﻟﻨﻬﺎﻳﺔ </label>
                                                    <input class="form-control pull-right text-right" type="text"
                                                           id="previewEditMessage_endDatePicker"
                                                    >
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 col-lg-12 col-sm-12 ">
                                                    <label class="pull-right alert alert-danger display-none"
                                                           id="previewMessageDateErrorLabel">
                                                        اﻟﺨﻄﺎ
                                                    </label>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                                    <button onclick="showMessageTableDiv();" type="button"
                                                            class="btn btn-default">ﻋﺮﺽ اﻟﺮﺳﺎﺋﻞ
                                                    </button>
                                                </div>
                                            </div>

                                            <div class="row display-none" id="messagesTableDiv">
                                                <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                                    <table class="table table-striped table-bordered"
                                                           cellspacing="0"
                                                           width="100%" id="messagesTable">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">اﻟﺨﻴﺎﺭاﺕ</th>
                                                            <th class="text-center">اﻟﻤﺪﺧﻞ</th>
                                                            <th class="text-center">ﺗﺎﺭﻳﺦ اﻻﺩﺧﺎﻝ</th>
                                                            <th class="text-center">ﺗﺎﺭﻳﺦ اﻟﻨﻬﺎﻳﺔ</th>
                                                            <th class="text-center">ﺗﺎﺭﻳﺦ اﻟﺒﺪاﻳﺔ</th>
                                                            <th class="text-center">اﻟﻤﺪﻳﻨﻪ</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

            <div class="modal fade" id="insertMessageModal" role="dialog" aria-labelledby="insertMessageModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close pull-left" onclick="hideInsertMessageDialog()"
                                    aria-hidden="true">
                                ×
                            </button>
                            <h4 class="modal-title pull-right" id="insertMessageModalLabel">
                                اﺿﺎﻓﺔ ﺭﺳﺎﻟﺔ
                            </h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12">
                                    <div class="panel-group" id="insertMessagePanel">
                                        <div class="panel panel-default">
                                            <!--<div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <button id="submitButton" onclick="submitMessage()"
                                                                type="button"
                                                                class="btn btn-default pull-left">ﺗﺨﺰﻳﻦ
                                                        </button>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <h4 class="panel-title pull-right text-right">
                                                            <a>ﻟﻮﺣﺔ ﻋﺮﺽ اﻟﺮﺳﺎﻟﺔ</a>
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>-->


                                            <div class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
                                                             id="insertMessageEditorDiv">
                                                                <textarea class="match-parent-width"
                                                                          id="insertMessageEditor"
                                                                          name="insertMessageEditor">
                                                                    <h1>المسجد الذكي</h1>
                                                                    <p> رسالة اليوم</p>
                                                                </textarea>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
                                                             id="insertMessagePreviewDiv">
                                                            <iframe class="match-parent-width iFrame-noBorder"
                                                                    src="<?= $screenBase ?>?mode=1&date=-1&country=Jordan&city=Amman"
                                                                    id="insertMessagePreviewFrame"></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" onclick="hideInsertMessageDialog()">
                                اﻟﻐﺎء
                            </button>
                            <button type="button" class="btn btn-primary" onclick="saveNewMessage()">
                                ﺣﻔﻆ
                            </button>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

            <div class="modal fade" id="editMessageModal" role="dialog" aria-labelledby="editMessageModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close pull-left" onclick="hideEditMessageDialog()"
                                    aria-hidden="true">
                                ×
                            </button>
                            <h4 class="modal-title pull-right" id="editMessageModalLabel">
                                ﺗﻌﺪﻳﻞ ﺭﺳﺎﻟﺔ
                            </h4>
                        </div>
                        <div class="modal-body">

                            <div class="row" id="editMessageRow">
                                <div class="col-md-12 col-lg-12 col-sm-12">
                                    <div class="panel-group" id="editMessagePanel">
                                        <div class="panel panel-default">
                                            <div class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
                                                             id="editMessageDiv">
                                                                <textarea class="match-parent-width"
                                                                          id="editMessageEditor"
                                                                          name="editMessageEditor">
                                                                   <h1>المسجد الذكي</h1>
                                                                    <p> رسالة اليوم</p>
                                                                </textarea>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
                                                             id="editMessagePreviewDiv">
                                                            <iframe class="match-parent-width iFrame-noBorder"
                                                                    src="<?= $screenBase ?>?mode=1&date=-1&country=Jordan&city=Amman"
                                                                    id="editMessagePreviewFrame"></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" onclick="hideEditMessageDialog()">
                                اﻟﻐﺎء
                            </button>
                            <button type="button" class="btn btn-primary" onclick="updateMessage(activeMessageID)">
                                ﺣﻔﻆ
                            </button>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

            <div class="modal fade" id="viewMessageModal" role="dialog" aria-labelledby="viewMessageModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close pull-left" onclick="hideViewMessageDialog()"
                                    aria-hidden="true">
                                ×
                            </button>
                            <h4 class="modal-title pull-right" id="viewMessageModalLabel">
                                ﻋﺮﺽ اﻟﺮﺳﺎﻟﺔ
                            </h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12">
                                    <div class="panel-group" id="viewMessagePanel">
                                        <div class="panel panel-default">
                                            <div class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-2 col-md-1 col-sm-0 col-xs-0">

                                                        </div>
                                                        <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12"
                                                             id="viewMessagePreviewDiv">
                                                            <iframe class="match-parent-width iFrame-noBorder"
                                                                    src="<?= $screenBase ?>?mode=1&date=-1&country=Jordan&city=Amman"
                                                                    id="viewMessagePreviewFrame"></iframe>
                                                        </div>
                                                        <div class="col-lg-2 col-md-1 col-sm-0 col-xs-0">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" onclick="hideViewMessageDialog()"
                                    data-dismiss="modal">
                                اﻟﻐﺎء
                            </button>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>

</div>
<input type="hidden" id="msg_dst" value="<?= $msg_dst ?>"/>
<input type="hidden" id="screenBase" value="<?= $screenBase ?>"/>
<script src="<?= $webBase ?>js/pri_msgs/index.js"></script>
<link href="<?= $webBase ?>css/pri_msgs/index.css" rel="stylesheet"/>

