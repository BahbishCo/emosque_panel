﻿validateSettingFile();

bodyWidth = 0;
bodyHeight = 0;
headerHeight = 0;
prayerTime = 0;
iqamaTimes = 0;
isJumaaPrayerTime = false;
runningMode = 0;
watchEl = document.getElementById("Watch");

var Opacity;

var isFirstRun = false;

var isIqamaCounterEnabled = false;

var fajrAthanTimeInMinutes;
var dhuhrAthanTimeInMinutes;
var asrAthanTimeInMinutes;
var maghribAthanTimeInMinutes;
var ishaaAthanTimeInMinutes;

//get iqama time of each prayer
var fajrIqamaTimeInMinutes;
var dhuhrIqamaTimeInMinutes;
var asrIqamaTimeInMinutes;
var maghribIqamaTimeInMinutes;
var ishaaIqamaTimeInMinutes;
var jumaaIqamaTimeInMinutes;

var fajrAzkarDispalyTimeInSec;
var dhuhrAzkarDispalyTimeInSec;
var asrAzkarDispalyTimeInSec;
var maghribAzkarDispalyTimeInSec;
var ishaaAzkarDispalyTimeInSec;
var jumaaAzkarDispalyTimeInSec;


var fajrHiddenHadeethStartTimeInSecond;
var duhurHiddenHadeethStartTimeInSecond;
var asrHiddenHadeethStartTimeInSecond;
var maghribHiddenHadeethStartTimeInSecond;
var ishaaHiddenHadeethStartTimeInSecond;
var jumaaHiddenHadeethStartTimeInSecond;

var fajrHiddenHadeethEndTimeInSecond;
var duhurHiddenHadeethEndTimeInSecond;
var asrHiddenHadeethEndTimeInSecond;
var maghribHiddenHadeethEndTimeInSecond;
var ishaaHiddenHadeethEndTimeInSecond;
var jumaaHiddenHadeethEndTimeInSecond;


var fajrEndTimeInSecond;
var duhurEndTimeInSecond;
var asrEndTimeInSecond;
var maghribEndTimeInSecond;
var ishaaEndTimeInSecond;
var jumaaEndTimeInSecond;

var prayersOffset;

var headerHeight;

var currentPrayerIqamaTime;

function init() {
    runningMode = getParamValueByName("mode");
    if (runningMode == Setting.Mode.PI) {
        bodyWidth = screen.width;
        bodyHeight = screen.height;
        loadCSSFile();
    }
    else if (runningMode == Setting.Mode.PreviewFrameSize) {
        bodyWidth = window.innerWidth;
        bodyHeight = window.innerHeight;


        var latitiude = getParamValueByName("latitiude");
        var longitude = getParamValueByName("longitude");
        var elevation = getParamValueByName("elevation");
        var timeZone = getParamValueByName("timeZone");
        var prayerCalculationMethod = getParamValueByName("prayerCalculationMethod");
        var asrJuristic = getParamValueByName("asrJuristic");
        var adjustHighLat = getParamValueByName("adjustHighLat");
        var midnightMethod = getParamValueByName("midnightMethod");
        var fajrAngle = getParamValueByName("fajrAngle");
        var ishaaAngle = getParamValueByName("ishaaAngle");
    }
    else if (runningMode == Setting.Mode.PreviewFullScreen) {
        bodyWidth = screen.width;
        bodyHeight = screen.height;
        var latitiude = getParamValueByName("latitiude");
        var longitude = getParamValueByName("longitude");
        var elevation = getParamValueByName("elevation");
        var timeZone = getParamValueByName("timeZone");
        var prayerCalculationMethod = getParamValueByName("prayerCalculationMethod");
        var asrJuristic = getParamValueByName("asrJuristic");
        var adjustHighLat = getParamValueByName("adjustHighLat");
        var midnightMethod = getParamValueByName("midnightMethod");
        var fajrAngle = getParamValueByName("fajrAngle");
        var ishaaAngle = getParamValueByName("ishaaAngle");
    }

    if (runningMode == Setting.Mode.PI) {

        var lat = screenSetting.prayerSetting.latitude;
        var long = screenSetting.prayerSetting.longitude;
        var elevation = screenSetting.prayerSetting.elevation;
        // var timeZone = screenSetting.prayerSetting.timeZone;
        var d = new Date()
        var n = d.getTimezoneOffset();
        n = -1 * n;
        var timeZone = n / 60;
        prayTimes.setMethod(screenSetting.prayerSetting.calculationMethod);
        prayTimes.adjust({
            asr: screenSetting.prayerSetting.asrJuristic,
            highLats: screenSetting.prayerSetting.adjusthighLat,
            midnight: screenSetting.prayerSetting.midNightMethod,
            fajr: screenSetting.prayerSetting.fajrAngle,
            isha: screenSetting.prayerSetting.ishaAngle
        });
        meladiDate = new Date();
    }
    else {

        var lat = latitiude;
        var long = longitude;
        var elevation = elevation;
        var timeZone = timeZone;
        prayTimes.setMethod(prayerCalculationMethod);
        prayTimes.adjust({
            asr: asrJuristic,
            highLats: adjustHighLat,
            midnight: midnightMethod,
            fajr: fajrAngle,
            isha: ishaaAngle
        });

        if (getParamValueByName("date") == -1) {
            meladiDate = new Date();
        }
        else {
            var dateString = getParamValueByName("date");
            var year = dateString.split("-")[2];
            var month = parseInt(dateString.split("-")[1]) - 1;
            var day = parseInt(dateString.split("-")[0]) + 1;
            meladiDate = new Date(year, month, day);
        }

    }


    /* The following block is responsible of resizing elements except the prayer window*/

    document.body.style.maxHeight = bodyHeight + "px";
    document.body.style.maxWidth = bodyWidth + "px";

    document.getElementById("pageContainer").style.width = bodyWidth + "px";

    headerHeight = .096 * bodyHeight;
    document.getElementById("header").style.height = headerHeight + "px";


    //set the height of all elements in the header
    headerImages = document.getElementsByClassName("imageInHeader");
    for (var i = 0; i < headerImages.length; i++) {
        headerImages[i].style.height = headerHeight - 15 + "px";
        headerImages[i].style.marginTop = 8 + "px";
        headerImages[i].style.marginLeft = 8 + "px";
        headerImages[i].style.marginRight = 8 + "px";
    }


    document.getElementById("dateMeladi").style.height = headerHeight + "px";
    //document.getElementById("dateHijri").style.height = headerHeight + "px";
    document.getElementById("Watch").style.height = headerHeight + "px";


    var text = document.getElementById("bigIqamaText");
    var counter = document.getElementById("bigIqamaCounter");
    counter.style.height = .2 * bodyHeight + "px";
    text.style.height = .2 * bodyHeight + "px";

    text.style.top = (bodyHeight - headerHeight - 300 - footerHeight) / 2 + "px";
    text.style.left = (2 / 8 * bodyWidth) + "px";
    counter.style.top = (bodyHeight - headerHeight - 300 - footerHeight) / 2 + text.clientHeight + 20 + "px";
    counter.style.left = (3 / 10 * bodyWidth) + "px";

    document.getElementById("prayerWindow").style.height = bodyHeight - headerHeight - 20 + 30 + "px";

    var analogClockFrame = document.getElementById("analogClockFrame");
    analogClockFrame.style.height = bodyHeight - headerHeight + "px";
    analogClockFrame.style.width = bodyWidth * 8 / 12 + "px";
    analogClockFrame.style.position = "absolute";
    analogClockFrame.style.left = "0" + "px";
    analogClockFrame.style.top = headerHeight + "px";

    if (screenSetting.showAnalogClock) {
        analogClockFrame.style.visibility = "visible";
        analogClockFrame.style.display = "block";
        analogClockFrame.src = "html-pages/analogClockFrame/analogClockFrame.html";
    }
    else {
        analogClockFrame.style.visibility = "hidden";
        analogClockFrame.src = "";
        analogClockFrame.style.display = "none";
    }


    var silentLogo = document.getElementById("silentLogo");
    silentLogo.style.position = "absolute";
    silentLogo.style.left = "5" + "px";
    silentLogo.style.top = bodyHeight - (bodyHeight - headerHeight - 10 * 6 - footerHeight) / 6 - 3 + "px";
    silentLogo.style.height = (bodyHeight - headerHeight - 10 * 6 - footerHeight) / 6 - 10 + "px";


    var prayerTimeOutPanel = document.getElementById("prayerTimeOutPanel");
    var prayerTimeOutPanelWidth = document.getElementById("hadeethWindow").clientWidth;
    prayerTimeOutPanel.style.top = headerHeight + "px";
    //prayerTimeOutPanel.style.left = 8/12 * bodyWidth + "px";
    prayerTimeOutPanel.style.left = prayerTimeOutPanelWidth + "px";
    prayerTimeOutPanel.style.height = bodyHeight - headerHeight + 10 + "px";


    var elHight = bodyHeight - header.clientHeight - 10 * 6 - 30 + 30 - footerHeight;
    var elWidth = document.getElementById("prayerTimeOutPanel").clientWidth;
    elHight = elHight / 6;

    var prayerTimeOutEl = document.getElementsByClassName("timeOutEl");
    for (var i = 0; i < prayerTimeOutEl.length; i++) {
        prayerTimeOutEl[i].style.height = elHight + "px";
        prayerTimeOutEl[i].style.width = elWidth + "px";

    }

    var blackBackground = document.getElementById("blackBackground");
    blackBackground.style.width = bodyWidth + "px";
    blackBackground.style.height = bodyHeight + "px";
    //blackBackground.style.backgroundColor = 'rgba(93,134,187,0)';





    /*
     left: 100px;
     top: 150px;
     */


    // resize after prayers widndow

    /* var afterPrayerWindow = document.getElementById("afterPrayersWindow");
     afterPrayerWindow.style.top = headerHeight + 5 + "px";
     afterPrayerWindow.style.width = bodyWidth - 2 * 0.078 * bodyWidth + "px";
     afterPrayerWindow.style.height = bodyHeight - headerHeight - 10 - footerHeight + "px";
     afterPrayerWindow.style.left = 0.078 * bodyWidth + "px";

     document.getElementById("afterPrayerHeader").style.height = headerHeight + "px";
     document.getElementById("afterPrayerCarousel").style.height = bodyHeight - 2 * headerHeight - 15 - footerHeight + "px"
     */
    /*var blackScreen = document.getElementById("blackScreen");
     blackScreen.style.position = "absolute";
     blackScreen.style.top = 1 + "px";
     blackScreen.style.left = 1 + "px";
     blackScreen.style.height = bodyHeight + "px";
     blackScreen.style.width = bodyWidth + "px";
     */

    prayersOffset = screenSetting.prayerSetting.offset;
    prayTimes.tune(prayersOffset);

    prayerTime = prayTimes.getTimes(meladiDate, [lat, long, elevation], timeZone, 0, '24h');

    adjustDate();
    adjustPrayers();

    var iqamaTimes = [screenSetting.timeBetweenAzanAndIqama.fajr,
        screenSetting.timeBetweenAzanAndIqama.duhur,
        screenSetting.timeBetweenAzanAndIqama.asr,
        screenSetting.timeBetweenAzanAndIqama.maghrib,
        screenSetting.timeBetweenAzanAndIqama.ishaa,
        screenSetting.timeBetweenAzanAndIqama.jumaa
    ];
    /* The following block is responsable for checking if within iqama time of any prayes*/
    // get all prayers times in minutes
    fajrAthanTimeInMinutes = (parseInt((prayerTime.fajr.substr(0, 2))) * 60 + parseInt((prayerTime.fajr.substr(3, 2)))) * 60;
    sunriseAthanTimeInMinutes = (parseInt((prayerTime.sunrise.substr(0, 2))) * 60 + parseInt((prayerTime.sunrise.substr(3, 2)))) * 60;
    dhuhrAthanTimeInMinutes = (parseInt((prayerTime.dhuhr.substr(0, 2))) * 60 + parseInt((prayerTime.dhuhr.substr(3, 2)))) * 60;
    asrAthanTimeInMinutes = (parseInt((prayerTime.asr.substr(0, 2))) * 60 + parseInt((prayerTime.asr.substr(3, 2)))) * 60;
    maghribAthanTimeInMinutes = (parseInt((prayerTime.maghrib.substr(0, 2))) * 60 + parseInt((prayerTime.maghrib.substr(3, 2)))) * 60;
    ishaaAthanTimeInMinutes = (parseInt((prayerTime.isha.substr(0, 2))) * 60 + parseInt((prayerTime.isha.substr(3, 2)))) * 60;

    //get iqama time of each prayer
    fajrIqamaTimeInMinutes = fajrAthanTimeInMinutes + iqamaTimes[0] * 60;
    dhuhrIqamaTimeInMinutes = dhuhrAthanTimeInMinutes + iqamaTimes[1] * 60;
    asrIqamaTimeInMinutes = asrAthanTimeInMinutes + iqamaTimes[2] * 60;
    maghribIqamaTimeInMinutes = maghribAthanTimeInMinutes + iqamaTimes[3] * 60;
    ishaaIqamaTimeInMinutes = ishaaAthanTimeInMinutes + iqamaTimes[4] * 60;
    jumaaIqamaTimeInMinutes = dhuhrAthanTimeInMinutes + iqamaTimes[5] * 60;

    fajrAzkarDispalyTimeInSec = fajrAthanTimeInMinutes + screenSetting.timeBetweenAzanAndAzkar.fajr * 60;
    dhuhrAzkarDispalyTimeInSec = dhuhrAthanTimeInMinutes + screenSetting.timeBetweenAzanAndAzkar.duhur * 60;
    asrAzkarDispalyTimeInSec = asrAthanTimeInMinutes + screenSetting.timeBetweenAzanAndAzkar.asr * 60;
    maghribAzkarDispalyTimeInSec = maghribAthanTimeInMinutes + screenSetting.timeBetweenAzanAndAzkar.maghrib * 60;
    ishaaAzkarDispalyTimeInSec = ishaaAthanTimeInMinutes + screenSetting.timeBetweenAzanAndAzkar.ishaa * 60;
    jumaaAzkarDispalyTimeInSec = dhuhrAthanTimeInMinutes + screenSetting.timeBetweenAzanAndAzkar.jumaa * 60;


    fajrHiddenHadeethStartTimeInSecond = fajrIqamaTimeInMinutes + Setting.PrayerBlinkingCounter;
    duhurHiddenHadeethStartTimeInSecond = dhuhrIqamaTimeInMinutes + Setting.PrayerBlinkingCounter;
    asrHiddenHadeethStartTimeInSecond = asrIqamaTimeInMinutes + Setting.PrayerBlinkingCounter;
    maghribHiddenHadeethStartTimeInSecond = maghribIqamaTimeInMinutes + Setting.PrayerBlinkingCounter;
    ishaaHiddenHadeethStartTimeInSecond = ishaaIqamaTimeInMinutes + Setting.PrayerBlinkingCounter;
    jumaaHiddenHadeethStartTimeInSecond = jumaaIqamaTimeInMinutes + Setting.PrayerBlinkingCounter;


    fajrHiddenHadeethEndTimeInSecond = fajrAzkarDispalyTimeInSec;
    duhurHiddenHadeethEndTimeInSecond = dhuhrAzkarDispalyTimeInSec;
    asrHiddenHadeethEndTimeInSecond = asrAzkarDispalyTimeInSec;
    maghribHiddenHadeethEndTimeInSecond = maghribAzkarDispalyTimeInSec;
    ishaaHiddenHadeethEndTimeInSecond = ishaaAzkarDispalyTimeInSec;
    jumaaHiddenHadeethEndTimeInSecond = jumaaAzkarDispalyTimeInSec;


    fajrEndTimeInSecond = fajrAzkarDispalyTimeInSec + screenSetting.azkarDisplayPeriod.fajr * 60;
    duhurEndTimeInSecond = dhuhrAzkarDispalyTimeInSec + screenSetting.azkarDisplayPeriod.duhur * 60;
    asrEndTimeInSecond = asrAzkarDispalyTimeInSec + screenSetting.azkarDisplayPeriod.asr * 60;
    maghribEndTimeInSecond = maghribAzkarDispalyTimeInSec + screenSetting.azkarDisplayPeriod.maghrib * 60;
    ishaaEndTimeInSecond = ishaaAzkarDispalyTimeInSec + screenSetting.azkarDisplayPeriod.ishaa * 60;
    jumaaEndTimeInSecond = jumaaAzkarDispalyTimeInSec + screenSetting.azkarDisplayPeriod.jumaa * 60;


    if(Setting.Language.English == screenSetting.language)
    {
        var prayerNamesCol = document.getElementById("prayerNamesCol");
        var prayerTimesCol = document.getElementById("prayerTimesCol");

        prayerNamesCol.className += " pull-left";
        prayerTimesCol.className += " pull-right";

        prayerNames= document.getElementsByClassName("prayerElTimes");
        for(var i=0;i<prayerNames.length;i++)
        {
            prayerNames[i].style.textAlign = "left";
        }

    }

    if (getParamValueByName("mode") == Setting.Mode.PI) {
        callPiFunctions();
    }
    else (getParamValueByName("mode") == Setting.Mode.PreviewFrameSize)
    {
        callPreviewFrameLoader();
    }

}


function callPreviewFrameLoader() {
    if (typeof displayPreviewFrame !== 'undefined' && typeof displayPreviewFrame === 'function') {
        displayPreviewFrame();
        return;
    }
    else {
        setTimeout(callPreviewFrameLoader, 100);
    }
}


function callPiFunctions() {

    if (typeof setMessageFramesHeight !== 'undefined' && typeof setMessageFramesHeight === 'function') {

        if (typeof isSecondaryMessage !== 'undefined' && typeof isSecondaryMessage === 'function') {
            setTimeout(setMessageFramesHeight,1000);
            setInterval(monitorStates, 1000);
            setTimeout(loadContent, 1000);
            readTemp();
            setInterval(readTemp, 10000);
        }
        else {
            setTimeout(callPiFunctions, 100);
        }

    }
    else {
        setTimeout(callPiFunctions, 100);

    }

}

var prayersIqamaTime = ['fajrIqamaTime', 'thohorIqamaTime', 'asrIqamaTime', 'magribIqamaTime', 'ishaaIqamaTime'];
var prayersIqama = ['fajrIqama', 'thohorIqama', 'asrIqama', 'magribIqama', 'ishaaIqama'];


function monitorStates() {

    var currentTimeInMinutes = (new Date().getHours() * 60 + new Date().getMinutes()) * 60 + new Date().getSeconds();

    /*******************************************
     Fajr State
     ******************************************/

    if (currentTimeInMinutes >= fajrAthanTimeInMinutes && currentTimeInMinutes < fajrEndTimeInSecond) {

        currentPrayerIqamaTime = fajrIqamaTimeInMinutes;
        if (currentTimeInMinutes >= fajrAthanTimeInMinutes && currentTimeInMinutes < fajrIqamaTimeInMinutes) {
            stopSwitching = true;
            highLight(0);

            primary_CurrentState = Setting.PrimaryMessageStates.Hadeeth;
            if (!isIqamaCounterEnabled) {
                isIqamaCounterEnabled = true;
                showIqama(fajrIqamaTimeInMinutes - currentTimeInMinutes);
            }
        }

        else if (currentTimeInMinutes >= fajrHiddenHadeethStartTimeInSecond && currentTimeInMinutes <= fajrHiddenHadeethEndTimeInSecond) {
            stopSwitching = true;
            highLight(0);

            primaryMessagePrevFileName = "";
            changeHadeethFlag = false;
            displayHadeeth = true;
            primary_CurrentState = Setting.PrimaryMessageStates.Hidden;
            secondary_CurrentState = Setting.SecondaryMessageStates.Hidden;
        }

        else if (currentTimeInMinutes >= fajrAzkarDispalyTimeInSec && currentTimeInMinutes <= fajrEndTimeInSecond) {
            isIqamaCounterEnabled = false;
            stopSwitching = false;
            highLight(1);
            primary_CurrentState = Setting.PrimaryMessageStates.Azkar;
            secondary_CurrentState = Setting.SecondaryMessageStates.Visible;
        }
    }

    /*******************************************
     Dhuhr State
     ******************************************/

    else if (currentTimeInMinutes >= dhuhrAthanTimeInMinutes && currentTimeInMinutes < duhurEndTimeInSecond && !isJumaa()) {

        currentPrayerIqamaTime = dhuhrIqamaTimeInMinutes;

        if (currentTimeInMinutes >= dhuhrAthanTimeInMinutes && currentTimeInMinutes < dhuhrIqamaTimeInMinutes) {
            stopSwitching = true;
            highLight(1);


            primary_CurrentState = Setting.PrimaryMessageStates.Hadeeth;
            if (!isIqamaCounterEnabled) {
                isIqamaCounterEnabled = true;
                showIqama(dhuhrIqamaTimeInMinutes - currentTimeInMinutes);
            }
        }

        else if (currentTimeInMinutes >= duhurHiddenHadeethStartTimeInSecond && currentTimeInMinutes <= duhurHiddenHadeethEndTimeInSecond) {
            stopSwitching = true;
            highLight(1);

            primaryMessagePrevFileName = "";
            displayHadeeth = true;
            primary_CurrentState = Setting.PrimaryMessageStates.Hidden;
            secondary_CurrentState = Setting.SecondaryMessageStates.Hidden;
        }

        else if (currentTimeInMinutes >= dhuhrAzkarDispalyTimeInSec && currentTimeInMinutes <= duhurEndTimeInSecond) {
            isIqamaCounterEnabled = false;
            stopSwitching = false;
            highLight(2);
            primary_CurrentState = Setting.PrimaryMessageStates.Azkar;
            secondary_CurrentState = Setting.SecondaryMessageStates.Visible;
        }
    }


    /*******************************************
     Asr State
     ******************************************/

    else if (currentTimeInMinutes >= asrAthanTimeInMinutes && currentTimeInMinutes < asrEndTimeInSecond) {

        currentPrayerIqamaTime = asrIqamaTimeInMinutes;

        if (currentTimeInMinutes >= asrAthanTimeInMinutes && currentTimeInMinutes < asrIqamaTimeInMinutes) {
            stopSwitching = true;
            highLight(2);

            primary_CurrentState = Setting.PrimaryMessageStates.Hadeeth;
            if (!isIqamaCounterEnabled) {
                isIqamaCounterEnabled = true;
                showIqama(asrIqamaTimeInMinutes - currentTimeInMinutes);
            }
        }

        else if (currentTimeInMinutes >= asrHiddenHadeethStartTimeInSecond && currentTimeInMinutes <= asrHiddenHadeethEndTimeInSecond) {
            stopSwitching = true;
            highLight(2);

            primaryMessagePrevFileName = "";
            displayHadeeth = true;
            primary_CurrentState = Setting.PrimaryMessageStates.Hidden;
            secondary_CurrentState = Setting.SecondaryMessageStates.Hidden;
        }

        else if (currentTimeInMinutes >= asrAzkarDispalyTimeInSec && currentTimeInMinutes <= asrEndTimeInSecond) {
            isIqamaCounterEnabled = false;
            stopSwitching = false;
            highLight(3);
            primary_CurrentState = Setting.PrimaryMessageStates.Azkar;
            secondary_CurrentState = Setting.SecondaryMessageStates.Visible;
        }
    }

    /*******************************************
     Maghrib State
     ******************************************/

    else if (currentTimeInMinutes >= maghribAthanTimeInMinutes && currentTimeInMinutes < maghribEndTimeInSecond) {

        currentPrayerIqamaTime = maghribIqamaTimeInMinutes;

        if (currentTimeInMinutes >= maghribAthanTimeInMinutes && currentTimeInMinutes < maghribIqamaTimeInMinutes) {
            stopSwitching = true;
            highLight(3);

            primary_CurrentState = Setting.PrimaryMessageStates.Hadeeth;
            if (!isIqamaCounterEnabled) {
                isIqamaCounterEnabled = true;
                showIqama(maghribIqamaTimeInMinutes - currentTimeInMinutes);
            }
        }

        else if (currentTimeInMinutes >= maghribHiddenHadeethStartTimeInSecond && currentTimeInMinutes <= maghribHiddenHadeethEndTimeInSecond) {
            stopSwitching = true;
            highLight(3);

            primaryMessagePrevFileName = "";
            displayHadeeth = true;
            primary_CurrentState = Setting.PrimaryMessageStates.Hidden;
            secondary_CurrentState = Setting.SecondaryMessageStates.Hidden;
        }

        else if (currentTimeInMinutes >= maghribAzkarDispalyTimeInSec && currentTimeInMinutes <= maghribEndTimeInSecond) {
            isIqamaCounterEnabled = false;
            stopSwitching = false;
            highLight(4);
            primary_CurrentState = Setting.PrimaryMessageStates.Azkar;
            secondary_CurrentState = Setting.SecondaryMessageStates.Visible;
        }
    }

    /*******************************************
     Ishaa State
     ******************************************/

    else if (currentTimeInMinutes >= ishaaAthanTimeInMinutes && currentTimeInMinutes < ishaaEndTimeInSecond) {

        currentPrayerIqamaTime = ishaaIqamaTimeInMinutes;

        if (currentTimeInMinutes >= ishaaAthanTimeInMinutes && currentTimeInMinutes < ishaaIqamaTimeInMinutes) {
            stopSwitching = true;
            highLight(4);
            primary_CurrentState = Setting.PrimaryMessageStates.Hadeeth;
            if (!isIqamaCounterEnabled) {
                isIqamaCounterEnabled = true;
                showIqama(ishaaIqamaTimeInMinutes - currentTimeInMinutes);
            }
        }

        else if (currentTimeInMinutes >= ishaaHiddenHadeethStartTimeInSecond && currentTimeInMinutes <= ishaaHiddenHadeethEndTimeInSecond) {
            stopSwitching = true;
            highLight(4);
            primaryMessagePrevFileName = "";
            displayHadeeth = true;
            primary_CurrentState = Setting.PrimaryMessageStates.Hidden;
            secondary_CurrentState = Setting.SecondaryMessageStates.Hidden;
        }

        else if (currentTimeInMinutes >= ishaaAzkarDispalyTimeInSec && currentTimeInMinutes <= ishaaEndTimeInSecond) {
            isIqamaCounterEnabled = false;
            stopSwitching = false;
            highLight(0);
            primary_CurrentState = Setting.PrimaryMessageStates.Azkar;
            secondary_CurrentState = Setting.SecondaryMessageStates.Visible;
        }
    }

    /*******************************************
     Jumaa State
     ******************************************/

    else if (currentTimeInMinutes >= dhuhrAthanTimeInMinutes && currentTimeInMinutes < jumaaEndTimeInSecond && isJumaa()) {


        if (currentTimeInMinutes >= dhuhrAthanTimeInMinutes && currentTimeInMinutes <= jumaaHiddenHadeethEndTimeInSecond) {

            stopSwitching = true;
            highLight(1);


            isIqamaCounterEnabled = false;
            primary_CurrentState = Setting.PrimaryMessageStates.Hidden;
            secondary_CurrentState = Setting.SecondaryMessageStates.Hidden;

        }

        else if (currentTimeInMinutes >= jumaaAzkarDispalyTimeInSec && currentTimeInMinutes <= jumaaEndTimeInSecond) {
            primaryMessagePrevFileName = "";
            displayHadeeth = true;
            isIqamaCounterEnabled = false;
            stopSwitching = false;
            highLight(2);
            primary_CurrentState = Setting.PrimaryMessageStates.Azkar;
            secondary_CurrentState = Setting.SecondaryMessageStates.Visible;
        }
    }


    else {


        if (primary_CurrentState != Setting.PrimaryMessageStates.Hadeeth) {
            primary_CurrentState = Setting.PrimaryMessageStates.Hadeeth;
        }

        if (currentTimeInMinutes < fajrAthanTimeInMinutes || currentTimeInMinutes > ishaaEndTimeInSecond) {
            highLight(0);
        }

        else if (currentTimeInMinutes > fajrEndTimeInSecond && currentTimeInMinutes < dhuhrAthanTimeInMinutes) {
            highLight(1);
        }
        else if (currentTimeInMinutes > duhurEndTimeInSecond && currentTimeInMinutes < asrAthanTimeInMinutes) {
            highLight(2);
        }
        else if (currentTimeInMinutes > asrEndTimeInSecond && currentTimeInMinutes < maghribAthanTimeInMinutes) {
            highLight(3);
        }
        else if (currentTimeInMinutes > maghribEndTimeInSecond && currentTimeInMinutes < ishaaAthanTimeInMinutes) {
            highLight(4);
        }
    }
}


function getParamValueByName(name, windowId) {
    if (windowId == null) {

        var query = window.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }
    else {
        var iframe = document.getElementById(windowId);
        var query = iframe.contentWindow.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }

}


function checkVarValidity(varParent, varName, varDefaultValue) {

    var isValid = true;
    if (varParent !== null) {
        if (typeof screenSetting[varParent][varName] === "undefined") {
            screenSetting[varParent][varName] = varDefaultValue;
            isValid = false;
        }
    }
    else {
        if (typeof screenSetting[varName] === "undefined") {
            screenSetting[varName] = varDefaultValue;
            isValid = false;
        }
    }
    return isValid;
}


function validateSettingFile()
{
    var d = new Date()
    var n = d.getTimezoneOffset();
    n = -1 * n;
    var timeZone = n / 60;
    if (typeof screenSetting === "undefined") {
        console.log("screen setting object is not defined");


        screenSetting =
        {
            prayerSetting: {
                country: "Jordan",
                city: "Amman",
                calculationMethod: "Karachi",
                asrJuristic: "Standard",
                adjusthighLat: "AngleBased",
                midNightMethod: "Standard",
                fajrAngle: 18,
                ishaAngle: 18,
                latitude: 32.02027777777778,
                longitude: 35.82944444444445,
                elevation: 1100,
                timeZone: timeZone,
                offset: {fajr: 0, dhuhr: 0, asr: 0, maghrib: 0, isha: 0,},
            },
            hijriCorrection: 0,
            timeBetweenAzanAndIqama: {fajr: 30, duhur: 18, asr: 18, maghrib: 8, ishaa: 13, jumaa: 0,},
            timeBetweenAzanAndAzkar: {fajr: 38, duhur: 26, asr: 26, maghrib: 18, ishaa: 25, jumaa: 50,},
            azkarDisplayPeriod: {fajr: 10, duhur: 10, asr: 10, maghrib: 10, ishaa: 10, jumaa: 10,},
            appQREnable: false,
            internalTempEnable: true,
            playAzan: false,
            showAnalogClock: false,
            showBahbishMessage: true,
            displayAdAfterPrayer: true,
        }
    }
    else {
        if (checkVarValidity(null, "prayerSetting", {
                country: "Jordan",
                city: "Amman",
                calculationMethod: "Karachi",
                asrJuristic: "Standard",
                adjusthighLat: "AngleBased",
                midNightMethod: "Standard",
                fajrAngle: 18,
                ishaAngle: 18,
                latitude: 32.02027777777778,
                longitude: 35.82944444444445,
                elevation: 1100,
                timeZone: timeZone,
                offset: {fajr: 0, dhuhr: 0, asr: 0, maghrib: 0, isha: 0,},
            })) {
            checkVarValidity("prayerSetting", "country", "Jordan");
            checkVarValidity("prayerSetting", "city", "Amman");
            checkVarValidity("prayerSetting", "calculationMethod", "Karachi");
            checkVarValidity("prayerSetting", "asrJuristic", "Standard");
            checkVarValidity("prayerSetting", "adjusthighLat", "AngleBased");
            checkVarValidity("prayerSetting", "midNightMethod", "Standard");
            checkVarValidity("prayerSetting", "fajrAngle", 18);
            checkVarValidity("prayerSetting", "ishaAngle", 18);
            checkVarValidity("prayerSetting", "latitude", 32.02027777777778);
            checkVarValidity("prayerSetting", "longitude", 35.82944444444445);
            checkVarValidity("prayerSetting", "elevation", 1100);
            checkVarValidity("prayerSetting", "timeZone", timeZone);
            checkVarValidity("prayerSetting", "offset", {fajr: 0, dhuhr: 0, asr: 0, maghrib: 0, isha: 0,});
        }

        checkVarValidity(null,"hijriCorrection",0);
        checkVarValidity(null,"timeBetweenAzanAndIqama",{fajr: 30, duhur: 18, asr: 18, maghrib: 8, ishaa: 13, jumaa: 0,});
        checkVarValidity(null,"timeBetweenAzanAndAzkar",{fajr: 38, duhur: 26, asr: 26, maghrib: 18, ishaa: 25, jumaa: 50,});
        checkVarValidity(null,"azkarDisplayPeriod",{fajr: 10, duhur: 10, asr: 10, maghrib: 10, ishaa: 10, jumaa: 10,});
        checkVarValidity(null,"appQREnable",false);
        checkVarValidity(null,"internalTempEnable",true);
        checkVarValidity(null,"playAzan",false);
        checkVarValidity(null,"showAnalogClock",false);
        checkVarValidity(null,"showBahbishMessage",true);
        checkVarValidity(null,"displayAdAfterPrayer",true);
    }
}
