import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Scanner;

public class TestAutomation {
	
	static SimpleDateFormat SDF = new SimpleDateFormat("HH:mm");
	static Calendar cal = Calendar.getInstance();
	static int dayOfWeek = cal.getTime().getDay();
	static PrayerTimesController prayerTimes;
	static ArrayList<String> ammanPrayerTimes;
	static int[] offsets = {0, 0, 0, 0, 0, 0, 0};
    static int timeZone;
    static float fajrAngle;
    static float ishaAngle;
    static int adjustHighLats;
    static int method;
    static int asrJuristic;
    static int midNightMethod;
    static double elevation;
    static double latitude;
    static double longitude;
    static String calculationMethod;
    static String asrJuristicc;
    static String adjusthighLat;
    static String midNightMethodd;
    
    static String[] timeZoneLineArray;
    static String[] calculationMethodArray;
    static String[] asrJuristicArray;
    static String[] fajrAngleArray;
    static String[] ishaAngleArray;
    static String[] latitudeArray;
    static String[] longitudeArray;
    static String[] elevationArray;
    static String[] adjusthighLatArray;
    static String[] midNightMethodArray;
    static String[] offsetArray;
    
    static String nextLine;
	static String eMosqueURLOpenCommand = "sudo midori -e Fullscreen -a localhost/emosquescreen/web-site/index.html";
	static String killMidoriCommand = "sudo pkill midori";
	static String Settings = "/var/www/emosquescreen/web-site/JSON/setting.js";
	static String SavedSettings = "/home/pi/.eMosqueSystem/processes/TestAutomation/setting.js";
	static File settingsFile = new File("/var/www/emosquescreen/web-site/JSON/setting.js");
	static Path FROM = Paths.get(Settings);
	static Path TO = Paths.get(SavedSettings);
	static Runtime rt = Runtime.getRuntime();
	static CopyOption[] options = new CopyOption[]
			{
			  StandardCopyOption.REPLACE_EXISTING,
			  StandardCopyOption.COPY_ATTRIBUTES
			};

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		getCalculationSettings();
		getPrayerTimes();

			new Thread()
			{
				public void run() {
			        
					try {
						
						// ---- save old settings ---- //
						
						saveOldSettings();
						sleep(2000);
						
						// ---- change to new settings ---- //
						
						changeToNewSettings();
						sleep(2000);
						
						//   ---- change time to alduhr friday ---- //

						setDayToFriday();
						getPrayerTimes();
						setTimeToAlduhr();
						rt.exec(killMidoriCommand);
						sleep(1000);
						rt.exec(eMosqueURLOpenCommand);
						sleep(300000);
						
						// ---- change time to alfajr sat ---- //
						
						setDayToSat();
						getPrayerTimes();
						setTimeToAlfajr();
						rt.exec(killMidoriCommand);
						sleep(1000);
						rt.exec(eMosqueURLOpenCommand);
						sleep(300000);
						
						// ---- set time to al-duhur ---- //
						
						setTimeToAlduhr();
						rt.exec(killMidoriCommand);
						sleep(1000);
						rt.exec(eMosqueURLOpenCommand);
						sleep(300000);
						
						// ---- set time to al-asr ---- //
						
						setTimeToAlasr();
						rt.exec(killMidoriCommand);
						sleep(1000);
						rt.exec(eMosqueURLOpenCommand);
						sleep(300000);
						
						// ---- set time to al-maghrib ---- //
						
						setTimeToAlMaghrib();
						rt.exec(killMidoriCommand);
						sleep(1000);
						rt.exec(eMosqueURLOpenCommand);
						sleep(300000);
						
						// ---- set time to al-ishaa ---- //
						
						setTimeToAlishaa();
						rt.exec(killMidoriCommand);
						sleep(1000);
						rt.exec(eMosqueURLOpenCommand);
						sleep(300000);
						
						// ---- restore old settings ---- //
						
						restoreOldSettings();
						sleep(3000);

						// ---- reboot system ---- //
						
						rebootSystem();
						
					} 
					catch (IOException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}  
					catch (InterruptedException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
			    }
			}.start();
	}
	
	public static void getCalculationSettings()
	{
		try 
		{
		Scanner scanner = new Scanner(settingsFile);

	    while (scanner.hasNextLine()) 
	    {
	    	nextLine = scanner.nextLine();
	    	
	        if(nextLine.contains("timeZone")) 
	        {
	        	timeZoneLineArray = nextLine.split(" ",-1);
	        	nextLine = timeZoneLineArray[1].substring(0, timeZoneLineArray[1].length()-1);
	        	timeZone = (int)Double.parseDouble(nextLine);
	        	System.out.println("timeZone : " + timeZone);
	        }
	        else if(nextLine.contains("calculationMethod")) 
	        {
	        	calculationMethodArray = nextLine.split(" ",-1);
	        	nextLine = calculationMethodArray[1].substring(1, calculationMethodArray[1].length()-2);
	        	calculationMethod = nextLine;
	        	
	        	if (calculationMethod.equals("Karachi")) 
	        	{
					method = PrayerTimes.Karachi;
				}
	        	else if (calculationMethod.equals("ISNA")) 
	        	{
	        		method = PrayerTimes.ISNA;
				}
	        	else if (calculationMethod.equals("MWL")) 
	        	{
	        		method = PrayerTimes.MWL;
				}
	        	else if (calculationMethod.equals("Makkah")) 
	        	{
	        		method = PrayerTimes.Makkah;
				}
	        	else if (calculationMethod.equals("Egypt")) 
	        	{
	        		method = PrayerTimes.Egypt;
				}
	        	else if (calculationMethod.equals("Tehran")) 
	        	{
	        		method = PrayerTimes.Tehran;
				}
	        	else if (calculationMethod.equals("Custom")) 
	        	{
	        		method = PrayerTimes.Custom;
				}
	        	System.out.println("\nTestAutomation Started ..");
	        	System.out.println("calculationMethod : " + calculationMethod);
	        }
	        else if(nextLine.contains("asrJuristic")) 
	        {
	        	asrJuristicArray = nextLine.split(" ",-1);
	        	nextLine = asrJuristicArray[1].substring(1, asrJuristicArray[1].length()-2);
	        	asrJuristicc = nextLine;
	        	
	        	if (asrJuristicc.equals("Standard")||asrJuristicc.equals("Shafii")) 
	        	{
					asrJuristic = PrayerTimes.Shafii;
				}
	        	else if (asrJuristicc.equals("Hanafi")) 
	        	{
	        		asrJuristic = PrayerTimes.Hanafi;
				}
	        	
	        	System.out.println("asrJuristic : " + asrJuristicc);
	        }
	        else if(nextLine.contains("adjusthighLat")) 
	        {
	        	adjusthighLatArray = nextLine.split(" ",-1);
	        	nextLine = adjusthighLatArray[1].substring(1, adjusthighLatArray[1].length()-2);
	        	adjusthighLat = nextLine;
	        	
	        	if (adjusthighLat.equals("AngleBased")) 
	        	{
					adjustHighLats = PrayerTimes.AngleBased;
				}
	        	else if (adjusthighLat.equals("MidNight")) 
	        	{
	        		adjustHighLats = PrayerTimes.MidNight;
				}
	        	else if (adjusthighLat.equals("OneSeventh")) 
	        	{
	        		adjustHighLats = PrayerTimes.OneSeventh;
				}
	        	
	        	System.out.println("adjusthighLat : " + adjusthighLat);
	        }
	        else if(nextLine.contains("midNightMethod")) 
	        {
	        	midNightMethodArray = nextLine.split(" ",-1);
	        	nextLine = midNightMethodArray[1].substring(1, midNightMethodArray[1].length()-2);
	        	midNightMethodd = nextLine;
	        	
	        	if (midNightMethodd.equals("Standard")) 
	        	{
	        		midNightMethod = 0;
				}
	        	else
	        	{
	        		midNightMethod = 1;
				}
	        	
	        	System.out.println("midNightMethod : " + midNightMethodd);
	        }
	        else if(nextLine.contains("fajrAngle")) 
	        {
	        	fajrAngleArray = nextLine.split(" ",-1);
	        	nextLine = fajrAngleArray[1].substring(0, fajrAngleArray[1].length()-1);
	        	fajrAngle = Integer.parseInt(nextLine);
	        	System.out.println("fajrAngle : " + fajrAngle);
	        }
	        else if(nextLine.contains("ishaAngle")) 
	        {
	        	ishaAngleArray = nextLine.split(" ",-1);
	        	nextLine = ishaAngleArray[1].substring(0, ishaAngleArray[1].length()-1);
	        	ishaAngle = Integer.parseInt(nextLine);
	        	System.out.println("ishaAngle : " + ishaAngle);
	        }
	        else if(nextLine.contains("latitude")) 
	        {
	        	latitudeArray = nextLine.split(" ",-1);
	        	nextLine = latitudeArray[1].substring(0, latitudeArray[1].length()-1);
	        	latitude = Double.parseDouble(nextLine);
	        	System.out.println("latitude : " + latitude);
	        }
	        else if(nextLine.contains("longitude")) 
	        {
	        	longitudeArray = nextLine.split(" ",-1);
	        	nextLine = longitudeArray[1].substring(0, longitudeArray[1].length()-1);
	        	longitude = Double.parseDouble(nextLine);
	        	System.out.println("longitude : " + longitude);
	        }
	        else if(nextLine.contains("elevation")) 
	        {
	        	elevationArray = nextLine.split(" ",-1);
	        	nextLine = elevationArray[1].substring(0, elevationArray[1].length()-1);
	        	elevation = Integer.parseInt(nextLine);
	        	System.out.println("elevation : " + elevation);
	        }
	        else if(nextLine.contains("offset")) 
	        {
	        	offsetArray = nextLine.split(" ",-1);
	        	offsets[0] = Integer.parseInt(offsetArray[2].substring(0, offsetArray[2].length()-1));
	        	offsets[2] = Integer.parseInt(offsetArray[4].substring(0, offsetArray[4].length()-1));
	        	offsets[3] = Integer.parseInt(offsetArray[6].substring(0, offsetArray[6].length()-1));
	        	offsets[5] = Integer.parseInt(offsetArray[8].substring(0, offsetArray[8].length()-1));
	        	offsets[6] = Integer.parseInt(offsetArray[10].substring(0, offsetArray[10].length()-1));
	        	
	        	System.out.println("offsets : {fajrOffset: " + offsets[0] + ", duhurOffset: " + offsets[2]+ ", asrOffset: " +offsets[3]+ ", maghribOffset: " + offsets[5]+ ", ishaaOffset: " + offsets[6]+"}");
	        }
	    }
	    scanner.close();
	 }
	 catch(FileNotFoundException e) 
	 {
	    System.out.println("Error : Settings file not found!");
	 }
	}
	
	public static void getPrayerTimes()
	{
		prayerTimes = new PrayerTimesController(latitude, longitude, elevation, timeZone,
				offsets, PrayerTimes.Time24, method, asrJuristic, adjustHighLats, fajrAngle,
				ishaAngle, 90 , midNightMethod);
		ammanPrayerTimes = prayerTimes.getPrayerTimes();
	}
	
	public static String reduceOneMin(String s)
	{
		try 
		{
			Date date = SDF.parse(s);
			date.setMinutes(date.getMinutes()-1);
			s = SDF.format(date);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}
	
	public static int addOneDay()
	{
		cal.add(Calendar.DAY_OF_YEAR, 1);
		return cal.getTime().getDay();
	}
	
	public static void saveOldSettings()
	{
		try 
		{
			System.out.println("Saving old settings ..");
			java.nio.file.Files.copy(FROM, TO, options);
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void changeToNewSettings()
	{
		try {
			System.out.println("Changing to new settings ..");
			Path path = Paths.get(Settings);
			Charset charset = StandardCharsets.UTF_8;
			String content;
			content = new String(Files.readAllBytes(path), charset);
			content = content.replace("timeBetweenAzanAndIqama: {fajr: 30, duhur: 18, asr: 18, maghrib: 10, ishaa: 23, jumaa: 50, },", "timeBetweenAzanAndIqama: {fajr: 1, duhur: 1, asr: 1, maghrib: 1, ishaa: 1, jumaa: 1, },");
			content = content.replace("timeBetweenAzanAndAzkar: {fajr: 36, duhur: 26, asr: 26, maghrib: 17, ishaa: 32, jumaa: 50, },", "timeBetweenAzanAndAzkar: {fajr: 2, duhur: 2, asr: 2, maghrib: 2, ishaa: 2, jumaa: 2, },");
			content = content.replace("azkarDisplayPeriod: {fajr: 10, duhur: 10, asr: 10, maghrib: 10, ishaa: 10, jumaa: 10, },", "azkarDisplayPeriod: {fajr: 1, duhur: 1, asr: 1, maghrib: 1, ishaa: 1, jumaa: 1, },");
			
			if(content.contains("playAzan: true,"))
			content = content.replace("playAzan: true,", "playAzan: false,");

			Files.write(path, content.getBytes(charset));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setDayToFriday()
	{
		while(true)
		{
			if (dayOfWeek==5) {
				break;
			}
			dayOfWeek = addOneDay();
		}
		
		try 
		{
			rt.exec(new String[]{"sudo","date","-s",cal.getTime().toString()});
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setDayToSat()
	{
		try 
		{
			dayOfWeek = addOneDay();
			rt.exec(new String[]{"sudo","date","-s",cal.getTime().toString()});
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setTimeToAlfajr()
	{
		try 
		{
			System.out.println("Changing time to Alfajr-"+ getDayOfWeek() + " " + reduceOneMin(ammanPrayerTimes.get(0)));
			rt.exec("sudo date -s "+ reduceOneMin(ammanPrayerTimes.get(0)));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setTimeToAlduhr()
	{
		
		try 
		{
			System.out.println("Changing time to Alduhr-"+ getDayOfWeek() + " " + reduceOneMin(ammanPrayerTimes.get(2)));
			rt.exec("sudo date -s "+ reduceOneMin(ammanPrayerTimes.get(2)));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setTimeToAlasr()
	{
		try 
		{
			System.out.println("Changing time to Alasr-"+ getDayOfWeek() + " " + reduceOneMin(ammanPrayerTimes.get(3)));
			rt.exec("sudo date -s "+ reduceOneMin(ammanPrayerTimes.get(3)));
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setTimeToAlMaghrib()
	{
		
		try 
		{
			System.out.println("Changing time to Almaghrib-"+ getDayOfWeek() + " " + reduceOneMin(ammanPrayerTimes.get(4)));
			rt.exec("sudo date -s "+ reduceOneMin(ammanPrayerTimes.get(4)));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setTimeToAlishaa()
	{
		
		try 
		{
			System.out.println("Changing time to Alishaa-"+ getDayOfWeek() + " " + reduceOneMin(ammanPrayerTimes.get(6)));
			rt.exec("sudo date -s "+ reduceOneMin(ammanPrayerTimes.get(6)));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void restoreOldSettings()
	{
		try 
		{
			System.out.println("Restoring old settings ..");
			FROM = Paths.get(SavedSettings);
			TO = Paths.get(Settings);
			java.nio.file.Files.copy(FROM, TO, options);
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void rebootSystem()
	{
		try 
		{
			System.out.println("Rebooting System ..");
			rt.exec("sudo hwclock -s");
			rt.exec("sudo reboot");
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String getDayOfWeek()
	{
		int s = cal.getTime().getDay();
		
		if (s==1) 
		{
			return "Monday";
		}
		else if (s==2) 
		{
			return "Tuesday";
		}
		else if (s==3) 
		{
			return "Wednesday";
		}
		else if (s==4) 
		{
			return "Thursday";
		}
		else if (s==5) 
		{
			return "Friday";
		}
		else if (s==6) 
		{
			return "Saturday";
		}
		else if (s==7) 
		{
			return "Sunday";
		}
		return "Error : Couldn't get the day name!";
	}
}



class PrayerTimesController {

    double latitude;

    double longitude;

    int timeZone;

    int[] offset;

    PrayerTimes prayerTimes;

    int timeFormat;

    int method;

    int asrJuristic;

    int adjustHighLats;

    double elevation;

    float fajrAngle,ishaaAngle,ishaaMinutes;

    int midNightMethod;

    public PrayerTimesController(double latitude, double longitude, double elevation, int timeZone, int[] offset,

                                 int timeFormat, int method, int asrJuristic, int adjustHighLats,

                                 float fajrAngle, float ishaaAngle, float ishaaMinutes, int midNightMethod

                                 ) {

        this.latitude = latitude;

        this.longitude = longitude;

        this.timeZone = timeZone;

        this.offset = offset;

        this.timeFormat = timeFormat;

        this.method = method;

        this.asrJuristic = asrJuristic;

        this.adjustHighLats = adjustHighLats;

        this.elevation = elevation;

        this.fajrAngle = fajrAngle;

        this.ishaaAngle = ishaaAngle;

        this.ishaaMinutes = ishaaMinutes;

        this.midNightMethod = midNightMethod;

        prayerTimes = new PrayerTimes();

    }


    public ArrayList<String> getPrayerTimes() {

        ArrayList<String> times;

        prayerTimes.setTimeFormat(timeFormat);

        prayerTimes.setCalcMethod(method);

        prayerTimes.setAsrJuristic(asrJuristic);

        prayerTimes.setAdjustHighLats(adjustHighLats);

        prayerTimes.setElevation(elevation);

        prayerTimes.setFajrAngle(fajrAngle);

        prayerTimes.setIshaAngle(ishaaAngle);

        //prayerTimes.tune(offset);

        Date now = new Date();

        Calendar cal = Calendar.getInstance();

        cal.setTime(now);

        times = prayerTimes.getPrayerTimes(cal, latitude, longitude, timeZone);

        return times;



    }


    public void displayPrayerTimes(ArrayList<String> prayerTimes) {

//        Log.d("appLog", "fajr : " + prayerTimes.get(0));

//        Log.d("appLog", "sunrise : " + prayerTimes.get(1));

//        Log.d("appLog", "duhur : " + prayerTimes.get(2));

//        Log.d("appLog", "asr : " + prayerTimes.get(3));

//        Log.d("appLog", "sunset : " + prayerTimes.get(4));

//        Log.d("appLog", "magrib : " + prayerTimes.get(5));

//        Log.d("appLog", "ishaa : " + prayerTimes.get(6));

//        Log.d("appLog", "===================================================================");



    }


    public PrayerTimesInMin getPrayerTimesInMin(ArrayList<String> prayerTimes) {

        PrayerTimesInMin times = new PrayerTimesInMin();

        times.FAJR = getTimeInMin(prayerTimes.get(0));

        times.SHOROQ = getTimeInMin(prayerTimes.get(1));

        times.DUHUR = getTimeInMin(prayerTimes.get(2));

        times.ASR = getTimeInMin(prayerTimes.get(3));

        times.MAGHRIB = getTimeInMin(prayerTimes.get(5));

        times.ISHAA = getTimeInMin(prayerTimes.get(6));

        return times;

    }


    public int getTimeInMin(String time) {

        int hour = Integer.parseInt(time.substring(0, 2));

        int min = Integer.parseInt(time.substring(3, 5));

        return hour * 60 + min;

    }


     class PrayerTimesInMin {

        public int FAJR;

        public int SHOROQ;

        public int DUHUR;

        public int ASR;

        public int MAGHRIB;

        public int ISHAA;

    }
}






 class PrayerTimes {
	 

    // ---------------------- Global Variables --------------------

    public static int calcMethod; // caculation method

    public static int asrJuristic; // Juristic method for Asr

    public static int dhuhrMinutes; // minutes after mid-day for Dhuhr

    public static int adjustHighLats; // adjusting method for higher latitudes

    public static int timeFormat; // time format

    // ------------------------------------------------------------

    // Calculation Methods

    public static int Jafari; // Ithna Ashari

    public static int Karachi; // University of Islamic Sciences, Karachi

    public static int ISNA; // Islamic Society of North America (ISNA)

    public static int MWL; // Muslim World League (MWL)

    public static int Makkah; // Umm al-Qura, Makkah

    public static int Egypt; // Egyptian General Authority of Survey

    public static int Custom; // Custom Setting

    public static int Tehran; // Institute of Geophysics, University of Tehran

    // Juristic Methods

    public static int Shafii; // Shafii (standard)

    public static int Hanafi; // Hanafi

    // Adjusting Methods for Higher Latitudes

    public static int None; // No adjustment

    public static int MidNight; // middle of night

    public static int OneSeventh; // 1/7th of night

    public static int AngleBased; // angle/60th of night

    // Time Formats

    public static int Time24; // 24-hour format

    public static int Time12; // 12-hour format

    public static int Time12NS; // 12-hour format with no suffix

    public static int Floating; // floating point number

    public double lat; // latitude

    public double lng; // longitude

    public double timeZone; // time-zone

    public double JDate; // Julian date

    // Time Names

    ArrayList<String> timeNames;

    String InvalidTime; // The string used for invalid times

    // --------------------- Technical Settings --------------------

    int numIterations; // number of iterations needed to compute times

    // ------------------- Calc Method Parameters --------------------

    HashMap<Integer, double[]> methodParams;



    /*

     * this.methodParams[methodNum] = new Array(fa, ms, mv, is, iv);

     *

     * fa : fajr angle ms : maghrib selector (0 = angle; 1 = minutes after

     * sunset) mv : maghrib parameter value (in angle or minutes) is : isha

     * selector (0 = angle; 1 = minutes after maghrib) iv : isha parameter value

     * (in angle or minutes)

     */

    double[] prayerTimesCurrent;

    int[] offsets;

    private double elv;



    public PrayerTimes() {

        // Initialize vars



        this.setCalcMethod(0);

        this.setAsrJuristic(0);

        this.setDhuhrMinutes(0);

        this.setAdjustHighLats(1);

        this.setTimeFormat(0);



        // Calculation Methods

        this.setJafari(0); // Ithna Ashari

        this.setKarachi(1); // University of Islamic Sciences, Karachi

        this.setISNA(2); // Islamic Society of North America (ISNA)

        this.setMWL(3); // Muslim World League (MWL)

        this.setMakkah(4); // Umm al-Qura, Makkah

        this.setEgypt(5); // Egyptian General Authority of Survey

        this.setTehran(6); // Institute of Geophysics, University of Tehran

        this.setCustom(7); // Custom Setting



        // Juristic Methods

        this.setShafii(0); // Shafii (standard)

        this.setHanafi(1); // Hanafi



        // Adjusting Methods for Higher Latitudes

        this.setNone(0); // No adjustment

        this.setMidNight(1); // middle of night

        this.setOneSeventh(2); // 1/7th of night

        this.setAngleBased(3); // angle/60th of night



        // Time Formats

        this.setTime24(0); // 24-hour format

        this.setTime12(1); // 12-hour format

        this.setTime12NS(2); // 12-hour format with no suffix

        this.setFloating(3); // floating point number



        // Time Names

        timeNames = new ArrayList<String>();

        timeNames.add("Fajr");

        timeNames.add("Sunrise");

        timeNames.add("Dhuhr");

        timeNames.add("Asr");

        timeNames.add("Sunset");

        timeNames.add("Maghrib");

        timeNames.add("Isha");



        InvalidTime = "-----"; // The string used for invalid times



        // --------------------- Technical Settings --------------------



        this.setNumIterations(1); // number of iterations needed to compute

        // times



        // ------------------- Calc Method Parameters --------------------



        // Tuning offsets {fajr, sunrise, dhuhr, asr, sunset, maghrib, isha}

        offsets = new int[7];

        offsets[0] = 0;

        offsets[1] = 0;

        offsets[2] = 0;

        offsets[3] = 0;

        offsets[4] = 0;

        offsets[5] = 0;

        offsets[6] = 0;



		/*

         *

		 * fa : fajr angle ms : maghrib selector (0 = angle; 1 = minutes after

		 * sunset) mv : maghrib parameter value (in angle or minutes) is : isha

		 * selector (0 = angle; 1 = minutes after maghrib) iv : isha parameter

		 * value (in angle or minutes)

		 */

        methodParams = new HashMap<Integer, double[]>();



        // Jafari

        double[] Jvalues = {16, 0, 4, 0, 14};

        methodParams.put(Integer.valueOf(this.getJafari()), Jvalues);



        // Karachi

        double[] Kvalues = {18, 1, 0, 0, 18};

        methodParams.put(Integer.valueOf(this.getKarachi()), Kvalues);



        // ISNA

        double[] Ivalues = {15, 1, 0, 0, 15};

        methodParams.put(Integer.valueOf(this.getISNA()), Ivalues);



        // MWL

        double[] MWvalues = {18, 1, 0, 0, 17};

        methodParams.put(Integer.valueOf(this.getMWL()), MWvalues);



        // Makkah

        double[] MKvalues = {18.5, 1, 0, 1, 90};

        methodParams.put(Integer.valueOf(this.getMakkah()), MKvalues);



        // Egypt

        double[] Evalues = {19.5, 1, 0, 0, 17.5};

        methodParams.put(Integer.valueOf(this.getEgypt()), Evalues);



        // Tehran

        double[] Tvalues = {17.7, 0, 4.5, 0, 14};

        methodParams.put(Integer.valueOf(this.getTehran()), Tvalues);



        // Custom

        double[] Cvalues = {18, 1, 0, 0, 17};

        methodParams.put(Integer.valueOf(this.getCustom()), Cvalues);



    }



    // ---------------------- Trigonometric Functions -----------------------

    // range reduce angle in degrees.

    double fixangle(double a) {



        a = a - (360 * (Math.floor(a / 360.0)));



        a = a < 0 ? (a + 360) : a;



        return a;

    }



    // range reduce hours to 0..23

    double fixhour(double a) {

        a = a - 24.0 * Math.floor(a / 24.0);

        a = a < 0 ? (a + 24) : a;

        return a;

    }



    // radian to degree

    double radiansToDegrees(double alpha) {

        return ((alpha * 180.0) / Math.PI);

    }



    // deree to radian

    double DegreesToRadians(double alpha) {

        return ((alpha * Math.PI) / 180.0);

    }



    // degree sin

    double dsin(double d) {

        return (Math.sin(DegreesToRadians(d)));

    }



    // degree cos

    double dcos(double d) {

        return (Math.cos(DegreesToRadians(d)));

    }



    // degree tan

    double dtan(double d) {

        return (Math.tan(DegreesToRadians(d)));

    }



    // degree arcsin

    double darcsin(double x) {

        double val = Math.asin(x);

        return radiansToDegrees(val);

    }



    // degree arccos

    double darccos(double x) {

        double val = Math.acos(x);

        return radiansToDegrees(val);

    }



    // degree arctan

    double darctan(double x) {

        double val = Math.atan(x);

        return radiansToDegrees(val);

    }



    // degree arctan2

    double darctan2(double y, double x) {

        double val = Math.atan2(y, x);

        return radiansToDegrees(val);

    }



    // degree arccot

    double darccot(double x) {

        double val = Math.atan2(1.0, x);

        return radiansToDegrees(val);

    }



    // ---------------------- Time-Zone Functions -----------------------

    // compute local time-zone for a specific date

    double getTimeZone1() {

        TimeZone timez = TimeZone.getDefault();

        double hoursDiff = (timez.getRawOffset() / 1000.0) / 3600;

        return hoursDiff;

    }



    // compute base time-zone of the system

    double getBaseTimeZone() {

        TimeZone timez = TimeZone.getDefault();

        double hoursDiff = (timez.getRawOffset() / 1000.0) / 3600;

        return hoursDiff;



    }



    // detect daylight saving in a given date

    double detectDaylightSaving() {

        TimeZone timez = TimeZone.getDefault();

        double hoursDiff = timez.getDSTSavings();

        return hoursDiff;

    }



    // ---------------------- Julian Date Functions -----------------------

    // calculate julian date from a calendar date

    double julianDate(int year, int month, int day) {



        if (month <= 2) {

            year -= 1;

            month += 12;

        }

        double A = Math.floor(year / 100.0);



        double B = 2 - A + Math.floor(A / 4.0);



        double JD = Math.floor(365.25 * (year + 4716))

                + Math.floor(30.6001 * (month + 1)) + day + B - 1524.5;



        return JD;

    }



    // convert a calendar date to julian date (second method)

    double calcJD(int year, int month, int day) {

        double J1970 = 2440588.0;

        Date date = new Date(year, month - 1, day);



        double ms = date.getTime(); // # of milliseconds since midnight Jan 1,

        // 1970

        double days = Math.floor(ms / (1000.0 * 60.0 * 60.0 * 24.0));

        return J1970 + days - 0.5;



    }



    double[] sunPosition(double jd) {



        double D = jd - 2451545;

        double g = fixangle(357.529 + 0.98560028 * D);

        double q = fixangle(280.459 + 0.98564736 * D);

        double L = fixangle(q + (1.915 * dsin(g)) + (0.020 * dsin(2 * g)));



        // double R = 1.00014 - 0.01671 * [self dcos:g] - 0.00014 * [self dcos:

        // (2*g)];

        double e = 23.439 - (0.00000036 * D);

        double d = darcsin(dsin(e) * dsin(L));

        double RA = (darctan2((dcos(e) * dsin(L)), (dcos(L)))) / 15.0;

        RA = fixhour(RA);

        double EqT = q / 15.0 - RA;

        double[] sPosition = new double[2];

        sPosition[0] = d;

        sPosition[1] = EqT;



        return sPosition;

    }



    // compute equation of time

    double equationOfTime(double jd) {

        double eq = sunPosition(jd)[1];

        return eq;

    }



    // compute declination angle of sun

    double sunDeclination(double jd) {

        double d = sunPosition(jd)[0];

        return d;

    }



    // compute mid-day (Dhuhr, Zawal) time

    double computeMidDay(double t) {

        double T = equationOfTime(this.getJDate() + t);

        double Z = fixhour(12 - T);

        return Z;

    }



    // compute time for a given angle G

    double computeTime(double G, double t) {



        double D = sunDeclination(this.getJDate() + t);

        double Z = computeMidDay(t);

        double Beg = -dsin(G) - dsin(D) * dsin(this.getLat());

        double Mid = dcos(D) * dcos(this.getLat());

        double V = darccos(Beg / Mid) / 15.0;



        return Z + (G > 90 ? -V : V);

    }



    // compute the time of Asr

    // Shafii: step=1, Hanafi: step=2

    double computeAsr(double step, double t) {

        double D = sunDeclination(this.getJDate() + t);

        double G = -darccot(step + dtan(Math.abs(this.getLat() - D)));

        return computeTime(G, t);

    }



    // ---------------------- Misc Functions -----------------------

    // compute the difference between two times

    double timeDiff(double time1, double time2) {

        return fixhour(time2 - time1);

    }



    // -------------------- Interface Functions --------------------

    // return prayer times for a given date

    ArrayList<String> getDatePrayerTimes(int year, int month, int day,

                                         double latitude, double longitude, double tZone) {

        this.setLat(latitude);

        this.setLng(longitude);

        this.setTimeZone(tZone);

        this.setJDate(julianDate(year, month, day));

        double lonDiff = longitude / (15.0 * 24.0);

        this.setJDate(this.getJDate() - lonDiff);

        return computeDayTimes();

    }



    // return prayer times for a given date

    ArrayList<String> getPrayerTimes(Calendar date, double latitude,

                                     double longitude, double tZone) {



        int year = date.get(Calendar.YEAR);

        int month = date.get(Calendar.MONTH);

        int day = date.get(Calendar.DATE);



        return getDatePrayerTimes(year, month + 1, day, latitude, longitude,

                tZone);

    }



    // set custom values for calculation parameters

    void setCustomParams(double[] params) {



        for (int i = 0; i < 5; i++) {

            if (params[i] == -1) {

                params[i] = methodParams.get(this.getCalcMethod())[i];

                methodParams.put(this.getCustom(), params);

            } else {

                methodParams.get(this.getCustom())[i] = params[i];

            }

        }

        this.setCalcMethod(this.getCustom());

    }



    // set the angle for calculating Fajr

    public void setFajrAngle(double angle) {

        double[] params = {angle, -1, -1, -1, -1};

        setCustomParams(params);

    }



    // set the angle for calculating Maghrib

    public void setMaghribAngle(double angle) {

        double[] params = {-1, 0, angle, -1, -1};

        setCustomParams(params);



    }



    // set the angle for calculating Isha

    public void setIshaAngle(double angle) {

        double[] params = {-1, -1, -1, 0, angle};

        //double[] params = {-1, -1, -1, -1, angle};



        setCustomParams(params);



    }



    // set the minutes after Sunset for calculating Maghrib

    public void setMaghribMinutes(double minutes) {

        double[] params = {-1, 1, minutes, -1, -1};

        setCustomParams(params);



    }



    // set the minutes after Maghrib for calculating Isha

    public void setIshaMinutes(double minutes) {

        double[] params = {-1, -1, -1, 1, minutes};

        setCustomParams(params);



    }



    // convert double hours to 24h format

    public String floatToTime24(double time) {



        String result;



        if (Double.isNaN(time)) {

            return InvalidTime;

        }



        time = fixhour(time + 0.5 / 60.0); // add 0.5 minutes to round

        int hours = (int) Math.floor(time);

        double minutes = Math.floor((time - hours) * 60.0);



        if ((hours >= 0 && hours <= 9) && (minutes >= 0 && minutes <= 9)) {

            result = "0" + hours + ":0" + Math.round(minutes);

        } else if ((hours >= 0 && hours <= 9)) {

            result = "0" + hours + ":" + Math.round(minutes);

        } else if ((minutes >= 0 && minutes <= 9)) {

            result = hours + ":0" + Math.round(minutes);

        } else {

            result = hours + ":" + Math.round(minutes);

        }

        return result;

    }



    // convert double hours to 12h format

    public String floatToTime12(double time, boolean noSuffix) {



        if (Double.isNaN(time)) {

            return InvalidTime;

        }



        time = fixhour(time + 0.5 / 60); // add 0.5 minutes to round

        int hours = (int) Math.floor(time);

        double minutes = Math.floor((time - hours) * 60);

        String suffix, result;

        if (hours >= 12) {

            suffix = "pm";

        } else {

            suffix = "am";

        }

        hours = ((((hours + 12) - 1) % (12)) + 1);

        /*

         * hours = (hours + 12) - 1; int hrs = (int) hours % 12; hrs += 1;

		 */

        if (noSuffix == false) {

            if ((hours >= 0 && hours <= 9) && (minutes >= 0 && minutes <= 9)) {

                result = "0" + hours + ":0" + Math.round(minutes) + " "

                        + suffix;

            } else if ((hours >= 0 && hours <= 9)) {

                result = "0" + hours + ":" + Math.round(minutes) + " " + suffix;

            } else if ((minutes >= 0 && minutes <= 9)) {

                result = hours + ":0" + Math.round(minutes) + " " + suffix;

            } else {

                result = hours + ":" + Math.round(minutes) + " " + suffix;

            }



        } else {

            if ((hours >= 0 && hours <= 9) && (minutes >= 0 && minutes <= 9)) {

                result = "0" + hours + ":0" + Math.round(minutes);

            } else if ((hours >= 0 && hours <= 9)) {

                result = "0" + hours + ":" + Math.round(minutes);

            } else if ((minutes >= 0 && minutes <= 9)) {

                result = hours + ":0" + Math.round(minutes);

            } else {

                result = hours + ":" + Math.round(minutes);

            }

        }

        return result;



    }



    // convert double hours to 12h format with no suffix

    public String floatToTime12NS(double time) {

        return floatToTime12(time, true);

    }



    // ---------------------- Compute Prayer Times -----------------------

    // compute prayer times at given julian date

    double[] computeTimes(double[] times) {



        double[] t = dayPortion(times);



        double Fajr = this.computeTime(

                180 - methodParams.get(this.getCalcMethod())[0], t[0]);



        double Sunrise = this.computeTime(

//                180 - 0.833,

                   180 - 0.833 - 0.0347 * Math.sqrt(elv*1.0),

                t[1]);



        double Dhuhr = this.computeMidDay(t[2]);

        double Asr = this.computeAsr(1 + this.getAsrJuristic(), t[3]);

        double Sunset = this.computeTime(

//                0.833

                             0.833 + 0.0347 * Math.sqrt(elv*1.0)

                , t[4]);





        double Maghrib = Sunset +

                this.computeTime(//47.744573068

                        methodParams.get(

//                                this.getCalcMethod())[2]

                                this.getCalcMethod())[2]

                        , t[5]);

        double Isha = this.computeTime(

                methodParams.get(this.getCalcMethod())[4], t[6]);



        double[] CTimes = {Fajr, Sunrise, Dhuhr, Asr, Sunset, Maghrib, Isha};



        return CTimes;



    }



    // compute prayer times at given julian date

    ArrayList<String> computeDayTimes() {

        double[] times = {5, 6, 12, 13, 18, 18, 18}; // default times



        for (int i = 1; i <= this.getNumIterations(); i++) {

            times = computeTimes(times);

        }



        times = adjustTimes(times);

        times = tuneTimes(times);



        return adjustTimesFormat(times);

    }



    // adjust times in a prayer time array

    double[] adjustTimes(double[] times) {

        for (int i = 0; i < times.length; i++) {

            times[i] += this.getTimeZone() - this.getLng() / 15;

        }



        times[2] += this.getDhuhrMinutes() / 60; // Dhuhr

        if (methodParams.get(this.getCalcMethod())[1] == 1) // Maghrib

        {

            times[5] = times[4] + methodParams.get(this.getCalcMethod())[2] / 60;

        }

        if (methodParams.get(this.getCalcMethod())[3] == 1) // Isha

        {

            times[6] = times[5] + methodParams.get(this.getCalcMethod())[4]

                    / 60;

        }



        if (this.getAdjustHighLats() != this.getNone()) {

            times = adjustHighLatTimes(times);

        }



        return times;

    }



    // convert times array to given time format

    ArrayList<String> adjustTimesFormat(double[] times) {



        ArrayList<String> result = new ArrayList<String>();

        //times[5] = times[4] + 3.0 / 60;



        if (this.getTimeFormat() == this.getFloating()) {

            for (double time : times) {

                result.add(String.valueOf(time));

            }

            return result;

        }



        for (int i = 0; i < 7; i++) {

            if (this.getTimeFormat() == this.getTime12()) {

                result.add(floatToTime12(times[i], false));

            } else if (this.getTimeFormat() == this.getTime12NS()) {

                result.add(floatToTime12(times[i], true));

            } else {

                result.add(floatToTime24(times[i]));

            }

        }

        return result;

    }



    // adjust Fajr, Isha and Maghrib for locations in higher latitudes

    double[] adjustHighLatTimes(double[] times) {

        double nightTime = timeDiff(times[4], times[1]); // sunset to sunrise



        // Adjust Fajr

        double FajrDiff = nightPortion(methodParams.get(this.getCalcMethod())[0])

                * nightTime;



        if (Double.isNaN(times[0]) || timeDiff(times[0], times[1]) > FajrDiff) {

            times[0] = times[1] - FajrDiff;

        }



        // Adjust Isha

        double IshaAngle = (methodParams.get(this.getCalcMethod())[3] == 0) ? methodParams

                .get(this.getCalcMethod())[4] : 18;

        double IshaDiff = this.nightPortion(IshaAngle) * nightTime;

        if (Double.isNaN(times[6])

                || this.timeDiff(times[4], times[6]) > IshaDiff) {

            times[6] = times[4] + IshaDiff;

        }



        // Adjust Maghrib

        double MaghribAngle = (methodParams.get(this.getCalcMethod())[1] == 0) ? methodParams

                .get(this.getCalcMethod())[2] : 4;

        double MaghribDiff = nightPortion(MaghribAngle) * nightTime;

        if (Double.isNaN(times[5])

                || this.timeDiff(times[4], times[5]) > MaghribDiff) {

            times[5] = times[4] + MaghribDiff;

        }



        return times;

    }



    // the night portion used for adjusting times in higher latitudes

    double nightPortion(double angle) {

        double calc = 0;



        if (adjustHighLats == AngleBased)

            calc = (angle) / 60.0;

        else if (adjustHighLats == MidNight)

            calc = 0.5;

        else if (adjustHighLats == OneSeventh)

            calc = 0.14286;



        return calc;

    }



    // convert hours to day portions

    double[] dayPortion(double[] times) {

        for (int i = 0; i < 7; i++) {

            times[i] /= 24;

        }

        return times;

    }



    // Tune timings for adjustments

    // Set time offsets

    public void tune(int[] offsetTimes) {



        for (int i = 0; i < offsetTimes.length; i++) { // offsetTimes length

            // should be 7 in order

            // of Fajr, Sunrise,

            // Dhuhr, Asr, Sunset,

            // Maghrib, Isha

            this.offsets[i] = offsetTimes[i];

        }

    }



    double[] tuneTimes(double[] times) {

        for (int i = 0; i < times.length; i++) {

            times[i] = times[i] + this.offsets[i] / 60.0;

        }



        return times;

    }





    public int getCalcMethod() {

        return calcMethod;

    }



    public void setCalcMethod(int calcMethod) {

        PrayerTimes.calcMethod = calcMethod;

    }



    public int getAsrJuristic() {

        return asrJuristic;

    }



    public void setAsrJuristic(int asrJuristic) {

        PrayerTimes.asrJuristic = asrJuristic;

    }



    public int getDhuhrMinutes() {

        return dhuhrMinutes;

    }



    public void setDhuhrMinutes(int dhuhrMinutes) {

        PrayerTimes.dhuhrMinutes = dhuhrMinutes;

    }



    public int getAdjustHighLats() {

        return adjustHighLats;

    }



    public void setAdjustHighLats(int adjustHighLats) {

        PrayerTimes.adjustHighLats = adjustHighLats;

    }



    public int getTimeFormat() {

        return timeFormat;

    }



    public void setTimeFormat(int timeFormat) {

        PrayerTimes.timeFormat = timeFormat;

    }



    public double getLat() {

        return lat;

    }



    public void setLat(double lat) {

        this.lat = lat;

    }



    public double getLng() {

        return lng;

    }



    public void setLng(double lng) {

        this.lng = lng;

    }



    public double getTimeZone() {

        return timeZone;

    }



    public void setTimeZone(double timeZone) {

        this.timeZone = timeZone;

    }



    public double getJDate() {

        return JDate;

    }



    public void setJDate(double jDate) {

        JDate = jDate;

    }



    int getJafari() {

        return Jafari;

    }



    void setJafari(int jafari) {

        Jafari = jafari;

    }



    int getKarachi() {

        return Karachi;

    }



    void setKarachi(int karachi) {

        Karachi = karachi;

    }



    int getISNA() {

        return ISNA;

    }



    void setISNA(int iSNA) {

        ISNA = iSNA;

    }



    int getMWL() {

        return MWL;

    }



    void setMWL(int mWL) {

        MWL = mWL;

    }



    int getMakkah() {

        return Makkah;

    }



    void setMakkah(int makkah) {

        Makkah = makkah;

    }



    int getEgypt() {

        return Egypt;

    }



    void setEgypt(int egypt) {

        Egypt = egypt;

    }



    int getCustom() {

        return Custom;

    }



    void setCustom(int custom) {

        Custom = custom;

    }



    int getTehran() {

        return Tehran;

    }



    void setTehran(int tehran) {

        Tehran = tehran;

    }



    int getShafii() {

        return Shafii;

    }



    void setShafii(int shafii) {

        Shafii = shafii;

    }



    int getHanafi() {

        return Hanafi;

    }



    void setHanafi(int hanafi) {

        Hanafi = hanafi;

    }



    int getNone() {

        return None;

    }



    void setNone(int none) {

        None = none;

    }



    int getMidNight() {

        return MidNight;

    }



    void setMidNight(int midNight) {

        MidNight = midNight;

    }



    int getOneSeventh() {

        return OneSeventh;

    }



    void setOneSeventh(int oneSeventh) {

        OneSeventh = oneSeventh;

    }



    int getAngleBased() {

        return AngleBased;

    }



    void setAngleBased(int angleBased) {

        AngleBased = angleBased;

    }



    int getTime24() {

        return Time24;

    }



    void setTime24(int time24) {

        Time24 = time24;

    }



    int getTime12() {

        return Time12;

    }



    void setTime12(int time12) {

        Time12 = time12;

    }



    int getTime12NS() {

        return Time12NS;

    }



    void setTime12NS(int time12ns) {

        Time12NS = time12ns;

    }



    int getFloating() {

        return Floating;

    }



    void setFloating(int floating) {

        Floating = floating;

    }



    int getNumIterations() {

        return numIterations;

    }



    void setNumIterations(int numIterations) {

        this.numIterations = numIterations;

    }



    public ArrayList<String> getTimeNames() {

        return timeNames;

    }



    public void setElevation(double elevation) {

        this.elv = elevation;

        //Log.d("appLog", "angle = " + (.833 + 0.0347 * Math.sqrt(elv * 1.0)));

//        this.setMaghribAngle((.833 + 0.0347 * Math.sqrt(elv * 1.0)));

    }

}


