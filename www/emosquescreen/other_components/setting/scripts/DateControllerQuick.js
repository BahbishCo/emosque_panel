﻿var editMiladiDateCheckBox = document.getElementById("editMiladiDateCheckBox");
var miladiDayName = document.getElementById("miladiDayName");
var miladiDaySelector = document.getElementById("miladiDay");
var miladiMonthSelector = document.getElementById("miladiMonth");
var miladiYearSelector = document.getElementById("miladiYear");
var hourSelector = document.getElementById("hour");
var minSelector = document.getElementById("min");

var hijriDateCorrectionSelector = document.getElementById("hijriCorrection");
var miladiDateDiv = document.getElementById("miladiDateDiv");
var currentMiladiDateText = document.getElementById("currentMiladiDate");
var currentHijriDateText = document.getElementById("currentHijriDate");
var currectedHijriDateText = document.getElementById("currectedHijriDate");
var currectedHijriDateText2 = document.getElementById("currectedHijriDate2");

var submitButton = document.getElementById("submitButton");
var currectedHijriDate = document.getElementById("currectedHijriDate");

var daysNamesArabic = new Array("الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت");
var daysNames = new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
var iMonthNames = new Array("محرم", "صفر", "ربيع الأول", "ربيع الأخر",
"جمادى الأولى", "جمادى الأخرة", "رجب", "شعبان",
"رمضان", "شوال", "ذو القعدة", "ذو الحجة");
var iMonthNamesEnglish = new Array("Muḥarram", "	Ṣafar", "Rabī‘ alawwal", "Rabī‘ ath-thānī",
    "Jumādá al-ūlá", "Jumādá al-ākhirah", "	Rajab", "Sha‘bān",
    "Ramaḍān", "	Shawwāl", "Dhū al-Qa‘dah", "Dhū al-Ḥijjah");
initDays(miladiDaySelector);
initMonths(miladiMonthSelector);
initYears(miladiYearSelector);
initHijriCorrection(hijriDateCorrectionSelector);
initHoursAndMinSelectors();
adjustDate();
attachListeners();

$(document).ready(function ()
{
   onMiladiDateChange(); 
});

function replaceNumbers(num)
{
    num = num.toString();
    num = num.replace("0", "۰");
    num = num.replace("1", "۱");
    num = num.replace("2", "۲");
    num = num.replace("3", "۳");
    num = num.replace("4", "٤");
    num = num.replace("5", "۵");
    num = num.replace("6", "٦");
    num = num.replace("7", "۷");
    num = num.replace("8", "۸");
    num = num.replace("9", "۹");
    return num;
}


function addOneDay()
{
    if(hijriDateCorrectionSelector.selectedIndex<6)
    {
    hijriDateCorrectionSelector.selectedIndex = hijriDateCorrectionSelector.selectedIndex + 1;
    onMiladiDateChange();
    submitButton.disabled = false;
    submitButton.style.background = '#afbec5';
    submitButton.style.color = '#000000'

	currectedHijriDate.style.visibility = 'hidden';

	setTimeout(function(){
    	currectedHijriDate.style.visibility = 'visible';
    	},170);
    }
}

function reduceOneDay()
{
    if(hijriDateCorrectionSelector.selectedIndex>0)
    {
    hijriDateCorrectionSelector.selectedIndex = hijriDateCorrectionSelector.selectedIndex - 1;
    onMiladiDateChange();
    submitButton.disabled = false;
    submitButton.style.background = '#afbec5';
    submitButton.style.color = '#000000'

	currectedHijriDate.style.visibility = 'hidden';

	setTimeout(function(){
    	currectedHijriDate.style.visibility = 'visible';
    	},170);
    }
}

function adjustDate() {

    var date = new Date();
    var dateMeladi = document.getElementById("dateMeladi");
    var month = parseInt(date.getMonth()) + 1;
    var hour = date.getHours();
    var min = date.getMinutes();
    var dayNum = date.getDay();
    meladiDay = date.getDate();
    meladiMonth = month;
    meladiYear = date.getFullYear();
    
    miladiDayName.options[dayNum].selected = true;
    miladiDaySelector.options[date.getDate() - 1].selected = true;
    miladiMonthSelector.options[month - 1].selected = true;
    miladiYearSelector.options[date.getFullYear() - 2015].selected = true;
    hijriDateCorrectionSelector.options[3].selected = true
    hourSelector.options[hour].selected = true;
    minSelector.options[min].selected = true;

    fullMeladiDate = date.getDate() + "/" + month + "/" + date.getFullYear();
    //currentMiladiDateText.innerHTML = "التاريخ الميلادي الحالي : "+fullMeladiDate;

   }


function initHoursAndMinSelectors()
{
    for (var i = 0; i <= 23; i++)
    {
        var option = document.createElement("option");
        option.text = i
        option.value = i;
        hourSelector.add(option);
    }
    for (var i = 0; i <= 60; i++)
    {
        var option = document.createElement("option");
        option.text = i
        option.value = i;
        minSelector.add(option);
    }
}


function initDays(selector)
{
    for (var i = 1; i <= 31; i++)
    {
        var option = document.createElement("option");
        option.text = i
        option.value = i;
        selector.add(option);
    }
    for (var i = 0; i < Constants.daysNames.length; i++)
    {
        var option = document.createElement("option");
        option.text = Constants.daysNames[i]
        option.value = Constants.daysNames[i]
        miladiDayName.add(option);
    }
}



function initMonths(selector)
{
    for (var i = 1; i <= 12; i++)
    {
        var option = document.createElement("option");
        option.text = i
        option.value = i;
        selector.add(option);
    }
}

function initYears(selector)
{
    for (var i = 2015; i <= 2100; i++)
    {
        var option = document.createElement("option");
        option.text = i
        option.value = i;
        selector.add(option);
    }
}

function initHijriCorrection(selector)
{
    for (var i = -3; i <= 3; i++)
    {
        var option = document.createElement("option");
        option.text = i
        option.value = i;
        selector.add(option);
    }
}

function onMiladiEditEnableChange()
{
    if (editMiladiDateCheckBox.checked)
    {
        var date = new Date();
        var month = parseInt(date.getMonth()) + 1;
        var hour = date.getHours();
        var min = date.getMinutes();
        var dayNum = date.getDay();

        miladiDayName.disabled = false;
        miladiDaySelector.disabled = false;
        miladiMonthSelector.disabled = false;
        miladiYearSelector.disabled = false;
        hourSelector.disabled = false;
        minSelector.disabled = false;

        miladiDayName.options[dayNum].selected = true;
        miladiDaySelector.options[date.getDate() -1 ].selected = true;
        miladiMonthSelector.options[month - 1].selected = true;
        miladiYearSelector.options[date.getFullYear() - 2015].selected = true;
        hourSelector.options[hour].selected = true;
        minSelector.options[min].selected = true;
        onMiladiDateChange();
        miladiDateDiv.style.visibility = "visible";
    }

    else
    {
        var date = new Date();
        var month = parseInt(date.getMonth()) + 1;
        var hour = date.getHours();
        var min = date.getMinutes();
        var dayNum = date.getDay();

        miladiDayName.options[dayNum].selected = true;
        miladiDaySelector.options[date.getDate() - 1].selected = true;
        miladiMonthSelector.options[month - 1].selected = true;
        miladiYearSelector.options[date.getFullYear() - 2015].selected = true;
        hourSelector.options[hour].selected = true;
        minSelector.options[min].selected = true;

        miladiDayName.disabled = true;
        miladiDaySelector.disabled = true;
        miladiMonthSelector.disabled = true;
        miladiYearSelector.disabled = true;
        hourSelector.disabled = true;
        minSelector.disabled = true;

        miladiDateDiv.style.visibility = "hidden"
    }
}


function onMiladiDateChange() {
    //year, month, day, hours, minutes, seconds, milliseconds
    var date = new Date(miladiYearSelector.value, miladiMonthSelector.value-1, miladiDaySelector.value)
    var hijriDate = writeIslamicDate(hijriDateCorrectionSelector.value,date);
    day = daysNamesArabic[parseInt(date.getDay())];
    var dayNo = hijriDate[5];
    var year = hijriDate[7];
    var month = hijriDate[6];
    hijriDate = document.getElementById("dateHijri");


    hijriDay = day.toString();
    dayNo = dayNo.toString();


    if (dayNo.length == 1)
    {
        if(document.title=="Panel Settings")
        {
            hijriMonth = iMonthNamesEnglish[month];

            fullHijriDate = hijriMonth + " " + (dayNo);

        }else
        {
            hijriMonth = iMonthNames[month];

            fullHijriDate = hijriMonth + " " + replaceNumbers(dayNo);

        }
        //hijriDayNo = replaceNumbers(dayNo);
    }
    if (dayNo.length == 2)
    {
        if(document.title=="Panel Settings")
        {
            fullHijriDate = hijriMonth + " " +  (dayNo[0]) + (dayNo[1]);

        }else
        {
            fullHijriDate = hijriMonth + " " +  replaceNumbers(dayNo[0]) + replaceNumbers(dayNo[1]);

        }
        //hijriDayNo = replaceNumbers(dayNo[0]) + replaceNumbers(dayNo[1]);
    }

    currectedHijriDateText.innerHTML = fullHijriDate;
    currectedHijriDateText2.innerHTML = fullHijriDate;

}

function attachListeners()
{

    if (miladiDaySelector.addEventListener)
    {
        miladiDayName.addEventListener('change', onMiladiDateChange, false);
        miladiDaySelector.addEventListener('change', onMiladiDateChange, false);
        miladiMonthSelector.addEventListener('change', onMiladiDateChange, false);
        miladiYearSelector.addEventListener('change', onMiladiDateChange, false);
        hijriDateCorrectionSelector.addEventListener('change', onMiladiDateChange, false);
    }
    else
    {
        //IE
        miladiDayName.attachEvent('onchange', onMiladiDateChange, false);
        miladiDaySelector.attachEvent('onchange', onMiladiDateChange, false);
        miladiMonthSelector.attachEvent('onchange', onMiladiDateChange, false);
        miladiYearSelector.attachEvent('onchange', onMiladiDateChange, false);
        hijriDateCorrectionSelector.addEventListener('change', onMiladiDateChange, false);

    }

    editMiladiDateCheckBox.addEventListener('change', onMiladiEditEnableChange, false)

}

getDate();
function getDate()
{
    getRequest(
        'emosquescreen/other_components/setting/getDay.php', // URL for the PHP file
         displayPanelDate,  // handle successful request
         errorInDateRequest    // handle error
    );
    return false;
}
// handles drawing an error message
function errorInDateRequest()
{
    console.log("error in day request");
}
// handles the response, adds the html
function displayPanelDate(responseText) {
    document.getElementById("currentMiladiDate").innerHTML += " : " + responseText;

    var year = parseInt(responseText.substr(6,4));
    var month = parseInt(responseText.substr(3, 2));
    var day = parseInt(responseText.substr(0, 2));

    var date = new Date(year, month - 1, day);
    var hijriDate = writeIslamicDate(hijriDateCorrectionSelector.value, date);

    if(document.title == "Panel Settings")
    {
        day = daysNames[parseInt(date.getDay())];

    }else
    {
        day = daysNamesArabic[parseInt(date.getDay())];

    }

    var dayNo = hijriDate[5];
    var year = hijriDate[7];
    var month = hijriDate[6];

    hijriDay = day.toString();

    if(document.title == "Panel Settings")
    {
        hijriMonth = iMonthNamesEnglish[month];

    }else
    {
        hijriMonth = iMonthNames[month];

    }

    dayNo = dayNo.toString();


    if (dayNo.length == 1)
    {
        if(document.title == "Panel Settings")
        {
            fullHijriDate = hijriDay + " , " + (dayNo) + " " + hijriMonth;

        }else
        {
            fullHijriDate = hijriDay + " ، " + (dayNo) + " " + hijriMonth;

        }
        //hijriDayNo = replaceNumbers(dayNo);
    }
    if (dayNo.length == 2)
    {
        if(document.title == "Panel Settings")
        {
            fullHijriDate = hijriDay + " , " + (dayNo[0]) + (dayNo[1]) + " " + hijriMonth;

        }else
        {
            fullHijriDate = hijriDay + " ، " + replaceNumbers(dayNo[0]) + replaceNumbers(dayNo[1]) + " " + hijriMonth;

        }
        //hijriDayNo = replaceNumbers(dayNo[0]) + replaceNumbers(dayNo[1]);
    }

    if(document.title == "Panel Settings")
    {
        currentHijriDateText.innerHTML = "Current Hijri Date: " + fullHijriDate;

    }else
    {
        currentHijriDateText.innerHTML = "التاريخ الهجري الحالي : " + fullHijriDate;

    }

}
// helper function for cross-browser request object
function getRequest(url, success, error)
{
    var req = false;
    try
    {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e)
    {
        // IE
        try
        {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e)
        {
            // try an older version
            try
            {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e)
            {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () { };
    if (typeof error != 'function') error = function () { };
    req.onreadystatechange = function ()
    {
        if (req.readyState == 4)
        {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}



setInterval(getTime, 1000);
function getTime()
{
    getRequest(
        'emosquescreen/other_components/setting/getTime.php', // URL for the PHP file
         displayPanelTime,  // handle successful request
         errorInTimeRequest    // handle error
    );
    return false;
}
// handles drawing an error message
function errorInTimeRequest()
{
    console.log("error in day request");
}
// handles the response, adds the html
function displayPanelTime(responseText)
{
    if(document.title == "Panel Settings")
    {
        document.getElementById("currentTime").innerHTML = " Current Panel Time: "  + responseText;

    }else
    {
        document.getElementById("currentTime").innerHTML = "  وقت اللوحة الحالي:  "  + responseText;
    }
}
// helper function for cross-browser request object
