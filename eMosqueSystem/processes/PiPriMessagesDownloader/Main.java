import java.io.File;
import java.io.IOException;

import org.json.JSONObject;

public class Main {
	public static final String ISDEV_PATH = "/home/pi/.eMosqueSystem/.isdev";
	public static final boolean ISDEV = HelpingFunctions.fileExists(ISDEV_PATH);

	// Server Links
	public static final String BASE_URL = "http://bahbishco.cloudapp.net/" + (ISDEV ? "cpanel_dev" : "cpanel")
			+ "/index.php/";
	public static final String AZURE_LIVESTREAMING_STATUS = BASE_URL + "panels/getVideoBroadcasting";
	public static final String AZURE_MESSAGE_VERSION = BASE_URL + "pri_msgs/getCentralMsgVersion";
	private static String AZURE_MESSAGE_ZIP = BASE_URL + "pri_msgs/packMessagesFor/";

	// Bash Scripts
	public static String SCRIPTS_LOCATION = "sudo /home/pi/.eMosqueSystem/processes/PiPriMessagesDownloader/scripts/";
	// Git scripts
	public static String GIT_INIT_SCRIPT = SCRIPTS_LOCATION + "init.sh";
	public static String GIT_PULLING_FOR_UPDATES = SCRIPTS_LOCATION + "polling_for_updates.sh";
	public static String GIT_REBOOT = SCRIPTS_LOCATION + "reboot.sh";
	// Messages Scripts
	public static String MESSAGES_CLEAR_PREV_MESSAGES = SCRIPTS_LOCATION + "clear_prev_update.sh";
	public static String MESSAGES_INSTALL_MESSAGES = SCRIPTS_LOCATION + "install_content_update.sh";
	// LiveStreaming Scripts
	public static final String LIVE_STREAMING_START_SCRIPT = Main.SCRIPTS_LOCATION + "start.sh";
	public static final String LIVE_STREAMING_STOP_SCRIPT = Main.SCRIPTS_LOCATION + "stop.sh";

	// Locations/Files in the system
	public static final String ETH0_STATUS = "/sys/class/net/eth0/operstate";
	private static final String MAC_ADDRESS_LOCATION = "/sys/class/net/eth0/address";

	public static final String MESSAGES_VERSION_FILE_LOCATION = "/var/www/emosquescreen/other_components/version/content.txt";
	private static final String MESSAGES_UPDATE_FILE = "/home/pi/updates/emosquescreen.zip";
	private static final String REBOOT_INDICATTION = "/etc/updateAvailable";
	public static String ISLIVESTREAMING_FLAG_PATH = "/home/pi/isStreaming";
	public static String ingestFilePath = "/home/pi/ingestURL";
	// TimeSpacing between iterations
	public static final int TIME_BETWEEN_ITERATIONS_SECONDS = 5;

	// Country and City
	public static String countryID = null;
	public static String cityID = null;

	public static void main(String[] args) {
		OP0ReadCountyAndCityIDs();

		// INITIALIZATION
		System.out.println("Executing initialization");
		HelpingFunctions.execCmd(GIT_INIT_SCRIPT);
		System.out.println("Executed initialization");
		while (true) {
			System.out.println("=================================");
			System.out.println("Start of iteration");
			boolean isWifiRouter = false;
			try {
				isWifiRouter = (HelpingFunctions.isTempSensorConnected() && HelpingFunctions.isValidIP((byte) (1)));
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			if (HelpingFunctions.twoStringsAreLogicallyEqual(HelpingFunctions.readFile(ETH0_STATUS), "up")
					|| isWifiRouter) {
				System.out.println("Ethernet is connected");
				if (cityID != null && countryID != null) {
					System.out.println("** ---- OP #1: Messages Updates -----");
					OP1MessagesUpdate();
					System.out.println("** ---- OP #1 is over -----");
				}

				System.out.println("** ---- OP #2: System Update -----");
				OP2SystemUpdate();
				System.out.println("** ---- OP #2  -----");

				System.out.println("** ---- OP #3: Checking live Streaming Signal -----");
				OP3HandleLiveStreaming();
				System.out.println("** ---- OP #3 is over -----");
			} else {
				System.out.println("Ethernet is not connected, hence, Skipping this iteration.");
			}
			System.out.println("End of iteration");
			System.out.println("=================================");
			System.out.println("Next Iteration starts in " + TIME_BETWEEN_ITERATIONS_SECONDS + " S");
			try {
				Thread.sleep(TIME_BETWEEN_ITERATIONS_SECONDS * 1000);
			} catch (InterruptedException e) {
			}
			if (HelpingFunctions.fileExists(REBOOT_INDICATTION)) {
				System.out.println("Will reboot now!");
				HelpingFunctions.execCmd(GIT_REBOOT);
			} else {
				System.out.println("Will not reboot.");
			}
		}
	}

	private static void OP3HandleLiveStreaming() {
		try {
			System.out.println("Will get live streaming status from server.");
			String ServerWordOnLiveStreaming = HelpingFunctions.executePost(AZURE_LIVESTREAMING_STATUS,
					"bssid=" + HelpingFunctions.readFile(MAC_ADDRESS_LOCATION).trim());

			if (ServerWordOnLiveStreaming != null) {
				System.out.println("ServerWordOnLiveStreaming: " + ServerWordOnLiveStreaming);
				JSONObject resObj = new JSONObject(ServerWordOnLiveStreaming);
				String status = resObj.optString("status", "0");
				String ingestURL = resObj.optString("ingestURL", "");

				if (HelpingFunctions.twoStringsAreLogicallyEqual(status, "1")) {
					if (!HelpingFunctions.LiveStreamingHandle.isCurrentlyLiveStreaming()) {
						System.out.println("Live streaming is not currently active. Will turn it on now!");
						HelpingFunctions.writeFile(Main.ingestFilePath, ingestURL);
						HelpingFunctions.LiveStreamingHandle.startLiveStreaming();
						System.out.println("New Status: Live streaming is now "
								+ (HelpingFunctions.LiveStreamingHandle.isCurrentlyLiveStreaming() ? "on" : "off"));
					} else {
						System.out.println("Live streaming is already on, no need to turn it on.");
					}
				} else if (HelpingFunctions.twoStringsAreLogicallyEqual(status, "0")) {
					if (!HelpingFunctions.LiveStreamingHandle.isCurrentlyLiveStreaming()) {
						System.out.println("Live streaming is not currently active. Will do nothing");
					} else {
						System.out.println("Turning off live streaming");
						HelpingFunctions.writeFile(Main.ingestFilePath, "");
						HelpingFunctions.LiveStreamingHandle.stopLiveStreaming();
						System.out.println("New Status: Live streaming is now "
								+ (HelpingFunctions.LiveStreamingHandle.isCurrentlyLiveStreaming() ? "on" : "off"));
					}
				}

			} else {

				System.out.println("Non zero nor one Server word on live streaming, A problem occurred!");
				System.out.println("New Status: Live streaming is now "
						+ (HelpingFunctions.LiveStreamingHandle.isCurrentlyLiveStreaming() ? "on" : "off"));

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void OP0ReadCountyAndCityIDs() {
		// TODO fetch city and country ids
		String panelSetting = HelpingFunctions.readFile(Constants.Locations.SettingFile);
		String[] lines;
		if (panelSetting != null) {
			lines = panelSetting.split("\n");
			String currentLine;
			String[] temp;
			for (int i = 0; i < lines.length; i++) {
				currentLine = lines[i];
				if (currentLine.contains("country")) {
					temp = currentLine.split(":");
					if (temp.length == 2) {
						temp[1] = temp[1].replace(" ", "");
						temp[1] = temp[1].replace("\"", "");
						temp[1] = temp[1].replace(",", "");
						countryID = temp[1].trim();
					}
				}

				if (currentLine.contains("city")) {
					temp = currentLine.split(":");
					if (temp.length == 2) {
						temp[1] = temp[1].replace(" ", "");
						temp[1] = temp[1].replace("\"", "");
						temp[1] = temp[1].replace(",", "");
						cityID = temp[1].trim();
					}
				}

			}
		}
		// countryID = "1";
		// cityID = "12";
	}

	private static void OP2SystemUpdate() {
		System.out.println("Will try to fetch new System updates now.");
		HelpingFunctions.execCmd(GIT_PULLING_FOR_UPDATES);
		System.out.println("Finished pulling, will reboot if new update is present");
	}

	private static void OP1MessagesUpdate() {
		String onlineVersion = HelpingFunctions
				.downloadString(AZURE_MESSAGE_VERSION + "?countryID=" + countryID + "&cityID=" + cityID);
		System.out.println("onlineVersion: " + onlineVersion + ", countryID:" + countryID + ", cityID=" + cityID);
		// checking for messages
		if (!HelpingFunctions.stringISEmpty(onlineVersion)) {
			if (!HelpingFunctions.twoStringsAreLogicallyEqual(onlineVersion,
					HelpingFunctions.readFile(MESSAGES_VERSION_FILE_LOCATION))) {
				System.out.println("Clearing Previous Messages");
				HelpingFunctions.execCmd(MESSAGES_CLEAR_PREV_MESSAGES);
				System.out.println("Making sure Update Directory exists, /home/pi/updates");
				HelpingFunctions.makeSureDirExists("/home/pi/updates");
				System.out.println("Removing previous update file.");
				HelpingFunctions.execCmd("sudo rm " + MESSAGES_UPDATE_FILE);
				System.out.println("Downloading Update..");
				boolean downloadedUpdateCorrectly = false;
				if (HelpingFunctions.downloadFile(AZURE_MESSAGE_ZIP + countryID + "/" + cityID + "/1",
						MESSAGES_UPDATE_FILE))
					downloadedUpdateCorrectly = HelpingFunctions.fileExists(MESSAGES_UPDATE_FILE);
				System.out.println("Update download is " + (downloadedUpdateCorrectly ? "" : "not") + "Successful");
				if (downloadedUpdateCorrectly) {
					System.out.println("Extracting and deploying update now.");
					HelpingFunctions.execCmd(MESSAGES_INSTALL_MESSAGES);
					System.out.println("Updating Messages version now.");
					HelpingFunctions.writeFile("/var/www/emosquescreen/other_components/version/content.txt",
							onlineVersion.trim());
				} else {
					System.out.println("Will try to download the update next iteration.");
				}
			} else {
				System.out.println("No new messages to fetch!");
			}
		} else {
			System.out.println("No internet connection, hence, cannot fetch messages.");
		}
	}
}
