

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;



public class CMDHelper {
	public static Runtime runTime = Runtime.getRuntime();

	public static Process execCmd(String cmd) {
		Process proc = null;
		try {
			proc = runTime.exec(cmd);
			proc.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return proc;
		}

		return proc;
	}

	public static Process execCmdWithoutBlockExec(String cmd) {
		Process proc = null;
		try {
			proc = runTime.exec(cmd);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return proc;
	}

	public static Process execCmdWithoutBlockExec(String[] cmd) {
		Process proc = null;
		try {
			String[] processCommand = cmd;
			proc = runTime.exec(processCommand);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return proc;
	}

	public static Process createProcess(String[] cmd) {
		Process proc = null;
		try {
			proc = new ProcessBuilder(cmd).start();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return proc;
	}

	public static String readProcessOutput(Process proc) {
		StringBuffer output = new StringBuffer();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		proc.destroyForcibly();

		if (output.length() != 0)
			return output.toString().substring(0, output.length() - 1);
		else
			return "";
	}

}
