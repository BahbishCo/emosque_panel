import java.io.*;
import java.util.Scanner;

public class setDefaultValues {

	static String[] longitudeLineArray;
	static String[] latitudeLineArray;
	static String[] countryLineArray;
	static String[] cityLineArray;
	static String[] timezoneLineArray;
    static String nextLine;
	static double longitude;
	static double latitude;
	static String country;
	static String city;
	static String timezone;
	static String elevation;
	static String prayerCalcMethod;
	static String asrJuristic;
	static String adjustHighLats;
	static String midnightMethod;
	static String ishaaAngle;
	static String fajrAngle;
	
	static StringBuilder sb = new StringBuilder();
	static File settingsFile = new File("/var/www/emosquescreen/web-site/customFiles/settings/setting.js");
	static File defaultValuesFile = new File("/var/www/emosquescreen/other_components/setting/panelInfo/defaultValues.txt");
	
	static Runtime rt = Runtime.getRuntime();
	static String killMidoriCommand = "sudo pkill midori";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scanner = null;

		try
		{
			scanner = new Scanner(defaultValuesFile);

			while (scanner.hasNextLine())
			{
				nextLine = scanner.nextLine();

				if (nextLine.contains("longitude"))
				{
					longitudeLineArray = nextLine.split(" ", -1);
					nextLine = longitudeLineArray[1].substring(0, longitudeLineArray[1].length());
					longitude = Double.parseDouble(nextLine);
				}
				if (nextLine.contains("latitude"))
				{
					latitudeLineArray = nextLine.split(" ", -1);
					nextLine = latitudeLineArray[1].substring(0, latitudeLineArray[1].length());
					latitude = Double.parseDouble(nextLine);
				}
				if (nextLine.contains("country"))
				{
					countryLineArray = nextLine.split(" ", -1);
					nextLine = countryLineArray[1].substring(0, countryLineArray[1].length());
					country = nextLine;
				}
				if (nextLine.contains("city"))
				{
					cityLineArray = nextLine.split(" ", -1);
					nextLine = cityLineArray[1].substring(0, cityLineArray[1].length());
					city = nextLine;
				}
				if (nextLine.contains("timezone"))
				{
					timezoneLineArray = nextLine.split(" ", -1);
					nextLine = timezoneLineArray[1].substring(0, timezoneLineArray[1].length());
					double temp = Double.parseDouble(nextLine);
					if(temp>=0)
					timezone = "+"+nextLine;
					else
					timezone = nextLine;
				}
			}
			
			
			if (country.equals("Jordan")) 
			{
				switch (city) {
				case "Amman":
					elevation = "1100";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Irbid":
					elevation = "623";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Ajloun":
					elevation = "975";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Karak":
					elevation = "1100";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Mafrag":
					elevation = "700";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Jarash":
					elevation = "975";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Balqaa":
					elevation = "1071";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Zarqa":
					elevation = "886";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Tafelih":
					elevation = "1257";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Aqaba":
					elevation = "150";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				case "Maan":
					elevation = "1140";
					prayerCalcMethod = "Karachi";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "18";
					fajrAngle = "18";
					break;
				default:
					break;
				}
			}
			else if (country.equals("Qatar"))
			{
				switch (city) {
				case "Duha":
					elevation = "40";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "Alrayyan":
					elevation = "40";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "Alwakrah":
					elevation = "10";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "UmSiad":
					elevation = "40";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "KhorAlUdaid":
					elevation = "40";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "AlKhor":
					elevation = "10";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "RasLaffan":
					elevation = "10";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "Mesaieed":
					elevation = "10";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "AlShamal":
					elevation = "10";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "AlRuwais":
					elevation = "10";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				case "Dukhan":
					elevation = "20";
					prayerCalcMethod = "Makkah";
					asrJuristic = "Standard";
					adjustHighLats = "AngleBased";
					midnightMethod = "Standard";
					ishaaAngle = "90 min";
					fajrAngle = "18.5";
					break;
				default:
					break;
				}
			}
					sb.append("var screenSetting =\n{\n");
					sb.append("prayerSetting:\n{\n");
					sb.append("country: \"" + country+"\",\n");
					sb.append("city: \"" + city+"\",\n");
					sb.append("calculationMethod: \"" + prayerCalcMethod+"\",\n");
					sb.append("asrJuristic: \"" + asrJuristic+"\",\n");
					sb.append("adjusthighLat: \"" + adjustHighLats+"\",\n");
					sb.append("midNightMethod: \"" + midnightMethod+"\",\n");
					sb.append("fajrAngle: " + fajrAngle+",\n");
					if(country.equals("Qatar"))
					sb.append("ishaAngle: \"" + ishaaAngle+"\",\n");
					else
					sb.append("ishaAngle: " + ishaaAngle+",\n");
					sb.append("latitude: " + latitude+",\n");
					sb.append("longitude: " + longitude+",\n");
					sb.append("elevation: " + elevation+",\n");
					sb.append("timeZone: " + timezone+",\n");
					sb.append("offset: {fajrOffset: 0, duhurOffset: 0, asrOffset: 0, maghribOffset: 0, ishaaOffset: 0, },\n");
					sb.append("},\n");
					sb.append("hijriCorrection: 0,\n");
					sb.append("timeBetweenAzanAndIqama: {fajr: 30, duhur: 18, asr: 18, maghrib: 10, ishaa: 23, jumaa: 50, },\n");
					sb.append("timeBetweenAzanAndAzkar: {fajr: 36, duhur: 26, asr: 26, maghrib: 17, ishaa: 32, jumaa: 50, },\n");
					sb.append("azkarDisplayPeriod: {fajr: 10, duhur: 10, asr: 10, maghrib: 10, ishaa: 10, jumaa: 10, },\n");
					sb.append("appQREnable: false,\n");
					sb.append("internalTempEnable: true,\n");
					sb.append("playAzan: false,\n");
					sb.append("showAnalogClock: true,\n");
					sb.append("showBahbishMessage: false,\n}");
			FileWriter fstreamWrite = new FileWriter(settingsFile);
			BufferedWriter out = new BufferedWriter(fstreamWrite);
			out.write(sb.toString());
			out.flush();
			out.close();
			rt.exec(killMidoriCommand);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
