<?php
/**
 * Created by PhpStorm.
 * User: Salah
 * Date: 3/15/2016
 * Time: 4:15 PM
 */
$date = $_GET["date"];
$all = $_GET["all"];
$today = $_GET["today"];

if ($all == "true") {
    $files = glob(dirname(dirname(dirname(getcwd()))) . "/web-site/customFiles/contents/primaryMessages/ImamMessages/*"); // get all file names
    $dir = dirname(dirname(dirname(getcwd()))) . "/web-site/customFiles/contents/primaryMessages/ImamMessages";

    if (is_dir_empty($dir)) {
        echo "لا يوجد أي رسائل ليتم حذفها";
    } else {
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
        }
        exec("sudo pkill midori");
        exec("sudo pkill iceweasel");
    }

    $files = glob(dirname(dirname(dirname(getcwd()))) . "/web-site/customFiles/contents/primaryMessages/ImamMessages/imgs/*"); // get all file names
    if (is_dir_empty($dir)) {
    } else {
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
        }
    }


} else if ($all == "false") {
    $filename = dirname(dirname(dirname(getcwd()))) . "/web-site/customFiles/contents/secondaryMessages/ImamMessages/" . $date . ".html";
    if (file_exists($filename)) {
        unlink($filename);
        if($today=="true")
        {
            exec("sudo pkill midori");
            exec("sudo pkill iceweasel");
        }
    }
}

function is_dir_empty($dir)
{
    if (!is_readable($dir)) return NULL;
    $handle = opendir($dir);
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            return FALSE;
        }
    }
    return TRUE;
}

?>