/**
 * Created by Ahmad on 8/2/2016.
 */

$(document).ready(function () {
    initSel();
});
function saveNewCss() {

    var colorSelChx = document.getElementById("colorSelChx");
    var themeSelChx = document.getElementById("themeSelChx");

    if (colorSelChx.checked == false && themeSelChx.checked == false) {
        //alert("يرجى اختيار احدى الخيارات");
        return;
    }

    if (colorSelChx.checked) {
        var backgroundColorSel = document.getElementById("backgroundColorSel");
        var mainTextColorSel = document.getElementById("mainTextColorSel");
        var headerElColorSel = document.getElementById("headerElColorSel");
        var timeoutColorSel = document.getElementById("prayerTimeoutColorSel");
        var prayerTimesColorSel = document.getElementById("prayerTimesColorSel");
        var prayerNameColorSel = document.getElementById("prayerNamesColorSel");
        var internalTempColorSel = document.getElementById("tempColorSel");
        var hadeethColorSel = document.getElementById("hadeethColorSel");
        var azkarColorSel = document.getElementById("azkarColorSel");
        var secondaryColorSel = document.getElementById("secondaryMsgColorSel");
        var primaryColorSel = document.getElementById("primaryMsgColorSel");
        var silentAppAdColorSel = document.getElementById("silentAppAdColor");

        $.post("php/saveCSS.php",
            {
                backgroundType: "0",
                backgroundColor: backgroundColorSel.value,
                mainTextColor: mainTextColorSel.value,
                headerElColor: headerElColorSel.value,
                timeoutElColor: timeoutColorSel.value,
                prayerTimesColor: prayerTimesColorSel.value,
                prayerNamesColor: prayerNameColorSel.value,
                internalTempColor: internalTempColorSel.value,
                hadeethColor: hadeethColorSel.value,
                azkarColor: azkarColorSel.value,
                primaryMsgColor: secondaryColorSel.value,
                secondaryMsgColor: primaryColorSel.value,
                silentAppAdColor: silentAppAdColorSel.value,
            },
            function (data, status) {
                console.log(data);
                window.location = window.location;
            });
    }
    else if (themeSelChx.checked) {
        console.log("theme opetion");
        var sunsetTheme = document.getElementById("sunSetTheme");
        var nightTheme = document.getElementById("nightTheme");
        if (sunsetTheme.checked == false && nightTheme.checked == false) {
            //alert("يرجى اختيار احدى الاشكال");
            return;
        }
        if (sunsetTheme.checked) {
            var themeID = 1;
        }
        else if (nightTheme.checked) {
            var themeID = 2;
        }

        $.post("php/saveCSS.php",
            {
                backgroundType: "1",
                themeID: themeID
            },
            function (data, status) {
                console.log(data);
                window.location = window.location;
            });
    }

}


function setDefaultValues() {
    $.post("php/saveCSS.php",
        {
            backgroundType: "0",
            backgroundColor: "#000000",
            mainTextColor: "#FFFFFF",
            headerElColor: "#FFFFFF",
            timeoutElColor: "#FFFFFF",
            prayerTimesColor: "#FFFFFF",
            prayerNamesColor: "#FFFFFF",
            internalTempColor: "#FF0000",
            hadeethColor: "#FFFFFF",
            azkarColor: "#FFFFFF",
            primaryMsgColor: "#FFFFFF",
            secondaryMsgColor: "#FFFFFF",
            silentAppAdColor: "#FFFFFF",
        },
        function (data, status) {
            window.location = window.location;
        });
}

function initSel() {

    var colorSelChx = document.getElementById("colorSelChx");
    var themeSelChx = document.getElementById("themeSelChx");
    var colorSelRow = document.getElementById("colorSelectionRow");
    var themeSelRow = document.getElementById("themeSelectionRow");

    colorSelChx.checked = false;
    themeSelChx.checked = false;
    colorSelRow.style.display = "none";
    themeSelRow.style.display = "none";

}

function initColor() {
    var backgroundColorSel = document.getElementById("backgroundColorSel");
    var mainTextColorSel = document.getElementById("mainTextColorSel");
    var headerElColorSel = document.getElementById("headerElColorSel");
    var timeoutColorSel = document.getElementById("prayerTimeoutColorSel");
    var prayerTimesColorSel = document.getElementById("prayerTimesColorSel");
    var prayerNameColorSel = document.getElementById("prayerNamesColorSel");
    var internalTempColorSel = document.getElementById("tempColorSel");
    var hadeethColorSel = document.getElementById("hadeethColorSel");
    var azkarColorSel = document.getElementById("azkarColorSel");
    var secondaryColorSel = document.getElementById("secondaryMsgColorSel");
    var primaryColorSel = document.getElementById("primaryMsgColorSel");
    var silentAppAdColorSel = document.getElementById("silentAppAdColor");

    backgroundColorSel.value = "#000000";
    mainTextColorSel.value = "#FFFFFF";
    headerElColorSel.value = "#FFFFFF";
    timeoutColorSel.value = "#FFFFFF";
    prayerTimesColorSel.value = "#FFFFFF";
    prayerNameColorSel.value = "#FFFFFF";
    internalTempColorSel.value = "#FF0000";
    hadeethColorSel.value = "#FFFFFF";
    azkarColorSel.value = "#FFFFFF";
    secondaryColorSel.value = "#FFFFFF";
    primaryColorSel.value = "#FFFFFF";
    silentAppAdColorSel.value = "#FFFFFF";


    backgroundColorSel.value = backgroundColor;
    mainTextColorSel.value = mainTextColor;
    headerElColorSel.value = headerElColor;
    timeoutColorSel.value = timeoutColor;
    prayerTimesColorSel.value = prayerTimesColor;
    prayerNameColorSel.value = prayerNameColor;
    internalTempColorSel.value = internalTempColor;
    hadeethColorSel.value = hadeethColor;
    azkarColorSel.value = azkarColor;
    secondaryColorSel.value = secondaryColor;
    primaryColorSel.value = primaryColor;
    silentAppAdColorSel.value = silentAppAdColor;
}

function chxCallback(chx) {
    var colorSelChx = document.getElementById("colorSelChx");
    var themeSelChx = document.getElementById("themeSelChx");
    var colorSelRow = document.getElementById("colorSelectionRow");
    var themeSelRow = document.getElementById("themeSelectionRow");
    if (colorSelChx == chx) {
        colorSelChx.checked = true;
        themeSelChx.checked = false;
        colorSelRow.style.display = "block";
        themeSelRow.style.display = "none";
        initColor();
    }
    else if (themeSelChx == chx) {
        themeSelChx.checked = true;
        colorSelChx.checked = false;
        colorSelRow.style.display = "none";
        themeSelRow.style.display = "block";
    }
}

function switchTheme(chx) {
    var sunsetTheme = document.getElementById("sunSetTheme");
    var nightTheme = document.getElementById("nightTheme");

    if (chx == sunsetTheme) {
        nightTheme.checked = false;
    }
    else if (chx == nightTheme) {
        sunsetTheme.checked = false;
    }
}