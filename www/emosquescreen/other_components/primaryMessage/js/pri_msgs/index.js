$(document).ready(function () {
    initDateSelector();
    initEditor();
    var previewFrame = document.getElementById("insertMessagePreviewFrame");
    $(previewFrame).ready(updateMessagePreview);
    var msgDst = $("#msg_dst").val();
    setInterval(setImagesCss, 1000);
});
var screenHeight = screen.height;

var cities = {};

var screenBase = $("#screenBase").val();
var updatePreviewActive = true;

var activeFrame = '';
var activeEditor = '';
var activeMessageID = -1;

var count = 0;


function initEditor() {
    // $('#insertMessageEditorDiv').height(screenHeight/2+"px");
    width = $('#insertMessageEditor').width();
    $('#insertMessageEditor').height(width * 9 / 16 + "px");
    $('#insertMessagePreviewFrame').height(width * 9 / 16 + "px");

    new nicEditor({
        fullPanel: false,
        unselectable: 'off',
        iconsPath: 'imgs/nicEditorIcons.gif',
        buttonList: ['bold', 'italic', 'underline', 'hr', 'ol', 'ul', 'upload'],
        maxHeight: screenHeight / 2,
        height: screenHeight / 2
    }).panelInstance('insertMessageEditor');
}
function initDateSelector() {
    $(function () {
        $("#insertMessage_startDatePicker").datepicker({
            minDate: new Date(),
            maxDate: '+2Y',
            dateFormat: "yy-mm-dd",
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });
        $("#insertMessage_endDatePicker").datepicker({
            minDate: new Date(),
            maxDate: '+2Y',
            dateFormat: "yy-mm-dd",
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });

        $("#previewEditMessage_startDatePicker").datepicker({
            maxDate: '+2Y',
            dateFormat: "yy-mm-dd",
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });

        $("#previewEditMessage_endDatePicker").datepicker({
            maxDate: '+2Y',
            dateFormat: "yy-mm-dd",
            onSelect: function (d, i) {
                if (d !== i.lastVal) {
                    $(this).change();
                }
            }
        });

    });

    $('#insertMessage_startDatePicker').change(function () {
        var errorID = "insertMessageDateErrorLabel";
        var errorLabel = document.getElementById("insertMessageDateErrorLabel");
        var startDateSelector = document.getElementById("insertMessage_startDatePicker");
        var endDateSelector = document.getElementById("insertMessage_endDatePicker");
        var isDate = Date.parseExact(startDateSelector.value, "yyyy-MM-dd");
        if (isDate == null) {
            errorLabel.innerHTML = "يجب ادخال تاريخ البداية بشكل صحيح";
            $('#' + errorID).removeClass('display-none');
            if ($('#' + errorID).hasClass('display-block')) {
                $('#' + errorID).removeClass('display-block');
                $('#' + errorID).addClass('display-none');
                setTimeout(function () {
                    $('#' + errorID).addClass('display-block');
                    startDateSelector.value = "";
                }, 500);
            }
            else {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
            }
            return;
        }

        $('#' + errorID).removeClass('display-block');
        $('#' + errorID).addClass('display-none');

        if (endDateSelector.value == "") {
            endDateSelector.value = startDateSelector.value;
        }

        validateDate("insertMessage_startDatePicker", "insertMessage_endDatePicker", "insertMessageDateErrorLabel");

    });


    $('#insertMessage_endDatePicker').change(function () {
        var errorID = "insertMessageDateErrorLabel";
        var errorLabel = document.getElementById("insertMessageDateErrorLabel");
        var endDateSelector = document.getElementById("insertMessage_endDatePicker");
        var startDateSelector = document.getElementById("insertMessage_startDatePicker");
        isDate = Date.parseExact(endDateSelector.value, "yyyy-MM-dd");
        if (isDate == null) {

            errorLabel.innerHTML = "يجب ادخال تاريخ النهاية بشكل صحيح";
            $('#' + errorID).removeClass('display-none');
            if ($('#' + errorID).hasClass('display-block')) {
                $('#' + errorID).removeClass('display-block');
                $('#' + errorID).addClass('display-none');
                setTimeout(function () {
                    $('#' + errorID).addClass('display-block');
                    endDateSelector.value = "";
                }, 500);
            }
            else {
                $('#' + errorID).addClass('display-block');
                endDateSelector.value = "";
            }

            return;
        }

        $('#' + errorID).removeClass('display-block');
        $('#' + errorID).addClass('display-none');

        if (startDateSelector.value != "")
            validateDate("insertMessage_startDatePicker", "insertMessage_endDatePicker", "insertMessageDateErrorLabel");
    });


    $('#previewEditMessage_startDatePicker').change(function () {
        killEditPreviewMessageTab();
        var errorID = "previewMessageDateErrorLabel";
        var errorLabel = document.getElementById("previewMessageDateErrorLabel");
        var startDateSelector = document.getElementById("previewEditMessage_startDatePicker");
        var endDateSelector = document.getElementById("previewEditMessage_endDatePicker");
        var isDate = Date.parseExact(startDateSelector.value, "yyyy-MM-dd");
        if (isDate == null) {
            errorLabel.innerHTML = "يجب ادخال تاريخ البداية بشكل صحيح";
            $('#' + errorID).removeClass('display-none');
            if ($('#' + errorID).hasClass('display-block')) {
                $('#' + errorID).removeClass('display-block');
                $('#' + errorID).addClass('display-none');
                setTimeout(function () {
                    $('#' + errorID).addClass('display-block');
                    startDateSelector.value = "";
                }, 500);
            }
            else {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
            }
            return;
        }

        $('#' + errorID).removeClass('display-block');
        $('#' + errorID).addClass('display-none');

        if (endDateSelector.value == "") {
            endDateSelector.value = startDateSelector.value;
        }

        validateDate("previewEditMessage_startDatePicker", "previewEditMessage_endDatePicker", "previewMessageDateErrorLabel");

    });

    $('#previewEditMessage_endDatePicker').change(function () {
        killEditPreviewMessageTab();
        var errorID = "previewMessageDateErrorLabel";
        var errorLabel = document.getElementById("previewMessageDateErrorLabel");
        var startDateSelector = document.getElementById("previewEditMessage_startDatePicker");
        var endDateSelector = document.getElementById("previewEditMessage_endDatePicker");
        var isDate = Date.parseExact(endDateSelector.value, "yyyy-MM-dd");
        if (isDate == null) {
            errorLabel.innerHTML = "يجب ادخال تاريخ النهاية بشكل صحيح";
            $('#' + errorID).removeClass('display-none');
            if ($('#' + errorID).hasClass('display-block')) {
                $('#' + errorID).removeClass('display-block');
                $('#' + errorID).addClass('display-none');
                setTimeout(function () {
                    $('#' + errorID).addClass('display-block');
                    endDateSelector.value = "";
                }, 500);
            }
            else {
                $('#' + errorID).addClass('display-block');
                endDateSelector.value = "";
            }
            return;
        }

        $('#' + errorID).removeClass('display-block');
        $('#' + errorID).addClass('display-none');

        if (endDateSelector.value == "") {
            endDateSelector.value = startDateSelector.value;
        }

        validateDate("previewEditMessage_startDatePicker", "previewEditMessage_endDatePicker", "previewMessageDateErrorLabel");

    });
}


function validateDate(startDateID, endDateID, errorID) {
    var startDateSelector = document.getElementById(startDateID);
    var endDateSelector = document.getElementById(endDateID);
    var errorLabel = document.getElementById(errorID);


    var isDate = Date.parseExact(startDateSelector.value, "yyyy-MM-dd");
    if (isDate == null) {
        errorLabel.innerHTML = "يجب ادخال تاريخ البداية بشكل صحيح";
        $('#' + errorID).removeClass('display-none');
        if ($('#' + errorID).hasClass('display-block')) {
            $('#' + errorID).removeClass('display-block');
            $('#' + errorID).addClass('display-none');
            setTimeout(function () {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
            }, 500);
        }
        else {
            $('#' + errorID).addClass('display-block');
        }
        return false;
    }

    isDate = Date.parseExact(endDateSelector.value, "yyyy-MM-dd");
    if (isDate == null) {
        errorLabel.innerHTML = "يجب ادخال تاريخ النهاية بشكل صحيح";
        $('#' + errorID).removeClass('display-none');
        if ($('#' + errorID).hasClass('display-block')) {
            $('#' + errorID).removeClass('display-block');
            $('#' + errorID).addClass('display-none');
            setTimeout(function () {
                $('#' + errorID).addClass('display-block');
                endDateSelector.value = "";
            }, 500);
        }
        else {
            $('#' + errorID).addClass('display-block');
        }
        return false;
    }

    var start = Date.parseExact(startDateSelector.value, "yyyy-MM-dd");
    var end = Date.parseExact(endDateSelector.value, "yyyy-MM-dd");


    if (end.getTime() < start.getTime()) {

        errorLabel.innerHTML = "تاريخ البداية والنهاية غير متوافقات";
        $('#' + errorID).removeClass('display-none');
        if ($('#' + errorID).hasClass('display-block')) {
            $('#' + errorID).removeClass('display-block');
            $('#' + errorID).addClass('display-none');
            setTimeout(function () {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
                endDateSelector.value = "";
            }, 500);
        }
        else {
            $('#' + errorID).addClass('display-block');
            startDateSelector.value = "";
            endDateSelector.value = "";
        }
        return false;
    }


    if (startDateSelector.value == "" || endDateSelector.value == "") {

        errorLabel.innerHTML = "يجب اختيار تاريخ البداية والنهاية";
        $('#' + errorID).removeClass('display-none');
        if ($('#' + errorID).hasClass('display-block')) {
            $('#' + errorID).removeClass('display-block');
            $('#' + errorID).addClass('display-none');
            setTimeout(function () {
                $('#' + errorID).addClass('display-block');
                startDateSelector.value = "";
                endDateSelector.value = "";
            }, 500);
        }
        else {
            $('#' + errorID).addClass('display-block');
            startDateSelector.value = "";
            endDateSelector.value = "";
        }
        return false;
    }
    return true;
}


function showMessageTableDiv() {
    $("#countrySelector_preview").parent().removeClass("has-error");
    $("#citySelector_preview").parent().removeClass("has-error");

    var dateValidation = validateDate("previewEditMessage_startDatePicker", "previewEditMessage_endDatePicker", "previewMessageDateErrorLabel");

    if ($("#citySelector_preview").val() != "" && $("#countrySelector_preview").val() != "" && dateValidation == true) {
        var cityID = cities[$("#citySelector_preview").val()];
        var dateFrom = $("#previewEditMessage_startDatePicker").val();
        var dateTo = $("#previewEditMessage_endDatePicker").val();

        $.ajax({
            url: "pri_msgs/listMessages",

            data: {
                cityID: cityID,
                dateFrom: dateFrom,
                dateTo: dateTo
            },

            method: "POST",

            complete: function (response, status, xhr) {
                var data = JSON.parse(response.responseText);
                console.log("data = " + data)
                var message = data.Message;
                var allMessages = data.AllMessages;
                console.log("allMessages = " + allMessages)
                if (status == 'success') {
                    $("#messagesTable").DataTable({
                        data: allMessages,
                        columns: [
                            {"data": "ID"},
                            {"data": "insertedBy"},
                            {"data": "insertionDate"},
                            {"data": "dateTo"},
                            {"data": "dateFrom"},
                            {"data": "cityName"}
                        ],
                        columnDefs: [
                            {
                                className: "text-center",
                                targets: [0],
                                data: null,
                                render: function (data) {
                                    return '<span onclick="setTimeout(showEditMessageDialog(' + data + '),500);">' +
                                        '<a href="#" data-toggle="tooltip" title="تعديل">' +
                                        '<span class="glyphicon glyphicon-edit"></span>' +
                                        '</a>' +
                                        '</span>' +
                                        '<span onclick="setTimeout(showViewMessageDialog(' + data + '),500);">' +
                                        '<a href="#" data-toggle="tooltip" title="عرض الرسالة">' +
                                        '<span class="glyphicon glyphicon-eye-open"></span>' +
                                        '</a>' +
                                        '</span>' +
                                        '<span onclick="setTimeout(removeMessageDialog(' + data + '),500);">' +
                                        '<a href="#" data-toggle="tooltip" title="حذف الرسالة">' +
                                        '<span class="glyphicon glyphicon-remove"></span>' +
                                        '</a>' +
                                        '</span>';
                                }
                            },
                            {
                                className: "text-center",
                                targets: [1, 2, 3, 4, 5],
                                data: null
                            }
                        ],
                        "destroy": true
                    });
                }
                else {
                    // $.each(validation, function(idx, field) {
                    //     if(field.fieldName == 'cityName')
                    //     {
                    //         $("#citySelector_preview").parent().addClass("has-error");
                    //         $("#citySelector_preview").focus();
                    //     }

                    //     if(field.fieldName == 'dateFrom')
                    //     {
                    //         $("#insertMessage_startDatePicker").parent().addClass("has-error");
                    //         $("#insertMessage_startDatePicker").focus();
                    //     }

                    //     if(field.fieldName == 'dateTo')
                    //     {
                    //         $("#insertMessage_endDatePicker").parent().addClass("has-error");
                    //         $("#insertMessage_endDatePicker").focus();
                    //     }
                    // });
                }
            }

        });

        var messagesTable = $('#messagesTableDiv');
        messagesTable.addClass(' display-block');
        messagesTable.removeClass(' display-none');
    }
    else {
        if ($("#citySelector_preview").val() == "") {
            $("#citySelector_preview").parent().addClass("has-error");
            $("#citySelector_preview").focus();
        }

        if ($("#countrySelector_preview").val() == "") {
            $("#countrySelector_preview").parent().addClass("has-error");
            $("#countrySelector_preview").focus();
        }

        return;
    }
}


function killEditPreviewMessageTab() {
    var messagesTable = $('#messagesTableDiv');
    messagesTable.removeClass(' display-block');
    messagesTable.addClass(' display-none');
}


function getParamValueByName(name, windowId) {
    if (windowId == null) {

        var query = window.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }
    else {
        var iframe = document.getElementById(windowId);
        var query = iframe.contentWindow.location.search.substring(1);
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split("=")[0];
            var paramValue = params[i].split("=")[1];
            if (name == paramName) {
                return paramValue;
            }
        }
        return null;
    }

}

function loadScript(url, callback) {

    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    script.onreadystatechange = callback;
    script.onload = callback;

    head.appendChild(script);
}


function getRequest(url, success, error) {
    var req = false;
    try {
        // most browsers
        req = new XMLHttpRequest();
    } catch (e) {
        // IE
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    if (!req) return false;
    if (typeof success != 'function') success = function () {
    };
    if (typeof error != 'function') error = function () {
    };
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            return req.status === 200 ?
                success(req.responseText) : error(req.status);
        }
    }
    req.open("GET", url, true);
    req.send(null);
    return req;
}

function saveNewMessage() {
    var dateFrom = $("#insertMessage_startDatePicker").val();
    var dateTo = $("#insertMessage_endDatePicker").val();

    if (!validateDate("insertMessage_startDatePicker", "insertMessage_endDatePicker", "insertMessageDateErrorLabel")) {
        window.scrollTo(0,0);
        return;
    }
    var messageBody = nicEditors.findEditor("insertMessageEditor").getContent();

    var wrapper = document.createElement('div');
    wrapper.innerHTML = messageBody;
    var imgs = wrapper.getElementsByTagName("img");

    for (var i = 0; i < imgs.length; i++) {
        imgs[i].removeAttribute("height");
    }


    $.ajax({
        url: "php/saveMsg.php",

        data: {
            dateFrom: dateFrom,
            dateTo: dateTo,
            msg: messageBody
        },

        method: "POST",

        complete: function (response, status, xhr) {
            console.log("data = " + response.responseText);
            var data = JSON.parse(response.responseText);
            var message = data.Message;

            alert(message);

        }

    });
}

function updateMessage(messageID) {
    if (messageID == -1) {
        return false;
    }

    var messageBody = nicEditors.findEditor("editMessageEditor").getContent();

    $.ajax({
        url: "pri_msgs/updateMessage/" + messageID,

        data: {
            messageBody: messageBody
        },

        method: "POST",

        complete: function (response, status, xhr) {
            var data = JSON.parse(response.responseText);
            var message = data.Message;

            alert(message);
        }

    });
}


function showEditMessageDialog(messageID) {
    var editorDiv = document.getElementById("editMessageDiv");
    var editor = document.getElementById("editMessageEditor");
    var previewFrame = document.getElementById("editMessagePreviewFrame");
    previewFrame.style.visibility = "v";
    var editMessageModal = $('#editMessageModal');
    editMessageModal.removeClass("display-none");
    editMessageModal.addClass("display-block");

    var editorWidth = editor.clientWidth;
    var editorHeight = editorWidth * 9 / 16;

    var editorDivWidth = editorDiv.clientWidth;
    var editorDivHeight = editorDivWidth * 9 / 16;

    editor.clientWidth = editorWidth;

    previewFrame.style.height = editorDivHeight + "px";
    editor.style.height = editorDivHeight + "px";


    if (!nicEditors.findEditor('editMessageEditor')) {
        new nicEditor({
            fullPanel: false,
            unselectable: 'off',
            iconsPath: 'images/text-editor/nicEditorIcons.gif',
            buttonList: ['bold', 'italic', 'underline', 'left', 'center', 'right', 'justify', 'indent', 'outdent', 'hr', 'fontFormat'],
            maxHeight: editorHeight,
        }).panelInstance('editMessageEditor');
    }

    $.ajax({
        url: "pri_msgs/getMessage/" + messageID,

        method: "GET",

        complete: function (response, status, xhr) {
            var data = JSON.parse(response.responseText);
            var message = data.Message;
            var messageBody = data.MessageBody;

            if (status == 'success') {
                nicEditors.findEditor('editMessageEditor').setContent(messageBody);
                hideInsertMessageDialog();
            }
            else {
                alert('Error occured while fetching message<br><B>Details: ' + message);
            }
        }

    });

    previewFrame.src = screenBase + "index.html?mode=1&date=-1&country=Jordan&city=Amman";
    editMessageModal.addClass("in");
    editMessageModal.addClass("modal-open");

    updatePreviewActive = true;
    activeFrame = "editMessagePreviewFrame";
    activeEditor = "editMessageEditor";
    activeMessageID = messageID;
    $(previewFrame).ready(updateMessagePreview);
}

function hideEditMessageDialog() {
    updatePreviewActive = false;
    var editMessageModal = $('#editMessageModal');
    editMessageModal.removeClass("in");
    editMessageModal.removeClass("modal-open");
    editMessageModal.removeClass("display-block");
    editMessageModal.addClass("display-none");
}


function updateMessagePreview() {

    var activeFrameWindow = document.getElementById(activeFrame);
    $("#activeFrame").ready(function () {
        var iframeContent = activeFrameWindow.contentDocument;
        if (iframeContent == null) {
            setTimeout(updateMessagePreview, 1000);
            return;
        }
        if (iframeContent.childNodes.length == 0 && updatePreviewActive == true) {
            setTimeout(updateMessagePreview, 1000);
            return;
        }
        var contentWindow = iframeContent.getElementById("hadeethWindow");

        $(contentWindow).ready(function () {
            if (contentWindow == null) {
                setTimeout(updateMessagePreview, 1000);
                return;
            }
            var contentDoc = contentWindow.contentDocument;
            if (contentDoc.childNodes.length == 0) {
                setTimeout(updateMessagePreview, 1000);
                return;
            }
            var contentDiv = contentDoc.getElementById("previewDiv");
            var editor = nicEditors.findEditor(activeEditor);

            if (contentDiv == null || editor == null) {
                setTimeout(updateMessagePreview, 1000);
                return;
            }

            if (activeFrameWindow.style.visibility == "hidden") {
                activeFrameWindow.style.visibility = "visible";
            }
            contentDiv.innerHTML = editor.getContent();
            if (updatePreviewActive == true) {
                setTimeout(updateMessagePreview, 100);
            }
        });

    });
}

function removeMessageDialog(messageId) {
    var userAction = confirm("This message will be deleted, Press Ok to confirm");
    if (userAction == true) {
        $.ajax({
            url: "pri_msgs/deleteMsg" + "?msgId=" + messageId,

            method: "GET",

            complete: function (response, status, xhr) {
                var data = JSON.parse(response.responseText);

                if (status == 'success') {
                    showMessageTableDiv();
                    showMessageTableDiv();
                }
                else {
                    alert('Error occured while fetching cities list<br><B>Message: ' + message);
                    //$("#"+field.fieldName).parent().addClass("has-error");
                }
            }

        });
    } else {

    }
}


function setImagesCss() {

    activeEditor = "insertMessageEditor";
    var editor = nicEditors.findEditor(activeEditor);

    if (editor == null) {
        return;
    }
    var shouldUpdate = false;
    var content = editor.getContent();
    // console.log("content = " + content);
    var tempDiv = document.createElement('div');
    tempDiv.innerHTML = content;
    var imgs = tempDiv.getElementsByTagName("img");
    for (var i = 0; i < imgs.length; i++) {
        if (imgs[i].style.width != "90%") {
            imgs[i].style.width = "90%";
            imgs[i].removeAttribute("height");
            shouldUpdate = true;
        }
    }
    if (shouldUpdate) {
        editor.setContent(tempDiv.innerHTML);
    }
}

function updateMessagePreview() {

    activeFrame = "insertMessagePreviewFrame";
    activeEditor = "insertMessageEditor";
    var activeFrameWindow = document.getElementById(activeFrame);
    $("#" + activeFrame).ready(function () {
        var iframeContent = activeFrameWindow.contentDocument;
        if (iframeContent == null) {
            setTimeout(updateMessagePreview, 1000);
            return;
        }
        if (iframeContent.childNodes.length == 0 && updatePreviewActive == true) {
            setTimeout(updateMessagePreview, 1000);
            return;
        }
        var contentWindow = iframeContent.getElementById("hadeethWindow");

        $(contentWindow).ready(function () {
            if (contentWindow == null) {
                setTimeout(updateMessagePreview, 1000);
                return;
            }
            var contentDoc = contentWindow.contentDocument;
            if (contentDoc.childNodes.length == 0) {
                setTimeout(updateMessagePreview, 1000);
                return;
            }
            var contentDiv = contentDoc.getElementById("previewDiv");
            var editor = nicEditors.findEditor(activeEditor);

            if (contentDiv == null || editor == null) {
                setTimeout(updateMessagePreview, 1000);
                return;
            }

            if (activeFrameWindow.style.visibility == "hidden") {
                activeFrameWindow.style.visibility = "visible";
            }
            var content = editor.getContent();
            contentDiv.innerHTML = content;
            if (updatePreviewActive == true) {
                setTimeout(updateMessagePreview, 100);
            }
        });

    });
}

function deleteAllMsg() {
    var userAction = confirm("سوف يتم حذف جميع الرسائل. هل انت متأكد؟");
    if (userAction == true) {
        $.ajax({
            url: "php/deleteMsgs.php?all=true",

            method: "GET",

            complete: function (response, status, xhr) {
                var data = JSON.parse(response.responseText);

                if (status == 'success') {

                }
            }

        });
    }
}

