<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 6/21/2017
 * Time: 12:31 AM
 */

if ($_GET["userMode"] == null) {
    echo("<script> " . "alert(\"Some Information Is Missing!!, Please fill the form again and re-submit it.\")" . "</script>");
    return;
}
$rebootCmd = "sudo reboot";

$userMode = $_GET["userMode"];
$jsFile = fopen("/var/www/emosquescreen/userMode.js", 'w') or die("can't open JS file");
$javaFile = fopen("/home/pi/.eMosqueSystem/userSelectedMode.txt", 'w') or die("can't open JAVA file");

$strJava = "mode=" . $userMode;
$strJS = "userMode = " . $userMode;
fwrite($jsFile, $strJS);
fclose($jsFile);
fwrite($javaFile, $strJava);
fclose($javaFile);
echo("Operation Completed");
exec($rebootCmd, $x, $y);

?>