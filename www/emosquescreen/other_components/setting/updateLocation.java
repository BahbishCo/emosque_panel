import java.io.*;
import java.util.Scanner;

public class updateLocation {

	static String[] longitudeLineArray;
	static String[] latitudeLineArray;
    static String nextLine;
	static double longitude;
	static double latitude;
	static StringBuilder sb = new StringBuilder();
	static File settingsFile = new File("/var/www/emosquescreen/web-site/JSON/setting.js");
	static File locationFile = new File("/var/www/emosquescreen/other_components/setting/panelInfo/location.txt");

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scanner = null;

		try
		{
			scanner = new Scanner(locationFile);

			while (scanner.hasNextLine())
			{
				nextLine = scanner.nextLine();

				if (nextLine.contains("longitude"))
				{
					longitudeLineArray = nextLine.split(" ", -1);
					nextLine = longitudeLineArray[1].substring(0, longitudeLineArray[1].length() - 1);
					longitude = Double.parseDouble(nextLine);
				}
				if (nextLine.contains("latitude"))
				{
					latitudeLineArray = nextLine.split(" ", -1);
					nextLine = latitudeLineArray[1].substring(0, latitudeLineArray[1].length() - 1);
					latitude = Double.parseDouble(nextLine);
				}
			}

			scanner = new Scanner(settingsFile);

			while (scanner.hasNextLine())
			{
				nextLine = scanner.nextLine();

				if (nextLine.contains("longitude"))
				{
					sb.append("longitude: " + longitude+",\n");
				}
				else if (nextLine.contains("latitude"))
				{
					sb.append("latitude: " + latitude+",\n");
				}
				else
				{
					sb.append(nextLine+"\n");
				}
			}
			FileWriter fstreamWrite = new FileWriter(settingsFile);
			BufferedWriter out = new BufferedWriter(fstreamWrite);
			out.write(sb.toString());
			out.flush();
			out.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
